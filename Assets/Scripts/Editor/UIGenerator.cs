using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIGenerator : MonoBehaviour
{
    private static UIFactory uiFactory;

    [MenuItem("EditorScripts/GenerateMenus")]
    public static void GenerateMenus()
    {
        uiFactory = new UIFactory();
        GameObject canvasGo = GenerateCanvas();
        GenerateTitleMenu(canvasGo);
        GeneratePauseMenu(canvasGo);
        GenerateLoadMenu(canvasGo);
        GenerateVideoSettingsMenu(canvasGo);
        GenerateAudioSettingsMenu(canvasGo);
        GenerateBlurBackground(canvasGo);
        GenerateFadeImage(canvasGo);
    }

    private static GameObject GenerateCanvas()
    {
        var canvasGo = new GameObject("Canvas");
        var canvas = canvasGo.AddComponent<Canvas>();
        canvas.renderMode = RenderMode.ScreenSpaceOverlay;
        canvas.pixelPerfect = true;
        var scaler = canvasGo.AddComponent<CanvasScaler>();
        scaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
        scaler.referenceResolution = new Vector2(1920, 1080);
        scaler.matchWidthOrHeight = 0;
        scaler.referencePixelsPerUnit = 32;
        canvasGo.AddComponent<GraphicRaycaster>();
        canvasGo.AddComponent<EventSystem>();
        return canvasGo;
    }

    private static void GenerateTitleMenu(GameObject canvasGo)
    {
        var titleMenuGo = new GameObject("TitleMenu");
        titleMenuGo.transform.SetParent(canvasGo.transform);
        var titleMenu = uiFactory.AddTitleMenu(titleMenuGo);
        AddVerticalLayoutGroup(titleMenuGo);
        AddHeaderRow(titleMenuGo.transform, "TitleMenuHeader", "Game Title");        
        titleMenu.Options = new TextMeshProUGUI[6];
        titleMenu.Options[0] = AddButton("ContinueButton", "Continue", titleMenuGo.transform);
        titleMenu.Options[1] = AddButton("NewGameButton", "New Game", titleMenuGo.transform);
        titleMenu.Options[2] = AddButton("ControlSettingsButtonButton", "Control Settings", titleMenuGo.transform);
        titleMenu.Options[3] = AddButton("VideoSettingsButton", "Video Settings", titleMenuGo.transform);
        titleMenu.Options[4] = AddButton("AudioSettingsButton", "Audio Settings", titleMenuGo.transform);
        titleMenu.Options[5] = AddButton("ExitGameButton", "Exit Game", titleMenuGo.transform);

        titleMenuGo.GetComponent<RectTransform>().SetAnchor(AnchorPresets.StretchAll);
        titleMenuGo.SetActive(false);
        
        
    }

    private static void GeneratePauseMenu(GameObject canvasGo)
    {
        var pauseMenuGo = new GameObject("PauseMenu");
        pauseMenuGo.transform.SetParent(canvasGo.transform);
        var pauseMenu = pauseMenuGo.AddComponent<PauseMenu>();
        AddVerticalLayoutGroup(pauseMenuGo);
        AddHeaderRow(pauseMenuGo.transform, "PauseMenuHeader", "Game Paused");
        pauseMenu.Options = new TextMeshProUGUI[6];
        pauseMenu.Options[0] = AddButton("ReturnToGameButton", "Return to Game", pauseMenuGo.transform);
        pauseMenu.Options[1] = AddButton("ControlsButton", "Controls", pauseMenuGo.transform);
        pauseMenu.Options[2] = AddButton("VideoSettingsButton", "Video Settings", pauseMenuGo.transform);
        pauseMenu.Options[3] = AddButton("AudioSettingsButton", "Audio Settings", pauseMenuGo.transform);
        pauseMenu.Options[4] = AddButton("ReturnToTitleButton", "Return to Title", pauseMenuGo.transform);
        pauseMenu.Options[5] = AddButton("ExitGameButton", "Exit Game", pauseMenuGo.transform);
        pauseMenuGo.GetComponent<RectTransform>().SetAnchor(AnchorPresets.StretchAll);
        pauseMenuGo.SetActive(false);
    }

    private static void GenerateLoadMenu(GameObject canvasGo)
    {
        var loadMenuGo = new GameObject("LoadMenu");
        loadMenuGo.transform.SetParent(canvasGo.transform);
        var loadMenuRectTransform = loadMenuGo.AddComponent<RectTransform>();
        var loadMenu = loadMenuGo.AddComponent<LoadMenu>();
        AddHeaderRow(loadMenuGo.transform, "LoadGameHeader", "Load Game");
        AddVerticalLayoutGroup(loadMenuGo);
        loadMenu.Options = new TextMeshProUGUI[6];
        loadMenu.Options[0] = AddButton("CancelBtn", "Cancel", loadMenuGo.transform);
        loadMenu.Options[1] = AddButton("Slot0Btn", "Slot0", loadMenuGo.transform);
        loadMenu.Options[2] = AddButton("Slot1Btn", "Slot1", loadMenuGo.transform);
        loadMenu.Options[3] = AddButton("Slot2Btn", "Slot2", loadMenuGo.transform);
        loadMenu.Options[4] = AddButton("Slot3Btn", "Slot3", loadMenuGo.transform);
        loadMenu.Options[5] = AddButton("Slot4Btn", "Slot4", loadMenuGo.transform);
        loadMenuRectTransform.SetAnchor(AnchorPresets.StretchAll);
        loadMenuGo.SetActive(false);
    }

    private static void GenerateVideoSettingsMenu(GameObject canvasGo)
    {
        var videoSettingsGo = new GameObject("VideoSettingsMenu");
        videoSettingsGo.transform.SetParent(canvasGo.transform);
        var videoSettingsRt = videoSettingsGo.AddComponent<RectTransform>();
        var videoSettingsMenu = videoSettingsGo.AddComponent<VideoSettingsMenu>();
        AddHeaderRow(videoSettingsGo.transform, "VideoSettingsHeader", "Video Settings");
        AddVerticalLayoutGroup(videoSettingsGo);
        AddFullscreenRow(videoSettingsRt, videoSettingsMenu);
        AddCancelAndApplyRow(videoSettingsRt, "VideoSettings");
        videoSettingsGo.SetActive(false);
    }
    private static void GenerateAudioSettingsMenu(GameObject canvasGo)
    {
        var audioSettingsGo = new GameObject("AudioSettingsMenu");
        audioSettingsGo.transform.SetParent(canvasGo.transform);
        var audioSettingsRt = audioSettingsGo.AddComponent<RectTransform>();
        var audioSettingsMenu = audioSettingsGo.AddComponent<AudioSettingsMenu>();
        audioSettingsRt.SetAnchor(AnchorPresets.StretchAll);
        AddHeaderRow(audioSettingsGo.transform, "AudioSettingsHeader", "Audio Settings");
        AddVerticalLayoutGroup(audioSettingsGo);

        AddVolumeSliderRow("Master", audioSettingsGo);
        AddVolumeSliderRow("Music", audioSettingsGo);
        AddVolumeSliderRow("Sound", audioSettingsGo);
        
        AddCancelAndApplyRow(audioSettingsRt, "AudioSettings");
        audioSettingsGo.SetActive(false);
    }        

    private static void AddCancelAndApplyRow(RectTransform parent, string name)
    {
        var panel = new GameObject($"{name}ButtonsPanel");
        panel.transform.SetParent(parent);
        AddHorizontalLayoutGroup(panel, 200);
        AddButton($"{name}CancelBtn", "Cancel", panel.transform, 72, 350, 200);
        AddButton($"{name}ApplyBtn", "Save & Apply", panel.transform, 72, 350, 200);
    }

    private static Button AddArrowButton(string name, Transform parentTo, Sprite arrowSprite)
    {
        var buttonGo = new GameObject(name);
        buttonGo.transform.SetParent(parentTo);
        var image = buttonGo.AddComponent<Image>();
        image.preserveAspect = true;
        image.sprite = arrowSprite;
        var button = buttonGo.AddComponent<Button>();
        button.targetGraphic = image;
        buttonGo.GetComponent<RectTransform>().SetDimensions(75, 75);
        return button;
    }



    private static void GenerateBlurBackground(GameObject canvasGo)
    {
        var blurBackground = new GameObject("BlurBackground");
        blurBackground.transform.SetParent(canvasGo.transform);
        blurBackground.AddComponent<Image>().material = Resources.Load<Material>("BlurUIMaterial");
        blurBackground.GetComponent<RectTransform>().SetAnchor(AnchorPresets.StretchAll);
        blurBackground.SetActive(false);
    }
    private static void GenerateFadeImage(GameObject canvasGo)
    {

    }

    private static void AddFullscreenRow(RectTransform videoSettingsRt, VideoSettingsMenu videoSettingsMenu)
    {
        var fullscreenPanelGo = new GameObject("FullscreenPanel");
        fullscreenPanelGo.transform.SetParent(videoSettingsMenu.transform);
        AddHorizontalLayoutGroup(fullscreenPanelGo);
        var fullscreenPanelRt = fullscreenPanelGo.GetComponent<RectTransform>();
        fullscreenPanelRt.SetDimensions(1200, 100);
        var fullscreenLabelGo = new GameObject("FullscreenLabel");
        var fullscreenLabelRt = fullscreenLabelGo.AddComponent<RectTransform>();
        fullscreenLabelRt.SetDimensions(500, 100);
        fullscreenLabelGo.transform.SetParent(fullscreenPanelGo.transform);
        AddLabel(fullscreenLabelGo, "Fullscreen");
        var toggle = AddToggle(fullscreenPanelGo);
        videoSettingsRt.SetAnchor(AnchorPresets.StretchAll);
        AddResolutionRow(videoSettingsRt, videoSettingsMenu);
        AddQualityRow(videoSettingsRt, videoSettingsMenu);

        // Wire up menu
        videoSettingsMenu.FullscreenToggle = toggle;
    }

    private static void AddResolutionRow(RectTransform videoSettingsRt, VideoSettingsMenu videoSettingsMenu)
    {
        var resolutionPanelGo = new GameObject("ResolutionPanel");
        resolutionPanelGo.transform.SetParent(videoSettingsMenu.transform);

        AddHorizontalLayoutGroup(resolutionPanelGo);
        var resolutionLabelGo = new GameObject("ResolutionLabel");
        resolutionLabelGo.transform.SetParent(resolutionPanelGo.transform);
        var resolutionLabelRt = resolutionLabelGo.AddComponent<RectTransform>();
        AddLabel(resolutionLabelGo, "Resolution");
        resolutionLabelRt.SetDimensions(500, 100);

        var leftArrowButton = AddArrowButton("LeftResButton", resolutionPanelGo.transform, Resources.Load<Sprite>("LeftArrow"));
        var labelGo = new GameObject("ResolutionValueTxt");
        var resolutionValueLabel = AddLabel(labelGo, "9999x9999");
        labelGo.transform.SetParent(resolutionPanelGo.transform);
        labelGo.GetComponent<RectTransform>().SetDimensions(400, 75);
        var rightArrowButton = AddArrowButton("RightResButton", resolutionPanelGo.transform, Resources.Load<Sprite>("RightArrow"));

        // Wire up menu
        videoSettingsMenu.ResolutionLeftButton = leftArrowButton;
        videoSettingsMenu.ResolutionValueLabel = resolutionValueLabel;
        videoSettingsMenu.ResolutionRightButton = rightArrowButton;
    }

    private static void AddQualityRow(RectTransform videoSettingsRt, VideoSettingsMenu videoSettingsMenu)
    {
        var qualityPanelGo = new GameObject("QualityPanel");
        qualityPanelGo.transform.SetParent(videoSettingsMenu.transform);

        AddHorizontalLayoutGroup(qualityPanelGo);
        var qualityPanelRt = qualityPanelGo.GetComponent<RectTransform>();
        var qualityLabelGo = new GameObject("QualityLabel");
        qualityLabelGo.transform.SetParent(qualityPanelGo.transform);
        var qualityLabelRt = qualityLabelGo.AddComponent<RectTransform>();
        AddLabel(qualityLabelGo, "Quality");
        qualityLabelRt.SetDimensions(500, 100);

        var leftArrowButton = AddArrowButton("LeftQualityButton", qualityPanelGo.transform, Resources.Load<Sprite>("LeftArrow"));
        var labelGo = new GameObject("QualityValueTxt");
        var qualityValueLabel = AddLabel(labelGo, "QQQQQQQQ");
        labelGo.transform.SetParent(qualityPanelGo.transform);
        labelGo.GetComponent<RectTransform>().SetDimensions(400, 75);
        var rightArrowButton = AddArrowButton("RightQualityButton", qualityPanelGo.transform, Resources.Load<Sprite>("RightArrow"));

        // Wire up menu
        videoSettingsMenu.QualityLeftButton = leftArrowButton;
        videoSettingsMenu.QualityValueLabel = qualityValueLabel;
        videoSettingsMenu.QualityRightButton = rightArrowButton;
    }

    private static TextMeshProUGUI AddButton(string buttonName, string message, Transform parentTo, float fontSize = 72, float width = 700, float height = 100)
    {
        var parentGo = new GameObject(buttonName);
        parentGo.transform.SetParent(parentTo);
        parentGo.AddComponent<RectTransform>().SetDimensions(width, height);
        var image = parentGo.AddComponent<Image>();
        image.sprite = AssetDatabase.GetBuiltinExtraResource<Sprite>("UI/Skin/InputFieldBackground.psd");
        image.type = Image.Type.Sliced;
        image.fillCenter = false;
        var button = parentGo.AddComponent<Button>();
        var textGo = new GameObject($"{buttonName}Txt");
        textGo.transform.SetParent(parentGo.transform);
        textGo.AddComponent<RectTransform>().SetDimensions(width, height);
        return AddLabel(textGo, message);
    }

    private static void AddHeaderRow(Transform parentTo, string rowObjectName, string message)
    {
        var rowGo = new GameObject(rowObjectName);
        rowGo.transform.SetParent(parentTo);
        var rt = rowGo.AddComponent<RectTransform>();
        rt.SetDimensions(700, 100);
        AddLabel(rowGo, message);
    }

    private static void AddVerticalLayoutGroup(GameObject go)
    {
        var layoutGroup = go.AddComponent<VerticalLayoutGroup>();
        layoutGroup.padding = new RectOffset(0, 0, 0, 0);
        layoutGroup.spacing = 10;
        layoutGroup.childAlignment = TextAnchor.MiddleCenter;
    }

    private static void AddHorizontalLayoutGroup(GameObject parentGo, float height = 100)
    {
        var layoutGroup = parentGo.AddComponent<HorizontalLayoutGroup>();
        layoutGroup.padding = new RectOffset(0, 0, 0, 0);
        layoutGroup.spacing = 20;
        layoutGroup.childAlignment = TextAnchor.MiddleCenter;
        parentGo.GetComponent<RectTransform>().SetDimensions(1200, height);
    }

    private static Toggle AddToggle(GameObject parent)
    {
        var toggleGo = new GameObject("Toggle");
        toggleGo.transform.SetParent(parent.transform);
        var toggle = toggleGo.AddComponent<Toggle>();
        
        var toggleRt = toggleGo.GetComponent<RectTransform>();
        toggleRt.SetDimensions(160, 20);
        
        var backgroundGo = new GameObject("Background");
        backgroundGo.transform.SetParent(toggleGo.transform);

        var backgroundImg = backgroundGo.AddComponent<Image>();
        backgroundImg.sprite = AssetDatabase.GetBuiltinExtraResource<Sprite>("UI/Skin/UISprite.psd");
        backgroundImg.type = Image.Type.Sliced;
        var backgroundRt = backgroundGo.GetComponent<RectTransform>();
        backgroundRt.SetAnchor(AnchorPresets.TopLeft);
        backgroundRt.SetDimensions(60, 60);
        
        var checkmarkGo = new GameObject("Checkmark");
        checkmarkGo.transform.SetParent(backgroundGo.transform);
        var checkmarkImg = checkmarkGo.AddComponent<Image>();
        checkmarkImg.preserveAspect = true;
        checkmarkImg.sprite = AssetDatabase.GetBuiltinExtraResource<Sprite>("UI/Skin/Checkmark.psd");
        var checkmarkRt = checkmarkGo.GetComponent<RectTransform>();
        checkmarkRt.SetAnchor(AnchorPresets.MiddleCenter);
        checkmarkRt.SetDimensions(60, 60);
        toggle.graphic = checkmarkImg;
        return toggle;
    }

    private static TextMeshProUGUI AddLabel(GameObject menuGo, string message, bool bold = true, float size = 72)
    {
        var tmp = menuGo.AddComponent<TextMeshProUGUI>();
        tmp.transform.SetParent(menuGo.transform);
        tmp.fontSize = size;
        tmp.text = message;
        tmp.fontStyle = bold ? FontStyles.Bold : FontStyles.Normal;
        tmp.alignment = TextAlignmentOptions.Center;
        return tmp;
    }

    private static void AddVolumeSliderRow(string name, GameObject parent)
    {
        var panelGo = new GameObject($"{name}Panel");
        panelGo.transform.SetParent(parent.transform);
        AddHorizontalLayoutGroup(panelGo);
        var panelRt = panelGo.GetComponent<RectTransform>();
        var labelGo = new GameObject($"{name}Label");
        labelGo.transform.SetParent(panelGo.transform);
        panelRt.SetAnchor(AnchorPresets.BottomLeft);
        panelRt.SetDimensions(1000, 100);
        AddLabel(labelGo, $"{name} Volume");
        var labelRt = labelGo.GetComponent<RectTransform>();

        var sliderGo = new GameObject($"{name}Slider");
        sliderGo.transform.SetParent(panelGo.transform);
        var slider = sliderGo.AddComponent<Slider>();
        var sliderRt = sliderGo.GetComponent<RectTransform>();
        sliderRt.localScale = Vector3.one * 2;
        labelRt.SetDimensions(600, 100);
        sliderRt.SetDimensions(450, 100);
        slider.direction = Slider.Direction.LeftToRight;

        var backgroundGo = new GameObject("Background");
        backgroundGo.transform.SetParent(sliderGo.transform);
        var backgroundImg = backgroundGo.AddComponent<Image>();
        backgroundImg.type = Image.Type.Sliced;
        var backgroundRt = backgroundGo.GetComponent<RectTransform>();
        backgroundRt.localScale = Vector3.one * 0.5f;
        backgroundImg.sprite = AssetDatabase.GetBuiltinExtraResource<Sprite>("UI/Skin/Background.psd");
        backgroundRt.SetAnchor(AnchorPresets.Slider);
        backgroundRt.SetPivot(PivotPresets.MiddleCenter);

        var fillAreaGo = new GameObject("FillArea");
        var fillAreaRt = fillAreaGo.AddComponent<RectTransform>();
        //fillAreaRt.localScale = Vector3.one * 0.5f;
        fillAreaRt.SetParent(sliderRt);

        var fillGo = new GameObject("Fill");
        var fillImg = fillGo.AddComponent<Image>();
        fillImg.sprite = AssetDatabase.GetBuiltinExtraResource<Sprite>("UI/Skin/UISprite.psd");
        fillImg.type = Image.Type.Sliced;
        var fillRt = fillGo.GetComponent<RectTransform>();

        fillAreaRt.SetAnchor(AnchorPresets.Slider);
        fillAreaRt.SetPivot(PivotPresets.MiddleCenter);
        fillRt.SetParent(fillAreaRt);
        fillRt.SetAnchor(AnchorPresets.BottomLeft);
        fillRt.SetPivot(PivotPresets.MiddleCenter);

        var handleSlideAreaGo = new GameObject("HandleSlideArea");
        var handleSlideRt = handleSlideAreaGo.AddComponent<RectTransform>();
        handleSlideRt.localScale = Vector3.one;
        handleSlideRt.SetAnchor(AnchorPresets.StretchAll);
        handleSlideRt.SetDimensions(430, 100);

        handleSlideRt.SetParent(sliderRt);
        var handleGo = new GameObject("Handle");
        var handleImg = handleGo.AddComponent<Image>();
        handleImg.sprite = AssetDatabase.GetBuiltinExtraResource<Sprite>("UI/Skin/Knob.psd");
        var handleRt = handleGo.GetComponent<RectTransform>();
        handleRt.SetParent(handleSlideRt);
        handleRt.position = new Vector2(-205, 0);
        handleRt.SetAnchor(AnchorPresets.VertStretchLeft);
        handleRt.SetDimensions(60, 100);
        handleRt.localScale = Vector3.one;
        handleImg.preserveAspect = true;

        // Wire up slider
        slider.fillRect = fillRt;
        slider.handleRect = handleRt;
        slider.direction = Slider.Direction.LeftToRight;
        slider.minValue = 0.0001f;
        slider.maxValue = 3;
    }
}