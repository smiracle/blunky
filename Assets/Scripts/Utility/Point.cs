﻿using UnityEngine;

[System.Serializable]
public struct Point
{
    public int x;
    public int y;

    public Point(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public Point(Vector3 vector3)
    {
        this.x = (int)vector3.x;
        this.y = (int)vector3.z;
    }

    public override string ToString()
    {
        return string.Format("({0},{1})", x, y);
    }

    public static bool operator ==(Point a, Point b)
    {
        return a.x == b.x && a.y == b.y;
    }

    public static bool operator !=(Point a, Point b)
    {
        return !(a == b);
    }

    public static Point operator +(Point a, Point b)
    {
        return new Point(a.x + b.x, a.y + b.y);
    }

    public static Point operator -(Point p1, Point p2)
    {
        return new Point(p1.x - p2.x, p1.y - p2.y);
    }

    public override bool Equals(object obj)
    {
        if (obj is Point)
        {
            Point p = (Point)obj;
            return x == p.x && y == p.y;
        }
        return false;
    }

    public bool Equals(Point p)
    {
        return x == p.x && y == p.y;
    }

    public override int GetHashCode()
    {
        return x ^ y;
    }

    public static implicit operator Point(Vector3 vector)
    {
        return new Point((int)vector.x, (int)vector.z);
    }

    public static implicit operator Vector3(Point point)
    {
        return new Vector3(point.x, 0, point.y);
    }
}
