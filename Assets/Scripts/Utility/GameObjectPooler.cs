﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class GameObjectPooler : MonoBehaviour
{
    public static GameObjectPooler Instance;
    Dictionary<string, List<GameObject>> ObjectPool = new Dictionary<string, List<GameObject>>();

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            throw new Exception($"Multiple instances of {nameof(GameObjectPooler)} not allowed");
        }
    }

    public void PreloadGameObject(GameObject prefab)
    {
        var nameOfPrefab = GetPrefabName(prefab);
        var gameObject = InstatiateNewGameObject(prefab);
        AddToObjectPool(gameObject, nameOfPrefab);
    }

    public GameObject GetObject(GameObject prefab)
    {
        var nameOfPrefab = this.GetPrefabName(prefab);
        GameObject gameObject;
        if (this.ObjectPool.ContainsKey(nameOfPrefab) && this.HasInactiveGameObject(nameOfPrefab))
        {
            gameObject = this.GetInactiveGameObject(nameOfPrefab);
        }
        else
        {
            gameObject = this.InstatiateNewGameObject(prefab);
            AddToObjectPool(gameObject, nameOfPrefab);
        }
        return gameObject;
    }

    public GameObject GetObject(GameObject prefab, Transform parentTransform)
    {
        var nameOfPrefab = this.GetPrefabName(prefab);
        GameObject gameObject;
        if (this.ObjectPool.ContainsKey(nameOfPrefab) && this.HasInactiveGameObject(nameOfPrefab))
        {
            gameObject = this.GetInactiveGameObject(nameOfPrefab);
        }
        else
        {
            gameObject = this.InstatiateNewGameObject(prefab);
            gameObject.transform.SetParent(parentTransform, false);
            AddToObjectPool(gameObject, nameOfPrefab);
        }
        return gameObject;
    }

    public void DestroyObject(GameObject toDestroy, GameObject originalPrefab)
    {
        // Be warned, this is an inefficient operation, although it frees up memory.
        RemoveObjectFromExistingPool(toDestroy, originalPrefab.name);
        Destroy(toDestroy);
    }

    public void DestroyObjectsOfType(GameObject originalPrefab)
    {
        if (ObjectPool.ContainsKey(originalPrefab.name))
        {
            for (int i = 0; i < ObjectPool[originalPrefab.name].Count; i++)
            {
                Destroy(ObjectPool[originalPrefab.name][i]);
            }
            ObjectPool.Remove(originalPrefab.name);
        }
    }

    private string GetPrefabName(GameObject prefab)
    {
        var nameOfPrefab = prefab.name.Replace("(Clone)", "");
        return nameOfPrefab;
    }

    private bool HasInactiveGameObject(string nameOfPrefab)
    {
        var hasInactiveGameObject = false;
        foreach (GameObject gameObject in ObjectPool[nameOfPrefab])
        {
            if (gameObject == null)
            {
                Debug.Log("COULD NOT FIND " + nameOfPrefab);
            }
            if (!gameObject.activeInHierarchy)
            {
                hasInactiveGameObject = true;
            }
        }
        return hasInactiveGameObject;
    }

    private GameObject GetInactiveGameObject(string nameOfPrefab)
    {
        GameObject gameObject = null;
        foreach (GameObject pooledGameObject in ObjectPool[nameOfPrefab])
        {
            if (!pooledGameObject.activeInHierarchy)
            {
                gameObject = pooledGameObject;
            }
        }
        return gameObject;
    }

    private void AddToObjectPool(GameObject gameObject, string nameOfPrefab)
    {
        if (ObjectPool.ContainsKey(nameOfPrefab))
        {
            AddObjectToExistingPool(gameObject, nameOfPrefab);
        }
        else
        {
            AddToNewObjectPool(gameObject, nameOfPrefab);
        }
    }


    private void AddToNewObjectPool(GameObject gameObject, string nameOfPrefab)
    {
        var list = GetNewGameObjectList(gameObject);
        ObjectPool.Add(nameOfPrefab, list);
    }

    private void AddObjectToExistingPool(GameObject gameObject, string nameOfPrefab)
    {
        ObjectPool[nameOfPrefab].Add(gameObject);
    }

    private void RemoveObjectFromExistingPool(GameObject gameObject, string nameOfPrefab)
    {
        for (int i = 0; i < ObjectPool[nameOfPrefab].Count; i++)
        {
            if (ObjectPool[nameOfPrefab][i] == gameObject)
            {
                ObjectPool[nameOfPrefab].RemoveAt(i);
            }
        }

        if (ObjectPool[nameOfPrefab].Count == 0)
        {
            ObjectPool.Remove(nameOfPrefab);
        }
    }

    private List<GameObject> GetNewGameObjectList(GameObject gameObject)
    {
        var list = new List<GameObject>();
        list.Add(gameObject);
        return list;
    }

    private GameObject InstatiateNewGameObject(GameObject gameObject)
    {
        gameObject = (GameObject)Instantiate(gameObject);
        gameObject.SetActive(false);
        return gameObject;
    }
}