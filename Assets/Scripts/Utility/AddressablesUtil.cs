﻿using System.Threading.Tasks;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

public class AddressablesUtil
{
    public static Task<T> LoadAssetAsync<T>(object key)
    {
        TaskCompletionSource<T> taskCompletionSource = new TaskCompletionSource<T>();

        AsyncOperationHandle<T> loadAssetHandle = Addressables.LoadAssetAsync<T>(key);

        loadAssetHandle.Completed += (AsyncOperationHandle<T> completedHandle) =>
        {
            if (completedHandle.OperationException != null)
            {
                taskCompletionSource.SetException(completedHandle.OperationException);
            }
            else
            {
                taskCompletionSource.SetResult(completedHandle.Result);
            }
        };

        return taskCompletionSource.Task;
    }
}