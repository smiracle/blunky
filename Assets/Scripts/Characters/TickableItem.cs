﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AddressableAssets;

public class TickableItem : Item, ITickable
{
    public int Speed { get; set; } = 5;
    public int MoveWaitMaximum { get; set; } = 2;

    private int waitCounter = 0;

    public TickableItem(IGameManager gameManager) : base(gameManager)
    {
        GameManager = gameManager;
        DataManager = gameManager.DataManager;

        switch (TileIdentifier)
        {
            case TileIdentifiers.TickableItem_Ignited_Bomb:
                MoveWaitMaximum = 2;
                break;
        }

    }

    public override void OnTouch()
    {
        switch (TileIdentifier)
        {
            case TileIdentifiers.TickableItem_Ignited_Bomb:
                // Do nothing
                return;
        }
    }

    public override void Destroy()
    {
       // Debug.Log($"Destroying tickable item {TileIdentifier}");
        DataManager.UnregisterTickableAtTile(DataManager.Tiles[Scene][Position]);
        DataManager.Tiles[Scene][Position].Interactable = null;
        DataManager.DrawOneTile(DataManager.Tiles[Scene][Position]);
    }

    public override Interactable DeepCopy()
    {
        var item = new TickableItem(GameManager)
        {
            Position = new Vector3Int(Position.x, Position.y, 0),
            TileIdentifier = TileIdentifier,
            Tile = Tile,
            Scene = Scene,
            GameManager = GameManager,
            Hp = Hp,
            MoveWaitMaximum = MoveWaitMaximum
        };
        return item;
    }

    public void Tick()
    {
        //Debug.Log($"{TileIdentifier} at {Position} tick");
        if (TileIdentifier == TileIdentifiers.TickableItem_Ignited_Bomb)
        {
            BombTick();
        }
    }

    private void BombTick()
    {
        if (waitCounter > MoveWaitMaximum)
        {
            GameManager.PlaySound(AudioClipBanks.Sounds_Explosion);
            DamageSurroundingInteractables();
            Destroy();
        }
        else
        {
            waitCounter++;
        }
    }

    private void DamageSurroundingInteractables()
    {
        var list = new List<TileInfo>();
        for (int w = Position.x - 1; w <= Position.x + 1; w++)
        {
            for (int h = Position.y - 1; h <= Position.y + 1; h++)
            {
                if (DataManager.Tiles[Scene].TryGetValue(new Vector3Int(w, h, 0), out TileInfo tile))
                {
                    list.Add(tile);
                }
            }
        }

        foreach (var tile in list)
        {
            if (tile.Interactable != null)
            {
                if (tile.Interactable is Enemy)
                {
                    var enemy = tile.Interactable as Enemy;
                    var damage = DataManager.Random.Next(9, 12);
                    enemy.Hp -= damage;
                    var damageTextObj = GameObjectPooler.Instance.GetObject(GameManager.DamageTextPrefab);
                    damageTextObj.SetActive(true);
                    damageTextObj.GetComponent<DamageTextPositioner>().Run(GameManager, tile.CenterPosition, damage.ToString(), Color.red, 30);
                }
            }
            if (tile.IsDestructable)
            {
                // Destroy any adjacent destructable tiles
                DataManager.DestroyDestructableTile(Scene, tile.Position);
            }

            DataManager.SpawnExplosionVFX(tile.CenterPosition);
            
        }
    }
}