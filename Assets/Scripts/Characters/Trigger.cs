﻿using UnityEngine;

public class Trigger : Interactable
{
    public Trigger(IGameManager gameManager)
    {
        GameManager = gameManager;
        DataManager = gameManager.DataManager;
    }

    public override void OnTouch()
    {
       // Debug.Log($"Stepped on trigger {TileIdentifier} at {Position}");
        switch (TileIdentifier)
        {
            case TileIdentifiers.RotateRight:
                var room = DataManager.GetRoomAssociatedWithPosition(Scene, Position);
                DataManager.RotateRoomAtRuntime(room);
                break;
        }
    }
    public override Interactable DeepCopy()
    {
        var item = new Trigger(GameManager)
        {
            Position = new Vector3Int(Position.x, Position.y, 0),
            TileIdentifier = TileIdentifier,
            Tile = Tile,
            Scene = Scene,
            GameManager = GameManager,
        };
        return item;
    }
}