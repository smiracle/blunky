﻿using UnityEngine;

public class Item : Interactable
{
    public Item(IGameManager gameManager)
    {
        GameManager = gameManager;
        DataManager = gameManager.DataManager;
    }

    public override void OnTouch()
    {
        switch (TileIdentifier)
        {
            case TileIdentifiers.Item_Apple:
                GameManager.Player.Hp += 3;
                GameManager.PlaySound(AudioClipBanks.Sounds_Apple);
                break;
            case TileIdentifiers.Item_Heart:
                GameManager.Player.Hp += 10;
                GameManager.PlaySound(AudioClipBanks.Sounds_Heart);
                break;
            case TileIdentifiers.Item_HeartContainer:
                GameManager.Player.MaxHp += 5;
                GameManager.PlaySound(AudioClipBanks.Sounds_HeartContainer);
                break;
            case TileIdentifiers.Item_Coin:
                GameManager.Player.Gold += 1;
                GameManager.PlaySound(AudioClipBanks.Sounds_Coin);
                break;
            case TileIdentifiers.Item_Coins:
                GameManager.Player.Gold += 5;
                GameManager.PlaySound(AudioClipBanks.Sounds_Coin);
                break;
            case TileIdentifiers.Item_Bomb:
                GameManager.Player.Bombs += 1;
                GameManager.PlaySound(AudioClipBanks.Sounds_Powerup);
                break;
            case TileIdentifiers.Items_Buyable_Bombs:
                if (GameManager.Player.Gold < 30)
                {
                    GameManager.PlaySound(AudioClipBanks.Sounds_Denied);
                    return;
                }
                GameManager.Player.Bombs += 20;
                GameManager.Player.Gold -= 30;
                GameManager.PlaySound(AudioClipBanks.Sounds_Powerup);
                break;
            case TileIdentifiers.Items_Buyable_HealingPotion:
                if (GameManager.Player.Gold < 20)
                {
                    GameManager.PlaySound(AudioClipBanks.Sounds_Denied);
                    return;
                }
                GameManager.Player.HealthPotions += 1;
                GameManager.Player.Gold -= 20;
                GameManager.PlaySound(AudioClipBanks.Sounds_Powerup);
                break;
            case TileIdentifiers.Items_Buyable_AttackPower:
                if(GameManager.Player.Gold < 30)
                {
                    GameManager.PlaySound(AudioClipBanks.Sounds_Denied);
                    return;
                }
                GameManager.Player.AttackMax += 1;
                GameManager.Player.AttackMin += 1;
                GameManager.Player.Gold -= 30;
                GameManager.PlaySound(AudioClipBanks.Sounds_Powerup);
                break;
        }

        Destroy();
    }

    public override void Destroy()
    {
        //Debug.Log($"Destroying item {TileIdentifier}");
        DataManager.Tiles[Scene][Position].Interactable = null;
        DataManager.DrawOneTile(DataManager.Tiles[Scene][Position]);
    }

    public override Interactable DeepCopy()
    {
        var item = new Item(GameManager)
        {
            Position = new Vector3Int(Position.x, Position.y, 0),
            TileIdentifier = TileIdentifier,
            Tile = Tile,
            Scene = Scene,
            GameManager = GameManager,
            Hp = Hp
        };
        return item;
    }
}