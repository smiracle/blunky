﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCharacter : MonoBehaviour
{
    private IGameManager gameManager;
    private DataManager dataManager;

    private int _selectedToolIndex = 0;
    public int SelectedToolIndex { get { return _selectedToolIndex; } 
        set {              
            // Loop the selection
            if (ToolsAvailable.Count > 0)
            {
                if (value >= ToolsAvailable.Count)
                {
                    _selectedToolIndex = 0;
                }
                else if (value < 0)
                {
                    _selectedToolIndex = ToolsAvailable.Count - 1;
                }
                else
                {
                    _selectedToolIndex = value;
                }
            }
            else
            {
                _selectedToolIndex = 0;
            }
        } }
    private List<TileIdentifiers> _toolsAvailable = new List<TileIdentifiers>();
    public List<TileIdentifiers> ToolsAvailable { get { return _toolsAvailable; } set 
        {
            var temp = _toolsAvailable.Count;
            if (temp == 0 && value.Count > 0)
            {
                gameManager.UIManager.Hud.UpdateToolSelection();
            }
            _toolsAvailable = value; 
        } }

    // Return empty if no tools have been obtained yet
    public TileIdentifiers SelectedTool
    {
        get
        {
            if (ToolsAvailable.Count > 0)
            { return ToolsAvailable[SelectedToolIndex]; }
            return TileIdentifiers.Empty;
        }
    }

    private int _gold = 0;
    public int Gold { get { return _gold; }
        set
        {
            var newVal = Mathf.Clamp(value, 0, 9999);
            if (gameManager.DataManager.GameStates.Contains(GameStates.BowAndArrowUnlocked))
            {
                if (newVal > 0)
                {
                    MakeToolAvailable(TileIdentifiers.Item_BowAndArrow);
                }
                else
                {
                    MakeToolUnavailable(TileIdentifiers.Item_BowAndArrow);
                }
            }
            _gold = newVal;
            gameManager.UIManager.Hud.UpdateHud();
        }
    }

    private int _bombs = 0;
    public int Bombs
    {
        get { return _bombs; }
        set
        {
            var newVal = Mathf.Clamp(value, 0, 99);
            if (newVal > 0)
            {
                MakeToolAvailable(TileIdentifiers.Item_Bomb);
            }
            else
            {
                MakeToolUnavailable(TileIdentifiers.Item_Bomb);
            }
            _bombs = newVal; 
            gameManager.UIManager.Hud.UpdateHud();
        }
    }

    private int _healthPotions = 0;
    public int HealthPotions {
        get { return _healthPotions; }
        set
        {
            var newVal = Mathf.Clamp(value, 0, 1);
            if (newVal > 0)
            {
                MakeToolAvailable(TileIdentifiers.Item_HealthPotion);
            }
            else
            {
                MakeToolUnavailable(TileIdentifiers.Item_HealthPotion);
            }
            _healthPotions = newVal;
            gameManager.UIManager.Hud.UpdateHud();
        }
    }

    private int _maxHp;
    public int MaxHp { get { return _maxHp; } set { _maxHp = value; gameManager.UIManager.Hud.UpdateHud(); } }
    private int _hp;
    public int Hp { get { return _hp; } set { _hp = Mathf.Clamp(value, 0, _maxHp); gameManager.UIManager.Hud.UpdateHud(); } }

    private int _attackMin = 1;
    public int AttackMin { get { return _attackMin; } set { _attackMin = value; gameManager.UIManager.Hud.UpdateHud(); } }
    private int _attackMax = 5;
    public int AttackMax { get { return _attackMax; } set { _attackMax = value; gameManager.UIManager.Hud.UpdateHud(); } }

    public Vector3Int MostRecentOverworldPos = Vector3Int.zero;
    public HashSet<Scenes> Visited = new HashSet<Scenes>();
    public bool IsMoving;
    public Vector3Int Position { get { return Vector3Int.FloorToInt(transform.position); } }
    public Point PreviousMove { get; set; }

    public void Setup(IGameManager gameManager)
    {
        this.gameManager = gameManager;
        this.dataManager = gameManager.DataManager;

    }

    public void Move(object sender, InfoEventArgs<Point> move)
    {
        if (IsMoving)
        {
            return;
        }
        var moveToPosition = new Vector3(transform.position.x + move.info.x, transform.position.y + move.info.y, 0);
        var nextTileV3Int = gameManager.CollisionMap.WorldToCell(moveToPosition);
        StartCoroutine(LerpMove(transform.position, moveToPosition, nextTileV3Int));
        PreviousMove = move.info;
    }

    public void MakeToolAvailable(TileIdentifiers id)
    {
        if (id != TileIdentifiers.Item_BowAndArrow && id != TileIdentifiers.Item_Bomb && id != TileIdentifiers.Item_HealthPotion)
        {
            //Debug.LogWarning($"Item type {id} not allowed");
            return;
        }

        if (!ToolsAvailable.Contains(id))
        {
            ToolsAvailable.Add(id);
            gameManager.UIManager.Hud.UpdateToolSelection();
        }
    }

    public void MakeToolUnavailable(TileIdentifiers id)
    {
        if (id != TileIdentifiers.Item_BowAndArrow && id != TileIdentifiers.Item_Bomb && id != TileIdentifiers.Item_HealthPotion)
        {
            //Debug.LogWarning($"Item type {id} not allowed");
            return;
        }

        if (ToolsAvailable.Contains(id))
        {
            for (int i = 0; i < ToolsAvailable.Count; i++)
            {
                if (ToolsAvailable[i] == id)
                {
                    ToolsAvailable.RemoveAt(i);
                    break;
                }
            }
        }

        if(_selectedToolIndex >= ToolsAvailable.Count)
        {
            _selectedToolIndex = 0;
        }

        gameManager.UIManager.Hud.UpdateToolSelection();
    }

    public TileIdentifiers GetSelectedTool()
    {
        if (ToolsAvailable.Count > 0)
        {
            return ToolsAvailable[_selectedToolIndex];
        }
        return TileIdentifiers.Empty;
    }

    private IEnumerator LerpMove(Vector2 start, Vector2 moveTo, Vector3Int nextTileV3Int)
    {
        float startTime = Time.time;
        float timeElapsed = 0;
        float lerpDuration = 0.15f;
        var current = new Vector2(start.x, start.y);

        if (gameManager.CollisionMap.GetTile(nextTileV3Int) == null && dataManager.Tiles[dataManager.ActiveScene].ContainsKey(nextTileV3Int))
        {
            IsMoving = true;
            while (timeElapsed < lerpDuration)
            {
                //Debug.Log($"{x},{y}");
                current.x = Mathf.Lerp(start.x, moveTo.x, timeElapsed / lerpDuration);
                current.y = Mathf.Lerp(start.y, moveTo.y, timeElapsed / lerpDuration);
                transform.position = current;
                timeElapsed = Time.time - startTime;
                yield return new WaitForEndOfFrame();
            }

            // The player's move has completed
            transform.position = moveTo;
        }
        IsMoving = false;
        //Debug.Log($"Pos {Position}. Grid pos {dataManager.GetRoomAssociatedWithPosition(dataManager.ActiveScene, Position).GridPosition}");

        if (gameManager.DataManager.ActiveScene == Scenes.Overworld)
        {
            MostRecentOverworldPos = Position;
        }
        HandleCollision(nextTileV3Int);
        gameManager.DataManager.RunTick();
        yield return null;
    }

    private void HandleCollision(Vector3Int nextTileV3Int)
    {
        if (dataManager.Tiles[dataManager.ActiveScene].TryGetValue(nextTileV3Int, out TileInfo tileInfo))
        {
            if (tileInfo.Interactable != null)
            {
                tileInfo.Interactable.OnTouch();

                var nextGroundTile = gameManager.GroundMap.GetTile(nextTileV3Int);
                if (nextGroundTile != null)
                {
                    //Debug.Log($"Walked over {nextGroundTile.name} at {tileInfo.Position}");
                }

                var nextCollisionTile = gameManager.CollisionMap.GetTile(nextTileV3Int);
                if (nextCollisionTile != null)
                {
                    //Debug.Log($"Collided with {nextCollisionTile.name} at {tileInfo.Position}");
                }
            }
        }
    }
}

