﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;

public class Enemy : Interactable, ITickable
{    
    public int AttackMinimum;
    public int AttackMaximum;
    public int MoveRange = 1;
    public int Speed { get; set; } = 5;
    public int MoveWaitMaximum { get; set; } = 2;
    public float MaxSightDistance = 7;
    private int waitCounter;

    public Enemy(IGameManager gameManager)
    {
        GameManager = gameManager;
        DataManager = gameManager.DataManager;
    }

    public override void Destroy()
    {
        DataManager.UnregisterTickableAtTile(DataManager.Tiles[Scene][Position]);

        // Remove the enemy from the tilemap and associated data
        GameManager.CollisionMap.SetTile(Position, null);
        DataManager.Tiles[Scene][Position].Interactable = null;

        DropLoot();
    }

    public override Interactable DeepCopy()
    {
        return new Enemy(GameManager)
        {
            Hp = Hp,
            MoveRange = MoveRange,
            MoveWaitMaximum = MoveWaitMaximum,
            waitCounter = waitCounter,
            AttackMinimum = AttackMinimum,
            AttackMaximum = AttackMaximum,
            MaxSightDistance = MaxSightDistance,
            Position = new Vector3Int(Position.x, Position.y, 0),
            TileIdentifier = TileIdentifier,
            Tile = Tile,
            Scene = Scene,
            Speed = Speed,
        };
    }

    public override void OnTouch()
    {
        var damage = DataManager.Random.Next(GameManager.Player.AttackMin, GameManager.Player.AttackMax + 1);
        Hp -= damage;
        GameManager.PlaySound(AudioClipBanks.Sounds_Hurt);
        //Debug.Log($"Player dealt {damage} to {TileIdentifier}. Enemy HP {Hp}!");

        var damageTextObj = GameObjectPooler.Instance.GetObject(GameManager.DamageTextPrefab);
        damageTextObj.SetActive(true);
        damageTextObj.GetComponent<DamageTextPositioner>().Run(GameManager, CenterPosition, damage.ToString(), Color.red, 30);
    }

    private void DropLoot()
    {
        var random = DataManager.Random.Next(0, 100);
        switch (TileIdentifier)
        {
            case TileIdentifiers.Enemy_Bat:
                if (random < 20)
                {
                    // 20% chance to drop coin
                    DataManager.CreateItemAtRuntime(TileIdentifiers.Item_Coin, Scene, Position);
                    DataManager.DrawOneTile(DataManager.Tiles[Scene][Position]);
                }
                else if (random < 40)
                {
                    DataManager.CreateItemAtRuntime(TileIdentifiers.Item_Heart, Scene, Position);
                    DataManager.DrawOneTile(DataManager.Tiles[Scene][Position]);
                }
                break;
            case TileIdentifiers.Enemy_Spider:
                DropStandardLoot(random);
                break;
            case TileIdentifiers.Enemy_Ogre:
                DropUpgradedLoot(random);
                break;
            case TileIdentifiers.Enemy_Demon:
                DropSuperiorLoot(random);
                break;
            case TileIdentifiers.Enemy_Orc:
                DropUpgradedLoot(random);
                break;
            case TileIdentifiers.Enemy_Ghost:
                DropUpgradedLoot(random);
                break;
            case TileIdentifiers.Enemy_Ghost2:
                DropUpgradedLoot(random);
                break;
            case TileIdentifiers.Enemy_Ghost3:
                DropSuperiorLoot(random);
                break;
            case TileIdentifiers.Enemy_Spider2:
                DropUpgradedLoot(random);
                break;
            case TileIdentifiers.Enemy_Spider3:
                DropSuperiorLoot(random);
                break;
            case TileIdentifiers.Enemy_Skeleton:
                DropStandardLoot(random);
                break;
            default:
                DropStandardLoot(random);
                break;
        }
    }

    private void DropSuperiorLoot(int random)
    {
        if (random < 40)
        {
            DataManager.CreateItemAtRuntime(TileIdentifiers.Item_Heart, Scene, Position);
            DataManager.DrawOneTile(DataManager.Tiles[Scene][Position]);
        }
        else if (random < 80)
        {
            DataManager.CreateItemAtRuntime(TileIdentifiers.Item_Coins, Scene, Position);
            DataManager.DrawOneTile(DataManager.Tiles[Scene][Position]);
        }
        else if (random < 95)
        {
            DataManager.CreateItemAtRuntime(TileIdentifiers.Item_Bomb, Scene, Position);
            DataManager.DrawOneTile(DataManager.Tiles[Scene][Position]);
        }
    }

    private void DropUpgradedLoot(int random)
    {
        if (random < 20)
        {
            // 20% chance to drop an apple
            DataManager.CreateItemAtRuntime(TileIdentifiers.Item_Apple, Scene, Position);
            DataManager.DrawOneTile(DataManager.Tiles[Scene][Position]);
        }
        else if (random < 40)
        {
            // 20% chance to drop a coin
            DataManager.CreateItemAtRuntime(TileIdentifiers.Item_Coin, Scene, Position);
            DataManager.DrawOneTile(DataManager.Tiles[Scene][Position]);
        }
        else if (random < 45)
        {
            // 5% chance to drop coins
            DataManager.CreateItemAtRuntime(TileIdentifiers.Item_Coins, Scene, Position);
            DataManager.DrawOneTile(DataManager.Tiles[Scene][Position]);
        }
        else if (random < 60)
        {
            // 5% chance to drop coins
            DataManager.CreateItemAtRuntime(TileIdentifiers.Item_Heart, Scene, Position);
            DataManager.DrawOneTile(DataManager.Tiles[Scene][Position]);
        }
        else if (random < 80 && DataManager.GameStates.Contains(GameStates.ReceivedBombs))
        {
            // 45% chance to drop bomb if unlocked
            DataManager.CreateItemAtRuntime(TileIdentifiers.Item_Bomb, Scene, Position);
            DataManager.DrawOneTile(DataManager.Tiles[Scene][Position]);
        }
    }

    private void DropStandardLoot(int random)
    {
        if (random < 20)
        {
            // 20% chance to drop an apple
            DataManager.CreateItemAtRuntime(TileIdentifiers.Item_Apple, Scene, Position);
            DataManager.DrawOneTile(DataManager.Tiles[Scene][Position]);
        }
        else if (random < 40)
        {
            // 20% chance to drop a coin
            DataManager.CreateItemAtRuntime(TileIdentifiers.Item_Coin, Scene, Position);
            DataManager.DrawOneTile(DataManager.Tiles[Scene][Position]);
        }
        else if (random < 45)
        {
            // 5% chance to drop coins
            DataManager.CreateItemAtRuntime(TileIdentifiers.Item_Coins, Scene, Position);
            DataManager.DrawOneTile(DataManager.Tiles[Scene][Position]);
        }
        else if (random < 95 && DataManager.GameStates.Contains(GameStates.ReceivedBombs))
        {
            // 45% chance to drop bomb if unlocked
            DataManager.CreateItemAtRuntime(TileIdentifiers.Item_Bomb, Scene, Position);
            DataManager.DrawOneTile(DataManager.Tiles[Scene][Position]);
        }
    }

    public void Tick()
    {
        if (DataManager.Tiles[Scene].TryGetValue(GameManager.Player.Position, out TileInfo playerTile))
        {
            //Debug.Log($"{TileIdentifier} at {Position} tick");
            if (IsPlayerAdjacent())
            {
                AttackPlayer();
            }
            else if(Vector3Int.Distance(Position, GameManager.Player.Position) > MaxSightDistance)
            {
                // Player is too far away to see
                PerformRandomMove();
            }
            else
            {
                PerformMoveTowardPlayer();
            }
        }
        else
        {
            //Debug.LogWarning("Player out of bounds");
        }
    }

    private void PerformRandomMove()
    {        
        waitCounter++;
        if (waitCounter > MoveWaitMaximum)
        {
            waitCounter = 0;
            var randomTilePos = GetRandomOpenAdjacentTile();
            // Todo: Refactor so that GetRandomOpenAdjacentTile() returns something other than Vector3Int.zero
            if (randomTilePos != Vector3Int.zero)
            {
                //Debug.Log($"Moving {TileIdentifier} to {Position}...");
                var oldPos = Position;
                //DataManager.MoveInteractable(this, randomTilePos);
                MoveTo(randomTilePos);
                DataManager.DrawOneTile(DataManager.Tiles[Scene][oldPos]);
                DataManager.DrawOneTile(DataManager.Tiles[Scene][randomTilePos]);
            }
            else
            {
                //Debug.Log($"{TileIdentifier} blocked and unable to move at {Position}");
            }
        }
    }

    private void AttackPlayer()
    {
        var damage = DataManager.Random.Next(AttackMinimum, AttackMaximum + 1);
        GameManager.Player.Hp -= damage;
        var damageTextObj = GameObjectPooler.Instance.GetObject(GameManager.DamageTextPrefab);
        damageTextObj.SetActive(true);
        damageTextObj.GetComponent<DamageTextPositioner>().Run(GameManager, GameManager.Player.transform.position, damage.ToString(), Color.red, 30);
        GameManager.PlaySound(AudioClipBanks.Sounds_Hurt);
    }

    private void PerformMoveTowardPlayer()
    {
        waitCounter++;
        if (waitCounter > MoveWaitMaximum)
        {
            waitCounter = 0;
            var moveTo = GetUnoccupiedTileNearestToDestination(DataManager.Tiles[Scene][GameManager.Player.Position]);
            MoveTo(moveTo.Position);
        }
    }

    private void MoveTo(Vector3Int moveTo)
    {
        if (moveTo != Vector3Int.zero)
        {
            //Debug.Log($"Moving {TileIdentifier} at {Position} to {moveTo}...");
            var oldPos = Position;
            DataManager.MoveInteractable(this, moveTo);
            DataManager.DrawOneTile(DataManager.Tiles[Scene][oldPos]);
            DataManager.DrawOneTile(DataManager.Tiles[Scene][moveTo]);
        }
        else
        {
            //Debug.Log($"{TileIdentifier} blocked and unable to move at {Position}");
        }
    }

    public TileInfo GetUnoccupiedTileNearestToDestination(TileInfo destination)
    {
        var tilesInRange = GetTilesInMovementRange();
        float distance = Vector3.Distance(Position, destination.CenterPosition);
        float tempTarget;
        float moverToTempTargetDist = 0f;

        // Assume the nearest tile to destination is the enemy's starting tile until a closer tile is found
        TileInfo target = DataManager.Tiles[Scene][Position];
        foreach (var tile in tilesInRange)
        {
            tempTarget = Vector3.Distance(tile.CenterPosition, destination.CenterPosition);
            if (!tile.IsOccupied())
            {
                if (tempTarget < distance)
                {
                    moverToTempTargetDist = Vector3.Distance(tile.CenterPosition, CenterPosition);
                    distance = tempTarget;
                    target = tile;
                }
                else if (tempTarget == distance)
                {
                    if (Vector3.Distance(tile.CenterPosition, CenterPosition) < moverToTempTargetDist)
                    {
                        moverToTempTargetDist = Vector3.Distance(tile.CenterPosition, CenterPosition);
                        distance = tempTarget;
                        target = tile;
                    }
                }
            }
        }
        return target;
    }

    private HashSet<TileInfo> GetTilesInMovementRange()
    {
        var tilesWithinMovementRange = Search(DataManager.Tiles[Scene][Position], ExpandSearchWithinMovementRange);
        FilterOccupiedTiles(tilesWithinMovementRange);
        return tilesWithinMovementRange;
    }

    private bool ExpandSearchWithinMovementRange(TileInfo tile)
    {
        return tile.distance <= MoveRange;
    }

    private void FilterOccupiedTiles(HashSet<TileInfo> tiles)
    {
        TileInfo[] array = new TileInfo[tiles.Count];
        tiles.CopyTo(array);
        for (int i = array.Length - 1; i >= 0; --i)
        {
            if (array[i].IsOccupied())
            {
                tiles.Remove(array[i]);
            }
        }
    }

    public HashSet<TileInfo> Search(TileInfo start, Func<TileInfo, bool> addTile)
    {
        ClearSearch();
        start.distance = 0;
        var returnValue = new HashSet<TileInfo> { start };
        var checkNext = new Queue<TileInfo>();
        var checkNow = new Queue<TileInfo>();
        checkNow.Enqueue(start);

        while (checkNow.Count > 0)
        {
            TileInfo t = checkNow.Dequeue();
            for (int i = 0; i < DataManager.DirectionValues.Length; i++)
            {
                if (DataManager.Tiles[Scene].TryGetValue(t.Position + DataManager.DirectionValues[i], out TileInfo next))
                {
                    if (returnValue.Contains(next) && t.distance + 1 >= next.distance)
                    {
                        continue;
                    }

                    next.distance = t.distance + 1;
                    next.prev = t;

                    if (addTile(next))
                    {
                        checkNext.Enqueue(next);
                        returnValue.Add(next);
                    }
                }
            }

            if (checkNow.Count == 0)
            {
                SwapReference(ref checkNow, ref checkNext);
            }
        }

        return returnValue;
    }

    public void ComputeAdjacencyLists()
    {
        foreach (var tileInfo in DataManager.Tiles[Scene].Values)
        {
            DataManager.CalculateAdjacencyList(tileInfo);
        }
    }

    private void SwapReference(ref Queue<TileInfo> a, ref Queue<TileInfo> b)
    {
        Queue<TileInfo> temp = a;
        a = b;
        b = temp;
    }

    private void ClearSearch()
    {
        foreach (var tileInfo in DataManager.Tiles[Scene].Values)
        {
            tileInfo.prev = null;
            tileInfo.distance = int.MaxValue;
        }
    }

    private bool IsPlayerAdjacent()
    {
        foreach (var dirVal in DataManager.DirectionValues)
        {
            if (dirVal + Position == GameManager.Player.Position)
            {
                return true;
            }
        }
        return false;
    }
}