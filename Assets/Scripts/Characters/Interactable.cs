﻿using System;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Interactable
{
    protected int _hp = 1;
    public int Hp
    {
        get { return _hp; }
        set
        {
            _hp = value;
            if (_hp <= 0)
            {
                Destroy();
            }
        }
    }

    [SerializeField] public TileIdentifiers TileIdentifier { get; set; }
    [SerializeField] public Vector3Int Position { get; set; }
    public Vector2 CenterPosition { get { return new Vector2(Position.x + 0.5f, Position.y + 0.5f); } }
    [SerializeField] public TileBase Tile { get; set; }
    public Scenes Scene { get; set; }

    public IGameManager GameManager { get; set; }
    public DataManager DataManager { get; set; }    

    public virtual void OnTouch()
    {
        throw new System.NotImplementedException();
    }
    public virtual Interactable DeepCopy()
    {
        throw new System.NotImplementedException();
    }

    public virtual void Destroy()
    {
        throw new System.NotImplementedException();
    }

    protected Vector3Int GetRandomOpenAdjacentTile()
    {
        var maxNumDirections = Enum.GetValues(typeof(Directions)).Length - 1;
        var randomDir = DataManager.Random.Next(0, maxNumDirections);
        Directions direction;
        var nextPos = Vector3Int.zero;
        for (int i = 0; i < maxNumDirections; i++)
        {
            if (i + randomDir > maxNumDirections)
            {
                direction = (Directions)(randomDir + i - maxNumDirections);
            }
            else
            {
                direction = (Directions)(randomDir + i);
            }
            nextPos = DataManager.GetTilePositionInDirection(direction, Position);

            Interactable interactableCollidedWith = null;
            if (DataManager.Tiles[Scene].TryGetValue(nextPos, out TileInfo tileinfo))
            {
                interactableCollidedWith = tileinfo.Interactable;
            }

            // If there is no collision with another tile, or interactable, or the player, and the next move is in bounds
            if (GameManager.CollisionMap.GetTile(nextPos) == null && interactableCollidedWith == null
                && GameManager.Player.Position != nextPos && DataManager.Tiles[Scene].ContainsKey(nextPos))
            {
                return nextPos;
            }
            nextPos = Vector3Int.zero;
        }
        return nextPos;
    }
}
