﻿using System;
using UnityEngine;

public class NPC : Interactable, ITickable {
    public bool MoveRandomly;
    public string Dialogue;
    public int MoveRange = 1;
    public int Speed { get; set; } = 5;
    public int MoveWaitMaximum { get; set; } = 2;
    private int waitCounter;

    public NPC(IGameManager gameManager, TileIdentifiers id) {
        GameManager = gameManager;
        DataManager = gameManager.DataManager;

        string dialogue = string.Empty;
        bool moveRandomly = false;
        int hp = 10;
        var moveWaitMaximum = 2;

        switch (id) {
            case TileIdentifiers.NPC_Queen:
                dialogue = "Who said I need rescuing? Anyway, we should really get out of here, don't you think?";
                break;
            case TileIdentifiers.NPC_King:
                dialogue = "Ah! Blunky! You've come at last. Let us escape this cursed place together!";
                break;
            case TileIdentifiers.NPC_Labcoat:
                dialogue = "Miztah Blunky? You fought all ze way here? I vill follow you home! Lead the vay!";
                break;
            case TileIdentifiers.NPC_Labcoat2:
                dialogue = "My associate, Dr. Stiglitz has gone missing in the catacombs to the north. It's not safe to go there for now.";
                break;
            case TileIdentifiers.NPC_Monk:
                dialogue = "What? You want a sword? But why? They're dangerous! You could cut yourself, you know.";
                break;
            case TileIdentifiers.NPC_Duck:
                moveRandomly = true;
                dialogue = "Mr. Duck...";
                break;
            case TileIdentifiers.NPC_Wizard:
                dialogue = "Welcome, stranger. Hehehe.";
                break;
            case TileIdentifiers.NPC_HoodedMan:
                dialogue = "Blunky, there are people in the village in need of your help! Find them across the bridge.";
                break;
            case TileIdentifiers.Enemy_Crab:
                moveRandomly = true;
                dialogue = "Got crabs?";
                break;
            case TileIdentifiers.NPC_Cat:
                moveRandomly = true;
                dialogue = "I am not just any cat. I am a talking cat, thank you. Saying meow would be so beneath me.";
                break;
            case TileIdentifiers.NPC_Cat2:
                moveRandomly = true;
                dialogue = "Meow";
                break;
            case TileIdentifiers.NPC_Cow:
                moveRandomly = true;
                moveWaitMaximum = 4;
                dialogue = "Good cow. Nice cow.";
                break;
            case TileIdentifiers.NPC_Chicken:
                moveRandomly = true;
                moveWaitMaximum = 1;
                dialogue = "Cluck cluck.";
                break;
            case TileIdentifiers.NPC_Horse:
                moveRandomly = true;
                dialogue = "Hello, horsie.";
                break;
            case TileIdentifiers.NPC_Dog:
                moveRandomly = true;
                dialogue = "Oh hey doggie! Some dogs are very smart. Some of them.";
                break;
            case TileIdentifiers.NPC_Dwarf:
                dialogue = "'Ey laddie. Explosives can be used to blow away brittle rocks!";
                break;
            case TileIdentifiers.NPC_Guard:
                dialogue = "Halt! Who goes there?";
                break;
            case TileIdentifiers.NPC_FemaleYouth:
                moveRandomly = true;
                dialogue = "Hey mister, my mommy and daddy have gone missing! Maybe that cave to the south... Can you help me?";
                break;
            case TileIdentifiers.NPC_MaleYouth:
                dialogue = "I'm running away from home! Nobody can stop me! Nyahaha!";
                break;
            case TileIdentifiers.NPC_Woman:
                dialogue = "Did you know that all of the dungeons are procedurally generated? Standing on certain tiles will rotate an entire room!";
                break;
            case TileIdentifiers.NPC_Elder:
                dialogue = "What ho, young traveller? Some items are only unlocked when village citizens are found.";
                break;
            case TileIdentifiers.NPC_Elder2:
                dialogue = "Nice day for a walk, eh? No, I'm not Canadian.";
                break;
            case TileIdentifiers.NPC_Signpost:
                dialogue = "East => Town. North => Volcano. West => Desert. South => Marshland.";
                break;
            case TileIdentifiers.NPC_Swordsman:
                dialogue = "Blunky, have you tried attacking the enemy? Just move at them!";
                break;
            case TileIdentifiers.NPC_Spearman:
                dialogue = "Good day, Blunky. Did you know that you can use items with the F key?";
                break;
            case TileIdentifiers.NPC_Spearman2:
                dialogue = "I spare no expense.";
                break;
            case TileIdentifiers.NPC_Cowboy:
                dialogue = "Well, I'll tell you what fer, boy. Them cattle sure is looking well fed.";
                break;
            case TileIdentifiers.NPC_Worker:
                dialogue = "Twenty bombs, 30 gold. Healing potion, 20 gold. Attack power 30 gold. Take what you need and get out!";
                break;
            case TileIdentifiers.NPC_Faerie:
                moveRandomly = true;
                break;
        }

        TileIdentifier = id;
        Tile = DataManager.RoomFactory.GetRefForTile(id);
        Hp = hp;
        Dialogue = dialogue;
        MoveRandomly = moveRandomly;
        MoveWaitMaximum = moveWaitMaximum;
    }

    public override Interactable DeepCopy() {
        return new NPC(GameManager, TileIdentifier) {
            Hp = Hp,
            MoveRandomly = MoveRandomly,
            MoveWaitMaximum = MoveWaitMaximum,
            waitCounter = waitCounter,
            Dialogue = Dialogue,
            Position = new Vector3Int(Position.x, Position.y, 0),
            TileIdentifier = TileIdentifier,
            Tile = Tile,
            Scene = Scene,
        };
    }

    public void DropLoot() {

    }

    public override void OnTouch() {
        switch (TileIdentifier) {
            case TileIdentifiers.NPC_FemaleYouth:
                ShowFemaleYouthDialogue();
                break;
            case TileIdentifiers.NPC_Queen:
                ShowQueenDialogue();
                break;
            case TileIdentifiers.NPC_King:
                ShowKingDialogue();
                break;
            case TileIdentifiers.NPC_Labcoat:
                ShowLabcoatDialogue();
                break;
            case TileIdentifiers.NPC_Labcoat2:
                ShowLabcoat2Dialogue();
                break;
            case TileIdentifiers.NPC_Faerie:
                ShowFaerieDialogue();
                break;
            case TileIdentifiers.NPC_Dwarf:
                ShowDwarfDialogue();
                break;
            case TileIdentifiers.NPC_Fire:
                GameManager.Player.Hp -= 1;
                GameManager.PlaySound(AudioClipBanks.Sounds_Hurt);
                var damageTextObj = GameObjectPooler.Instance.GetObject(GameManager.DamageTextPrefab);
                damageTextObj.SetActive(true);
                damageTextObj.GetComponent<DamageTextPositioner>().Run(GameManager, GameManager.Player.transform.position, 1.ToString(), Color.red, 30);
                break;
            case TileIdentifiers.NPC_Campfire:
                GameManager.Player.Hp -= 1;
                GameManager.PlaySound(AudioClipBanks.Sounds_Hurt);
                var damageTextObj2 = GameObjectPooler.Instance.GetObject(GameManager.DamageTextPrefab);
                damageTextObj2.SetActive(true);
                damageTextObj2.GetComponent<DamageTextPositioner>().Run(GameManager, GameManager.Player.transform.position, 1.ToString(), Color.red, 30);
                break;
            default:
                GameManager.UIManager.DialogueWindow.ShowDialogue(Dialogue);
                break;
        }
    }

    private void ShowFemaleYouthDialogue() {
        if (DataManager.GameStates.Contains(GameStates.FoundKing)) {
            Dialogue = "Mommy and daddy are home! YAAAAAY!";
            GameManager.UIManager.DialogueWindow.ShowDialogue(Dialogue);
        }
        else if (DataManager.GameStates.Contains(GameStates.FoundQueen)) {
            Dialogue = "Thank you for bringing mommy back. But where's daddy?";
            GameManager.UIManager.DialogueWindow.ShowDialogue(Dialogue);
        }
        else {
            GameManager.UIManager.DialogueWindow.ShowDialogue(Dialogue);
        }
    }

    private void ShowQueenDialogue() {
        if (DataManager.GameStates.Contains(GameStates.FoundDoctor)) {
            Dialogue = "Hey there Blunky. Maybe now I can get back to teaching my husband how to live.";
            GameManager.UIManager.DialogueWindow.ShowDialogue(Dialogue);
        }
        else if (DataManager.GameStates.Contains(GameStates.FoundKing)) {
            Dialogue = "The doctor is still missing. Perhaps there is a third dungeon?";
            GameManager.UIManager.DialogueWindow.ShowDialogue(Dialogue);
        }
        else if (DataManager.GameStates.Contains(GameStates.ReceivedBombs) && GameManager.Player.Bombs > 0) {
            Dialogue = "Go find my useless husband, will you? Those bombs should help.";
            GameManager.UIManager.DialogueWindow.ShowDialogue(Dialogue);
        }
        else if (DataManager.GameStates.Contains(GameStates.ReceivedBombs) && GameManager.Player.Bombs == 0) {
            Dialogue = "Need more bombs? Here you go. Put them to good use.";
            GameManager.Player.Bombs += 3;
            GameManager.UIManager.DialogueWindow.ShowDialogue(Dialogue);
        }
        else if (DataManager.GameStates.Contains(GameStates.FoundQueen)) {
            DataManager.GameStates.Add(GameStates.ReceivedBombs);
            GameManager.Player.Bombs += 10;
            EventManager.OnEndDialogueEvent += DestroyNPCOnDialogueEnd;
            Dialogue = "Thanks, Blunky. Take these bombs. Use them to clear your way with the F key! I'll see you in town.";
            GameManager.UIManager.DialogueWindow.ShowDialogue(Dialogue);
        }
        else {
            DataManager.GameStates.Add(GameStates.FoundQueen);
            EventManager.OnEndDialogueEvent += DestroyNPCOnDialogueEnd;
            GameManager.UIManager.DialogueWindow.ShowDialogue(Dialogue);
        }
    }

    private void ShowDwarfDialogue() {
        if (GameManager.Player.Bombs > 0) {
            Dialogue = "Got yerself some bombs I see. Press F while standing still to use 'em and Q or E to change items.";
            GameManager.UIManager.DialogueWindow.ShowDialogue(Dialogue);
        }
        else {
            Dialogue = "Ey laddie. Explosives can be used to blow away brittle rocks!";
            GameManager.UIManager.DialogueWindow.ShowDialogue(Dialogue);
        }
    }

    private void RestorePlayerHealthOnDialogueEnd(object sender, EventArgs args) {
        GameManager.Player.Hp = GameManager.Player.MaxHp;
        GameManager.PlaySound(AudioClipBanks.Sounds_Powerup);
        EventManager.OnEndDialogueEvent -= RestorePlayerHealthOnDialogueEnd;
    }

    private void DestroyNPCOnDialogueEnd(object sender, EventArgs args) {
        Destroy();
        EventManager.OnEndDialogueEvent -= DestroyNPCOnDialogueEnd;
    }

    private void ShowKingDialogue() {
        if (DataManager.GameStates.Contains(GameStates.FoundDoctor)) {
            Dialogue = "Blunky, we are forever in your debt!";
            GameManager.UIManager.DialogueWindow.ShowDialogue(Dialogue);
        }
        else if (DataManager.GameStates.Contains(GameStates.FoundKing)) {
            Dialogue = "You saved me! And erm... found my queen. We are still missing Dr. Stiglitz, however.";
            GameManager.UIManager.DialogueWindow.ShowDialogue(Dialogue);
        }
        else {
            DataManager.GameStates.Add(GameStates.FoundKing);
            EventManager.OnEndDialogueEvent += DestroyNPCOnDialogueEnd;
            GameManager.UIManager.DialogueWindow.ShowDialogue(Dialogue);
        }
    }

    private void ShowLabcoatDialogue() {
        if (DataManager.GameStates.Contains(GameStates.FoundDoctor)) {
            Dialogue = "You haf found all ze missing villagers. Bravo, Mr. Blunky! You've won ze game!";
            GameManager.UIManager.DialogueWindow.ShowDialogue(Dialogue);
        }
        else {
            EventManager.OnEndDialogueEvent += DestroyNPCOnDialogueEnd;
            GameManager.UIManager.DialogueWindow.ShowDialogue(Dialogue);
            DataManager.GameStates.Add(GameStates.FoundDoctor);
        }
    }

    private void ShowFaerieDialogue() {
        if (GameManager.Player.Hp < GameManager.Player.MaxHp) {
            EventManager.OnEndDialogueEvent += RestorePlayerHealthOnDialogueEnd;
            Dialogue = "Blunky, be strong again!";
            GameManager.UIManager.DialogueWindow.ShowDialogue(Dialogue);
        }
        else {
            Dialogue = "Come back and visit me anytime you're hurt!";
            GameManager.UIManager.DialogueWindow.ShowDialogue(Dialogue);
        }
    }


    private void ShowLabcoat2Dialogue() {
        if (DataManager.GameStates.Contains(GameStates.FoundKing)) {
            EventManager.OnEndDialogueEvent += DestroyNPCOnDialogueEnd;
            Dialogue = "Ah! You rescued the king! You seem strong enough to find Dr. Stiglitz now, no sense in barring your way any longer.";
            GameManager.UIManager.DialogueWindow.ShowDialogue(Dialogue);
        }
        else {
            GameManager.UIManager.DialogueWindow.ShowDialogue(Dialogue);
        }
    }

    public override void Destroy() {
        DataManager.UnregisterTickableAtTile(DataManager.Tiles[Scene][Position]);

        // Remove the enemy from the tilemap and associated data
        GameManager.CollisionMap.SetTile(Position, null);
        DataManager.Tiles[Scene][Position].Interactable = null;
        DropLoot();
    }

    public void Tick() {
        //Debug.Log($"{TileIdentifier} at {Position} tick");
        PerformRandomMove();
    }

    private void PerformRandomMove() {
        if (MoveRandomly) {
            waitCounter++;
            if (waitCounter > MoveWaitMaximum) {
                waitCounter = 0;
                var randomTilePos = GetRandomOpenAdjacentTile();
                // Todo: Refactor so that GetRandomOpenAdjacentTile() returns something other than Vector3Int.zero
                if (randomTilePos != Vector3Int.zero) {
                    //Debug.Log($"Moving {TileIdentifier} to {Position}...");
                    var oldPos = Position;
                    DataManager.MoveInteractable(this, randomTilePos);
                    DataManager.DrawOneTile(DataManager.Tiles[Scene][oldPos]);
                    DataManager.DrawOneTile(DataManager.Tiles[Scene][randomTilePos]);
                }
                else {
                    //Debug.Log($"{TileIdentifier} blocked and unable to move at {Position}");
                }
            }
        }
    }
}