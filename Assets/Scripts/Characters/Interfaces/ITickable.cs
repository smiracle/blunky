﻿public interface ITickable
{
    public int Speed { get; set; }
    public int MoveWaitMaximum { get; set; }
    public void Tick();
}
