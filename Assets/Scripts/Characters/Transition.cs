﻿using UnityEngine;

public class Transition : Interactable
{
    public Scenes SceneToTransitionTo;
    public Vector3Int PositionToTransitionTo;

    public Transition(IGameManager gameManager)
    {
        GameManager = gameManager;
        DataManager = gameManager.DataManager;
    }
    public override void OnTouch()
    {
       // Debug.Log($"Touched transition {TileIdentifier}");
        GameManager.Transition(SceneToTransitionTo, PositionToTransitionTo);
    }

    public override void Destroy()
    {

    }

    public override Interactable DeepCopy()
    {
        var transition = new Transition(GameManager)
        {
            SceneToTransitionTo = SceneToTransitionTo,
            PositionToTransitionTo = new Vector3Int(PositionToTransitionTo.x, PositionToTransitionTo.y, 0),
            Position = new Vector3Int(Position.x, Position.y, 0),
            TileIdentifier = TileIdentifier,
            Tile = Tile,
            Scene = Scene,
            Hp = Hp
        };        
        return transition;
    }
}