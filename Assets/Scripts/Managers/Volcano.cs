﻿public static class Volcano
{
    public static void Setup(IGameManager gameManager)
    {
        var dataManager = gameManager.DataManager;
        var volcanoStart1 = new string[] {
          //  1     2     3     4     5     6     7   
            "108","108","108","000","108","108","108", // 1
            "108","000","000","000","000","000","108", // 2
            "108","000","000","000","000","000","108", // 3
            "000","000","000","289","000","000","000", // 4
            "108","000","979","000","000","000","108", // 5
            "108","000","000","000","000","000","108", // 6
            "108","108","108","000","108","108","108", // 7
        };
        dataManager.RoomDefinitions_Volcano.Add(RoomLabels.VolcanoStart1,
            new RoomDefinition(gameManager, SceneTypes.Volcano, RoomLabels.VolcanoStart1, volcanoStart1, RoomOrientations.OOOO, RoomTypes.Start));

        var volcanoStart2 = new string[] {
          //  1     2     3     4     5     6     7   
            "108","108","108","000","108","108","108", // 1
            "108","000","000","000","000","000","108", // 2
            "108","000","000","000","000","000","108", // 3
            "000","000","000","289","000","979","000", // 4
            "108","000","000","000","000","000","108", // 5
            "108","000","000","000","000","000","108", // 6
            "108","108","108","000","108","108","108", // 7
        };
        dataManager.RoomDefinitions_Volcano.Add(RoomLabels.VolcanoStart2,
            new RoomDefinition(gameManager, SceneTypes.Volcano, RoomLabels.VolcanoStart2, volcanoStart2, RoomOrientations.XXOX, RoomTypes.Start));

        var volcanoStart3 = new string[] {
          //  1     2     3     4     5     6     7   
            "108","000","000","000","000","000","108", // 1
            "108","000","000","000","000","000","108", // 2
            "108","000","000","000","000","000","108", // 3
            "000","000","979","000","000","000","108", // 4
            "108","000","000","000","000","000","108", // 5
            "108","000","000","289","000","000","108", // 6
            "108","108","108","108","108","108","108", // 7
        };
        dataManager.RoomDefinitions_Volcano.Add(RoomLabels.VolcanoStart3,
            new RoomDefinition(gameManager, SceneTypes.Volcano, RoomLabels.VolcanoStart3, volcanoStart3, RoomOrientations.OXXX, RoomTypes.Start));

        var volcanoStart4 = new string[] {
          //  1     2     3     4     5     6     7   
            "108","108","108","108","108","108","108", // 1
            "000","000","000","000","000","000","000", // 2
            "000","000","000","289","000","000","000", // 3
            "000","000","000","000","000","979","000", // 4
            "000","000","000","000","000","000","000", // 5
            "000","000","000","000","000","000","000", // 6
            "000","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Volcano.Add(RoomLabels.VolcanoStart4,
            new RoomDefinition(gameManager, SceneTypes.Volcano, RoomLabels.VolcanoStart4, volcanoStart4, RoomOrientations.XOOO, RoomTypes.Start));

        var volcanoStart5 = new string[] {
          //  1     2     3     4     5     6     7   
            "108","108","108","108","108","108","108", // 1
            "000","000","000","000","000","000","108", // 2
            "000","000","979","000","000","289","108", // 3
            "000","000","000","000","000","000","108", // 4
            "000","000","000","000","000","000","108", // 5
            "000","000","000","000","000","000","108", // 6
            "108","108","108","108","108","108","108", // 7
        };
        dataManager.RoomDefinitions_Volcano.Add(RoomLabels.VolcanoStart5,
            new RoomDefinition(gameManager, SceneTypes.Volcano, RoomLabels.VolcanoStart5, volcanoStart5, RoomOrientations.XXXO, RoomTypes.Start));

        var volcanoStart6 = new string[] {
          //  1     2     3     4     5     6     7   
            "108","108","108","108","108","108","108", // 1
            "000","000","000","000","000","289","108", // 2
            "000","000","000","000","000","000","108", // 3
            "000","000","000","000","000","979","108", // 4
            "000","000","494","000","000","000","108", // 5
            "000","000","000","000","000","000","108", // 6
            "000","000","000","000","000","000","108", // 7
        };
        dataManager.RoomDefinitions_Volcano.Add(RoomLabels.VolcanoStart6,
            new RoomDefinition(gameManager, SceneTypes.Volcano, RoomLabels.VolcanoStart6, volcanoStart6, RoomOrientations.XXOO, RoomTypes.Start));

        var volcanoStart7 = new string[] {
          //  1     2     3     4     5     6     7   
            "108","000","000","000","000","000","000", // 1
            "108","000","000","000","000","000","000", // 2
            "108","000","289","000","000","000","000", // 3
            "108","979","000","000","000","000","000", // 4
            "108","000","000","000","000","000","000", // 5
            "108","000","000","000","000","000","000", // 6
            "108","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Volcano.Add(RoomLabels.VolcanoStart7,
            new RoomDefinition(gameManager, SceneTypes.Volcano, RoomLabels.VolcanoStart7, volcanoStart7, RoomOrientations.OOOX, RoomTypes.Start));

        var volcanoStart8 = new string[] {
          //  1     2     3     4     5     6     7   
            "108","108","108","108","108","108","108", // 1
            "108","289","000","000","000","000","000", // 2
            "108","000","000","000","000","000","000", // 3
            "108","000","000","000","000","000","000", // 4
            "108","000","000","000","000","000","979", // 5
            "108","000","000","000","000","000","000", // 6
            "108","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Volcano.Add(RoomLabels.VolcanoStart8,
            new RoomDefinition(gameManager, SceneTypes.Volcano, RoomLabels.VolcanoStart8, volcanoStart8, RoomOrientations.XOOX, RoomTypes.Start));

        var volcanoStart9 = new string[] {
          //  1     2     3     4     5     6     7   
            "108","108","108","108","108","108","108", // 1
            "000","000","000","000","000","000","000", // 2
            "000","000","000","000","000","000","000", // 3
            "000","000","000","289","000","000","000", // 4
            "979","000","000","000","000","000","000", // 5
            "000","000","000","000","000","000","000", // 6
            "108","108","108","108","108","108","108", // 7
        };
        dataManager.RoomDefinitions_Volcano.Add(RoomLabels.VolcanoStart9,
            new RoomDefinition(gameManager, SceneTypes.Volcano, RoomLabels.VolcanoStart9, volcanoStart9, RoomOrientations.XOXO, RoomTypes.Start));

        var volcanoStart10 = new string[] {
          //  1     2     3     4     5     6     7   
            "108","108","108","108","108","108","108", // 1
            "108","000","000","000","000","000","979", // 2
            "108","000","000","000","000","000","000", // 3
            "108","000","000","289","000","494","000", // 4
            "108","000","000","000","000","000","000", // 5
            "108","000","000","000","000","000","000", // 6
            "108","108","108","108","108","108","108", // 7
        };
        dataManager.RoomDefinitions_Volcano.Add(RoomLabels.VolcanoStart10,
            new RoomDefinition(gameManager, SceneTypes.Volcano, RoomLabels.VolcanoStart10, volcanoStart10, RoomOrientations.XOXX, RoomTypes.Start));

        var volcanoStart11 = new string[] {
          //  1     2     3     4     5     6     7   
            "108","000","000","000","000","000","108", // 1
            "108","000","000","000","000","000","108", // 2
            "108","000","000","000","000","000","108", // 3
            "108","000","000","289","000","000","108", // 4
            "108","000","000","000","000","000","108", // 5
            "108","000","000","000","000","000","108", // 6
            "108","000","979","000","000","000","108", // 7
        };
        dataManager.RoomDefinitions_Volcano.Add(RoomLabels.VolcanoStart11,
            new RoomDefinition(gameManager, SceneTypes.Volcano, RoomLabels.VolcanoStart11, volcanoStart11, RoomOrientations.OXOX, RoomTypes.Start));

        var volcanoStart12 = new string[] {
          //  1     2     3     4     5     6     7   
            "108","000","000","000","000","000","000", // 1
            "108","000","000","000","000","000","000", // 2
            "108","000","000","000","000","000","000", // 3
            "108","000","000","289","000","000","000", // 4
            "108","000","000","000","000","000","000", // 5
            "108","000","000","000","000","979","000", // 6
            "108","108","108","108","108","108","108", // 7
        };
        dataManager.RoomDefinitions_Volcano.Add(RoomLabels.VolcanoStart12,
            new RoomDefinition(gameManager, SceneTypes.Volcano, RoomLabels.VolcanoStart12, volcanoStart12, RoomOrientations.OOXX, RoomTypes.Start));

        var volcanoStart13 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","108", // 1
            "000","979","000","000","000","000","108", // 2
            "000","000","494","000","000","000","108", // 3
            "000","000","000","289","000","000","108", // 4
            "000","000","000","000","000","000","108", // 5
            "000","000","000","000","000","000","108", // 6
            "000","000","000","000","000","000","108", // 7
        };
        dataManager.RoomDefinitions_Volcano.Add(RoomLabels.VolcanoStart13,
            new RoomDefinition(gameManager, SceneTypes.Volcano, RoomLabels.VolcanoStart13, volcanoStart13, RoomOrientations.OXOO, RoomTypes.Start));

        var volcanoStart14 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","000", // 1
            "000","000","000","000","000","000","000", // 2
            "000","000","000","000","000","000","000", // 3
            "000","000","000","289","000","000","000", // 4
            "000","000","000","000","000","000","000", // 5
            "000","000","000","000","000","000","979", // 6
            "108","108","108","108","108","108","108", // 7
        };
        dataManager.RoomDefinitions_Volcano.Add(RoomLabels.VolcanoStart14,
            new RoomDefinition(gameManager, SceneTypes.Volcano, RoomLabels.VolcanoStart14, volcanoStart14, RoomOrientations.OOXO, RoomTypes.Start));

        var volcanoStart15 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","108", // 1
            "000","979","000","000","000","000","108", // 2
            "000","000","000","000","000","000","108", // 3
            "000","000","518","289","000","000","108", // 4
            "000","000","000","000","000","000","108", // 5
            "000","000","000","000","000","000","108", // 6
            "108","108","108","108","108","108","108", // 7
        };
        dataManager.RoomDefinitions_Volcano.Add(RoomLabels.VolcanoStart15,
            new RoomDefinition(gameManager, SceneTypes.Volcano, RoomLabels.VolcanoStart15, volcanoStart15, RoomOrientations.OXXO, RoomTypes.Start));

        var volcano1_demons = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","000", // 1
            "000","000","000","979","000","000","000", // 2
            "000","000","000","000","000","000","000", // 3
            "000","000","000","122","000","000","000", // 4
            "000","000","000","000","000","000","000", // 5
            "000","494","000","000","000","000","000", // 6
            "000","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Volcano.Add(RoomLabels.VolcanoDemons1,
            new RoomDefinition(gameManager, SceneTypes.Volcano, RoomLabels.VolcanoDemons1, volcano1_demons, RoomOrientations.OOOO));

        var volcano1_empty = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","000", // 1
            "000","000","000","979","000","000","000", // 2
            "000","000","000","000","000","000","000", // 3
            "000","518","723","723","723","000","000", // 4
            "000","000","000","000","000","000","000", // 5
            "000","000","000","232","000","000","000", // 6
            "000","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Volcano.Add(RoomLabels.VolcanoEmpty1,
            new RoomDefinition(gameManager, SceneTypes.Volcano, RoomLabels.VolcanoEmpty1, volcano1_empty, RoomOrientations.OOOO));

        var volcano1_ogres = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","000", // 1
            "000","000","000","213","000","000","000", // 2
            "000","000","000","000","000","000","000", // 3
            "000","000","000","317","000","979","000", // 4
            "000","000","000","000","000","000","000", // 5
            "000","000","000","213","000","000","000", // 6
            "000","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Volcano.Add(RoomLabels.VolcanoOgres1,
            new RoomDefinition(gameManager, SceneTypes.Volcano, RoomLabels.VolcanoOgres1, volcano1_ogres, RoomOrientations.OOOO));

        var volcano1_spiders = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","000", // 1
            "000","000","000","564","000","000","000", // 2
            "000","000","000","000","564","000","000", // 3
            "000","000","518","268","000","000","000", // 4
            "000","979","000","000","564","000","000", // 5
            "000","000","000","564","000","000","000", // 6
            "000","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Volcano.Add(RoomLabels.VolcanoSpiders1,
            new RoomDefinition(gameManager, SceneTypes.Volcano, RoomLabels.VolcanoSpiders1, volcano1_spiders, RoomOrientations.OOOO));

        var volcano2_demons = new string[] {
          //  1     2     3     4     5     6     7   
            "108","000","000","000","000","000","108", // 1
            "108","000","000","000","000","000","108", // 2
            "108","000","000","000","000","000","109", // 3
            "108","122","000","979","000","122","108", // 4
            "108","000","000","000","000","000","001", // 5
            "001","000","000","000","000","000","001", // 6
            "001","000","000","000","000","000","001", // 7
        };
        dataManager.RoomDefinitions_Volcano.Add(RoomLabels.VolcanoDemons2,
            new RoomDefinition(gameManager, SceneTypes.Volcano, RoomLabels.VolcanoDemons2, volcano2_demons, RoomOrientations.OXOX));

        var volcano2_spiders = new string[] {
          //  1     2     3     4     5     6     7   
            "001","000","000","000","000","000","001", // 1
            "001","000","000","000","000","000","001", // 2
            "001","000","000","000","000","000","001", // 3
            "108","564","564","000","564","564","108", // 4
            "108","268","000","000","000","000","108", // 5
            "108","000","000","979","000","000","001", // 6
            "001","000","000","000","000","000","001", // 7
        };
        dataManager.RoomDefinitions_Volcano.Add(RoomLabels.VolcanoSpiders2,
            new RoomDefinition(gameManager, SceneTypes.Volcano, RoomLabels.VolcanoSpiders2, volcano2_spiders, RoomOrientations.OXOX));

        var volcanoDemons3 = new string[] {
          //  1     2     3     4     5     6     7   
            "108","108","001","108","108","001","001", // 1
            "108","000","000","000","000","000","108", // 2
            "000","000","000","000","000","000","000", // 3
            "000","000","000","122","000","000","979", // 4
            "000","000","000","000","000","000","000", // 5
            "001","000","000","000","000","000","001", // 6
            "001","108","108","001","001","001","108", // 7
        };
        dataManager.RoomDefinitions_Volcano.Add(RoomLabels.VolcanoDemons3,
            new RoomDefinition(gameManager, SceneTypes.Volcano, RoomLabels.VolcanoDemons3, volcanoDemons3, RoomOrientations.XOXO));

        var volcano3_spiders = new string[] {
          //  1     2     3     4     5     6     7   
            "108","001","001","001","108","108","108", // 1
            "001","000","000","000","000","000","001", // 2
            "000","000","000","000","564","000","000", // 3
            "979","518","000","268","000","000","000", // 4
            "000","000","564","000","000","000","000", // 5
            "108","000","000","000","000","000","108", // 6
            "108","001","001","001","108","108","108", // 7
        };
        dataManager.RoomDefinitions_Volcano.Add(RoomLabels.VolcanoSpiders3,
            new RoomDefinition(gameManager, SceneTypes.Volcano, RoomLabels.VolcanoSpiders3, volcano3_spiders, RoomOrientations.XOXO));

        var volcanoDemons4 = new string[] {
          //  1     2     3     4     5     6     7   
            "108","001","001","001","108","108","108", // 1
            "108","000","000","000","000","000","108", // 2
            "000","000","000","000","000","000","001", // 3
            "000","000","000","000","000","979","001", // 4
            "000","000","494","000","000","000","001", // 5
            "000","000","000","000","000","000","001", // 6
            "000","000","000","000","000","108","108", // 7
        };
        dataManager.RoomDefinitions_Volcano.Add(RoomLabels.VolcanoDemons4,
            new RoomDefinition(gameManager, SceneTypes.Volcano, RoomLabels.VolcanoDemons4, volcanoDemons4, RoomOrientations.XXOO));

        var volcanoDemons5 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","108", // 1
            "000","000","000","000","000","000","108", // 2
            "000","000","000","000","000","000","001", // 3
            "000","000","000","122","000","979","001", // 4
            "000","000","000","000","000","000","001", // 5
            "000","000","000","000","000","000","001", // 6
            "108","108","108","108","108","108","108", // 7
        };
        dataManager.RoomDefinitions_Volcano.Add(RoomLabels.VolcanoDemons5,
            new RoomDefinition(gameManager, SceneTypes.Volcano, RoomLabels.VolcanoDemons5, volcanoDemons5, RoomOrientations.OXXO));

        var volcano_empty5 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","108", // 1
            "000","000","000","000","000","979","108", // 2
            "000","564","000","000","000","000","001", // 3
            "000","213","000","000","000","213","001", // 4
            "000","000","000","000","000","000","108", // 5
            "000","000","000","000","564","000","108", // 6
            "108","108","108","108","108","108","108", // 7
        };
        dataManager.RoomDefinitions_Volcano.Add(RoomLabels.VolcanoEmpty5,
            new RoomDefinition(gameManager, SceneTypes.Volcano, RoomLabels.VolcanoEmpty5, volcano_empty5, RoomOrientations.OXXO));

        var volcanoDemons6 = new string[] {
          //  1     2     3     4     5     6     7   
            "108","000","000","000","000","000","000", // 1
            "108","000","000","000","000","000","000", // 2
            "001","000","000","000","000","000","000", // 3
            "001","979","518","122","518","000","000", // 4
            "001","000","000","000","000","000","000", // 5
            "108","000","000","000","000","000","000", // 6
            "108","108","108","001","001","108","108", // 7
        };
        dataManager.RoomDefinitions_Volcano.Add(RoomLabels.VolcanoDemons6,
            new RoomDefinition(gameManager, SceneTypes.Volcano, RoomLabels.VolcanoDemons6, volcanoDemons6, RoomOrientations.OOXX));

        var volcano6_empty = new string[] {
          //  1     2     3     4     5     6     7   
            "108","000","000","000","000","000","000", // 1
            "108","000","000","000","000","000","000", // 2
            "108","000","000","000","000","000","000", // 3
            "108","000","000","000","000","979","000", // 4
            "108","000","000","000","000","000","000", // 5
            "108","000","000","000","000","000","000", // 6
            "108","001","001","001","108","108","108", // 7
        };
        dataManager.RoomDefinitions_Volcano.Add(RoomLabels.VolcanoEmpty6,
            new RoomDefinition(gameManager, SceneTypes.Volcano, RoomLabels.VolcanoEmpty6, volcano6_empty, RoomOrientations.OOXX));

        var volcanoDemons7 = new string[] {
          //  1     2     3     4     5     6     7   
            "108","001","001","001","001","001","108", // 1
            "108","000","000","000","000","000","108", // 2
            "000","000","000","000","000","000","000", // 3
            "000","518","000","122","000","000","000", // 4
            "000","000","000","000","000","000","000", // 5
            "108","000","494","000","000","000","001", // 6
            "001","000","000","000","000","000","108", // 7
        };
        dataManager.RoomDefinitions_Volcano.Add(RoomLabels.VolcanoDemons7,
            new RoomDefinition(gameManager, SceneTypes.Volcano, RoomLabels.VolcanoDemons7, volcanoDemons7, RoomOrientations.XOOO));

        var volcano7_spiders = new string[] {
          //  1     2     3     4     5     6     7   
            "001","001","001","108","108","001","001", // 1
            "108","000","000","000","000","000","108", // 2
            "000","000","000","000","268","000","000", // 3
            "000","979","564","564","564","000","000", // 4
            "000","000","000","000","564","268","000", // 5
            "108","000","000","000","000","000","001", // 6
            "001","000","000","000","000","000","108", // 7
        };
        dataManager.RoomDefinitions_Volcano.Add(RoomLabels.VolcanoSpiders7,
            new RoomDefinition(gameManager, SceneTypes.Volcano, RoomLabels.VolcanoSpiders7, volcano7_spiders, RoomOrientations.XOOO));

        var volcanoDemons8 = new string[] {
          //  1     2     3     4     5     6     7   
            "108","108","108","001","001","001","001", // 1
            "000","000","000","000","000","979","108", // 2
            "000","000","000","000","000","000","108", // 3
            "000","000","000","122","000","000","001", // 4
            "000","000","000","000","000","000","001", // 5
            "000","000","000","000","000","000","001", // 6
            "108","108","108","001","001","108","108", // 7
        };
        dataManager.RoomDefinitions_Volcano.Add(RoomLabels.VolcanoDemons8,
            new RoomDefinition(gameManager, SceneTypes.Volcano, RoomLabels.VolcanoDemons8, volcanoDemons8, RoomOrientations.XXXO));

        var volcano8_empty = new string[] {
          //  1     2     3     4     5     6     7   
            "108","108","108","001","001","108","108", // 1
            "000","000","000","000","000","000","108", // 2
            "000","000","000","722","000","000","108", // 3
            "000","000","979","000","232","000","108", // 4
            "000","000","000","722","000","000","108", // 5
            "000","000","000","000","000","000","001", // 6
            "001","001","001","001","108","108","001", // 7
        };
        dataManager.RoomDefinitions_Volcano.Add(RoomLabels.VolcanoEmpty8,
            new RoomDefinition(gameManager, SceneTypes.Volcano, RoomLabels.VolcanoEmpty8, volcano8_empty, RoomOrientations.XXXO));

        var volcanoDemons9 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","108", // 1
            "000","108","000","000","000","000","001", // 2
            "000","000","000","000","000","000","001", // 3
            "000","000","000","122","000","979","001", // 4
            "000","000","000","000","000","000","108", // 5
            "000","108","000","000","000","000","108", // 6
            "000","000","000","000","000","000","108", // 7
        };
        dataManager.RoomDefinitions_Volcano.Add(RoomLabels.VolcanoDemons9,
            new RoomDefinition(gameManager, SceneTypes.Volcano, RoomLabels.VolcanoDemons9, volcanoDemons9, RoomOrientations.OXOO));

        var volcano9_empty = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","108", // 1
            "000","564","000","000","000","000","108", // 2
            "000","000","564","000","000","000","001", // 3
            "000","518","237","518","518","979","001", // 4
            "000","000","564","000","000","000","001", // 5
            "000","564","000","000","000","000","108", // 6
            "000","000","000","000","000","000","108", // 7
        };
        dataManager.RoomDefinitions_Volcano.Add(RoomLabels.VolcanoEmpty9,
            new RoomDefinition(gameManager, SceneTypes.Volcano, RoomLabels.VolcanoEmpty9, volcano9_empty, RoomOrientations.OXOO));

        var volcano9_spiders = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","108", // 1
            "000","564","000","000","000","000","001", // 2
            "000","000","564","000","000","000","001", // 3
            "000","000","000","268","000","979","001", // 4
            "000","000","564","000","000","000","108", // 5
            "000","564","000","000","000","000","108", // 6
            "000","000","000","000","000","000","108", // 7
        };
        dataManager.RoomDefinitions_Volcano.Add(RoomLabels.VolcanoSpiders9,
            new RoomDefinition(gameManager, SceneTypes.Volcano, RoomLabels.VolcanoSpiders9, volcano9_spiders, RoomOrientations.OXOO));

        var volcanoDemons10 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","000", // 1
            "000","108","000","000","000","000","000", // 2
            "000","000","000","000","000","000","000", // 3
            "000","000","000","122","000","000","000", // 4
            "000","000","494","000","000","000","000", // 5
            "000","108","000","000","000","000","000", // 6
            "108","001","001","001","001","108","108", // 7
        };
        dataManager.RoomDefinitions_Volcano.Add(RoomLabels.VolcanoDemons10,
            new RoomDefinition(gameManager, SceneTypes.Volcano, RoomLabels.VolcanoDemons10, volcanoDemons10, RoomOrientations.OOXO));

        var volcanoEmpty10 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","000", // 1
            "000","108","000","000","000","000","000", // 2
            "000","000","518","518","518","000","000", // 3
            "000","979","518","518","518","000","000", // 4
            "000","000","518","518","518","000","000", // 5
            "000","108","000","000","000","000","000", // 6
            "108","108","001","001","001","108","108", // 7
        };
        dataManager.RoomDefinitions_Volcano.Add(RoomLabels.VolcanoEmpty10,
            new RoomDefinition(gameManager, SceneTypes.Volcano, RoomLabels.VolcanoEmpty10, volcanoEmpty10, RoomOrientations.OOXO));

        var volcano10_ogres = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","000", // 1
            "000","000","000","000","000","000","000", // 2
            "000","000","000","000","000","000","000", // 3
            "000","000","237","317","979","000","000", // 4
            "000","000","000","000","000","000","000", // 5
            "000","000","000","000","000","000","000", // 6
            "108","001","001","001","108","108","108", // 7
        };
        dataManager.RoomDefinitions_Volcano.Add(RoomLabels.VolcanoOgres10,
            new RoomDefinition(gameManager, SceneTypes.Volcano, RoomLabels.VolcanoOgres10, volcano10_ogres, RoomOrientations.OOXO));

        var volcanoDemons11 = new string[] {
          //  1     2     3     4     5     6     7   
            "001","001","001","001","108","108","108", // 1
            "108","108","000","000","000","000","001", // 2
            "001","000","494","000","000","000","108", // 3
            "108","000","000","122","000","000","108", // 4
            "108","000","000","000","000","000","108", // 5
            "108","108","000","000","000","979","001", // 6
            "108","000","000","000","000","000","108", // 7
        };
        dataManager.RoomDefinitions_Volcano.Add(RoomLabels.VolcanoDemons11,
            new RoomDefinition(gameManager, SceneTypes.Volcano, RoomLabels.VolcanoDemons11, volcanoDemons11, RoomOrientations.XXOX));

        var volcano11_ogres = new string[] {
          //  1     2     3     4     5     6     7   
            "108","108","108","001","001","001","108", // 1
            "001","001","000","000","000","979","001", // 2
            "001","000","494","000","000","000","001", // 3
            "001","000","000","317","000","000","108", // 4
            "108","000","000","000","000","000","108", // 5
            "108","108","000","000","000","108","001", // 6
            "108","000","000","000","000","000","108", // 7
        };
        dataManager.RoomDefinitions_Volcano.Add(RoomLabels.VolcanoOgres11,
            new RoomDefinition(gameManager, SceneTypes.Volcano, RoomLabels.VolcanoOgres11, volcano11_ogres, RoomOrientations.XXOX));

        var volcanoDemons12 = new string[] {
          //  1     2     3     4     5     6     7   
            "108","000","000","000","000","000","108", // 1
            "108","000","000","000","000","000","001", // 2
            "001","000","000","000","000","000","001", // 3
            "108","000","000","122","000","000","108", // 4
            "108","000","000","000","000","000","108", // 5
            "108","000","000","000","000","000","108", // 6
            "108","001","001","108","108","108","108", // 7
        };
        dataManager.RoomDefinitions_Volcano.Add(RoomLabels.VolcanoDemons12,
            new RoomDefinition(gameManager, SceneTypes.Volcano, RoomLabels.VolcanoDemons12, volcanoDemons12, RoomOrientations.OXXX));

        var volcano12_empty = new string[] {
          //  1     2     3     4     5     6     7   
            "108","000","000","000","000","000","108", // 1
            "001","000","000","000","000","000","001", // 2
            "001","000","896","000","896","000","108", // 3
            "108","000","000","979","000","000","001", // 4
            "001","000","896","000","896","000","108", // 5
            "108","000","000","000","000","000","001", // 6
            "001","001","108","108","108","001","001", // 7
        };
        dataManager.RoomDefinitions_Volcano.Add(RoomLabels.VolcanoEmpty12,
            new RoomDefinition(gameManager, SceneTypes.Volcano, RoomLabels.VolcanoEmpty12, volcano12_empty, RoomOrientations.OXXX));

        var volcano12_ogres = new string[] {
          //  1     2     3     4     5     6     7   
            "001","000","000","000","000","000","001", // 1
            "001","000","000","000","000","000","001", // 2
            "001","000","000","000","000","000","001", // 3
            "108","317","000","979","000","317","108", // 4
            "108","000","000","000","000","000","108", // 5
            "108","000","000","000","000","000","108", // 6
            "108","001","001","001","108","108","108", // 7
        };
        dataManager.RoomDefinitions_Volcano.Add(RoomLabels.VolcanoOgres12,
            new RoomDefinition(gameManager, SceneTypes.Volcano, RoomLabels.VolcanoOgres12, volcano12_ogres, RoomOrientations.OXXX));

        var volcano13_ogres = new string[] {
          //  1     2     3     4     5     6     7   
            "108","000","000","000","000","000","000", // 1
            "001","000","000","000","000","000","000", // 2
            "001","000","000","000","000","000","000", // 3
            "001","000","000","317","000","000","000", // 4
            "108","000","000","000","000","000","000", // 5
            "108","000","000","000","000","000","000", // 6
            "001","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Volcano.Add(RoomLabels.VolcanoOgres13,
            new RoomDefinition(gameManager, SceneTypes.Volcano, RoomLabels.VolcanoOgres13, volcano13_ogres, RoomOrientations.OOOX));

        var volcanoDemons14 = new string[] {
          //  1     2     3     4     5     6     7   
            "001","108","108","001","001","108","108", // 1
            "108","000","000","000","000","000","000", // 2
            "108","000","000","000","000","000","000", // 3
            "001","979","000","317","000","000","000", // 4
            "001","000","000","000","000","000","000", // 5
            "001","000","000","000","000","000","000", // 6
            "108","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Volcano.Add(RoomLabels.VolcanoDemons14,
            new RoomDefinition(gameManager, SceneTypes.Volcano, RoomLabels.VolcanoDemons14, volcanoDemons14, RoomOrientations.XOOX));

        var volcano15_ogres = new string[] {
          //  1     2     3     4     5     6     7   
            "108","001","001","108","108","108","108", // 1
            "108","000","000","000","000","000","000", // 2
            "001","000","000","000","000","000","000", // 3
            "001","000","000","000","317","000","000", // 4
            "001","000","000","000","000","000","000", // 5
            "108","000","000","000","000","000","000", // 6
            "108","108","108","108","108","108","001", // 7
        };
        dataManager.RoomDefinitions_Volcano.Add(RoomLabels.VolcanoOgres15,
            new RoomDefinition(gameManager, SceneTypes.Volcano, RoomLabels.VolcanoOgres15, volcano15_ogres, RoomOrientations.XOXX));

        var volcanoEnd1 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","000", // 1
            "000","000","494","000","494","000","000", // 2
            "000","000","000","000","000","000","000", // 3
            "000","000","000","171","000","000","000", // 4
            "000","000","000","000","000","000","000", // 5
            "000","000","494","000","494","000","000", // 6
            "000","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Volcano.Add(RoomLabels.VolcanoEnd1,
            new RoomDefinition(gameManager, SceneTypes.Volcano, RoomLabels.VolcanoEnd1, volcanoEnd1, RoomOrientations.OOOO, RoomTypes.End));

        var volcanoEnd2 = new string[] {
          //  1     2     3     4     5     6     7   
            "001","001","001","108","108","108","108", // 1
            "108","000","494","000","494","000","108", // 2
            "108","000","000","000","000","000","001", // 3
            "001","000","000","171","000","979","001", // 4
            "001","000","000","000","000","000","001", // 5
            "108","000","000","000","000","000","108", // 6
            "108","000","000","000","000","000","108", // 7
        };
        dataManager.RoomDefinitions_Volcano.Add(RoomLabels.VolcanoEnd2,
            new RoomDefinition(gameManager, SceneTypes.Volcano, RoomLabels.VolcanoEnd2, volcanoEnd2, RoomOrientations.XXOX, RoomTypes.End));

        var volcanoEnd3 = new string[] {
          //  1     2     3     4     5     6     7   
            "108","001","001","001","108","108","108", // 1
            "108","000","000","000","000","000","000", // 2
            "001","000","000","000","000","000","000", // 3
            "001","000","000","171","000","000","000", // 4
            "108","000","494","000","494","000","000", // 5
            "108","000","000","000","000","000","000", // 6
            "108","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Volcano.Add(RoomLabels.VolcanoEnd3,
            new RoomDefinition(gameManager, SceneTypes.Volcano, RoomLabels.VolcanoEnd3, volcanoEnd3, RoomOrientations.XOOX, RoomTypes.End));

        var volcanoEnd4 = new string[] {
          //  1     2     3     4     5     6     7   
            "108","001","001","108","108","001","001", // 1
            "000","000","000","000","000","000","108", // 2
            "000","000","000","000","494","000","108", // 3
            "000","000","000","171","000","000","001", // 4
            "000","000","494","000","000","000","001", // 5
            "000","000","000","000","000","000","108", // 6
            "000","000","000","000","000","000","108", // 7
        };
        dataManager.RoomDefinitions_Volcano.Add(RoomLabels.VolcanoEnd4,
            new RoomDefinition(gameManager, SceneTypes.Volcano, RoomLabels.VolcanoEnd4, volcanoEnd4, RoomOrientations.XXOO, RoomTypes.End));

        var volcanoEnd5 = new string[] {
          //  1     2     3     4     5     6     7   
            "108","001","001","108","108","108","001", // 1
            "000","000","000","000","000","000","000", // 2
            "000","000","000","000","000","000","000", // 3
            "000","494","000","171","000","494","000", // 4
            "000","000","000","000","000","000","000", // 5
            "000","000","000","000","000","979","000", // 6
            "108","108","108","108","001","001","001", // 7
        };
        dataManager.RoomDefinitions_Volcano.Add(RoomLabels.VolcanoEnd5,
            new RoomDefinition(gameManager, SceneTypes.Volcano, RoomLabels.VolcanoEnd5, volcanoEnd5, RoomOrientations.XOXO, RoomTypes.End));

        var volcanoEnd6 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","108", // 1
            "000","000","000","000","000","000","108", // 2
            "000","000","000","000","000","000","001", // 3
            "000","494","000","171","000","494","001", // 4
            "000","000","000","000","000","000","001", // 5
            "000","000","000","000","000","000","108", // 6
            "108","108","108","108","001","001","108", // 7
        };
        dataManager.RoomDefinitions_Volcano.Add(RoomLabels.VolcanoEnd6,
            new RoomDefinition(gameManager, SceneTypes.Volcano, RoomLabels.VolcanoEnd6, volcanoEnd6, RoomOrientations.OXXO, RoomTypes.End));

        var volcanoEnd7 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","001", // 1
            "000","000","000","000","000","000","001", // 2
            "000","000","000","000","000","000","001", // 3
            "000","494","000","171","000","494","108", // 4
            "000","000","000","000","000","000","108", // 5
            "000","000","000","000","000","000","001", // 6
            "000","000","000","000","000","000","108", // 7
        };
        dataManager.RoomDefinitions_Volcano.Add(RoomLabels.VolcanoEnd7,
            new RoomDefinition(gameManager, SceneTypes.Volcano, RoomLabels.VolcanoEnd7, volcanoEnd7, RoomOrientations.OXOO, RoomTypes.End));

        var volcanoEnd8 = new string[] {
          //  1     2     3     4     5     6     7   
            "108","000","000","000","000","000","000", // 1
            "108","000","000","000","000","000","000", // 2
            "108","000","494","000","000","000","000", // 3
            "001","494","000","171","000","000","000", // 4
            "108","000","494","000","000","000","000", // 5
            "108","000","000","000","000","000","000", // 6
            "108","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Volcano.Add(RoomLabels.VolcanoEnd8,
            new RoomDefinition(gameManager, SceneTypes.Volcano, RoomLabels.VolcanoEnd8, volcanoEnd8, RoomOrientations.OOOX, RoomTypes.End));

        var volcanoEnd9 = new string[] {
          //  1     2     3     4     5     6     7   
            "108","000","000","000","000","000","000", // 1
            "108","000","000","000","000","000","000", // 2
            "108","494","000","000","000","000","000", // 3
            "001","000","000","000","000","000","000", // 4
            "001","000","171","000","000","000","000", // 5
            "108","000","000","000","494","000","000", // 6
            "108","001","001","108","001","001","108", // 7
        };
        dataManager.RoomDefinitions_Volcano.Add(RoomLabels.VolcanoEnd9,
            new RoomDefinition(gameManager, SceneTypes.Volcano, RoomLabels.VolcanoEnd9, volcanoEnd9, RoomOrientations.OOXX, RoomTypes.End));

        var volcanoEnd10 = new string[] {
          //  1     2     3     4     5     6     7   
            "108","001","001","108","108","108","108", // 1
            "000","000","494","000","000","000","108", // 2
            "000","000","000","000","000","000","108", // 3
            "000","000","000","000","171","000","001", // 4
            "000","000","000","000","000","000","001", // 5
            "000","000","494","000","000","000","001", // 6
            "108","001","108","108","108","108","108", // 7
        };
        dataManager.RoomDefinitions_Volcano.Add(RoomLabels.VolcanoEnd10,
            new RoomDefinition(gameManager, SceneTypes.Volcano, RoomLabels.VolcanoEnd10, volcanoEnd10, RoomOrientations.XXXO, RoomTypes.End));

        var volcanoEnd11 = new string[] {
          //  1     2     3     4     5     6     7   
            "108","000","000","000","000","000","108", // 1
            "108","000","494","000","494","000","108", // 2
            "108","000","000","000","000","000","108", // 3
            "108","000","000","171","000","000","108", // 4
            "108","000","000","000","000","000","108", // 5
            "108","000","494","000","494","000","108", // 6
            "108","000","000","000","000","000","108", // 7
        };
        dataManager.RoomDefinitions_Volcano.Add(RoomLabels.VolcanoEnd11,
            new RoomDefinition(gameManager, SceneTypes.Volcano, RoomLabels.VolcanoEnd11, volcanoEnd11, RoomOrientations.OXOX, RoomTypes.End));

        var volcanoEnd12 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","000", // 1
            "000","000","494","000","000","000","000", // 2
            "000","000","000","000","000","000","000", // 3
            "000","000","000","171","000","000","000", // 4
            "000","000","000","000","000","000","000", // 5
            "000","000","494","000","000","000","000", // 6
            "108","108","108","108","108","108","108", // 7
        };
        dataManager.RoomDefinitions_Volcano.Add(RoomLabels.VolcanoEnd12,
            new RoomDefinition(gameManager, SceneTypes.Volcano, RoomLabels.VolcanoEnd12, volcanoEnd12, RoomOrientations.OOXO, RoomTypes.End));

        var volcanoEnd13 = new string[] {
          //  1     2     3     4     5     6     7   
            "108","000","000","000","000","000","108", // 1
            "108","000","494","000","494","000","108", // 2
            "108","000","000","000","000","000","108", // 3
            "108","000","000","171","000","000","108", // 4
            "108","237","000","000","000","237","108", // 5
            "108","000","494","000","494","000","108", // 6
            "108","108","108","108","108","108","108", // 7
        };
        dataManager.RoomDefinitions_Volcano.Add(RoomLabels.VolcanoEnd13,
            new RoomDefinition(gameManager, SceneTypes.Volcano, RoomLabels.VolcanoEnd13, volcanoEnd13, RoomOrientations.OXXX, RoomTypes.End));

        var volcanoEnd14 = new string[] {
          //  1     2     3     4     5     6     7   
            "108","108","108","108","108","108","108", // 1
            "108","000","494","000","494","000","000", // 2
            "108","237","000","000","000","000","000", // 3
            "108","000","000","171","000","000","000", // 4
            "108","237","000","000","000","000","000", // 5
            "108","000","494","000","494","000","000", // 6
            "108","108","108","108","108","108","108", // 7
        };
        dataManager.RoomDefinitions_Volcano.Add(RoomLabels.VolcanoEnd14,
            new RoomDefinition(gameManager, SceneTypes.Volcano, RoomLabels.VolcanoEnd14, volcanoEnd14, RoomOrientations.XOXX, RoomTypes.End));

        var volcanoEnd15 = new string[] {
          //  1     2     3     4     5     6     7   
            "108","108","108","108","108","108","108", // 1
            "000","000","494","000","494","000","000", // 2
            "000","000","000","000","000","000","000", // 3
            "000","000","000","171","000","000","000", // 4
            "000","000","000","000","000","000","000", // 5
            "000","000","494","000","494","000","000", // 6
            "000","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Volcano.Add(RoomLabels.VolcanoEnd15,
            new RoomDefinition(gameManager, SceneTypes.Volcano, RoomLabels.VolcanoEnd15, volcanoEnd15, RoomOrientations.XOOO, RoomTypes.End));
    }
}