using Rewired;
using System;
using System.Linq;
using UnityEngine;


public class InputManager : MonoBehaviour
{
    private EventManager eventManager;
    private DataManager dataManager;
    private PlayerMouse Mouse;
    private Player player;
    private const float repeatThreshold = 0.2f;
    private const float repeatRate = 0.15f;
    private float prevSelectMove = 0;
    private float moveNext;

    private int x = 0, y = 0;
    private bool confirm;
    private bool cancel;
    private bool running;
    private Vector2 move;
    public InputTypes LastUsedInputType { get; set; }

    public enum InputTypes { Mouse, NonMouse };

    public void Setup(IGameManager gameManager)
    {
        eventManager = gameManager.EventManager;
        dataManager = gameManager.DataManager;

        if (FindObjectOfType<Rewired.InputManager>() != null && ReInput.players != null)
        {
            player = ReInput.players.GetPlayer(0);
            Mouse = PlayerMouse.Factory.Create();
            Mouse.playerId = player.id;
        }
    }

    public void Run()
    {
        running = true;
        EventManager.RefreshLastControllerTypeUsedEvent += RefreshLastControllerTypeUsed;
    }

    public void Stop()
    {
        running = false;
        EventManager.RefreshLastControllerTypeUsedEvent -= RefreshLastControllerTypeUsed;
    }

    private void RefreshLastControllerTypeUsed(object sender, EventArgs args)
    {
        //ControllerType? lastUsed = GetLastActiveControllerType();
        //dataManager.LastUsedControllerType = lastUsed == null ? ControllerTypes.None : (ControllerTypes)lastUsed;
    }


    private void Update()
    {
        if (running)
        {
            GetInput();
        }
    }

    private void GetInput()
    {
        move.x = player.GetAxis("MoveHorizontal");
        move.y = player.GetAxis("MoveVertical");
        Move();

        confirm = player.GetButtonDown("Confirm");
        if (confirm)
        {
            eventManager.TriggerConfirmEvent(this);
        }

        cancel = player.GetButtonDown("Cancel");        
        if (cancel)
        {
            eventManager.TriggerCancelEvent(this);
        }
        
        if (player.GetButtonDown("SelectItemLeft") && Time.time > prevSelectMove + repeatThreshold)
        {
            prevSelectMove = Time.time;
            eventManager.TriggerSelectLeftEvent(this);
        }

        if (player.GetButtonDown("SelectItemRight") && Time.time > prevSelectMove + repeatThreshold)
        {
            prevSelectMove = Time.time;
            eventManager.TriggerSelectRightEvent(this);
        }
    }

    private void Move()
    {
        x = 0;
        y = 0;
        if (Mathf.Approximately(move.x, 0) && Mathf.Approximately(move.y, 0))
        {
            moveNext = 0;
        }
        else
        {
            if (Time.time > moveNext)
            {
                if (move.x < 0)
                {
                    x = -1;
                }
                else if (move.x == 0)
                {
                    x = 0;
                }
                else
                {
                    x = 1;
                }

                if (move.y < 0)
                {
                    y = -1;
                }
                else if(move.y == 0)
                {
                    y = 0;
                }
                else
                {
                    y = 1;
                }
                
                moveNext = Time.time + repeatRate;
                eventManager.TriggerMoveEvent(this, new InfoEventArgs<Point>(new Point(x, y)));
            }
        }
    }

}
