﻿using Rewired.UI.ControlMapper;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : StateMachine
{
    public ITitleMenu TitleMenu { get; set; }
    public ILoadMenu LoadMenu { get; set; }
    public ISaveMenu SaveMenu { get; set; }
    public IPauseMenu PauseMenu { get; set; }    
    public IVideoSettingsMenu VideoSettingsMenu { get; set; }
    public IAudioSettingsMenu AudioSettingsMenu { get; set; }
    public Hud Hud { get; set; }
    public DialogueWindow DialogueWindow { get; set; }
    public bool ReturnToTitleMenu { get; set; }
    public Canvas Canvas { get { return CanvasReference; } set { CanvasReference = value; } }
    public Canvas CanvasReference;
    public GameObject BlurBackgroundObject { get { return BlurBackgroundObjectReference; } set { BlurBackgroundObjectReference = value; } }
    public GameObject BlurBackgroundObjectReference;
    public GameObject ControlMapperPrefab;
    private List<GameObject> saveFileMenuItems = new List<GameObject>();
    private Image screenFadeImage = null;
    private float _currentFadeLevel = 1.0f;
    private IEnumerator _coroutine = null;

    public void Setup(IGameManager gameManager)
    {
        this.GameManager = gameManager;
        Canvas.gameObject.SetActive(true);
        TitleMenu = GetComponentInChildren<ITitleMenu>();
        TitleMenu.Setup(gameManager);
        //LoadMenu = GetComponentInChildren<ILoadMenu>();
        //LoadMenu.Setup(gameManager);
        //LoadMenu.SetActive(false);
        //SaveMenu = GetComponentInChildren<ISaveMenu>();
        //SaveMenu.Setup(gameManager);
        //SaveMenu.SetActive(false);
        PauseMenu = GetComponentInChildren<IPauseMenu>();
        PauseMenu.Setup(gameManager);
        PauseMenu.SetActive(false);
        VideoSettingsMenu = GetComponentInChildren<IVideoSettingsMenu>();
        VideoSettingsMenu.Setup(gameManager);
        VideoSettingsMenu.SetActive(false);
        AudioSettingsMenu = GetComponentInChildren<IAudioSettingsMenu>();
        AudioSettingsMenu.Setup(gameManager);
        AudioSettingsMenu.SetActive(false);
        DialogueWindow = GetComponentInChildren<DialogueWindow>();
        DialogueWindow.Setup(gameManager);
        DialogueWindow.gameObject.SetActive(false);
        Hud = GetComponentInChildren<Hud>();
        Hud.Setup(gameManager);
        Hud.gameObject.SetActive(false);

        DontDestroyOnLoad(gameObject);

        //Addressables.LoadAssetAsync<GameObject>("Assets/Prefabs/Canvas.prefab").Completed += (operation) =>
        //{
        //var canvas = Addressables.InstantiateAsync("Assets/Prefabs/Canvas.prefab").Result;
        //canvas.transform.SetParent(transform);
        //};

        if (screenFadeImage)
        {
            Color color = screenFadeImage.color;
            color.a = _currentFadeLevel;
            screenFadeImage.color = color;
        }
    }    

    public IEnumerator DelayedFadeIn(float delayTime, float fadeTime)
    {
        yield return new WaitForSeconds(delayTime);
        Fade(fadeTime, ScreenFadeType.FadeIn);
    }
    
    // Set in control mapper inspector settings.
    public void TransitionToControlMapperMenu()
    {
        ChangeState<ControlMapperState>();
    }

    public void Fade(float seconds, ScreenFadeType direction)
    {
        if (_coroutine != null)
        {
            StopCoroutine(_coroutine);
        }
        float targetFade = 0.0f;

        switch (direction)
        {
            case ScreenFadeType.FadeIn:
                targetFade = 0.0f;
                break;

            case ScreenFadeType.FadeOut:
                targetFade = 1.0f;
                break;
        }

        _coroutine = FadeInternal(seconds, targetFade);
        StartCoroutine(_coroutine);
    }

    IEnumerator FadeInternal(float seconds, float targetFade)
    {
        if (!screenFadeImage)
        {
            yield break;
        }

        float timer = 0;
        float srcFade = _currentFadeLevel;
        Color oldColor = screenFadeImage.color;
        if (seconds < 0.1f)
        {
            seconds = 0.1f;
        }

        while (timer < seconds)
        {
            timer += Time.deltaTime;
            _currentFadeLevel = Mathf.Lerp(srcFade, targetFade, timer / seconds);
            oldColor.a = _currentFadeLevel;
            screenFadeImage.color = oldColor;
            yield return null;
        }

        oldColor.a = _currentFadeLevel = targetFade;
        screenFadeImage.color = oldColor;
    }
}