﻿public static class Overworld
{
    public static void Setup(IGameManager gameManager)
    {
        var dataManager = gameManager.DataManager;
        var overworld_0_0 = new string[] {
          //  1     2     3     4     5     6     7   
            "101","000","000","000","000","000","000", // 1
            "101","000","000","000","000","101","000", // 2
            "101","000","101","000","000","000","000", // 3
            "101","000","000","000","305","000","101", // 4
            "101","000","000","101","000","000","000", // 5
            "101","000","000","000","000","000","000", // 6
            "247","247","247","247","247","247","247", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_0_0,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_0_0, overworld_0_0, RoomOrientations.OOOO));

        var overworld_7_0 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","000", // 1
            "000","000","000","000","000","000","000", // 2
            "000","000","000","000","000","000","000", // 3
            "000","000","000","000","000","000","000", // 4
            "095","000","000","000","000","095","000", // 5
            "005","006","005","095","005","005","006", // 6
            "247","247","247","247","247","247","247", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_7_0,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_7_0, overworld_7_0, RoomOrientations.OOOO));

        var overworld_8_0_skeletondungeon1entrance = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","003", // 1
            "000","607","003","003","003","000","003", // 2
            "000","003","003","003","003","607","003", // 3
            "000","003","003","-2 ","003","000","003", // 4
            "003","003","003","000","003","003","003", // 5
            "006","006","005","607","005","006","003", // 6
            "247","247","247","247","247","247","247", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_8_0,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_8_0, overworld_8_0_skeletondungeon1entrance, RoomOrientations.OOOO));

        var overworld_0_1 = new string[] {
          //  1     2     3     4     5     6     7   
            "101","101","000","000","000","101","000", // 1
            "101","000","000","000","101","101","000", // 2
            "101","1032","101","000","101","000","000", // 3
            "101","101","101","101","101","101","101", // 4
            "101","101","101","101","101","101","000", // 5
            "101","101","-6 ","101","101","000","000", // 6
            "101","101","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_0_1,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_0_1, overworld_0_1, RoomOrientations.OOOO));

        var overworld_0_2 = new string[] {
          //  1     2     3     4     5     6     7   
            "101","000","000","000","000","000","000", // 1
            "101","000","000","000","000","000","101", // 2
            "101","000","101","000","000","000","000", // 3
            "101","000","000","000","000","000","000", // 4
            "101","000","000","000","101","000","000", // 5
            "101","000","000","000","000","000","000", // 6
            "101","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_0_2,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_0_2, overworld_0_2, RoomOrientations.OOOO));

        var overworld_0_3 = new string[] {
          //  1     2     3     4     5     6     7   
            "101","000","000","000","000","000","000", // 1
            "101","003","003","003","003","003","003", // 2
            "101","003","003","003","101","003","003", // 3
            "003","003","000","101","003","000","000", // 4
            "003","003","000","000","003","000","000", // 5
            "101","101","000","000","000","000","000", // 6
            "101","000","000","100","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_0_3,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_0_3, overworld_0_3, RoomOrientations.OOOO));

        var overworld_0_4 = new string[] {
          //  1     2     3     4     5     6     7   
            "003","000","000","000","000","000","000", // 1
            "003","000","003","003","000","000","003", // 2
            "003","000","000","003","000","000","003", // 3
            "003","003","000","000","003","000","000", // 4
            "003","003","000","000","003","000","000", // 5
            "101","000","000","000","000","000","000", // 6
            "101","000","000","100","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_0_4,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_0_4, overworld_0_4, RoomOrientations.OOOO));

        var overworld_0_7 = new string[] {
          //  1     2     3     4     5     6     7   
            "003","000","000","000","000","000","000", // 1
            "003","000","000","000","000","000","000", // 2
            "003","000","000","000","000","000","000", // 3
            "003","000","000","000","000","000","000", // 4
            "003","000","000","000","053","000","000", // 5
            "003","000","000","000","000","000","000", // 6
            "003","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_0_7,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_0_7, overworld_0_7, RoomOrientations.OOOO));

        var overworld_1_7 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","003", // 1
            "000","263","000","000","000","003","003", // 2
            "000","000","000","053","000","003","000", // 3
            "000","000","000","000","003","003","000", // 4
            "000","000","000","000","000","003","000", // 5
            "000","000","000","000","000","000","000", // 6
            "000","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_1_7,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_1_7, overworld_1_7, RoomOrientations.OOOO));

        var overworld_2_7 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","000", // 1
            "000","000","000","000","000","000","000", // 2
            "000","000","000","000","000","000","000", // 3
            "000","000","000","000","003","000","000", // 4
            "000","000","000","000","000","000","000", // 5
            "000","000","000","000","000","000","000", // 6
            "000","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_2_7,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_2_7, overworld_2_7, RoomOrientations.OOOO));

        var overworld_3_7 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","003","000","000","000", // 1
            "000","000","000","097","000","000","000", // 2
            "000","003","000","000","000","000","000", // 3
            "000","097","000","000","000","000","000", // 4
            "000","000","000","000","000","000","000", // 5
            "000","000","000","000","000","000","000", // 6
            "000","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_3_7,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_3_7, overworld_3_7, RoomOrientations.OOOO));

        var overworld_4_7_BottomOfSkullRock = new string[] {
          //  1     2     3     4     5     6     7   
            "000","003","003","000","003","003","000", // 1
            "000","003","000","000","000","003","003", // 2
            "003","003","003","000","000","003","003", // 3
            "003","000","003","000","003","000","003", // 4
            "097","000","000","000","000","000","000", // 5
            "000","000","000","000","000","000","000", // 6
            "000","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_4_7,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_4_7, overworld_4_7_BottomOfSkullRock, RoomOrientations.OOOO));

        var overworld_5_7 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","003","247","247","247","005","000", // 1
            "000","003","247","247","247","095","000", // 2
            "000","006","247","247","247","006","000", // 3
            "000","005","247","247","247","005","000", // 4
            "000","005","247","247","247","005","000", // 5
            "000","005","247","247","247","005","000", // 6
            "000","095","247","247","247","005","000", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_5_7,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_5_7, overworld_5_7, RoomOrientations.OOOO));

        var overworld_6_7 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","000", // 1
            "000","000","000","000","000","000","000", // 2
            "000","672","672","000","672","672","000", // 3
            "000","-1 ","-1 ","000","-1 ","-1 ","000", // 4
            "000","672","672","000","672","672","000", // 5
            "000","-1 ","-1 ","000","-1 ","-1 ","000", // 6
            "000","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_6_7,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_6_7, overworld_6_7, RoomOrientations.OOOO));

        var overworld_7_7 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","000", // 1
            "000","000","000","099","000","000","000", // 2
            "000","000","003","003","003","000","000", // 3
            "000","099","003","-4 ","003","099","000", // 4
            "000","000","003","000","003","000","000", // 5
            "000","000","000","000","000","000","000", // 6
            "000","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_7_7,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_7_7, overworld_7_7, RoomOrientations.OOOO));

        var overworld_8_7 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","003", // 1
            "000","000","000","000","000","000","003", // 2
            "000","672","672","000","672","672","003", // 3
            "000","001","-1 ","000","-1 ","-1 ","003", // 4
            "000","672","672","000","672","672","003", // 5
            "000","-1 ","-1 ","000","-1 ","-1 ","003", // 6
            "000","000","000","000","000","000","003", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_8_7,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_8_7, overworld_8_7, RoomOrientations.OOOO));

        var overworld_0_8 = new string[] {
          //  1     2     3     4     5     6     7   
            "003","003","003","003","003","003","003", // 1
            "003","000","000","000","000","000","000", // 2
            "003","000","000","173","000","000","000", // 3
            "003","000","000","493","000","000","000", // 4
            "003","000","000","1032","000","000","000", // 5
            "003","000","053","000","000","000","000", // 6
            "003","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_0_8,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_0_8, overworld_0_8, RoomOrientations.OOOO));

        var overworld_1_8 = new string[] {
          //  1     2     3     4     5     6     7   
            "003","003","003","003","003","003","003", // 1
            "000","000","000","003","000","003","003", // 2
            "000","000","000","000","000","000","003", // 3
            "000","000","000","000","000","000","003", // 4
            "000","000","000","000","053","000","003", // 5
            "000","000","000","000","000","000","003", // 6
            "000","000","000","000","000","003","003", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_1_8,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_1_8, overworld_1_8, RoomOrientations.OOOO));

        var overworld_2_8 = new string[] {
          //  1     2     3     4     5     6     7   
            "003","003","003","003","003","003","003", // 1
            "003","000","000","000","000","000","000", // 2
            "003","000","000","003","000","000","000", // 3
            "003","000","000","097","000","000","000", // 4
            "003","000","000","000","000","000","003", // 5
            "003","000","000","000","000","000","097", // 6
            "003","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_2_8,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_2_8, overworld_2_8, RoomOrientations.OOOO));

        var overworld_3_8 = new string[] {
          //  1     2     3     4     5     6     7   
            "003","003","003","003","003","003","003", // 1
            "000","000","000","000","000","000","000", // 2
            "000","000","000","000","000","000","000", // 3
            "000","000","000","000","000","000","000", // 4
            "000","000","000","000","000","000","000", // 5
            "000","000","000","000","000","000","000", // 6
            "000","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_3_8,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_3_8, overworld_3_8, RoomOrientations.OOOO));

        var overworld_4_8_Dungeon2 = new string[] {
          //  1     2     3     4     5     6     7   
            "003","003","003","003","003","003","003", // 1
            "000","000","000","000","000","000","000", // 2
            "000","003","003","003","003","000","000", // 3
            "000","003","494","003","494","003","000", // 4
            "003","003","003","003","003","003","003", // 5
            "003","003","003","003","003","003","003", // 6
            "000","003","003","-3 ","003","003","000", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_4_8,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_4_8, overworld_4_8_Dungeon2, RoomOrientations.OOOO));

        var overworld_5_8 = new string[] {
          //  1     2     3     4     5     6     7   
            "003","003","247","247","247","003","003", // 1
            "000","005","247","247","247","005","000", // 2
            "000","095","247","247","247","005","000", // 3
            "000","006","247","247","247","006","000", // 4
            "000","005","247","247","247","005","000", // 5
            "000","005","247","247","247","095","000", // 6
            "000","005","247","247","247","005","000", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_5_8,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_5_8, overworld_5_8, RoomOrientations.OOOO));

        var overworld_6_8 = new string[] {
          //  1     2     3     4     5     6     7   
            "003","003","003","003","003","003","003", // 1
            "000","000","000","000","000","000","000", // 2
            "000","672","672","000","672","672","000", // 3
            "000","-1 ","-1 ","000","-1 ","-1 ","000", // 4
            "000","672","672","000","672","672","000", // 5
            "000","-1 ","-1 ","000","-1 ","-1 ","000", // 6
            "000","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_6_8,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_6_8, overworld_6_8, RoomOrientations.OOOO));

        var overworld_7_8 = new string[] {
          //  1     2     3     4     5     6     7   
            "003","003","003","003","003","003","003", // 1
            "000","000","000","000","000","000","000", // 2
            "000","672","672","000","672","672","000", // 3
            "000","-1 ","-1 ","000","-1 ","001","000", // 4
            "000","672","672","000","672","672","000", // 5
            "000","-1 ","-1 ","000","-1 ","-1 ","000", // 6
            "000","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_7_8,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_7_8, overworld_7_8, RoomOrientations.OOOO));

        var overworld_8_8 = new string[] {
          //  1     2     3     4     5     6     7   
            "003","003","003","003","003","003","003", // 1
            "000","000","000","269","000","1032","003", // 2
            "000","672","672","000","672","672","003", // 3
            "000","-1 ","-1 ","000","-1 ","-1 ","003", // 4
            "000","672","672","000","672","672","003", // 5
            "000","-1 ","-1 ","000","-1 ","-1 ","003", // 6
            "000","000","000","000","000","000","003", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_8_8,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_8_8, overworld_8_8, RoomOrientations.OOOO));

        var overworld_1_4 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","000", // 1
            "000","000","000","000","000","000","000", // 2
            "000","000","000","000","000","000","000", // 3
            "000","000","000","100","000","000","000", // 4
            "000","000","000","000","000","000","000", // 5
            "000","000","000","000","000","000","000", // 6
            "000","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_1_4,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_1_4, overworld_1_4, RoomOrientations.OOOO));

        var overworld_2_4 = new string[] {
          //  1     2     3     4     5     6     7   
            "003","003","003","003","003","003","000", // 1
            "000","000","000","000","000","003","003", // 2
            "047","000","000","000","000","047","003", // 3
            "047","000","000","100","000","047","003", // 4
            "000","000","000","000","000","000","003", // 5
            "000","000","047","047","000","003","003", // 6
            "000","000","000","000","003","000","000", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_2_4,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_2_4, overworld_2_4, RoomOrientations.OOOO));

        var overworld_3_4_Faerie = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","000", // 1
            "000","000","000","000","000","100","000", // 2
            "000","000","000","000","000","000","000", // 3
            "000","000","247","247","000","000","000", // 4
            "000","270","247","247","000","000","000", // 5
            "000","000","247","247","000","000","000", // 6
            "000","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_3_4,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_3_4, overworld_3_4_Faerie, RoomOrientations.OOOO));

        var overworld_1_3 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","000", // 1
            "000","000","000","000","000","000","000", // 2
            "003","003","003","003","003","003","000", // 3
            "003","003","101","000","003","003","003", // 4
            "003","101","000","000","101","003","003", // 5
            "000","000","000","101","000","000","003", // 6
            "101","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_1_3,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_1_3, overworld_1_3, RoomOrientations.OOOO));

        var overworld_1_0 = new string[] {
          //  1     2     3     4     5     6     7   
            "101","101","000","000","000","000","000", // 1
            "000","101","101","000","101","000","000", // 2
            "000","101","101","101","101","101","000", // 3
            "000","000","000","000","000","101","000", // 4
            "000","000","000","000","000","101","000", // 5
            "000","000","000","264","000","000","000", // 6
            "247","247","247","247","247","247","247", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_1_0,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_1_0, overworld_1_0, RoomOrientations.OOOO));

        var overworld_1_1 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","000", // 1
            "000","000","000","000","101","000","000", // 2
            "000","000","000","000","000","000","000", // 3
            "000","000","000","000","000","000","000", // 4
            "000","000","101","000","101","000","000", // 5
            "101","101","101","101","000","000","000", // 6
            "000","101","101","000","000","000","101", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_1_1,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_1_1, overworld_1_1, RoomOrientations.OOOO));

        var overworld_1_2 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","000", // 1
            "000","101","000","000","000","000","000", // 2
            "000","000","000","000","000","000","000", // 3
            "306","000","000","000","100","000","000", // 4
            "000","000","000","000","101","101","000", // 5
            "101","000","000","000","000","000","000", // 6
            "000","000","101","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_1_2,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_1_2, overworld_1_2, RoomOrientations.OOOO));

        var overworld_2_0 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","000", // 1
            "000","000","000","000","000","000","047", // 2
            "000","000","000","000","000","000","000", // 3
            "000","000","006","095","000","000","000", // 4
            "000","000","005","006","095","000","000", // 5
            "000","005","360","006","095","000","000", // 6
            "247","247","247","247","247","247","247", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_2_0,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_2_0, overworld_2_0, RoomOrientations.OOOO));

        var overworld_2_1 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","047", // 1
            "000","000","000","000","000","000","000", // 2
            "000","000","000","047","000","000","000", // 3
            "000","000","000","000","000","000","000", // 4
            "000","000","000","000","000","000","000", // 5
            "000","000","000","000","000","000","000", // 6
            "000","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_2_1,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_2_1, overworld_2_1, RoomOrientations.OOOO));

        var overworld_2_2 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","000", // 1
            "000","000","000","000","000","000","000", // 2
            "000","000","000","000","000","000","000", // 3
            "000","000","100","000","000","000","000", // 4
            "000","000","000","000","000","000","000", // 5
            "000","000","000","000","000","000","000", // 6
            "000","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_2_2,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_2_2, overworld_2_2, RoomOrientations.OOOO));

        var overworld_2_3 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","000", // 1
            "000","000","000","000","000","000","000", // 2
            "000","000","000","000","000","000","000", // 3
            "003","000","000","000","000","000","000", // 4
            "003","003","000","000","000","003","003", // 5
            "003","003","003","003","003","003","003", // 6
            "100","101","000","101","003","000","101", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_2_3,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_2_3, overworld_2_3, RoomOrientations.OOOO));

        var overworld_3_0 = new string[] {
          //  1     2     3     4     5     6     7   
            "047","000","000","000","000","000","047", // 1
            "000","000","000","000","047","000","047", // 2
            "000","000","000","000","000","000","000", // 3
            "000","000","000","000","000","000","000", // 4
            "000","000","047","000","000","000","000", // 5
            "095","095","095","095","095","095","095", // 6
            "247","247","247","247","247","247","247", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_3_0,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_3_0, overworld_3_0, RoomOrientations.OOOO));

        var overworld_3_1 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","000", // 1
            "000","000","000","000","047","000","000", // 2
            "000","000","000","000","000","000","000", // 3
            "000","000","000","000","000","000","000", // 4
            "000","000","047","000","000","000","000", // 5
            "000","000","000","000","000","000","000", // 6
            "000","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_3_1,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_3_1, overworld_3_1, RoomOrientations.OOOO));

        var overworld_7_1 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","000", // 1
            "000","000","000","000","000","000","000", // 2
            "000","000","000","000","000","000","000", // 3
            "000","000","000","000","000","000","000", // 4
            "000","000","047","000","047","000","047", // 5
            "000","000","000","000","000","000","000", // 6
            "000","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_7_1,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_7_1, overworld_7_1, RoomOrientations.OOOO));

        var overworld_8_1 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","003", // 1
            "000","000","000","000","000","000","003", // 2
            "000","000","000","000","000","000","003", // 3
            "000","000","000","000","000","000","003", // 4
            "000","000","047","000","047","000","003", // 5
            "000","000","000","000","000","000","003", // 6
            "000","000","000","000","000","000","003", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_8_1,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_8_1, overworld_8_1, RoomOrientations.OOOO));

        var overworld_3_2 = new string[] {
          //  1     2     3     4     5     6     7   
            "003","003","003","003","003","003","003", // 1
            "000","000","000","000","000","000","003", // 2
            "000","000","000","000","000","000","000", // 3
            "000","000","000","000","000","000","000", // 4
            "000","000","000","000","000","000","000", // 5
            "000","000","000","000","000","000","000", // 6
            "000","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_3_2,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_3_2, overworld_3_2, RoomOrientations.OOOO));

        var overworld_3_3 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","000", // 1
            "000","000","000","000","047","000","000", // 2
            "000","000","000","000","000","000","000", // 3
            "003","003","003","003","003","003","003", // 4
            "003","003","003","003","003","003","003", // 5
            "003","003","003","003","003","003","003", // 6
            "000","003","003","003","003","003","003", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_3_3,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_3_3, overworld_3_3, RoomOrientations.OOOO));

        var overworld_4_1_StartMeadow = new string[] {
          //  1     2     3     4     5     6     7   
            "047","000","000", "000","000","000","000", // 1
            "000","000","000", "000","215","000","000", // 2
            "000","000","000", "000","000","000","000", // 3
            "000","000","955", "000","000","000","000", // 4
            "000","898","1026","901","000","000","000", // 5
            "047","000","951", "000","000 ","000","047", // 6
            "000","095","000", "000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_4_1,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_4_1, overworld_4_1_StartMeadow, RoomOrientations.OOOO));

        var overworld_4_2_MonkCave = new string[] {
          //  1     2     3     4     5     6     7   
            "003","003","003","003","000","003","003", // 1
            "003","-5 ","003","000","000","000","047", // 2
            "047","000","095","000","000","000","047", // 3
            "000","000","000","000","095","000","000", // 4
            "000","000","000","000","000","000","000", // 5
            "047","000","095","000","000","000","047", // 6
            "047","095","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_4_2,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_4_2, overworld_4_2_MonkCave, RoomOrientations.OOOO));

        var overworld_4_3 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","003", // 1
            "003","000","003","000","000","000","047", // 2
            "000","000","095","000","000","003","000", // 3
            "000","000","047","000","095","095","000", // 4
            "000","000","000","000","000","000","000", // 5
            "003","000","095","000","000","095","047", // 6
            "003","003","003","003","000","003","003", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_4_3,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_4_3, overworld_4_3, RoomOrientations.OOOO));

        var overworld_4_4_signpost = new string[] {
          //  1     2     3     4     5     6     7   
            "003","003","003","000","000","000","000", // 1
            "003","003","003","000","000","000","047", // 2
            "000","000","047","000","000","000","000", // 3
            "000","000","047","000","000","217","000", // 4
            "000","000","000","351","000","000","000", // 5
            "047","000","095","000","000","095","047", // 6
            "000","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_4_4,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_4_4, overworld_4_4_signpost, RoomOrientations.OOOO));

        var overworld_4_0 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","095","000","000","000","047", // 1
            "000","000","000","000","000","000","047", // 2
            "000","000","095","000","000","000","000", // 3
            "000","000","000","047","000","095","000", // 4
            "000","047","000","000","000","000","000", // 5
            "000","000","000","000","047","095","000", // 6
            "247","247","247","247","247","247","247", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_4_0,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_4_0, overworld_4_0, RoomOrientations.OOOO));

        var overworld_5_0 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","005","247","247","247","006","000", // 1
            "000","005","247","247","247","006","047", // 2
            "095","006","247","247","247","247","247", // 3
            "000","005","247","247","247","005","005", // 4
            "000","006","247","247","247","005","005", // 5
            "006","006","247","247","247","1032","005", // 6
            "247","247","247","247","247","247","247", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_5_0,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_5_0, overworld_5_0, RoomOrientations.OOOO));

        var overworld_5_1 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","005","247","247","247","005","000", // 1
            "000","095","247","247","247","005","000", // 2
            "047","006","247","247","247","005","047", // 3
            "000","005","247","247","247","005","095", // 4
            "000","005","247","247","247","005","000", // 5
            "000","005","247","247","247","006","000", // 6
            "000","005","247","247","247","005","000", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_5_1,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_5_1, overworld_5_1, RoomOrientations.OOOO));

        var overworld_5_2 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","006","247","247","247","145","145", // 1
            "000","005","247","247","247","095","000", // 2
            "047","095","247","247","247","006","047", // 3
            "000","006","247","247","247","005","000", // 4
            "000","005","247","247","247","005","000", // 5
            "047","005","247","247","247","005","047", // 6
            "000","005","247","247","247","006","000", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_5_2,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_5_2, overworld_5_2, RoomOrientations.OOOO));

        var overworld_5_3 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","005","247","247","247","726","726", // 1
            "000","095","247","247","247","095","000", // 2
            "047","006","247","247","247","006","047", // 3
            "000","005","247","247","247","005","000", // 4
            "095","005","247","247","247","005","000", // 5
            "003","003","247","247","247","005","000", // 6
            "003","003","247","247","247","006","000", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_5_3,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_5_3, overworld_5_3, RoomOrientations.OOOO));

        var overworld_5_4_Bridge = new string[] {
          //  1     2     3     4     5     6     7   
            "000","005","247","247","247","726","726", // 1
            "006","005","247","247","247","006","000", // 2
            "000","684","684","684","684","684","000", // 3
            "000","684","684","684","684","684","000", // 4
            "000","684","684","684","684","684","000", // 5
            "006","005","247","247","247","005","000", // 6
            "095","006","247","247","247","726","726", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_5_4,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_5_4, overworld_5_4_Bridge, RoomOrientations.OOOO));

        var overworld_6_0 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","047", // 1
            "000","000","000","000","047","000","047", // 2
            "247","000","216","000","000","000","000", // 3
            "247","220","493","000","000","000","000", // 4
            "247","095","000","095","979","095","000", // 5
            "247","005","006","005","005","005","005", // 6
            "247","247","247","247","247","247","247", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_6_0,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_6_0, overworld_6_0, RoomOrientations.OOOO));

        var overworld_6_1 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","000", // 1
            "000","000","000","000","000","000","000", // 2
            "000","000","000","000","000","000","000", // 3
            "000","000","000","047","000","000","000", // 4
            "000","000","000","000","000","000","000", // 5
            "000","000","000","000","000","000","000", // 6
            "000","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_6_1,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_6_1, overworld_6_1, RoomOrientations.OOOO));

        var overworld_6_2_Dog = new string[] {
          //  1     2     3     4     5     6     7   
            "145","145","145","000","145","145","726", // 1
            "000","000","000","000","000","000","000", // 2
            "000","000","000","000","000","000","000", // 3
            "366","000","000","047","000","000","000", // 4
            "000","000","000","000","000","000","000", // 5
            "000","000","000","000","000","000","000", // 6
            "000","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_6_2,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_6_2, overworld_6_2_Dog, RoomOrientations.OOOO));

        var overworld_7_2 = new string[] {
          //  1     2     3     4     5     6     7   
            "726","726","726","726","726","726","726", // 1
            "000","000","000","000","000","000","000", // 2
            "000","000","000","000","000","000","000", // 3
            "000","000","000","047","000","000","000", // 4
            "000","000","000","000","000","000","000", // 5
            "000","000","000","000","000","000","000", // 6
            "000","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_7_2,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_7_2, overworld_7_2, RoomOrientations.OOOO));

        var overworld_8_2_TownBottomRightEntryway = new string[] {
          //  1     2     3     4     5     6     7   
            "726","726","726","000","726","726","726", // 1
            "000","000","000","000","000","000","003", // 2
            "000","000","000","000","000","000","003", // 3
            "000","047","000","000","000","047","003", // 4
            "000","000","000","000","000","000","003", // 5
            "000","047","000","000","000","047","003", // 6
            "000","000","000","000","000","000","003", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_8_2,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_8_2, overworld_8_2_TownBottomRightEntryway, RoomOrientations.OOOO));

        var overworld_6_3_cattlepen = new string[] {
          //  1     2     3     4     5     6     7   
            "726","726","726","726","726","726","726", // 1
            "005","005","005","006","005","006","726", // 2
            "005","362","005","006","005","006","726", // 3
            "005","005","005","218","006","005","726", // 4
            "005","362","005","005","006","005","726", // 5
            "000","005","005","000","000","006","726", // 6
            "000","876","000","000","000","876","726", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_6_3,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_6_3, overworld_6_3_cattlepen, RoomOrientations.OOOO));

        var overworld_7_3_TownBottomRight = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","000", // 1
            "000","000","000","000","000","000","000", // 2
            "000","000","000","000","000","000","000", // 3
            "000","000","000","048","000","000","000", // 4
            "000","000","000","000","000","000","000", // 5
            "363","000","000","000","000","000","000", // 6
            "726","726","726","726","726","726","726", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_7_3,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_7_3, overworld_7_3_TownBottomRight, RoomOrientations.OOOO));

        var overworld_8_3 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","726", // 1
            "000","000","000","000","000","000","726", // 2
            "000","000","000","000","000","000","726", // 3
            "000","000","000","047","000","000","726", // 4
            "000","000","000","000","000","000","726", // 5
            "000","000","000","000","000","000","726", // 6
            "726","726","726","000","726","726","726", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_8_3,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_8_3, overworld_8_3, RoomOrientations.OOOO));

        var overworld_6_4_TownBottomLeft = new string[] {
          //  1     2     3     4     5     6     7   
            "726","000","000","000","000","000","000", // 1
            "000","000","000","000","000","000","000", // 2
            "000","000","000","000","000","000","000", // 3
            "000","000","000","000","000","000","000", // 4
            "000","393","000","000","000","000","000", // 5
            "000","000","000","000","000","000","000", // 6
            "726","726","726","726","726","726","726", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_6_4,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_6_4, overworld_6_4_TownBottomLeft, RoomOrientations.OOOO));

        var overworld_7_4_bottomofshop = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","629","629","629","000","000", // 1
            "000","000","629","-8 ","629","000","000", // 2
            "000","000","000","000","000","000","000", // 3
            "000","000","000","000","000","000","000", // 4
            "000","300","300","000","300","300","000", // 5
            "000","302","302","000","302","302","000", // 6
            "000","303","303","000","303","303","000", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_7_4,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_7_4, overworld_7_4_bottomofshop, RoomOrientations.OOOO));

        var overworld_8_4_bottom_of_tallhouse = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","629","629","629","000","726", // 1
            "000","000","629","-7 ","629","000","726", // 2
            "000","000","000","000","000","000","726", // 3
            "000","000","000","000","000","000","726", // 4
            "189","299","299","000","299","299","726", // 5
            "000","299","299","000","299","299","726", // 6
            "000","299","299","000","299","299","726", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_8_4,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_8_4, overworld_8_4_bottom_of_tallhouse, RoomOrientations.OOOO));

        var overworld_0_5_NewBombable = new string[] {
          //  1     2     3     4     5     6     7   
            "003","000","003","000","003","003","000", // 1
            "003","003","003","000","003","003","003", // 2
            "003","003","003","001","003","000","003", // 3
            "003","000","001","000","001","000","000", // 4
            "003","000","000","000","000","000","000", // 5
            "003","000","312","000","000","000","000", // 6
            "003","003","000","003","000","000","003", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_0_5,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_0_5, overworld_0_5_NewBombable, RoomOrientations.OOOO));

        var overworld_1_5 = new string[] {
          //  1     2     3     4     5     6     7   
            "003","003","003","003","003","002","003", // 1
            "003","003","002","002","000","000","000", // 2
            "002","003","000","000","000","000","000", // 3
            "003","000","000","003","000","000","000", // 4
            "000","000","003","003","000","000","000", // 5
            "000","000","000","000","000","000","000", // 6
            "000","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_1_5,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_1_5, overworld_1_5, RoomOrientations.OOOO));

        var overworld_2_5 = new string[] {
          //  1     2     3     4     5     6     7   
            "003","000","000","000","003","003","003", // 1
            "003","003","003","003","003","003","003", // 2
            "000","003","000","000","000","000","000", // 3
            "000","000","000","000","000","000","000", // 4
            "000","051","000","000","000","000","000", // 5
            "000","000","000","000","000","000","000", // 6
            "000","000","000","003","003","003","000", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_2_5,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_2_5, overworld_2_5, RoomOrientations.OOOO));

        var overworld_3_5 = new string[] {
          //  1     2     3     4     5     6     7   
            "003","003","003","003","003","003","003", // 1
            "003","003","003","003","003","003","000", // 2
            "000","003","000","000","000","000","000", // 3
            "000","000","000","047","000","000","000", // 4
            "000","000","000","000","000","000","000", // 5
            "000","000","000","000","000","000","000", // 6
            "000","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_3_5,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_3_5, overworld_3_5, RoomOrientations.OOOO));

        var overworld_4_5_Bombable = new string[] {
          //  1     2     3     4     5     6     7   
            "003","003","000","000","097","000","095", // 1
            "003","097","003","003","003","003","003", // 2
            "003","003","003","003","003","003","003", // 3
            "000","000","000","000","003","003","000", // 4
            "051","000","000","000","000","000","000", // 5
            "000","047","000","000","000","000","000", // 6
            "000","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_4_5,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_4_5, overworld_4_5_Bombable, RoomOrientations.OOOO));

        var overworld_5_5_AboveBridge = new string[] {
          //  1     2     3     4     5     6     7   
            "1032","095","247","247","247","006","000", // 1
            "003","003","247","247","247","005","000", // 2
            "003","003","247","247","247","005","000", // 3
            "000","006","247","247","247","005","000", // 4
            "000","005","247","247","247","006","364", // 5
            "005","005","247","247","247","005","1032", // 6
            "000","005","247","247","247","726","726", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_5_5,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_5_5, overworld_5_5_AboveBridge, RoomOrientations.OOOO));

        var overworld_6_5_TownTopLeft = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","000", // 1
            "726","726","726","726","726","726","726", // 2
            "726","726","726","726","726","726","726", // 3
            "726","094","000","047","000","000","000", // 4
            "726","000","299","299","299","000","000", // 5
            "726","000","299","299","299","000","000", // 6
            "726","000","299","299","299","000","000", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_6_5,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_6_5, overworld_6_5_TownTopLeft, RoomOrientations.OOOO));

        var overworld_7_5_topofshop = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","000", // 1
            "726","726","726","726","726","726","726", // 2
            "726","726","726","726","726","726","726", // 3
            "047","550","000","000","000","000","000", // 4
            "000","000","000","678","000","000","000", // 5
            "000","000","534","534","534","000","000", // 6
            "000","000","629","629","629","000","000", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_7_5,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_7_5, overworld_7_5_topofshop, RoomOrientations.OOOO));

        var overworld_8_5_TownTopRight = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","003", // 1
            "726","726","726","726","726","068","726", // 2
            "726","726","726","726","726","068","726", // 3
            "000","000","000","000","222","000","726", // 4
            "000","000","580","533","582","1032","726", // 5
            "000","000","629","629","629","118","726", // 6
            "000","000","629","629","629","118","726", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_8_5,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_8_5, overworld_8_5_TownTopRight, RoomOrientations.OOOO));

        var overworld_0_6 = new string[] {
          //  1     2     3     4     5     6     7   
            "003","000","000","000","000","000","000", // 1
            "003","000","000","000","000","000","000", // 2
            "003","000","000","000","000","000","000", // 3
            "003","053","000","000","000","000","000", // 4
            "003","003","000","000","053","000","000", // 5
            "003","000","000","000","000","000","000", // 6
            "003","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_0_6,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_0_6, overworld_0_6, RoomOrientations.OOOO));

        var overworld_1_6 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","003", // 1
            "000","000","000","000","000","000","003", // 2
            "000","000","000","000","000","000","003", // 3
            "000","000","000","000","000","003","003", // 4
            "263","000","000","000","054","003","003", // 5
            "000","000","000","000","000","003","000", // 6
            "000","000","000","000","000","003","000", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_1_6,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_1_6, overworld_1_6, RoomOrientations.OOOO));

        var overworld_2_6 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","000", // 1
            "000","000","000","000","000","000","000", // 2
            "000","000","000","000","000","000","000", // 3
            "000","000","000","000","000","000","000", // 4
            "000","000","000","000","000","000","000", // 5
            "000","000","000","000","000","000","000", // 6
            "000","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_2_6,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_2_6, overworld_2_6, RoomOrientations.OOOO));

        var overworld_3_6 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","000", // 1
            "000","000","000","000","000","000","000", // 2
            "000","000","000","000","000","000","000", // 3
            "000","000","000","000","000","000","000", // 4
            "000","000","000","000","000","000","000", // 5
            "000","000","000","000","000","000","000", // 6
            "000","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_3_6,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_3_6, overworld_3_6, RoomOrientations.OOOO));

        var overworld_4_6 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","000", // 1
            "000","000","000","000","000","000","000", // 2
            "000","000","000","000","000","000","000", // 3
            "000","000","003","000","000","000","000", // 4
            "000","000","097","000","000","000","000", // 5
            "047","000","000","000","000","003","000", // 6
            "000","000","000","000","003","097","095", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_4_6,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_4_6, overworld_4_6, RoomOrientations.OOOO));

        var overworld_5_6 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","095","247","247","247","005","000", // 1
            "000","005","247","247","247","095","000", // 2
            "000","095","247","247","247","006","000", // 3
            "000","006","247","247","247","005","000", // 4
            "000","005","247","247","247","005","000", // 5
            "000","005","247","247","247","005","000", // 6
            "000","005","247","247","247","005","000", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_5_6,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_5_6, overworld_5_6, RoomOrientations.OOOO));

        var overworld_6_6_CemetarySW = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","000", // 1
            "000","000","000","000","000","000","000", // 2
            "000","672","672","000","672","672","000", // 3
            "000","001","-1 ","000","-1 ","-1 ","000", // 4
            "000","672","672","000","672","672","000", // 5
            "000","-1 ","-1 ","000","-1 ","-1 ","000", // 6
            "000","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_6_6,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_6_6, overworld_6_6_CemetarySW, RoomOrientations.OOOO));

        var overworld_7_6 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","000", // 1
            "000","000","000","000","000","000","000", // 2
            "000","672","672","000","672","672","000", // 3
            "000","001","-1 ","000","-1 ","-1 ","000", // 4
            "000","672","672","000","672","672","000", // 5
            "000","-1 ","-1 ","000","-1 ","-1 ","000", // 6
            "000","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_7_6,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_7_6, overworld_7_6, RoomOrientations.OOOO));

        var overworld_8_6 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","003", // 1
            "000","000","000","000","000","000","003", // 2
            "000","672","672","000","672","672","003", // 3
            "000","-1 ","-1 ","000","-1 ","-1 ","003", // 4
            "000","672","672","000","672","672","003", // 5
            "000","-1 ","-1 ","000","001","-1 ","003", // 6
            "000","000","000","000","000","000","003", // 7
        };
        dataManager.RoomDefinitions_Overworld.Add(RoomLabels.Overworld_8_6,
            new RoomDefinition(gameManager, SceneTypes.Overworld, RoomLabels.Overworld_8_6, overworld_8_6, RoomOrientations.OOOO));
    }
}