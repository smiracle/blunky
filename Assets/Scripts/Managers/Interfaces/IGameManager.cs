﻿using UnityEngine;
using UnityEngine.Tilemaps;

public interface IGameManager
{
    AudioManager AudioManager { get; set; }
    DataManager DataManager { get; set; }
    EventManager EventManager { get; set; }
    InputManager InputManager { get; set; }
    SaveFileManager SaveFileManager { get; set; }
    UIManager UIManager { get; set; }
    PlayerCharacter Player { get; set; }
    Vector3Int PlayerStartPos { get; set; }
    Camera Camera { get; set; }
    public GameObject MapGo { get; set; }
    public Tilemap GroundMap { get; set; }
    public Tilemap CollisionMap { get; set; }

    public GameObject DamageTextPrefab { get;  }
    public GameObject BombExplosionPrefab { get; }
    public GameObject GameOverScreenPrefab { get; }


    public void Transition(Scenes scene, Vector3Int transitionTo);
    public void PlayMusic(MusicTracks track);
    public void PlaySound(AudioClipBanks audioClipBank);

    void LoadScene(Scenes scene);
    void LoadTitleScene();
    void Setup();
    public void StartNewGame(Vector3Int playerStartPos);
}