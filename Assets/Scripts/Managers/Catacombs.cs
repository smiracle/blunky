﻿public static class Catacombs
{
    public static void Setup(IGameManager gameManager)
    {
        var dataManager = gameManager.DataManager;
        var catacombsStart1 = new string[] {
          //  1     2     3     4     5     6     7   
            "001","001","672","000","672","672","672", // 1
            "672","000","000","000","000","000","672", // 2
            "672","000","000","000","000","000","001", // 3
            "000","000","000","289","000","000","000", // 4
            "672","000","979","000","000","000","672", // 5
            "672","000","000","000","000","000","672", // 6
            "001","001","672","000","672","672","672", // 7
        };
        dataManager.RoomDefinitions_Catacombs.Add(RoomLabels.CatacombsStart1,
            new RoomDefinition(gameManager, SceneTypes.Catacombs, RoomLabels.CatacombsStart1, catacombsStart1, RoomOrientations.OOOO, RoomTypes.Start));

        var catacombsStart2 = new string[] {
          //  1     2     3     4     5     6     7   
            "672","672","672","000","672","672","672", // 1
            "672","000","000","000","000","000","672", // 2
            "672","000","000","000","000","000","672", // 3
            "000","000","000","289","000","979","000", // 4
            "001","000","000","000","000","000","001", // 5
            "672","000","000","000","000","000","672", // 6
            "672","672","672","000","672","672","672", // 7
        };
        dataManager.RoomDefinitions_Catacombs.Add(RoomLabels.CatacombsStart2,
            new RoomDefinition(gameManager, SceneTypes.Catacombs, RoomLabels.CatacombsStart2, catacombsStart2, RoomOrientations.XXOX, RoomTypes.Start));

        var catacombsStart3 = new string[] {
          //  1     2     3     4     5     6     7   
            "672","000","000","000","000","000","672", // 1
            "672","000","000","000","000","000","672", // 2
            "672","000","000","000","000","000","672", // 3
            "000","000","979","000","000","000","672", // 4
            "672","000","000","000","000","000","672", // 5
            "672","000","000","289","000","000","672", // 6
            "001","672","672","672","672","672","672", // 7
        };
        dataManager.RoomDefinitions_Catacombs.Add(RoomLabels.CatacombsStart3,
            new RoomDefinition(gameManager, SceneTypes.Catacombs, RoomLabels.CatacombsStart3, catacombsStart3, RoomOrientations.OXXX, RoomTypes.Start));

        var catacombsStart4 = new string[] {
          //  1     2     3     4     5     6     7   
            "672","672","672","672","672","672","672", // 1
            "000","000","000","000","000","000","000", // 2
            "000","000","000","289","000","000","000", // 3
            "000","000","000","000","000","979","000", // 4
            "000","000","000","000","000","000","000", // 5
            "000","000","000","000","000","000","000", // 6
            "000","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Catacombs.Add(RoomLabels.CatacombsStart4,
            new RoomDefinition(gameManager, SceneTypes.Catacombs, RoomLabels.CatacombsStart4, catacombsStart4, RoomOrientations.XOOO, RoomTypes.Start));

        var catacombsStart5 = new string[] {
          //  1     2     3     4     5     6     7   
            "672","672","672","672","672","001","001", // 1
            "000","000","000","000","000","000","672", // 2
            "000","000","979","000","000","289","672", // 3
            "000","000","000","000","000","000","672", // 4
            "000","000","000","000","000","000","672", // 5
            "000","000","000","000","000","000","672", // 6
            "672","672","672","672","672","672","672", // 7
        };
        dataManager.RoomDefinitions_Catacombs.Add(RoomLabels.CatacombsStart5,
            new RoomDefinition(gameManager, SceneTypes.Catacombs, RoomLabels.CatacombsStart5, catacombsStart5, RoomOrientations.XXXO, RoomTypes.Start));

        var catacombsStart6 = new string[] {
          //  1     2     3     4     5     6     7   
            "672","672","672","672","672","001","001", // 1
            "000","000","000","000","000","289","672", // 2
            "000","000","000","000","000","000","672", // 3
            "000","000","000","000","000","979","672", // 4
            "000","000","607","000","000","000","672", // 5
            "000","000","000","000","000","000","672", // 6
            "000","000","000","000","000","000","672", // 7
        };
        dataManager.RoomDefinitions_Catacombs.Add(RoomLabels.CatacombsStart6,
            new RoomDefinition(gameManager, SceneTypes.Catacombs, RoomLabels.CatacombsStart6, catacombsStart6, RoomOrientations.XXOO, RoomTypes.Start));

        var catacombsStart7 = new string[] {
          //  1     2     3     4     5     6     7   
            "672","000","000","000","000","000","000", // 1
            "672","000","000","000","000","000","000", // 2
            "001","000","289","000","000","000","000", // 3
            "001","979","000","000","000","000","000", // 4
            "001","000","000","000","000","000","000", // 5
            "672","000","000","000","000","000","000", // 6
            "672","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Catacombs.Add(RoomLabels.CatacombsStart7,
            new RoomDefinition(gameManager, SceneTypes.Catacombs, RoomLabels.CatacombsStart7, catacombsStart7, RoomOrientations.OOOX, RoomTypes.Start));

        var catacombsStart8 = new string[] {
          //  1     2     3     4     5     6     7   
            "672","672","672","672","672","672","672", // 1
            "672","289","000","000","000","000","000", // 2
            "672","000","000","000","000","000","000", // 3
            "672","000","000","000","000","000","000", // 4
            "672","000","000","000","000","000","979", // 5
            "672","000","000","000","000","000","000", // 6
            "672","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Catacombs.Add(RoomLabels.CatacombsStart8,
            new RoomDefinition(gameManager, SceneTypes.Catacombs, RoomLabels.CatacombsStart8, catacombsStart8, RoomOrientations.XOOX, RoomTypes.Start));

        var catacombsStart9 = new string[] {
          //  1     2     3     4     5     6     7   
            "672","672","672","672","672","672","672", // 1
            "000","000","000","000","000","000","000", // 2
            "000","000","000","000","000","000","000", // 3
            "000","000","000","289","000","000","000", // 4
            "979","000","000","000","000","000","000", // 5
            "000","000","000","000","000","000","000", // 6
            "672","672","672","672","672","672","001", // 7
        };
        dataManager.RoomDefinitions_Catacombs.Add(RoomLabels.CatacombsStart9,
            new RoomDefinition(gameManager, SceneTypes.Catacombs, RoomLabels.CatacombsStart9, catacombsStart9, RoomOrientations.XOXO, RoomTypes.Start));

        var catacombsStart10 = new string[] {
          //  1     2     3     4     5     6     7   
            "001","001","672","672","672","672","672", // 1
            "672","000","000","000","000","000","979", // 2
            "672","000","000","000","000","000","000", // 3
            "672","000","000","289","000","607","000", // 4
            "672","000","000","000","000","000","000", // 5
            "672","000","000","000","000","000","000", // 6
            "672","672","672","672","672","672","672", // 7
        };
        dataManager.RoomDefinitions_Catacombs.Add(RoomLabels.CatacombsStart10,
            new RoomDefinition(gameManager, SceneTypes.Catacombs, RoomLabels.CatacombsStart10, catacombsStart10, RoomOrientations.XOXX, RoomTypes.Start));

        var catacombsStart11 = new string[] {
          //  1     2     3     4     5     6     7   
            "001","000","000","000","000","000","001", // 1
            "672","000","000","000","000","000","001", // 2
            "672","000","000","000","000","000","672", // 3
            "672","000","000","289","000","000","672", // 4
            "672","000","000","000","000","000","672", // 5
            "672","000","000","000","000","000","672", // 6
            "672","000","979","000","000","000","672", // 7
        };
        dataManager.RoomDefinitions_Catacombs.Add(RoomLabels.CatacombsStart11,
            new RoomDefinition(gameManager, SceneTypes.Catacombs, RoomLabels.CatacombsStart11, catacombsStart11, RoomOrientations.OXOX, RoomTypes.Start));

        var catacombsStart12 = new string[] {
          //  1     2     3     4     5     6     7   
            "672","000","000","000","000","000","000", // 1
            "672","000","000","000","000","000","000", // 2
            "672","000","000","000","000","000","000", // 3
            "672","000","000","289","000","000","000", // 4
            "672","000","000","000","000","000","000", // 5
            "672","000","000","000","000","979","000", // 6
            "672","672","672","672","672","672","672", // 7
        };
        dataManager.RoomDefinitions_Catacombs.Add(RoomLabels.CatacombsStart12,
            new RoomDefinition(gameManager, SceneTypes.Catacombs, RoomLabels.CatacombsStart12, catacombsStart12, RoomOrientations.OOXX, RoomTypes.Start));

        var catacombsStart13 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","672", // 1
            "000","979","000","000","000","000","672", // 2
            "000","000","607","000","000","000","001", // 3
            "000","1032","000","289","000","000","001", // 4
            "000","000","000","000","000","000","672", // 5
            "000","000","000","000","000","000","672", // 6
            "000","000","000","000","000","000","672", // 7
        };
        dataManager.RoomDefinitions_Catacombs.Add(RoomLabels.CatacombsStart13,
            new RoomDefinition(gameManager, SceneTypes.Catacombs, RoomLabels.CatacombsStart13, catacombsStart13, RoomOrientations.OXOO, RoomTypes.Start));

        var catacombsStart14 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","000", // 1
            "000","000","000","000","000","000","000", // 2
            "000","000","000","000","000","000","000", // 3
            "000","000","000","289","000","000","000", // 4
            "000","000","000","000","000","000","000", // 5
            "000","000","000","000","000","000","979", // 6
            "672","609","609","609","672","672","672", // 7
        };
        dataManager.RoomDefinitions_Catacombs.Add(RoomLabels.CatacombsStart14,
            new RoomDefinition(gameManager, SceneTypes.Catacombs, RoomLabels.CatacombsStart14, catacombsStart14, RoomOrientations.OOXO, RoomTypes.Start));

        var catacombsStart15 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","672", // 1
            "000","979","000","000","000","000","672", // 2
            "000","000","000","000","000","000","672", // 3
            "000","1032","000","289","000","000","672", // 4
            "000","000","000","000","000","000","672", // 5
            "000","000","000","000","000","000","672", // 6
            "672","609","609","609","609","672","672", // 7
        };
        dataManager.RoomDefinitions_Catacombs.Add(RoomLabels.CatacombsStart15,
            new RoomDefinition(gameManager, SceneTypes.Catacombs, RoomLabels.CatacombsStart15, catacombsStart15, RoomOrientations.OXXO, RoomTypes.Start));

        var catacombs1_ghosts = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","000", // 1
            "000","000","000","979","000","000","000", // 2
            "000","000","000","000","000","000","000", // 3
            "000","000","000","314","000","000","000", // 4
            "000","000","000","000","000","000","000", // 5
            "000","607","000","000","000","000","000", // 6
            "000","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Catacombs.Add(RoomLabels.CatacombsGhosts1,
            new RoomDefinition(gameManager, SceneTypes.Catacombs, RoomLabels.CatacombsGhosts1, catacombs1_ghosts, RoomOrientations.OOOO));

        var catacombs1_empty = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","000", // 1
            "000","232","000","979","000","232","000", // 2
            "000","232","000","000","000","232","000", // 3
            "000","232","723","723","723","232","000", // 4
            "000","232","000","1032","000","232","000", // 5
            "000","232","000","232","000","232","000", // 6
            "000","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Catacombs.Add(RoomLabels.CatacombsEmpty1,
            new RoomDefinition(gameManager, SceneTypes.Catacombs, RoomLabels.CatacombsEmpty1, catacombs1_empty, RoomOrientations.OOOO));

        var catacombs1_medghost = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","000", // 1
            "000","000","000","213","000","000","000", // 2
            "000","000","000","000","000","000","000", // 3
            "000","000","000","314","000","979","000", // 4
            "000","000","000","000","000","000","000", // 5
            "000","000","000","213","000","000","000", // 6
            "000","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Catacombs.Add(RoomLabels.CatacombsMedGhost1,
            new RoomDefinition(gameManager, SceneTypes.Catacombs, RoomLabels.CatacombsMedGhost1, catacombs1_medghost, RoomOrientations.OOOO));

        var catacombs1_spiders = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","000", // 1
            "000","000","000","564","000","000","000", // 2
            "000","000","000","000","564","000","000", // 3
            "000","000","000","269","000","000","000", // 4
            "000","979","000","000","564","000","000", // 5
            "000","000","000","564","000","000","000", // 6
            "000","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Catacombs.Add(RoomLabels.CatacombsSpiders1,
            new RoomDefinition(gameManager, SceneTypes.Catacombs, RoomLabels.CatacombsSpiders1, catacombs1_spiders, RoomOrientations.OOOO));

        var catacombs2_ghosts = new string[] {
          //  1     2     3     4     5     6     7   
            "001","000","000","000","000","000","001", // 1
            "001","000","000","000","000","000","001", // 2
            "672","000","000","000","000","000","672", // 3
            "672","314","000","979","000","314","672", // 4
            "001","000","000","000","000","000","001", // 5
            "672","000","000","000","000","000","672", // 6
            "672","000","000","000","000","000","672", // 7
        };
        dataManager.RoomDefinitions_Catacombs.Add(RoomLabels.CatacombsGhosts2,
            new RoomDefinition(gameManager, SceneTypes.Catacombs, RoomLabels.CatacombsGhosts2, catacombs2_ghosts, RoomOrientations.OXOX));

        var catacombs2_spiders = new string[] {
          //  1     2     3     4     5     6     7   
            "672","000","000","000","000","000","672", // 1
            "672","000","000","000","000","000","001", // 2
            "001","1032","000","000","000","000","001", // 3
            "001","564","564","000","564","564","001", // 4
            "672","269","000","000","000","000","672", // 5
            "672","000","000","979","000","000","672", // 6
            "609","000","000","000","000","000","609", // 7
        };
        dataManager.RoomDefinitions_Catacombs.Add(RoomLabels.CatacombsSpiders2,
            new RoomDefinition(gameManager, SceneTypes.Catacombs, RoomLabels.CatacombsSpiders2, catacombs2_spiders, RoomOrientations.OXOX));

        var catacombsGhosts3 = new string[] {
          //  1     2     3     4     5     6     7   
            "609","609","001","001","001","672","672", // 1
            "672","000","000","000","000","000","672", // 2
            "000","000","000","000","000","000","000", // 3
            "000","000","000","314","000","000","979", // 4
            "000","000","000","000","000","000","000", // 5
            "672","000","000","000","000","000","001", // 6
            "001","001","672","001","672","672","672", // 7
        };
        dataManager.RoomDefinitions_Catacombs.Add(RoomLabels.CatacombsGhosts3,
            new RoomDefinition(gameManager, SceneTypes.Catacombs, RoomLabels.CatacombsGhosts3, catacombsGhosts3, RoomOrientations.XOXO));

        var catacombs3_spiders = new string[] {
          //  1     2     3     4     5     6     7   
            "672","001","001","001","672","672","672", // 1
            "672","000","000","000","000","000","672", // 2
            "000","000","000","000","564","000","000", // 3
            "979","000","000","269","000","000","000", // 4
            "000","000","564","000","000","000","000", // 5
            "672","000","000","000","000","000","609", // 6
            "672","001","001","001","672","672","672", // 7
        };
        dataManager.RoomDefinitions_Catacombs.Add(RoomLabels.CatacombsSpiders3,
            new RoomDefinition(gameManager, SceneTypes.Catacombs, RoomLabels.CatacombsSpiders3, catacombs3_spiders, RoomOrientations.XOXO));

        var catacombsGhosts4 = new string[] {
          //  1     2     3     4     5     6     7   
            "001","001","672","001","001","672","672", // 1
            "672","000","000","000","000","000","672", // 2
            "000","000","000","000","000","000","001", // 3
            "000","000","1032","000","000","979","001", // 4
            "000","000","607","000","000","000","001", // 5
            "000","000","000","000","000","000","672", // 6
            "000","000","000","000","000","672","672", // 7
        };
        dataManager.RoomDefinitions_Catacombs.Add(RoomLabels.CatacombsGhosts4,
            new RoomDefinition(gameManager, SceneTypes.Catacombs, RoomLabels.CatacombsGhosts4, catacombsGhosts4, RoomOrientations.XXOO));

        var catacombsGhosts5 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","672", // 1
            "000","000","000","000","000","000","672", // 2
            "000","000","000","000","000","000","001", // 3
            "000","000","000","314","000","979","001", // 4
            "000","000","000","000","000","000","001", // 5
            "000","000","000","000","000","000","672", // 6
            "672","001","001","672","672","672","672", // 7
        };
        dataManager.RoomDefinitions_Catacombs.Add(RoomLabels.CatacombsGhosts5,
            new RoomDefinition(gameManager, SceneTypes.Catacombs, RoomLabels.CatacombsGhosts5, catacombsGhosts5, RoomOrientations.OXXO));

        var catacombs_empty5 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","672", // 1
            "000","000","232","232","232","979","001", // 2
            "000","232","000","000","232","000","001", // 3
            "000","213","1032","000","232","213","001", // 4
            "000","000","000","000","232","000","672", // 5
            "000","232","232","232","232","000","672", // 6
            "672","672","672","672","001","001","672", // 7
        };
        dataManager.RoomDefinitions_Catacombs.Add(RoomLabels.CatacombsEmpty5,
            new RoomDefinition(gameManager, SceneTypes.Catacombs, RoomLabels.CatacombsEmpty5, catacombs_empty5, RoomOrientations.OXXO));

        var catacombsGhosts6 = new string[] {
          //  1     2     3     4     5     6     7   
            "672","000","000","000","000","000","000", // 1
            "001","000","000","000","000","000","000", // 2
            "001","000","000","000","000","000","000", // 3
            "672","979","000","314","000","000","000", // 4
            "609","000","000","000","000","000","000", // 5
            "609","000","000","000","000","000","000", // 6
            "672","672","672","672","672","001","001", // 7
        };
        dataManager.RoomDefinitions_Catacombs.Add(RoomLabels.CatacombsGhosts6,
            new RoomDefinition(gameManager, SceneTypes.Catacombs, RoomLabels.CatacombsGhosts6, catacombsGhosts6, RoomOrientations.OOXX));

        var catacombs6_empty = new string[] {
          //  1     2     3     4     5     6     7   
            "672","000","000","000","000","000","000", // 1
            "672","000","000","000","000","000","000", // 2
            "672","000","000","000","000","000","000", // 3
            "672","000","1032","000","000","979","000", // 4
            "672","000","000","000","000","000","000", // 5
            "672","000","000","000","000","000","000", // 6
            "672","672","672","001","672","001","001", // 7
        };
        dataManager.RoomDefinitions_Catacombs.Add(RoomLabels.CatacombsEmpty6,
            new RoomDefinition(gameManager, SceneTypes.Catacombs, RoomLabels.CatacombsEmpty6, catacombs6_empty, RoomOrientations.OOXX));

        var catacombsGhosts7 = new string[] {
          //  1     2     3     4     5     6     7   
            "672","001","001","001","672","672","609", // 1
            "609","000","000","000","000","000","609", // 2
            "000","000","000","000","000","000","000", // 3
            "000","000","000","314","000","000","000", // 4
            "000","000","000","000","000","000","000", // 5
            "672","000","607","000","000","000","672", // 6
            "001","000","000","000","000","000","001", // 7
        };
        dataManager.RoomDefinitions_Catacombs.Add(RoomLabels.CatacombsGhosts7,
            new RoomDefinition(gameManager, SceneTypes.Catacombs, RoomLabels.CatacombsGhosts7, catacombsGhosts7, RoomOrientations.XOOO));

        var catacombs7_spiders = new string[] {
          //  1     2     3     4     5     6     7   
            "001","001","001","672","609","609","001", // 1
            "001","000","000","000","000","000","672", // 2
            "000","000","000","000","269","000","000", // 3
            "000","979","564","564","564","000","000", // 4
            "000","000","000","000","564","269","000", // 5
            "672","000","000","000","000","000","672", // 6
            "672","000","000","000","000","000","672", // 7
        };
        dataManager.RoomDefinitions_Catacombs.Add(RoomLabels.CatacombsSpiders7,
            new RoomDefinition(gameManager, SceneTypes.Catacombs, RoomLabels.CatacombsSpiders7, catacombs7_spiders, RoomOrientations.XOOO));

        var catacombsGhosts8 = new string[] {
          //  1     2     3     4     5     6     7   
            "672","672","672","672","001","001","001", // 1
            "000","000","000","000","000","979","001", // 2
            "000","676","000","000","000","000","609", // 3
            "000","676","1032","314","000","000","001", // 4
            "000","676","000","000","000","000","001", // 5
            "000","000","000","000","000","000","609", // 6
            "672","001","001","001","672","609","609", // 7
        };
        dataManager.RoomDefinitions_Catacombs.Add(RoomLabels.CatacombsGhosts8,
            new RoomDefinition(gameManager, SceneTypes.Catacombs, RoomLabels.CatacombsGhosts8, catacombsGhosts8, RoomOrientations.XXXO));

        var catacombs8_empty = new string[] {
          //  1     2     3     4     5     6     7   
            "672","609","609","609","672","672","672", // 1
            "000","000","000","000","000","000","672", // 2
            "000","000","000","722","000","000","001", // 3
            "000","000","979","000","232","000","001", // 4
            "000","000","000","722","000","000","001", // 5
            "000","000","000","000","000","000","672", // 6
            "672","609","609","609","609","672","672", // 7
        };
        dataManager.RoomDefinitions_Catacombs.Add(RoomLabels.CatacombsEmpty8,
            new RoomDefinition(gameManager, SceneTypes.Catacombs, RoomLabels.CatacombsEmpty8, catacombs8_empty, RoomOrientations.XXXO));

        var catacombsGhosts9 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","672", // 1
            "000","672","000","000","000","000","672", // 2
            "000","000","000","000","000","000","001", // 3
            "000","232","000","314","1032","979","001", // 4
            "000","000","000","000","000","000","001", // 5
            "000","672","000","000","000","000","672", // 6
            "000","000","000","000","000","000","672", // 7
        };
        dataManager.RoomDefinitions_Catacombs.Add(RoomLabels.CatacombsGhosts9,
            new RoomDefinition(gameManager, SceneTypes.Catacombs, RoomLabels.CatacombsGhosts9, catacombsGhosts9, RoomOrientations.OXOO));

        var catacombs9_empty = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","672", // 1
            "000","564","000","000","000","000","609", // 2
            "000","000","564","000","000","000","609", // 3
            "000","232","237","518","000","979","609", // 4
            "000","000","564","000","000","000","609", // 5
            "000","564","000","000","000","000","609", // 6
            "000","000","000","000","000","000","672", // 7
        };
        dataManager.RoomDefinitions_Catacombs.Add(RoomLabels.CatacombsEmpty9,
            new RoomDefinition(gameManager, SceneTypes.Catacombs, RoomLabels.CatacombsEmpty9, catacombs9_empty, RoomOrientations.OXOO));

        var catacombs9_spiders = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","672", // 1
            "000","564","000","000","000","000","672", // 2
            "000","000","564","000","000","000","001", // 3
            "000","232","000","269","000","979","001", // 4
            "000","000","564","000","000","000","672", // 5
            "000","564","000","000","000","000","001", // 6
            "000","000","000","000","000","000","672", // 7
        };
        dataManager.RoomDefinitions_Catacombs.Add(RoomLabels.CatacombsSpiders9,
            new RoomDefinition(gameManager, SceneTypes.Catacombs, RoomLabels.CatacombsSpiders9, catacombs9_spiders, RoomOrientations.OXOO));

        var catacombsGhosts10 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","000", // 1
            "000","672","000","000","000","000","000", // 2
            "000","000","000","000","676","000","000", // 3
            "000","232","000","314","000","676","000", // 4
            "000","000","607","000","676","000","000", // 5
            "000","672","000","000","000","000","000", // 6
            "672","001","001","001","672","672","001", // 7
        };
        dataManager.RoomDefinitions_Catacombs.Add(RoomLabels.CatacombsGhosts10,
            new RoomDefinition(gameManager, SceneTypes.Catacombs, RoomLabels.CatacombsGhosts10, catacombsGhosts10, RoomOrientations.OOXO));

        var catacombsEmpty10 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","000", // 1
            "000","001","000","000","000","000","000", // 2
            "000","000","000","000","000","000","000", // 3
            "000","979","000","000","000","000","000", // 4
            "000","000","000","000","000","000","000", // 5
            "000","672","000","000","000","000","000", // 6
            "001","001","001","672","672","672","672", // 7
        };
        dataManager.RoomDefinitions_Catacombs.Add(RoomLabels.CatacombsEmpty10,
            new RoomDefinition(gameManager, SceneTypes.Catacombs, RoomLabels.CatacombsEmpty10, catacombsEmpty10, RoomOrientations.OOXO));

        var catacombs10_medghost = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","000", // 1
            "000","000","000","000","000","000","000", // 2
            "000","000","000","000","000","000","000", // 3
            "000","232","237","314","979","000","000", // 4
            "000","000","000","000","000","000","000", // 5
            "000","000","000","000","000","000","000", // 6
            "672","609","609","672","672","001","001", // 7
        };
        dataManager.RoomDefinitions_Catacombs.Add(RoomLabels.CatacombsMedGhost10,
            new RoomDefinition(gameManager, SceneTypes.Catacombs, RoomLabels.CatacombsMedGhost10, catacombs10_medghost, RoomOrientations.OOXO));

        var catacombsGhosts11 = new string[] {
          //  1     2     3     4     5     6     7   
            "001","672","672","001","001","672","672", // 1
            "001","672","000","000","000","000","672", // 2
            "001","000","607","000","000","000","001", // 3
            "672","000","000","314","000","000","001", // 4
            "001","000","000","000","000","000","001", // 5
            "672","672","000","000","000","979","672", // 6
            "672","000","000","000","000","000","672", // 7
        };
        dataManager.RoomDefinitions_Catacombs.Add(RoomLabels.CatacombsGhosts11,
            new RoomDefinition(gameManager, SceneTypes.Catacombs, RoomLabels.CatacombsGhosts11, catacombsGhosts11, RoomOrientations.XXOX));

        var catacombs11_medghost = new string[] {
          //  1     2     3     4     5     6     7   
            "672","001","001","609","672","672","672", // 1
            "001","001","000","000","000","979","001", // 2
            "672","000","607","000","000","000","672", // 3
            "672","000","232","314","000","000","001", // 4
            "001","000","000","000","000","000","001", // 5
            "001","672","676","676","676","672","672", // 6
            "672","000","000","000","000","000","672", // 7
        };
        dataManager.RoomDefinitions_Catacombs.Add(RoomLabels.CatacombsMedGhost11,
            new RoomDefinition(gameManager, SceneTypes.Catacombs, RoomLabels.CatacombsMedGhost11, catacombs11_medghost, RoomOrientations.XXOX));

        var catacombsGhosts12 = new string[] {
          //  1     2     3     4     5     6     7   
            "672","000","000","000","000","000","672", // 1
            "672","000","000","000","000","000","001", // 2
            "001","000","000","000","000","000","672", // 3
            "672","000","000","314","000","000","001", // 4
            "001","000","000","000","000","000","609", // 5
            "609","000","000","000","000","000","672", // 6
            "672","001","001","672","672","672","672", // 7
        };
        dataManager.RoomDefinitions_Catacombs.Add(RoomLabels.CatacombsGhosts12,
            new RoomDefinition(gameManager, SceneTypes.Catacombs, RoomLabels.CatacombsGhosts12, catacombsGhosts12, RoomOrientations.OXXX));

        var catacombs12_empty = new string[] {
          //  1     2     3     4     5     6     7   
            "672","000","000","000","000","000","672", // 1
            "672","000","000","000","000","000","672", // 2
            "672","000","896","000","896","000","001", // 3
            "001","000","232","979","000","000","001", // 4
            "672","000","896","000","896","000","001", // 5
            "672","000","000","000","000","000","672", // 6
            "672","672","001","001","672","672","672", // 7
        };
        dataManager.RoomDefinitions_Catacombs.Add(RoomLabels.CatacombsEmpty12,
            new RoomDefinition(gameManager, SceneTypes.Catacombs, RoomLabels.CatacombsEmpty12, catacombs12_empty, RoomOrientations.OXXX));

        var catacombs12_medghost = new string[] {
          //  1     2     3     4     5     6     7   
            "672","000","000","000","000","000","672", // 1
            "672","000","000","000","000","000","672", // 2
            "672","000","000","000","000","000","001", // 3
            "001","314","000","979","000","314","001", // 4
            "001","000","000","000","000","000","672", // 5
            "672","000","000","000","000","000","672", // 6
            "672","001","001","001","672","672","672", // 7
        };
        dataManager.RoomDefinitions_Catacombs.Add(RoomLabels.CatacombsMedGhost12,
            new RoomDefinition(gameManager, SceneTypes.Catacombs, RoomLabels.CatacombsMedGhost12, catacombs12_medghost, RoomOrientations.OXXX));

        var catacombs13_medghost = new string[] {
          //  1     2     3     4     5     6     7   
            "672","000","000","000","000","000","000", // 1
            "672","000","000","000","000","000","000", // 2
            "001","000","000","000","000","676","000", // 3
            "001","000","232","314","000","676","000", // 4
            "609","000","000","000","000","676","000", // 5
            "609","000","000","000","000","000","000", // 6
            "672","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Catacombs.Add(RoomLabels.CatacombsMedGhost13,
            new RoomDefinition(gameManager, SceneTypes.Catacombs, RoomLabels.CatacombsMedGhost13, catacombs13_medghost, RoomOrientations.OOOX));

        var catacombsGhosts14 = new string[] {
          //  1     2     3     4     5     6     7   
            "672","001","001","672","672","672","609", // 1
            "672","000","000","000","000","000","000", // 2
            "001","000","000","000","000","000","000", // 3
            "001","979","000","314","000","000","000", // 4
            "672","000","000","000","000","000","000", // 5
            "672","000","000","000","000","000","000", // 6
            "672","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Catacombs.Add(RoomLabels.CatacombsGhosts14,
            new RoomDefinition(gameManager, SceneTypes.Catacombs, RoomLabels.CatacombsGhosts14, catacombsGhosts14, RoomOrientations.XOOX));

        var catacombs15_medghost = new string[] {
          //  1     2     3     4     5     6     7   
            "672","001","001","672","672","672","672", // 1
            "672","000","000","000","000","000","000", // 2
            "001","000","000","000","000","000","000", // 3
            "001","000","000","000","314","000","000", // 4
            "609","000","000","000","000","000","000", // 5
            "609","000","000","000","000","000","000", // 6
            "672","672","672","001","001","001","672", // 7
        };
        dataManager.RoomDefinitions_Catacombs.Add(RoomLabels.CatacombsMedGhost15,
            new RoomDefinition(gameManager, SceneTypes.Catacombs, RoomLabels.CatacombsMedGhost15, catacombs15_medghost, RoomOrientations.XOXX));

        var catacombsEnd1 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","000", // 1
            "000","000","607","000","607","000","000", // 2
            "000","000","000","000","000","000","000", // 3
            "000","000","000","221","000","000","000", // 4
            "000","000","000","000","000","000","000", // 5
            "000","000","607","000","607","000","000", // 6
            "000","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Catacombs.Add(RoomLabels.CatacombsEnd1,
            new RoomDefinition(gameManager, SceneTypes.Catacombs, RoomLabels.CatacombsEnd1, catacombsEnd1, RoomOrientations.OOOO, RoomTypes.End));

        var catacombsEnd2 = new string[] {
          //  1     2     3     4     5     6     7   
            "672","672","672","672","672","001","001", // 1
            "001","000","607","000","607","000","672", // 2
            "672","000","000","000","000","000","609", // 3
            "609","232","000","221","000","979","609", // 4
            "672","000","000","000","000","000","001", // 5
            "001","000","000","000","000","000","672", // 6
            "672","000","000","000","000","000","672", // 7
        };
        dataManager.RoomDefinitions_Catacombs.Add(RoomLabels.CatacombsEnd2,
            new RoomDefinition(gameManager, SceneTypes.Catacombs, RoomLabels.CatacombsEnd2, catacombsEnd2, RoomOrientations.XXOX, RoomTypes.End));

        var catacombsEnd3 = new string[] {
          //  1     2     3     4     5     6     7   
            "672","672","672","001","001","001","672", // 1
            "672","000","000","000","000","000","000", // 2
            "672","000","000","000","000","000","000", // 3
            "001","000","000","221","000","000","000", // 4
            "001","000","607","000","607","000","000", // 5
            "672","000","000","000","000","000","000", // 6
            "672","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Catacombs.Add(RoomLabels.CatacombsEnd3,
            new RoomDefinition(gameManager, SceneTypes.Catacombs, RoomLabels.CatacombsEnd3, catacombsEnd3, RoomOrientations.XOOX, RoomTypes.End));

        var catacombsEnd4 = new string[] {
          //  1     2     3     4     5     6     7   
            "672","672","609","609","672","001","001", // 1
            "000","000","000","000","000","000","001", // 2
            "000","000","000","000","607","000","001", // 3
            "000","676","000","221","000","000","001", // 4
            "000","000","607","000","000","000","672", // 5
            "000","000","000","000","000","000","672", // 6
            "000","000","000","000","000","000","672", // 7
        };
        dataManager.RoomDefinitions_Catacombs.Add(RoomLabels.CatacombsEnd4,
            new RoomDefinition(gameManager, SceneTypes.Catacombs, RoomLabels.CatacombsEnd4, catacombsEnd4, RoomOrientations.XXOO, RoomTypes.End));

        var catacombsEnd5 = new string[] {
          //  1     2     3     4     5     6     7   
            "672","001","001","672","672","672","001", // 1
            "000","000","000","000","000","000","000", // 2
            "000","000","676","000","676","000","000", // 3
            "000","607","000","221","232","607","000", // 4
            "000","000","676","000","676","000","000", // 5
            "000","000","000","000","000","979","000", // 6
            "672","001","001","672","609","609","672", // 7
        };
        dataManager.RoomDefinitions_Catacombs.Add(RoomLabels.CatacombsEnd5,
            new RoomDefinition(gameManager, SceneTypes.Catacombs, RoomLabels.CatacombsEnd5, catacombsEnd5, RoomOrientations.XOXO, RoomTypes.End));

        var catacombsEnd6 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","672", // 1
            "000","000","000","000","000","000","001", // 2
            "000","000","000","000","000","000","001", // 3
            "000","607","000","221","000","607","001", // 4
            "000","000","000","000","000","000","672", // 5
            "000","000","000","000","000","000","672", // 6
            "672","672","001","672","672","672","672", // 7
        };
        dataManager.RoomDefinitions_Catacombs.Add(RoomLabels.CatacombsEnd6,
            new RoomDefinition(gameManager, SceneTypes.Catacombs, RoomLabels.CatacombsEnd6, catacombsEnd6, RoomOrientations.OXXO, RoomTypes.End));

        var catacombsEnd7 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","672", // 1
            "000","676","000","000","676","000","672", // 2
            "000","000","000","000","000","000","672", // 3
            "000","607","000","221","000","607","672", // 4
            "000","000","000","000","000","000","672", // 5
            "000","676","000","000","676","000","672", // 6
            "000","000","000","000","000","000","672", // 7
        };
        dataManager.RoomDefinitions_Catacombs.Add(RoomLabels.CatacombsEnd7,
            new RoomDefinition(gameManager, SceneTypes.Catacombs, RoomLabels.CatacombsEnd7, catacombsEnd7, RoomOrientations.OXOO, RoomTypes.End));

        var catacombsEnd8 = new string[] {
          //  1     2     3     4     5     6     7   
            "672","000","000","000","000","000","000", // 1
            "672","000","000","000","000","000","000", // 2
            "609","000","607","000","000","000","000", // 3
            "609","607","000","221","000","000","000", // 4
            "609","000","607","000","000","000","000", // 5
            "672","000","000","000","000","000","000", // 6
            "672","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Catacombs.Add(RoomLabels.CatacombsEnd8,
            new RoomDefinition(gameManager, SceneTypes.Catacombs, RoomLabels.CatacombsEnd8, catacombsEnd8, RoomOrientations.OOOX, RoomTypes.End));

        var catacombsEnd9 = new string[] {
          //  1     2     3     4     5     6     7   
            "672","000","000","000","000","000","000", // 1
            "672","000","000","000","000","000","000", // 2
            "672","607","000","000","000","000","000", // 3
            "672","000","000","000","000","000","000", // 4
            "672","1032","221","000","000","000","000", // 5
            "672","000","000","000","607","000","000", // 6
            "672","001","001","672","672","672","001", // 7
        };
        dataManager.RoomDefinitions_Catacombs.Add(RoomLabels.CatacombsEnd9,
            new RoomDefinition(gameManager, SceneTypes.Catacombs, RoomLabels.CatacombsEnd9, catacombsEnd9, RoomOrientations.OOXX, RoomTypes.End));

        var catacombsEnd10 = new string[] {
          //  1     2     3     4     5     6     7   
            "672","001","001","672","672","672","672", // 1
            "000","000","607","000","000","000","672", // 2
            "000","000","000","000","000","000","672", // 3
            "000","1032","000","000","221","000","001", // 4
            "000","000","000","000","000","000","001", // 5
            "000","000","607","000","000","000","672", // 6
            "672","672","672","672","672","672","672", // 7
        };
        dataManager.RoomDefinitions_Catacombs.Add(RoomLabels.CatacombsEnd10,
            new RoomDefinition(gameManager, SceneTypes.Catacombs, RoomLabels.CatacombsEnd10, catacombsEnd10, RoomOrientations.XXXO, RoomTypes.End));

        var catacombsEnd11 = new string[] {
          //  1     2     3     4     5     6     7   
            "672","000","000","000","000","000","672", // 1
            "672","000","607","000","607","000","672", // 2
            "672","000","000","000","000","000","672", // 3
            "672","676","1032","221","000","676","672", // 4
            "001","000","000","000","000","000","001", // 5
            "672","000","607","000","607","000","672", // 6
            "672","000","000","000","000","000","672", // 7
        };
        dataManager.RoomDefinitions_Catacombs.Add(RoomLabels.CatacombsEnd11,
            new RoomDefinition(gameManager, SceneTypes.Catacombs, RoomLabels.CatacombsEnd11, catacombsEnd11, RoomOrientations.OXOX, RoomTypes.End));

        var catacombsEnd12 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","000", // 1
            "000","000","607","000","000","000","000", // 2
            "000","000","000","000","000","000","000", // 3
            "000","1032","000","221","000","000","000", // 4
            "000","000","000","000","000","000","000", // 5
            "000","000","607","000","000","000","000", // 6
            "672","672","672","672","672","001","001", // 7
        };
        dataManager.RoomDefinitions_Catacombs.Add(RoomLabels.CatacombsEnd12,
            new RoomDefinition(gameManager, SceneTypes.Catacombs, RoomLabels.CatacombsEnd12, catacombsEnd12, RoomOrientations.OOXO, RoomTypes.End));

        var catacombsEnd13 = new string[] {
          //  1     2     3     4     5     6     7   
            "672","000","000","000","000","000","672", // 1
            "001","000","607","000","607","000","672", // 2
            "001","000","000","000","000","000","672", // 3
            "672","1032","000","221","000","000","672", // 4
            "672","237","000","000","000","237","672", // 5
            "672","000","607","000","607","000","672", // 6
            "672","672","672","672","672","001","001", // 7
        };
        dataManager.RoomDefinitions_Catacombs.Add(RoomLabels.CatacombsEnd13,
            new RoomDefinition(gameManager, SceneTypes.Catacombs, RoomLabels.CatacombsEnd13, catacombsEnd13, RoomOrientations.OXXX, RoomTypes.End));

        var catacombsEnd14 = new string[] {
          //  1     2     3     4     5     6     7   
            "672","001","001","672","672","672","672", // 1
            "672","000","607","000","607","000","000", // 2
            "672","237","000","000","000","000","000", // 3
            "672","1032","000","221","000","000","000", // 4
            "672","237","000","000","000","000","000", // 5
            "672","000","607","000","607","000","000", // 6
            "672","672","672","672","672","672","672", // 7
        };
        dataManager.RoomDefinitions_Catacombs.Add(RoomLabels.CatacombsEnd14,
            new RoomDefinition(gameManager, SceneTypes.Catacombs, RoomLabels.CatacombsEnd14, catacombsEnd14, RoomOrientations.XOXX, RoomTypes.End));

        var catacombsEnd15 = new string[] {
          //  1     2     3     4     5     6     7   
            "672","672","672","672","672","672","672", // 1
            "000","676","607","000","607","000","000", // 2
            "000","000","000","000","000","000","000", // 3
            "000","1032","000","221","000","000","000", // 4
            "000","000","000","000","000","000","000", // 5
            "000","676","607","000","607","000","000", // 6
            "000","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_Catacombs.Add(RoomLabels.CatacombsEnd15,
            new RoomDefinition(gameManager, SceneTypes.Catacombs, RoomLabels.CatacombsEnd15, catacombsEnd15, RoomOrientations.XOOO, RoomTypes.End));
    }
}