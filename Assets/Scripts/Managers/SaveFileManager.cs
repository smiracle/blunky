﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public class SaveFileManager
{
    public const int MaxNumSaves = 5;
    private const string savExtension = ".sav";
    private const string dateTimeFormat = "MM\\/dd\\/yyyy hh:mm:ss";
    private const string subdirectoryString = "/SaveData/";
    private const string saveString = "Save";

    private IGameManager gameManager { get; set; }
    private DataManager dataManager { get; set; }

    public void Setup(IGameManager gameManager, DataManager dataManager)
    {
        this.gameManager = gameManager;
        this.dataManager = dataManager;
    }

    public List<SaveFileMetadata> GetSaveFileMetadata()
    {
        var directoryPath = Application.persistentDataPath + subdirectoryString;
        if (!Directory.Exists(directoryPath))
        {
            Directory.CreateDirectory(directoryPath);
            directoryPath = Application.persistentDataPath + subdirectoryString;
        }

        var files = Directory.GetFiles(directoryPath);
        var saveFileMetadata = new List<SaveFileMetadata>();
        foreach (var path in files)
        {
            if (path.EndsWith(savExtension))
            {
                var filename = Path.GetFileName(path);
                var timestampString = filename.Substring(saveString.Length + 2, filename.Length - savExtension.Length - 6);
                var timestamp = long.Parse(timestampString);
                var dateTime = UnixTimeStampToDateTime(timestamp);
                saveFileMetadata.Add(new SaveFileMetadata
                {
                    Filepath = directoryPath,
                    Filename = filename,
                    Timestamp = timestamp,
                    DisplayText = dateTime.ToString(dateTimeFormat)
                });
            }
        }
        saveFileMetadata = saveFileMetadata.OrderByDescending(x => x.Timestamp).ToList();
        return saveFileMetadata;
    }

    public void SaveToJson(int saveSlotIndex)
    {        
        var saveFileMetadata = GetSaveFileMetadata();        
        var currentTimestampString = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds().ToString();
        var pathPrefix = Application.persistentDataPath + subdirectoryString;

        // Delete the original file first to prevent duplication of json
        if (saveFileMetadata.Count > saveSlotIndex - 1)
        {
            var fileToDelete = $"{saveFileMetadata[saveSlotIndex - 1].Filepath}{saveFileMetadata[saveSlotIndex - 1].Filename}";
            File.Delete(fileToDelete);
            Debug.Log($"Deleted save file {fileToDelete}");
        }
        
        var filepath = $"{pathPrefix}{saveString}{saveSlotIndex-1}-{currentTimestampString}{savExtension}";
        var saveFile = CreateSaveFile();
        var json = JsonUtility.ToJson(saveFile);
        File.WriteAllText(filepath, json);
        Debug.Log("Game saved to " + filepath);
    }

    public void Load(int savedGameIndex)
    {
        dataManager.IsLoadingSavedGame = true;
        if (dataManager.SaveFileMetadata.Count >= savedGameIndex)
        {
            var metadata = dataManager.SaveFileMetadata[savedGameIndex - 1];
            var filepath = metadata.Filepath + metadata.Filename;
            var timestamp = metadata.Timestamp.ToString();
            SaveFile saveFile = JsonUtility.FromJson<SaveFile>(File.ReadAllText(filepath));
            Debug.Log($"Loaded from {filepath}");
            gameManager.StartNewGame(saveFile.PlayerPosition);
        }
    }

    private SaveFile CreateSaveFile()
    {
        var player = gameManager.Player;
        return new SaveFile()
        {
            ActiveScene = gameManager.DataManager.ActiveScene,
            PlayerPosition = Vector3Int.FloorToInt(player.transform.localPosition),
        };
    }

    private DateTime UnixTimeStampToDateTime(double unixTimeStamp)
    {
        var dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
        dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
        return dtDateTime;
    }
}