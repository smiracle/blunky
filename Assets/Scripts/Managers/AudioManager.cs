﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

// Wraps an AudioMixerGroup in Unity's AudioMixer. 
// Contains the name of the group (which is also its exposed volume paramater), 
// the group itself and an IEnumerator for doing track fades over time.
public class GroupInfo
{
    public string Name = string.Empty;
    public AudioMixerGroup Group = null;
    public IEnumerator GroupFader = null;
}

// Describes an audio entity in our pooling system.
public class AudioPoolItem
{
    public GameObject GameObject = null;
    public Transform Transform = null;
    public AudioSource AudioSource = null;
    public float Unimportance = float.MaxValue;
    public bool Playing = false;
    public IEnumerator Coroutine = null;
    public ulong ID = 0;
}


// Provides pooled one-shot functionality with priority system 
// and also wraps the Unity Audio Mixer to make easier manipulation of audiogroup volumes
public class AudioManager : MonoBehaviour
{
    [SerializeField] private bool MuteGameOnStart;

    [SerializeField] private AudioMixer mixer = null;
    [SerializeField] private int maxSoundsPooled = 10;
    public AudioCollection musicAudioCollection = null;
    public AudioCollection soundsAudioCollection = null;

    private Dictionary<string, GroupInfo> groupInfos = new Dictionary<string, GroupInfo>();
    private List<AudioPoolItem> pool = new List<AudioPoolItem>();
    private Dictionary<ulong, AudioPoolItem> activePool = new Dictionary<ulong, AudioPoolItem>();
    private List<LayeredAudioSource> layeredAudio = new List<LayeredAudioSource>();
    private ulong idGiver = 0;
    private bool isUpdating;
    private const string musicGroupName = "Music";
    private const string pooledAudioItemName = "Pooled Audio";

    public void Setup(IGameManager gameManager)
    {
        if (!mixer)
        {
            throw new Exception("Audio mixer asset must be assigned in audio manager");
        }

        // Fetch all the groups in the mixer - These will be our mixers tracks
        AudioMixerGroup[] groups = mixer.FindMatchingGroups(string.Empty);

        // Create our mixer tracks based on group name (Track -> AudioGroup)
        foreach (AudioMixerGroup group in groups)
        {
            GroupInfo trackInfo = new GroupInfo();
            trackInfo.Name = group.name;
            trackInfo.Group = group;
            trackInfo.GroupFader = null;
            groupInfos[group.name] = trackInfo;
        }

        // Generate Pool
        for (int i = 0; i < maxSoundsPooled; i++)
        {
            // Create GameObject and assigned AudioSource and Parent
            GameObject go = new GameObject(pooledAudioItemName);
            AudioSource audioSource = go.AddComponent<AudioSource>();
            go.transform.parent = transform;

            // Create and configure Pool Item
            AudioPoolItem poolItem = new AudioPoolItem();
            poolItem.GameObject = go;
            poolItem.AudioSource = audioSource;
            poolItem.Transform = go.transform;
            poolItem.Playing = false;
            go.SetActive(false);
            pool.Add(poolItem);
        }
        //GetAudioGroupByName("Master").audioMixer.GetFloat("MasterVolume", out float defaultMasterVol);
        //if (!GetAudioGroupByName("Master").audioMixer.SetFloat("MasterVolume", MuteGameOnStart ? -80 : defaultMasterVol))
        //{
        //    throw new Exception("Exposed MasterVolume parameter not found.");
        //}
        if(MuteGameOnStart)
        {
            AudioListener.pause = true;
        }
    }

    public void StartUpdate()
    {
        isUpdating = true;
    }

    public void StopUpdate()
    {
        isUpdating = false;
    }

    private void Update()
    {
        if (!isUpdating)
        {
            return;
        }

        foreach (LayeredAudioSource las in layeredAudio)
        {
            if (las != null)
            {
                las.Update();
            }
        }
    }

    public void StopAll()
    {
        foreach (var item in pool)
        {
            if (item.Playing)
            {
                item.AudioSource.Stop();
                item.Playing = false;
            }
        }
        StopUpdate();
    }

    // ------------------------------------------------------------------------------
    // Name	:	GetTrackVolume
    // Desc	:	Returns the volume of the AudioMixerGroup assign to the passed track.
    //			AudioMixerGroup MUST expose its volume variable to script for this to
    //			work and the variable MUST be the same as the name of the group
    // ------------------------------------------------------------------------------
    public float GetAudioGroupVolume(string groupName)
    {
        GroupInfo groupInfo;
        if (groupInfos.TryGetValue(groupName, out groupInfo))
        {
            float volume;
            mixer.GetFloat(groupName, out volume);
            return volume;
        }

        return float.MinValue;
    }

    public AudioMixerGroup GetAudioGroupByName(string name)
    {
        GroupInfo ti;
        if (groupInfos.TryGetValue(name, out ti))
        {
            return ti.Group;
        }

        throw new Exception("Audio mixer group " + name + " not found.");
    }

    // Sets the volume of the AudioMixerGroup assigned to the passed track. 
    // AudioMixerGroup MUST expose its volume variable to script for this to
    // work and the variable MUST be the same as the name of the group.
    // If a fade time is given a coroutine will be used to perform the fade.
    public void SetGroupVolume(string groupName, float volume, float fadeTime = 0.0f)
    {
        if (!mixer) return;
        GroupInfo trackInfo;
        if (groupInfos.TryGetValue(groupName, out trackInfo))
        {
            // Stop any coroutine that might be in the middle of fading this track
            if (trackInfo.GroupFader != null) StopCoroutine(trackInfo.GroupFader);

            if (fadeTime == 0.0f)
                mixer.SetFloat(groupName, volume);
            else
            {
                trackInfo.GroupFader = SetAudioGroupVolumeInternal(groupName, volume, fadeTime);
                StartCoroutine(trackInfo.GroupFader);
            }
        }

    }

    // Used by SetTrackVolume to implement a fade between volumes of a track over time.
    protected IEnumerator SetAudioGroupVolumeInternal(string groupName, float volume, float fadeTime)
    {
        float startVolume = 0.0f;
        float timer = 0.0f;
        mixer.GetFloat(groupName, out startVolume);

        while (timer < fadeTime)
        {
            timer += Time.unscaledDeltaTime;
            mixer.SetFloat(groupName, Mathf.Lerp(startVolume, volume, timer / fadeTime));
            yield return null;
        }

        mixer.SetFloat(groupName, volume);
    }

    private ulong ConfigurePoolObject(int poolIndex, string audioGroupName, AudioClip clip, Vector3 position, bool loop, float volume, float spatialBlend, float unimportance, float startTime, bool ignoreListenerPause)
    {
        // If poolIndex is out of range abort request
        if (poolIndex < 0 || poolIndex >= pool.Count)
        {
            return 0;
        }

        // Get the pool item
        AudioPoolItem poolItem = pool[poolIndex];

        // Stop any previously playing coroutine
        if (poolItem.Coroutine != null) StopCoroutine(poolItem.Coroutine);

        // Generate new ID so we can stop it later if we want to
        idGiver++;

        // Configure the audio source's position and column
        AudioSource source = poolItem.AudioSource;
        source.clip = clip;
        source.volume = volume;
        source.spatialBlend = spatialBlend;
        source.ignoreListenerPause = ignoreListenerPause;
        source.loop = loop;
        // Assign to requested audio group/track
        source.outputAudioMixerGroup = groupInfos[audioGroupName].Group;

        // Position source at requested position
        source.transform.position = position;

        // Enable GameObject and record that it is now playing
        poolItem.Playing = true;
        poolItem.Unimportance = unimportance;
        poolItem.ID = idGiver;
        source.time = Mathf.Min(startTime, source.clip.length);
        poolItem.GameObject.SetActive(true);
        source.Play();
        if (!source.loop)
        {
            poolItem.Coroutine = StopSoundDelayed(idGiver, source.clip.length);        
            StartCoroutine(poolItem.Coroutine);
        }

        // Add this sound to our active pool with its unique id
        activePool[idGiver] = poolItem;

        // Return the id to the caller
        return idGiver;
    }

    // Stop a one shot sound from playing after a number of seconds
    protected IEnumerator StopSoundDelayed(ulong id, float duration)
    {
        yield return new WaitForSeconds(duration);
        AudioPoolItem activeSound;

        // If this if exists in our active pool
        if (activePool.TryGetValue(id, out activeSound))
        {
            activeSound.Coroutine = null;
            activeSound.AudioSource.Stop();
            activeSound.AudioSource.clip = null;
            activeSound.GameObject.SetActive(false);
            activePool.Remove(id);

            // Make it available again
            activeSound.Playing = false;
        }
    }

    // Stop Sound with the passed One Shot ID
    public void StopSound(ulong id)
    {
        AudioPoolItem activeSound;
        if (activePool.TryGetValue(id, out activeSound))
        {
            if (activeSound.Coroutine != null)
            {
                StopCoroutine(activeSound.Coroutine);
                activeSound.Coroutine = null;
            }

            activeSound.AudioSource.Stop();
            activeSound.AudioSource.clip = null;
            activeSound.GameObject.SetActive(false);
            activePool.Remove(id);

            // Make it available again
            activeSound.Playing = false;

        }
    }

    public ulong PlayMusic(MusicTracks music, bool loop, float volume, float startTime = 0.0f, bool ignoreListenerPause = false)
    {
        if (!groupInfos.ContainsKey(musicGroupName))
        {
            throw new Exception("Music audio group does not exist.");
        }
        if (musicAudioCollection == null)
        {
            throw new Exception("'Music' audio collection must be set on Audio Manager.");
        }

        var clip = musicAudioCollection.GetMusicTrack(music);
        if (clip == null || volume.Equals(0.0f))
        {
            throw new Exception("Music " + music + " could not be played - clip is null or volume is zero.");
        }

        float unimportance = float.MaxValue;
        int leastImportantIndex = -1;
        float leastImportanceValue = float.MinValue;

        // Find an available audio source to use
        for (int i = 0; i < pool.Count; i++)
        {
            AudioPoolItem poolItem = pool[i];

            // Is this source available
            if (!poolItem.Playing)
            {
                return ConfigurePoolObject(i, musicGroupName, clip, transform.position, loop, volume, spatialBlend: 0, unimportance, startTime, ignoreListenerPause);
            }
            else
            {
                // We have a pool item that is less important than the one we are going to play
                if (poolItem.Unimportance > leastImportanceValue)
                {
                    // Record the least important sound we have found so far
                    // as a candidate to relace with our new sound request
                    leastImportanceValue = poolItem.Unimportance;
                    leastImportantIndex = i;

                }
            }
        }

        // If we get here all sounds are being used but we know the least important sound currently being
        // played so if it is less important than our sound request then replace it
        if (leastImportanceValue > unimportance)
        {
            return ConfigurePoolObject(leastImportantIndex, musicGroupName, clip, transform.position, loop, volume, spatialBlend: 0, unimportance, startTime, ignoreListenerPause);
        }

        throw new Exception("Music could not be played (no sound in the pool available)");
    }


    // Scores the priority of the sound and search for an unused pool item to use as the audio source. 
    // If one is not available an audio source with a lower priority will be stopped and reused
    public ulong PlayOneShotSound(string groupName, AudioClip clip, Vector3 position, float volume, float spatialBlend, int priority = 128, float startTime = 0.0f, bool ignoreListenerPause = false)
    {
        if (!groupInfos.ContainsKey(groupName))
        {
            throw new Exception("Group name " + groupName + " does not exist");
        }
        if (clip == null)
        {
            throw new Exception("Audio clip with group name " + groupName + " does not exist");
        }
        if (volume.Equals(0.0f))
        {
            throw new Exception("Volume for sound " + clip.name + " is zero");
        }

        var listenerPos = Camera.main.gameObject.transform.position;
        float unimportance = (listenerPos - position).sqrMagnitude / Mathf.Max(1, priority);
        int leastImportantIndex = -1;
        float leastImportanceValue = float.MinValue;

        // Find an available audio source to use
        for (int i = 0; i < pool.Count; i++)
        {
            AudioPoolItem poolItem = pool[i];

            // Is this source available
            if (!poolItem.Playing)
            {
                return ConfigurePoolObject(i, groupName, clip, position, loop: false, volume, spatialBlend, unimportance, startTime, ignoreListenerPause);
            }
            else
            {
                // We have a pool item that is less important than the one we are going to play
                if (poolItem.Unimportance > leastImportanceValue)
                {
                    // Record the least important sound we have found so far
                    // as a candidate to relace with our new sound request
                    leastImportanceValue = poolItem.Unimportance;
                    leastImportantIndex = i;
                }
            }
        }

        // If we get here all sounds are being used but we know the least important sound currently being
        // played so if it is less important than our sound request then use replace it
        if (leastImportanceValue > unimportance)
        {
            return ConfigurePoolObject(leastImportantIndex, groupName, clip, position, loop: false, volume, spatialBlend, unimportance, startTime, ignoreListenerPause);
        }

        throw new Exception("Sound could not be played (no sound in the pool available)");
    }

    // Queue up a one shot sound to be played after a number of seconds
    public IEnumerator PlayOneShotSoundDelayed(string track, AudioClip clip, Vector3 position, float volume, float spatialBlend, float duration, int priority = 128)
    {
        yield return new WaitForSeconds(duration);
        PlayOneShotSound(track, clip, position, volume, spatialBlend, priority);
    }

    public ILayeredAudioSource RegisterLayeredAudioSource(AudioSource source, int layers)
    {
        if (source != null && layers > 0)
        {
            // First check it doesn't exist already and if so just return the source
            for (int i = 0; i < layeredAudio.Count; i++)
            {
                LayeredAudioSource item = layeredAudio[i];
                if (item != null)
                {
                    if (item.audioSource == source)
                    {
                        return item;
                    }
                }
            }

            // Create a new layered audio item and add it to the managed list
            LayeredAudioSource newLayeredAudio = new LayeredAudioSource(this, source, layers);
            layeredAudio.Add(newLayeredAudio);

            return newLayeredAudio;
        }

        return null;
    }

    // -------------------------------------------------------------------------------
    // Name	:	UnregisterLayeredAudioSource (Overload)
    // Desc	:
    // -------------------------------------------------------------------------------
    public void UnregisterLayeredAudioSource(ILayeredAudioSource source)
    {
        layeredAudio.Remove((LayeredAudioSource)source);
    }

    // -------------------------------------------------------------------------------
    // Name	:	UnregisterLayeredAudioSource (Overload)
    // Desc	:
    // -------------------------------------------------------------------------------
    public void UnregisterLayeredAudioSource(AudioSource source)
    {
        for (int i = 0; i < layeredAudio.Count; i++)
        {
            LayeredAudioSource item = layeredAudio[i];
            if (item != null)
            {
                if (item.audioSource == source)
                {
                    layeredAudio.Remove(item);
                    return;
                }
            }
        }
    }
}