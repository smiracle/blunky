﻿
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.Tilemaps;
using UnityEngine.U2D;

[Serializable]
[RequireComponent(typeof(DataManager))]
[RequireComponent(typeof(UIManager))]
[RequireComponent(typeof(InputManager))]
[RequireComponent(typeof(EventManager))]
[RequireComponent(typeof(AudioManager))]
[RequireComponent(typeof(GameObjectPooler))]
public class GameManager : MonoBehaviour, IGameManager
{
    // True starting position:
    public Vector3Int PlayerStartPos { get; set; } = new Vector3Int(32, 7, 0);
    // Dungeon 1:
    //public Vector3Int PlayerStartPos { get; set; } = new Vector3Int(53, 5, 0);
    // Dungeon 3:
    //public Vector3Int PlayerStartPos { get; set; } = new Vector3Int(53, 45, 0);
    public DataManager DataManager { get; set; }
    public UIManager UIManager { get; set; }
    public EventManager EventManager { get; set; }
    public AudioManager AudioManager { get; set; }
    public InputManager InputManager { get; set; }
    public SaveFileManager SaveFileManager { get; set; }
    public PlayerCharacter Player { get; set; }
    [field: SerializeField] public Camera Camera { get; set; }
    [field: SerializeField] public GameObject MapGo { get; set; }
    [field: SerializeField] public Tilemap GroundMap { get; set; }
    [field: SerializeField] public Tilemap CollisionMap { get; set; }

    public GameObject PlayerPrefabRef;
    public GameObject BombExplosionPrefabRef;
    public GameObject DamageTextPrefabRef;
    public GameObject GameOverScreenPrefabRef;
    public GameObject PlayerPrefab { get { return PlayerPrefabRef; } }
    public GameObject BombExplosionPrefab { get { return BombExplosionPrefabRef; } }
    public GameObject DamageTextPrefab { get { return DamageTextPrefabRef; } }
    public GameObject GameOverScreenPrefab { get { return GameOverScreenPrefabRef; } }
    void Awake()
    {
        Setup();
    }

    public void Setup()
    {
        DataManager = GetComponent<DataManager>();
        UIManager = GetComponent<UIManager>();
        EventManager = GetComponent<EventManager>();
        AudioManager = GetComponent<AudioManager>();
        InputManager = GetComponent<InputManager>();
        SaveFileManager = new SaveFileManager();

        DataManager.Setup(this);
        EventManager.Setup();
        UIManager.Setup(this);
        AudioManager.Setup(this);
        InputManager.Setup(this);
        SaveFileManager.Setup(this, DataManager);
        ApplyUserSettings();
        AudioManager.StartUpdate();

        InputManager.Run();
        PlayMusic(MusicTracks.Title);
        UIManager.ChangeState<TitleMenuState>();
    }

    private void OnDisable()
    {
        InputManager.Stop();
    }

    public void StartNewGame(Vector3Int playerStartPos)
    {
        if (Player == null)
        {
            // Loading the game for the first time
            //DataManager.GameStates.Add(GameStates.FoundQueen);
            //DataManager.GameStates.Add(GameStates.ReceivedBombs);
            //SetMap(Scenes.Overworld, new Vector3Int(0, 0, 0), false);            
            //DataManager.ActiveScene = Scenes.Overworld;

        }
        else
        {
            //Addressables.ReleaseInstance(Player.gameObject);
            Player = null;
            Camera.transform.SetParent(transform);
            DataManager.ResetAllData();
        }

        // Load the player
        //Addressables.LoadAssetAsync<GameObject>("Player").Completed += (operation) =>
        //{
        //var playerObj = Addressables.InstantiateAsync("Player").Result;
        //Player = playerObj.GetComponent<PlayerCharacter>();
        //Player.Setup(this);
        //Player.Hp = Player.MaxHp = 20;
        //Camera.transform.SetParent(Player.transform);
        //Camera.transform.localPosition = new Vector3(0f, 0f, -16f);
        //Transition(Scenes.Overworld, playerStartPos);
        //Camera.enabled = true;
        //Camera.gameObject.GetComponent<PixelPerfectCamera>().enabled = true;
        //Camera.gameObject.GetComponent<AudioListener>().enabled = true;
        //UIManager.ChangeState<GameplayState>();
        //};
        var playerObj = GameObjectPooler.Instance.GetObject(PlayerPrefab);
        playerObj.SetActive(true);
        Player = playerObj.GetComponent<PlayerCharacter>();
        Player.Setup(this);
        Player.Hp = Player.MaxHp = 20;
        Camera.transform.SetParent(Player.transform);
        Camera.transform.localPosition = new Vector3(0f, 0f, -16f);
        Transition(Scenes.Overworld, playerStartPos);
        Camera.enabled = true;
        Camera.gameObject.GetComponent<PixelPerfectCamera>().enabled = true;
        Camera.gameObject.GetComponent<AudioListener>().enabled = true;
        UIManager.ChangeState<GameplayState>();
    }

    public void PlayMusic(MusicTracks track)
    {
        AudioManager.StopAll();
        AudioManager.PlayMusic(track, true, 0.5f);
    }

    public void Transition(Scenes scene, Vector3Int newPosition)
    {
        if (DataManager.ActiveScene == scene)

        {
            //Debug.LogWarning("Transitioning to active scene");
        }
        if (DataManager.ActiveScene != Scenes.Empty)
        {
            // Stop old scene NPCs
            DataManager.StopAllTickBehavior();
        }
        DataManager.ActiveScene = scene;
        //Debug.Log($"Transition to {scene} {newPosition}");

        // Spawn NPCs dynamically based on who has been found
        if (scene == Scenes.Overworld && DataManager.GameStates.Contains(GameStates.FoundQueen) && !DataManager.GameStates.Contains(GameStates.ReceivedBombs))
        {
            var skeletonCaveEntrance = new Vector3Int(59, 2, 0);
            DataManager.CreateNPCAtRuntime(Scenes.Overworld, skeletonCaveEntrance, TileIdentifiers.NPC_Queen);
        }
        else if (scene == Scenes.Interior_TallBuilding)
        {
            var queenHousePosition = new Vector3Int(1, 4, 0);
            var kingHousePosition = new Vector3Int(5, 4, 0);
            if (DataManager.GameStates.Contains(GameStates.ReceivedBombs) && DataManager.Tiles[scene][queenHousePosition].Interactable == null)
            {
                DataManager.CreateNPCAtRuntime(Scenes.Interior_TallBuilding, queenHousePosition, TileIdentifiers.NPC_Queen);
            }
            if (DataManager.GameStates.Contains(GameStates.FoundKing) && DataManager.Tiles[scene][kingHousePosition].Interactable == null)
            {
                DataManager.CreateNPCAtRuntime(Scenes.Interior_TallBuilding, kingHousePosition, TileIdentifiers.NPC_King);
            }
        }
        else if (scene == Scenes.Interior_Shop)
        {
            var buyBombPosition = new Vector3Int(1, 4, 0);
            if (DataManager.Tiles[scene][buyBombPosition].Interactable == null && DataManager.GameStates.Contains(GameStates.ReceivedBombs))
            {
                DataManager.CreateItemAtRuntime(TileIdentifiers.Items_Buyable_Bombs, scene, buyBombPosition);
            }

            var buyPotionPosition = new Vector3Int(3, 4, 0);
            if (DataManager.Tiles[scene][buyPotionPosition].Interactable == null && Player.HealthPotions == 0)
            {
                DataManager.CreateItemAtRuntime(TileIdentifiers.Items_Buyable_HealingPotion, scene, buyPotionPosition);
            }

            var buyAttackPosition = new Vector3Int(5, 4, 0);
            if (DataManager.Tiles[scene][buyAttackPosition].Interactable == null)
            {
                DataManager.CreateItemAtRuntime(TileIdentifiers.Items_Buyable_AttackPower, scene, buyAttackPosition);
            }
        }

        DataManager.DrawMapForScene(scene);
        DataManager.StartTickBehavior(scene);

        // Transition to the position
        Player.transform.position = new Vector3(newPosition.x + 0.5f, newPosition.y + 0.5f, 0);

        switch (DataManager.ActiveScene)
        {
            case Scenes.Overworld:
                PlayMusic(MusicTracks.Overworld);
                break;
            case Scenes.Dungeon1_SkeletonCave:
                PlayMusic(MusicTracks.Dungeon);
                break;
            case Scenes.Dungeon2_Volcano:
                PlayMusic(MusicTracks.Dungeon);
                break;
            case Scenes.Dungeon3_Catacombs:
                PlayMusic(MusicTracks.Dungeon2);
                break;
            case Scenes.Interior_Shop:
                PlayMusic(MusicTracks.Shop);
                break;
            default:
                PlayMusic(MusicTracks.Interior);
                break;
        }


        if (!Player.Visited.Contains(scene))
        {
            Player.Visited.Add(scene);
        }


    }

    public void PlaySound(AudioClipBanks audioClipBank)
    {
        var collection = AudioManager.soundsAudioCollection;
        AudioClip clip = collection.GetRandomClipFromBank(audioClipBank);
        if (clip)
        {
            var oneShotSoundID = AudioManager.PlayOneShotSound(collection.audioGroup,
                                               clip,
                                               Player.transform.position,
                                               collection.volume,
                                               collection.spatialBlend,
                                               collection.priority);
        }
    }

    public void LoadTitleScene()
    {
        UIManager.PauseMenu.SetActive(false);
    }

    public void LoadScene(Scenes scene)
    {
        var fadeTime = 2f;
        UIManager.TitleMenu.SetActive(false);
        UIManager.Fade(fadeTime, ScreenFadeType.FadeOut);
        StartCoroutine(LoadSceneAfterFadeOut(scene, fadeTime));
    }

    IEnumerator LoadSceneAfterFadeOut(Scenes scene, float fadeTime)
    {
        AudioManager.StopUpdate();
        yield return new WaitForSeconds(fadeTime);
        UIManager.BlurBackgroundObject.SetActive(false);
        DataManager.ActiveScene = scene;
    }

    void ApplyUserSettings()
    {
        UIManager.AudioSettingsMenu.ApplyJsonSettings();
        UIManager.VideoSettingsMenu.ApplyJsonSettings();
    }
}