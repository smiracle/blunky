﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AddressableAssets;

public class DataManager : MonoBehaviour
{
    public TileRefs TileRefs;
    public Scenes ActiveScene { get; set; }
    public static System.Random Random { get; set; } = new System.Random(Guid.NewGuid().GetHashCode());
    public List<SaveFileMetadata> SaveFileMetadata { get; set; }
    public bool IsLoadingSavedGame { get; set; }
    public SaveFile SaveFileToLoad { get; set; }
    public Dictionary<RoomLabels, RoomDefinition> RoomDefinitions_SkeletonCave { get; set; } = new Dictionary<RoomLabels, RoomDefinition>();
    public Dictionary<RoomLabels, RoomDefinition> RoomDefinitions_Volcano { get; set; } = new Dictionary<RoomLabels, RoomDefinition>();
    public Dictionary<RoomLabels, RoomDefinition> RoomDefinitions_Catacombs { get; set; } = new Dictionary<RoomLabels, RoomDefinition>();
    public Dictionary<RoomLabels, RoomDefinition> RoomDefinitions_Overworld { get; set; } = new Dictionary<RoomLabels, RoomDefinition>();
    public Dictionary<RoomLabels, RoomDefinition> RoomDefinitions_Interiors { get; set; } = new Dictionary<RoomLabels, RoomDefinition>();
    public Dictionary<RoomOrientations, List<RoomDefinition>> RoomDefsByOrientation_SkeletonCave { get; set; } = new Dictionary<RoomOrientations, List<RoomDefinition>>();
    public Dictionary<RoomOrientations, List<RoomDefinition>> RoomDefsByOrientation_Volcano { get; set; } = new Dictionary<RoomOrientations, List<RoomDefinition>>();
    public Dictionary<RoomOrientations, List<RoomDefinition>> RoomDefsByOrientation_Catacombs { get; set; } = new Dictionary<RoomOrientations, List<RoomDefinition>>();
    public Dictionary<RoomOrientations, List<RoomDefinition>> RoomDefsByOrientation_Overworld { get; set; } = new Dictionary<RoomOrientations, List<RoomDefinition>>();
    public Dictionary<RoomOrientations, List<RoomDefinition>> RoomDefsByOrientation_Interiors { get; set; } = new Dictionary<RoomOrientations, List<RoomDefinition>>();
    public Dictionary<Scenes, Dictionary<Vector3Int, Room>> RoomGrid { get; set; } = new Dictionary<Scenes, Dictionary<Vector3Int, Room>>();
    public HashSet<TileIdentifiers> GroundTiles { get; set; } = new HashSet<TileIdentifiers>();
    public List<ITickable> Turns { get; set; } = new List<ITickable>();
    public Dictionary<Scenes, Dictionary<Vector3Int, TileInfo>> Tiles { get; set; } = new Dictionary<Scenes, Dictionary<Vector3Int, TileInfo>>();
    public Dictionary<Scenes, List<Transition>> Transitions = new Dictionary<Scenes, List<Transition>>();    
    public HashSet<GameStates> GameStates = new HashSet<GameStates>();
    private HashSet<TileIdentifiers> interactableIDs = new HashSet<TileIdentifiers>();
    private HashSet<TileIdentifiers> destructableIDs = new HashSet<TileIdentifiers>();
    private HashSet<TileIdentifiers> npcIDs = new HashSet<TileIdentifiers>();
    private HashSet<TileIdentifiers> enemyIDs = new HashSet<TileIdentifiers>();
    private HashSet<TileIdentifiers> transitionIDs = new HashSet<TileIdentifiers>();
    private HashSet<TileIdentifiers> itemIDs = new HashSet<TileIdentifiers>();
    private HashSet<TileIdentifiers> triggerIDs = new HashSet<TileIdentifiers>();
    private bool wasPlayerRotated = false; // Hacky, but... it works
    public RoomFactory RoomFactory;
    public readonly Vector3Int[] DirectionValues = new Vector3Int[] {
        new Vector3Int(0, 1, 0),
        new Vector3Int(1, 0, 0),
        new Vector3Int(0, -1, 0),
        new Vector3Int(-1, 0, 0)
    };
    private IGameManager gameManager;

    public void Setup(IGameManager gameManager)
    {
        this.gameManager = gameManager;
        RoomFactory = new RoomFactory(gameManager);
        // These get ignored when setting tiles, since they're done separately after the map is ready
        IdentifyGroundTiles();
        IdentifyInteractables();
        IdentifyDestructables();
        IdentifyEnemies();
        IdentifyItems();
        IdentifyNPCs();
        IdentifyTransitions();
        IdentifyTriggers();
        ResetAllData();
    }


    public void ResetAllData()
    {
        PrepareDictionaries();
        RoomFactory.SetupInteriorsDefinitions();
        Overworld.Setup(gameManager);
        SkeletonCave.Setup(gameManager);
        Volcano.Setup(gameManager);
        Catacombs.Setup(gameManager);
        RoomFactory.SetupRoomArrangements();
        RoomFactory.AssembleAllRoomsForAllMaps();
        LinkAllTransitions();
        if (gameManager.Player != null)
        {
            gameManager.Player.Bombs = 0;
            gameManager.Player.HealthPotions = 0;
            gameManager.Player.Gold = 0;
            gameManager.Player.ToolsAvailable.Clear();
        }
    }

    public void SpawnExplosionVFX(Vector3 position)
    {
        var obj = GameObjectPooler.Instance.GetObject(gameManager.BombExplosionPrefab);
        obj.SetActive(true);
        obj.transform.position = position;
        StartCoroutine(DestroyExplosionAfterAnimation(obj));
    }

    private IEnumerator DestroyExplosionAfterAnimation(GameObject bombObject)
    {        
        yield return new WaitForSeconds(1f);
        bombObject.SetActive(false);
    }

    //public RoomDefinition GetRandomRoomByOrientation(RoomOrientations orientation)
    //{
    //    if(RoomDefsByOrientation_SkeletonCave[orientation].Count == 0)
    //    {
    //        throw new Exception($"No rooms associated with orientation {orientation}, add more definitions");
    //    }
    //    return RoomDefsByOrientation_SkeletonCave[orientation][Random.Next(0, RoomDefsByOrientation_SkeletonCave[orientation].Count)];
    //}

    public void RotateRoomAtRuntime(Room room)
    {
        RotateClockwise(room.Scene, room.Origin);
        StopAllTickBehavior();
        StartTickBehavior(ActiveScene);
        DrawMapForScene(room.Scene);
        gameManager.PlaySound(AudioClipBanks.Sounds_Rotate);
    }

    public Vector3Int GetTilePositionInDirection(Directions direction, Vector3Int fromPosition)
    {
        switch (direction)
        {
            case Directions.North:
                return new Vector3Int(fromPosition.x, (fromPosition.y + 1), 0);
            case Directions.East:
                return new Vector3Int((fromPosition.x + 1), (fromPosition.y), 0);
            case Directions.South:
                return new Vector3Int(fromPosition.x, (fromPosition.y - 1), 0);
            case Directions.West:
                return new Vector3Int((fromPosition.x - 1), (fromPosition.y), 0);
            default:
                throw new Exception($"No such direction {direction}");
        }
    }

    private void RotateClockwise(Scenes scene, Vector3Int roomOrigin)
    {
        var rotatedTiles = RotateTilesClockwiseAndValidate(Tiles[scene], roomOrigin);
        Tiles[scene] = rotatedTiles;
    }

    public void DestroyDestructableTile(Scenes scene, Vector3Int position)
    {
        //Debug.Log($"attempting to destroy tiles at ... {position}");
        if (Tiles[scene][position].IsDestructable)
        {
            Tiles[scene][position].TileIdentifier = TileIdentifiers.Empty;
            //Debug.Log($"destroyed tile at at ... {position}");
            DrawOneTile(Tiles[scene][position]);
        }
        else
        {
            throw new Exception($"Invalid destruction of tile at {position}");
        }
    }

    public Dictionary<Vector3Int, TileInfo> RotateTilesClockwiseAndValidate(Dictionary<Vector3Int, TileInfo> tiles, Vector3Int roomOrigin)
    {
        //ValidatePositionsForRoom(tiles, roomOrigin);
        //ClearDrawnInteractablesInSection(scene, sectionOrigin);
        var side = RoomFactory.RoomDimension;
        var originX = roomOrigin.x;
        var originY = roomOrigin.y;
        wasPlayerRotated = false;
        // Concentric iteration from outside in
        for (int iter = 0; iter < side / 2; iter++)
        {
            // Deep copy left side from bottom to top
            var leftCopy = new TileInfo[side - (iter * 2)];
            var i = 0;
            for (int l = iter; l < side - iter; l++)
            {
                leftCopy[i] = tiles[new Vector3Int(originX + iter, originY + l, 0)].DeepCopy();
                i++;
            }

            // Overwrite left with bot
            for (int l = iter; l < side - iter; l++)
            {
                var tileToMove = tiles[new Vector3Int(originX + side - 1 - l, originY + iter, 0)];                
                var newPos = new Vector3Int(originX + iter, originY + l, 0);
                OverwriteTileInfo(ref tiles, tileToMove, newPos);
                //ValidatePositionsForRoom(tiles, roomOrigin);
            }

            // Overwrite bot with right
            for (int b = iter; b < side - iter; b++)
            {
                var fromTile = tiles[new Vector3Int(originX + side - 1 - iter, originY + b, 0)];
                var toPos = new Vector3Int(originX + b, originY + iter, 0);
                OverwriteTileInfo(ref tiles, fromTile, toPos);
                //ValidatePositionsForRoom(tiles, roomOrigin);
            }

            // Overwrite right with top
            for (int r = iter; r < side - iter; r++)
            {
                var fromTile = tiles[new Vector3Int(originX + side - 1 - r, originY + side - 1 - iter, 0)];
                var toPos = new Vector3Int(originX + side - 1 - iter, originY + r, 0);
                OverwriteTileInfo(ref tiles, fromTile, toPos);
                //ValidatePositionsForRoom(tiles, roomOrigin);
            }

            // Overwrite top with the left deep copy
            for (int t = iter; t < side - iter; t++)
            {
                var fromTile = leftCopy[t - iter].DeepCopy();
                var toPos = new Vector3Int(originX + t, originY + side - 1 - iter, 0);
                OverwriteTileInfo(ref tiles, fromTile, toPos);
                //ValidatePositionsForRoom(tiles, roomOrigin);
            }
        }

        // Not efficient, remove this later
        ValidatePositionsForRoom(tiles, roomOrigin);
        return tiles;
    }

    private void OverwriteTileInfo(ref Dictionary<Vector3Int, TileInfo> tiles, TileInfo fromTile, Vector3Int newPos)
    {
        tiles[newPos] = fromTile.DeepCopy();
        tiles[newPos].Position = newPos;
        if (tiles[newPos].Interactable != null)
        {
            tiles[newPos].Interactable.Position = newPos;
        }

        // Move the player if they're here too
        if (!wasPlayerRotated && gameManager.Player.Position == fromTile.Position)
        {
            wasPlayerRotated = true;
            gameManager.Player.transform.position = tiles[newPos].CenterPosition;
            //Debug.Log($"Player rotated to {tiles[newPos].CenterPosition}");
        }
    }    

    public void ValidatePositionsForRoom(Dictionary<Vector3Int, TileInfo> tiles, Vector3Int roomOrigin)
    {
        for (int x = roomOrigin.x; x < roomOrigin.x + RoomFactory.RoomDimension; x++)
        {
            for (int y = roomOrigin.y; y < roomOrigin.y + RoomFactory.RoomDimension; y++)
            {
                var pos = new Vector3Int(x, y, 0);
                if (!tiles.ContainsKey(pos))
                {
                    throw new Exception($"{pos} does not exist in tile collection keys");
                }
                if (tiles[pos].Position != pos)
                {
                    throw new Exception($"{pos} did not match tile position {tiles[pos].Position} {tiles[pos].TileIdentifier}");
                }
                if (tiles[pos].Interactable != null && tiles[pos].Interactable.Position != pos)
                {
                    throw new Exception($"{pos} did not match interactable position {tiles[pos].Interactable.Position} {tiles[pos].Interactable.TileIdentifier}");
                }
            }
        }
    }

    private void IdentifyEnemies()
    {
        enemyIDs = new HashSet<TileIdentifiers>
        {
            TileIdentifiers.Enemy_Alien,
            TileIdentifiers.Enemy_Bat,
            TileIdentifiers.Enemy_Crab,
            TileIdentifiers.Enemy_Creeper,
            TileIdentifiers.Enemy_Cyclops,
            TileIdentifiers.Enemy_Demon,
            TileIdentifiers.Enemy_Ghost,
            TileIdentifiers.Enemy_Ghost2,
            TileIdentifiers.Enemy_Ghost3,
            TileIdentifiers.Enemy_Goblin,
            TileIdentifiers.Enemy_Ogre,
            TileIdentifiers.Enemy_Orc,
            TileIdentifiers.Enemy_Scorpion,
            TileIdentifiers.Enemy_Skeleton,
            TileIdentifiers.Enemy_Spider,
            TileIdentifiers.Enemy_Spider2,
            TileIdentifiers.Enemy_Spider3,
        };

    }

    private void IdentifyNPCs()
    {
        npcIDs = new HashSet<TileIdentifiers>
        {
            TileIdentifiers.NPC_Cat,
            TileIdentifiers.NPC_Cat2,
            TileIdentifiers.NPC_Chicken,
            TileIdentifiers.NPC_Cow,
            TileIdentifiers.NPC_Cowboy,
            TileIdentifiers.NPC_Dog,
            TileIdentifiers.NPC_Duck,
            TileIdentifiers.NPC_Dwarf,
            TileIdentifiers.NPC_Elder,
            TileIdentifiers.NPC_Elder2,
            TileIdentifiers.NPC_Faerie,
            TileIdentifiers.NPC_Fire,
            TileIdentifiers.NPC_Campfire,
            TileIdentifiers.NPC_FemaleYouth,
            TileIdentifiers.NPC_Guard,
            TileIdentifiers.NPC_HoodedMan,
            TileIdentifiers.NPC_Horse,
            TileIdentifiers.NPC_King,
            TileIdentifiers.NPC_Labcoat,
            TileIdentifiers.NPC_Labcoat2,
            TileIdentifiers.NPC_MaleYouth,
            TileIdentifiers.NPC_Monk,
            TileIdentifiers.NPC_Queen,
            TileIdentifiers.NPC_Robot,
            TileIdentifiers.NPC_Signpost,
            TileIdentifiers.NPC_Spearman,
            TileIdentifiers.NPC_Spearman2,
            TileIdentifiers.NPC_Swordsman,
            TileIdentifiers.NPC_Villain,
            TileIdentifiers.NPC_Wizard,
            TileIdentifiers.NPC_Woman,
            TileIdentifiers.NPC_Worker,
        };
    }

    private void IdentifyTriggers()
    {
        triggerIDs = new HashSet<TileIdentifiers>
        { 
            TileIdentifiers.RotateRight,
            TileIdentifiers.Dungeon_SpiderWeb,
        };
    }

    private void IdentifyTransitions()
    {
        transitionIDs = new HashSet<TileIdentifiers>
        { 
            TileIdentifiers.T_ExitToOverworld,
            TileIdentifiers.T_Dungeon1,
            TileIdentifiers.T_Dungeon2,
            TileIdentifiers.T_Dungeon3,
            TileIdentifiers.T_StairsDown,
            TileIdentifiers.T_StairsUp,
            TileIdentifiers.T_DoorEntrance,
            TileIdentifiers.T_WoodEntrance,
            TileIdentifiers.T_StartCave,
            TileIdentifiers.T_MarshCave,
            TileIdentifiers.T_TallBuilding,
            TileIdentifiers.T_Shop,
            TileIdentifiers.T_Entrance_Heavy,
            TileIdentifiers.T_Entrance_Light,
        };
    }

    private void IdentifyItems()
    {
        itemIDs = new HashSet<TileIdentifiers>
        {
            TileIdentifiers.Items_Buyable_AttackPower,
            TileIdentifiers.Items_Buyable_Bombs,
            TileIdentifiers.Items_Buyable_HealingPotion,
            TileIdentifiers.TickableItem_Ignited_Bomb,
            TileIdentifiers.Item_Apple,
            TileIdentifiers.Item_BlueDiamond,
            TileIdentifiers.Item_Bomb,
            TileIdentifiers.Item_Coin,
            TileIdentifiers.Item_Coins,
            TileIdentifiers.Item_GoldDiamond,
            TileIdentifiers.Item_Heart,
            TileIdentifiers.Item_HeartContainer,
            TileIdentifiers.Item_IceMagic,
            TileIdentifiers.Item_Pouch,
            TileIdentifiers.Item_Ruby,
            TileIdentifiers.Item_Sword1,
        };
    }

    private void IdentifyGroundTiles()
    {
        // Ground tiles are not interactable
        GroundTiles = new HashSet<TileIdentifiers>
        {
            TileIdentifiers.A_Bridge,
            TileIdentifiers.A_Crop,
            TileIdentifiers.A_Crop2,
            TileIdentifiers.A_Crop3,
            TileIdentifiers.A_Crop4,
            TileIdentifiers.A_Crop5,
            TileIdentifiers.A_CropDirtRows,
            TileIdentifiers.A_Grass,
            TileIdentifiers.A_Grass2,
            TileIdentifiers.A_Grass3,
            TileIdentifiers.Bone,
            TileIdentifiers.Cave_Debris,
            TileIdentifiers.Lettering_A,
            TileIdentifiers.Lettering_D,
            TileIdentifiers.Lettering_MoveSymbol,
            TileIdentifiers.Lettering_S,
            TileIdentifiers.Lettering_W,
            TileIdentifiers.Rocks,
            TileIdentifiers.Town_Ladder,
            TileIdentifiers.Town_Stool,
        };

    }

    private void IdentifyDestructables()
    {
        destructableIDs = new HashSet<TileIdentifiers>
        {
            TileIdentifiers.Cave_MediumRockWall,
            TileIdentifiers.Urn,
            TileIdentifiers.Urn2,
            TileIdentifiers.Skull_Animal,
            TileIdentifiers.Skull_Human,
        };
    }

    private void IdentifyInteractables()
    {
        interactableIDs = new HashSet<TileIdentifiers>
        {
            TileIdentifiers.Enemy_Alien,
            TileIdentifiers.Enemy_Bat,
            TileIdentifiers.Enemy_Crab,
            TileIdentifiers.Enemy_Creeper,
            TileIdentifiers.Enemy_Cyclops,
            TileIdentifiers.Enemy_Demon,
            TileIdentifiers.Enemy_Ghost,
            TileIdentifiers.Enemy_Ghost2,
            TileIdentifiers.Enemy_Ghost3,
            TileIdentifiers.Enemy_Goblin,
            TileIdentifiers.Enemy_Ogre,
            TileIdentifiers.Enemy_Orc,
            TileIdentifiers.Enemy_Scorpion,
            TileIdentifiers.Enemy_Skeleton,
            TileIdentifiers.Enemy_Spider,
            TileIdentifiers.Enemy_Spider2,
            TileIdentifiers.Enemy_Spider3,
            TileIdentifiers.NPC_Cat,
            TileIdentifiers.NPC_Cat2,
            TileIdentifiers.NPC_Chicken,
            TileIdentifiers.NPC_Cow,
            TileIdentifiers.NPC_Cowboy,
            TileIdentifiers.NPC_Dog,
            TileIdentifiers.NPC_Duck,
            TileIdentifiers.NPC_Dwarf,
            TileIdentifiers.NPC_Elder,
            TileIdentifiers.NPC_Elder2,
            TileIdentifiers.NPC_Fire,
            TileIdentifiers.NPC_Campfire,
            TileIdentifiers.NPC_Faerie,
            TileIdentifiers.NPC_FemaleYouth,
            TileIdentifiers.NPC_Guard,
            TileIdentifiers.NPC_HoodedMan,
            TileIdentifiers.NPC_Horse,
            TileIdentifiers.NPC_King,
            TileIdentifiers.NPC_Labcoat,
            TileIdentifiers.NPC_Labcoat2,
            TileIdentifiers.NPC_MaleYouth,
            TileIdentifiers.NPC_Monk,
            TileIdentifiers.NPC_Queen,
            TileIdentifiers.NPC_Robot,
            TileIdentifiers.NPC_Signpost,
            TileIdentifiers.NPC_Spearman,
            TileIdentifiers.NPC_Spearman2,
            TileIdentifiers.NPC_Swordsman,
            TileIdentifiers.NPC_Villain,
            TileIdentifiers.NPC_Wizard,
            TileIdentifiers.NPC_Woman,
            TileIdentifiers.NPC_Worker,
            TileIdentifiers.Item_Apple,
            TileIdentifiers.Item_BlueDiamond,
            TileIdentifiers.Item_Bomb,
            TileIdentifiers.Item_Coin,
            TileIdentifiers.Item_Coins,
            TileIdentifiers.Item_GoldDiamond,
            TileIdentifiers.Item_Heart,
            TileIdentifiers.Item_HeartContainer,
            TileIdentifiers.Item_IceMagic,
            TileIdentifiers.Item_Pouch,
            TileIdentifiers.Item_Ruby,
            TileIdentifiers.Item_Sword1,
            TileIdentifiers.Items_Buyable_AttackPower,
            TileIdentifiers.Items_Buyable_Bombs,
            TileIdentifiers.Items_Buyable_HealingPotion,
            TileIdentifiers.Dungeon_SpiderWeb,
            TileIdentifiers.RotateRight,
            TileIdentifiers.TickableItem_Ignited_Bomb,
            TileIdentifiers.T_ExitToOverworld,
            TileIdentifiers.T_Dungeon1,
            TileIdentifiers.T_Dungeon2,
            TileIdentifiers.T_Dungeon3,
            TileIdentifiers.T_StairsDown,
            TileIdentifiers.T_StairsUp,
            TileIdentifiers.T_DoorEntrance,
            TileIdentifiers.T_WoodEntrance,
            TileIdentifiers.T_StartCave,
            TileIdentifiers.T_MarshCave,            
            TileIdentifiers.T_TallBuilding,            
            TileIdentifiers.T_Shop,            
            TileIdentifiers.T_Entrance_Heavy,
            TileIdentifiers.T_Entrance_Light,
        };
    }

    public void CalculateAdjacencyList(TileInfo tileInfo)
    {
        tileInfo.Reset();
        var directionToCheck = Vector3Int.zero;
        foreach (var dir in Enum.GetValues(typeof(Directions)).Cast<Directions>())
        {
            TileInfo targetTile = null;
            directionToCheck = tileInfo.Position;
            directionToCheck += DirectionValues[(int)dir];
            tileInfo.adjacencyList.Add(targetTile);
        }
    }

    public void DrawMapForScene(Scenes scene)
    {
        gameManager.GroundMap.ClearAllTiles();
        gameManager.CollisionMap.ClearAllTiles();
        int largestX = 0;
        int largestY = 0;
        foreach (var pos in Tiles[scene].Keys)
        {
            largestX = Mathf.Max(largestX, pos.x);
            largestY = Mathf.Max(largestY, pos.y);
        }

        int x = 0;
        int y = largestY;

        foreach (var tile in Tiles[scene].Values)
        {
            if (x >= largestX)
            {
                x = 0;
                y -= 1;
            }

            DrawOneTile(tile);
            x += 1;
        }
    }

    public void DrawOneTile(TileInfo tile)
    {
        if (tile.Interactable != null)
        {
            //Debug.Log($"Drawing interactable {tile.Interactable.TileIdentifier}");
            if (itemIDs.Contains(tile.Interactable.TileIdentifier) || transitionIDs.Contains(tile.Interactable.TileIdentifier) || triggerIDs.Contains(tile.Interactable.TileIdentifier))
            {
                gameManager.GroundMap.SetTile(tile.Position, RoomFactory.GetRefForTile(tile.Interactable.TileIdentifier));
            }
            else
            {
                gameManager.CollisionMap.SetTile(tile.Position, RoomFactory.GetRefForTile(tile.Interactable.TileIdentifier));
            }
        }
        else if (!GroundTiles.Contains(tile.TileIdentifier))
        {
            gameManager.GroundMap.SetTile(tile.Position, null);
            gameManager.CollisionMap.SetTile(tile.Position, RoomFactory.GetRefForTile(tile.TileIdentifier));
        }
        else
        {
            gameManager.GroundMap.SetTile(tile.Position, RoomFactory.GetRefForTile(tile.TileIdentifier));
            gameManager.CollisionMap.SetTile(tile.Position, null);
        }
    }

    public void StartTickBehavior(Scenes scene)
    {
        foreach (var tile in Tiles[scene].Values)
        {
            RegisterTickableAtTile(tile);
        }
        Turns = Turns.OrderBy(x => x.Speed).ToList();
    }

    public NPC CreateNPCAtRuntime(Scenes scene, Vector3Int position, TileIdentifiers id)
    {
        var npc = new NPC(gameManager, id)
        {
            Scene = scene,
            Position = position
        };
        Tiles[scene][position].Interactable = npc;
        
        // Allow them to take turns too
        RegisterTickableAtTile(Tiles[scene][position]);
        Turns = Turns.OrderBy(x => x.Speed).ToList();
        return npc;
    }

    private void RegisterTickableAtTile(TileInfo tile)
    {
        if (tile.Interactable != null)
        {
            if (tile.Interactable is NPC)
            {
                var npc = tile.Interactable as NPC;
                //Debug.Log($"Starting behavior for NPC {npc.TileIdentifier}...");
                Turns.Add(npc);
            }
            else if (tile.Interactable is Enemy)
            {
                var enemy = tile.Interactable as Enemy;
                //Debug.Log($"Starting behavior for Enemy {enemy.TileIdentifier}...");
                Turns.Add(enemy);
            }
            else if (tile.Interactable is TickableItem)
            {
                var tickableItem = tile.Interactable as TickableItem;
                Turns.Add(tickableItem);
            }
        }
    }

    public void UnregisterTickableAtTile(TileInfo tile)
    {
        if (tile.Interactable != null && tile.Interactable is ITickable)
        {
            var tickable = tile.Interactable as ITickable;
            for (int i = 0; i < Turns.Count; i++)
            {
                if (Turns[i] == tickable)
                {
                    Turns.RemoveAt(i);
                    break;
                }
            }
        }
    }

    public void RunTick()
    {
        for (int i = 0; i < Turns.Count; i++)
        {
            Turns[i].Tick();
            if (gameManager.Player.Hp == 0)
            {
                gameManager.Camera.transform.SetParent(null);
                gameManager.Player.gameObject.SetActive(false);
                gameManager.UIManager.ChangeState<GameOverState>();
                break;
            }
        }
    }

    public void StopAllTickBehavior()
    {
        Turns.Clear();
    }

    private void LinkAllTransitions()
    {
        foreach (var scene in Enum.GetValues(typeof(Scenes)).Cast<Scenes>())
        {
            LinkTransitionsForScene(scene);
        }
    }

    private void LinkTransitionsForScene(Scenes scene)
    {
        if (!Transitions.ContainsKey(scene))
        {
            //Debug.LogWarning($"{nameof(LinkTransitionsForScene)} could not find any transitions for {scene}");
            return;
        }

        foreach (var transition in Transitions[scene])
        {
            // Get the target transition that matches the current scene being iterated over
            //Debug.Log($"Linking transition in {transition.Scene} {transition.Position} to {transition.SceneToTransitionTo}...");
            var targetTransition = Transitions[transition.SceneToTransitionTo].Where(x => x.SceneToTransitionTo == scene).First();
            if (targetTransition != null)
            {
                transition.PositionToTransitionTo = targetTransition.Position;
            }
        }
    }

    public bool IsEnemy(TileIdentifiers id)
    {
        if (enemyIDs.Contains(id))
        {
            return true;
        }
        return false;
    }

    public bool IsNPC(TileIdentifiers id)
    {
        if (npcIDs.Contains(id))
        {
            return true;
        }
        return false;
    }

    public bool IsInteractable(TileIdentifiers id)
    {
        if (interactableIDs.Contains(id))
        {
            return true;
        }
        return false;
    }
    
    public bool IsDestructable(TileIdentifiers id)
    {
        if(destructableIDs.Contains(id))
        {
            return true;
        }
        return false;
    }

    public bool IsTransition(TileIdentifiers id)
    {
        if (transitionIDs.Contains(id))
        {
            return true;
        }
        return false;
    }

    public bool IsItem(TileIdentifiers id)
    {
        if (itemIDs.Contains(id))
        {
            return true;
        }
        return false;
    }

    public bool IsTrigger(TileIdentifiers id)
    {
        if (triggerIDs.Contains(id))
        {
            return true;
        }
        return false;
    }

    private void AddInteractableToMap(Scenes scene, Vector3Int pos, Interactable toAdd)
    {
        if (!Tiles[scene].ContainsKey(pos))
        {
           //Debug.LogWarning($"Inconsistent move by {toAdd.TileIdentifier} at {pos}");
        }
        if (Tiles[scene][pos].Interactable != null)
        {
            throw new Exception($"Interactable {Tiles[scene][pos].Interactable.TileIdentifier} already at {pos}");
        }
        Tiles[scene][pos].Interactable = toAdd;
        toAdd.Position = pos;
    }

    public void AddOneTileInfoToMap(Scenes scene, Vector3Int pos, TileInfo toAdd)
    {
        if (Tiles[scene].ContainsKey(pos))
        {
            throw new Exception($"Overlapping tiles at {pos}");
        }
        else
        {
            toAdd.Position = pos;
            Tiles[scene].Add(pos, toAdd);
        }

        if (Tiles[scene][pos].Interactable != null)
        {
            if (Tiles[scene][pos].TileIdentifier == toAdd.TileIdentifier && toAdd.TileIdentifier != TileIdentifiers.Empty)
            {
                throw new Exception($"Tile id should not be set to interactable {toAdd.TileIdentifier}");
            }
            Tiles[scene][pos].Interactable.Position = pos;
            Tiles[scene][pos].Interactable.Scene = scene;

            // Register the transition if there is one, position to transition to gets set during LinkAllTransitions()
            if (IsTransition(toAdd.Interactable.TileIdentifier))
            {
                AddToTransitions(scene, toAdd.Interactable as Transition);
            }
        }
    }

    public Room GetRoomAssociatedWithPosition(Scenes scene, Vector3Int mapPosition)
    {
        foreach (var room in RoomGrid[scene].Values)
        {
            if ((room.Origin.x <= mapPosition.x && room.Origin.y <= mapPosition.y)
                && (room.Origin.x + RoomFactory.RoomDimension > mapPosition.x && room.Origin.y + RoomFactory.RoomDimension > mapPosition.y))
            {
                return room;
            }
        }
        throw new Exception($"Room could not be associated with {mapPosition} for scene {scene}");
    }

    public void MoveInteractable(Interactable mover, Vector3Int newPos)
    {
        // Remove from old position
        Tiles[mover.Scene][mover.Position].Interactable = null;
        // Add to new position
        AddInteractableToMap(mover.Scene, newPos, mover);
    }

    public void AddToTransitions(Scenes scene, Transition toAdd)
    {
        if (!Transitions.ContainsKey(scene))
        {
            Transitions.Add(scene, new List<Transition>());
        }
        Transitions[scene].Add(toAdd);
    }
    private void PrepareDictionaries()
    {
        GameStates.Clear();

        Tiles.Clear();
        foreach (var scene in Enum.GetValues(typeof(Scenes)).Cast<Scenes>())
        {
            Tiles.Add(scene, new Dictionary<Vector3Int, TileInfo>());
        }

        Transitions.Clear();
        foreach (var scene in Enum.GetValues(typeof(Scenes)).Cast<Scenes>())
        {
            Transitions.Add(scene, new List<Transition>());
        }

        RoomGrid.Clear();
        foreach (var scene in Enum.GetValues(typeof(Scenes)).Cast<Scenes>())
        {
            RoomGrid.Add(scene, new Dictionary<Vector3Int, Room>());
        }

        RoomDefinitions_Overworld.Clear();
        RoomDefsByOrientation_Overworld.Clear();
        foreach (var orientation in Enum.GetValues(typeof(RoomOrientations)).Cast<RoomOrientations>())
        {
            RoomDefsByOrientation_Overworld.Add(orientation, new List<RoomDefinition>());
        }


        RoomDefinitions_SkeletonCave.Clear();
        RoomDefsByOrientation_SkeletonCave.Clear();
        foreach (var orientation in Enum.GetValues(typeof(RoomOrientations)).Cast<RoomOrientations>())
        {
            RoomDefsByOrientation_SkeletonCave.Add(orientation, new List<RoomDefinition>());
        }

        RoomDefinitions_Volcano.Clear();
        RoomDefsByOrientation_Volcano.Clear();
        foreach (var orientation in Enum.GetValues(typeof(RoomOrientations)).Cast<RoomOrientations>())
        {
            RoomDefsByOrientation_Volcano.Add(orientation, new List<RoomDefinition>());
        }

        RoomDefinitions_Catacombs.Clear();
        RoomDefsByOrientation_Catacombs.Clear();
        foreach (var orientation in Enum.GetValues(typeof(RoomOrientations)).Cast<RoomOrientations>())
        {
            RoomDefsByOrientation_Catacombs.Add(orientation, new List<RoomDefinition>());
        }

        RoomDefinitions_Interiors.Clear();
        RoomDefsByOrientation_Interiors.Clear();
        foreach (var orientation in Enum.GetValues(typeof(RoomOrientations)).Cast<RoomOrientations>())
        {
            RoomDefsByOrientation_Interiors.Add(orientation, new List<RoomDefinition>());
        }

        Turns.Clear();
    }

    private void OnDisable()
    {
        StopAllTickBehavior();
    }

    public Dictionary<RoomLabels, RoomDefinition> GetRoomDefinitionsDictionaryForSceneType(SceneTypes sceneType)
    {
        switch (sceneType)
        {
            case SceneTypes.Overworld:
                return RoomDefinitions_Overworld;
            case SceneTypes.Interiors:
                return RoomDefinitions_Interiors;
            case SceneTypes.SkeletonCave:
                return RoomDefinitions_SkeletonCave;
            case SceneTypes.Volcano:
                return RoomDefinitions_Volcano;
            case SceneTypes.Catacombs:
                return RoomDefinitions_Catacombs;
            default:
                throw new Exception($"Unrecognized scene type {sceneType}");
        }
    }

    public Dictionary<RoomOrientations, List<RoomDefinition>> GetRoomDefinitionsDictionaryByOrientationForSceneType(SceneTypes sceneType)
    {
        switch (sceneType)
        {
            case SceneTypes.Overworld:
                return RoomDefsByOrientation_Overworld;
            case SceneTypes.SkeletonCave:
                return RoomDefsByOrientation_SkeletonCave;
            case SceneTypes.Volcano:
                return RoomDefsByOrientation_Volcano;
            case SceneTypes.Catacombs:
                return RoomDefsByOrientation_Catacombs;
            case SceneTypes.Interiors:
                return RoomDefsByOrientation_Interiors;
            default:
                throw new Exception($"Unrecognized scene type {sceneType}");
        }
    }

    public TickableItem CreateTickableItemAtRuntime(TileIdentifiers id, Scenes scene, Vector3Int position)
    {
        if (Tiles[scene][position].Interactable != null)
        {
            throw new Exception($"Already an interactable at {scene} {position}");
        }

        var tickableItem = new TickableItem(gameManager)
        {
            TileIdentifier = id,
            Tile = RoomFactory.GetRefForTile(id),
            Scene = scene,
            Position = position
        };
        Tiles[scene][position].Interactable = tickableItem;

        // Start ticking the item too
        RegisterTickableAtTile(Tiles[scene][position]);
        Turns = Turns.OrderBy(x => x.Speed).ToList();
        return tickableItem;
    }

    public Item CreateItemAtRuntime(TileIdentifiers id, Scenes scene, Vector3Int position)
    {
        if(Tiles[scene][position].Interactable != null)
        {
            throw new Exception($"Already an interactable at {scene} {position}");
        }

        switch (id)
        {
            case TileIdentifiers.Item_Sword1:
                break;
        }

        var item = new Item(gameManager)
        {
            TileIdentifier = id,
            Tile = RoomFactory.GetRefForTile(id),
            Scene = scene,
            Position = position
        };
        Tiles[scene][position].Interactable = item;
        return item;
    }
}
