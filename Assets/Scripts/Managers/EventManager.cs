﻿using System;
using UnityEngine;

public class EventManager : MonoBehaviour
{
    public static event EventHandler<InfoEventArgs<Point>> MoveEvent;
    public static event EventHandler<EventArgs> ConfirmEvent;
    public static event EventHandler<EventArgs> TickEvent;
    public static event EventHandler<EventArgs> CancelEvent;
    public static event EventHandler<EventArgs> SelectLeftEvent;
    public static event EventHandler<EventArgs> SelectRightEvent;
    public static event EventHandler<EventArgs> ChangeDeviceMouseEvent;
    public static event EventHandler<EventArgs> ChangeDeviceNonMouseEvent;
    public static event EventHandler<EventArgs> DoneControlMapperEvent;
    public static event EventHandler<EventArgs> OnEndDialogueEvent;
    public static event EventHandler<EventArgs> RefreshLastControllerTypeUsedEvent;

    public void Setup()
    {
    }

    public void TriggerEndDialogue(object sender)
    {
        OnEndDialogueEvent?.Invoke(sender, new EventArgs());
    }

    public void TriggerDoneControlMapperEvent(object sender)
    {
        DoneControlMapperEvent?.Invoke(sender, new EventArgs());
    }

    public void TriggerMoveEvent(object sender, InfoEventArgs<Point> move)
    {
        MoveEvent?.Invoke(sender, move);
    }

    public void TriggerTickEvent(object sender)
    {
        TickEvent?.Invoke(sender, new EventArgs());
    }

    public void TriggerChangeDeviceMouseEvent(object sender)
    {
        ChangeDeviceMouseEvent?.Invoke(sender, new EventArgs());
    }

    public void TriggerChangeDeviceNonMouseEvent(object sender)
    {
        ChangeDeviceNonMouseEvent?.Invoke(sender, new EventArgs());
    }

    public void TriggerConfirmEvent(object sender)
    {
        ConfirmEvent?.Invoke(sender, new EventArgs());
    }

    public void TriggerCancelEvent(object sender)
    {
        CancelEvent?.Invoke(sender, new EventArgs());
    }

    public void TriggerSelectLeftEvent(object sender)
    {
        SelectLeftEvent?.Invoke(sender, new EventArgs());
    }

    public void TriggerSelectRightEvent(object sender)
    {
        SelectRightEvent?.Invoke(sender, new EventArgs());
    }
}
