﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Tilemaps;

public class RoomFactory
{
    public const int RoomDimension = 7;
    private IGameManager gameManager { get; set; }
    private DataManager dataManager { get; set; }
    private TileRefs tileRefs { get { return dataManager.TileRefs; } }
    
    private Dictionary<Vector3Int, string> stringOrientations = new Dictionary<Vector3Int, string>();
    private List<Vector3Int> criticalPath = new List<Vector3Int>();
    private HashSet<Vector3Int> criticalPathHashSet = new HashSet<Vector3Int>();

    public RoomFactory(IGameManager gameManager)
    {
        this.gameManager = gameManager;
        this.dataManager = gameManager.DataManager;
    }

    public void SetupRoomArrangements()
    {
        // Todo: Assemble sections here!
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_0_0, new Vector3Int(0,0,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_1_0, new Vector3Int(1,0,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_2_0, new Vector3Int(2,0,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_3_0, new Vector3Int(3,0,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_4_0, new Vector3Int(4,0,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_5_0, new Vector3Int(5,0,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_6_0, new Vector3Int(6,0,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_7_0, new Vector3Int(7,0,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_8_0, new Vector3Int(8,0,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_0_1, new Vector3Int(0,1,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_1_1, new Vector3Int(1,1,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_2_1, new Vector3Int(2,1,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_3_1, new Vector3Int(3,1,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_4_1, new Vector3Int(4,1,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_5_1, new Vector3Int(5,1,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_6_1, new Vector3Int(6,1,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_7_1, new Vector3Int(7,1,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_8_1, new Vector3Int(8,1,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_0_2, new Vector3Int(0,2,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_1_2, new Vector3Int(1,2,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_2_2, new Vector3Int(2,2,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_3_2, new Vector3Int(3,2,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_4_2, new Vector3Int(4,2,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_5_2, new Vector3Int(5,2,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_6_2, new Vector3Int(6,2,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_7_2, new Vector3Int(7,2,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_8_2, new Vector3Int(8,2,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_0_3, new Vector3Int(0,3,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_1_3, new Vector3Int(1,3,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_2_3, new Vector3Int(2,3,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_3_3, new Vector3Int(3,3,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_4_3, new Vector3Int(4,3,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_5_3, new Vector3Int(5,3,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_6_3, new Vector3Int(6,3,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_7_3, new Vector3Int(7,3,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_8_3, new Vector3Int(8,3,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_0_4, new Vector3Int(0,4,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_1_4, new Vector3Int(1,4,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_2_4, new Vector3Int(2,4,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_3_4, new Vector3Int(3,4,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_4_4, new Vector3Int(4,4,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_5_4, new Vector3Int(5,4,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_6_4, new Vector3Int(6,4,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_7_4, new Vector3Int(7,4,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_8_4, new Vector3Int(8,4,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_0_5, new Vector3Int(0,5,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_1_5, new Vector3Int(1,5,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_2_5, new Vector3Int(2,5,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_3_5, new Vector3Int(3,5,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_4_5, new Vector3Int(4,5,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_5_5, new Vector3Int(5,5,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_6_5, new Vector3Int(6,5,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_7_5, new Vector3Int(7,5,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_8_5, new Vector3Int(8,5,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_0_6, new Vector3Int(0,6,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_1_6, new Vector3Int(1,6,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_2_6, new Vector3Int(2,6,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_3_6, new Vector3Int(3,6,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_4_6, new Vector3Int(4,6,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_5_6, new Vector3Int(5,6,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_6_6, new Vector3Int(6,6,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_7_6, new Vector3Int(7,6,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_8_6, new Vector3Int(8,6,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_0_7, new Vector3Int(0,7,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_1_7, new Vector3Int(1,7,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_2_7, new Vector3Int(2,7,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_3_7, new Vector3Int(3,7,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_4_7, new Vector3Int(4,7,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_5_7, new Vector3Int(5,7,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_6_7, new Vector3Int(6,7,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_7_7, new Vector3Int(7,7,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_8_7, new Vector3Int(8,7,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_0_8, new Vector3Int(0,8,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_1_8, new Vector3Int(1,8,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_2_8, new Vector3Int(2,8,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_3_8, new Vector3Int(3,8,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_4_8, new Vector3Int(4,8,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_5_8, new Vector3Int(5,8,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_6_8, new Vector3Int(6,8,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_7_8, new Vector3Int(7,8,0)));
        AddRoomToGrid(Scenes.Overworld, new Room(gameManager, Scenes.Overworld, SceneTypes.Overworld, RoomLabels.Overworld_8_8, new Vector3Int(8,8,0)));
        AddRoomToGrid(Scenes.Interior_StartCave, new Room(gameManager, Scenes.Interior_StartCave, SceneTypes.Interiors, RoomLabels.Interior_StartCave, new Vector3Int(0, 0, 0)));
        AddRoomToGrid(Scenes.Interior_MarshCave, new Room(gameManager, Scenes.Interior_MarshCave, SceneTypes.Interiors, RoomLabels.Interior_MarshCave, new Vector3Int(0, 0, 0)));
        AddRoomToGrid(Scenes.Interior_TallBuilding, new Room(gameManager, Scenes.Interior_TallBuilding, SceneTypes.Interiors, RoomLabels.Interior_TallBuilding, new Vector3Int(0, 0, 0)));
        AddRoomToGrid(Scenes.Interior_Shop, new Room(gameManager, Scenes.Interior_Shop, SceneTypes.Interiors, RoomLabels.Interior_Shop, new Vector3Int(0, 0, 0)));       

        GenerateDungeon(Scenes.Dungeon1_SkeletonCave, SceneTypes.SkeletonCave, 6, 6);
        GenerateDungeon(Scenes.Dungeon2_Volcano, SceneTypes.Volcano, 8, 8);
        GenerateDungeon(Scenes.Dungeon3_Catacombs, SceneTypes.Catacombs, 15, 15);
        //dataManager.ValidatePositionsForRoom(rightRoom.Tiles, rightRoom.Origin);
        //rightRoom.Rotate();
        //AddRoomToMap(Scenes.Dungeon1, rightRoom);
    }  

    public void SetupInteriorsDefinitions()
    {
        var startCave = new string[] {
          //  1     2     3     4     5     6     7   
            "003","003","003","003","003","003","003", // 1
            "003","000","000","000","000","000","003", // 2
            "003","722","000","168","000","722","003", // 3
            "003","000","000","000","000","000","003", // 4
            "003","000","000","000","000","000","003", // 5
            "003","000","000","000","000","000","003", // 6
            "003","003","003","-99","003","003","003", // 7
        };
        dataManager.RoomDefinitions_Interiors.Add(RoomLabels.Interior_StartCave,
            new RoomDefinition(gameManager, SceneTypes.Interiors, RoomLabels.Interior_StartCave, startCave, RoomOrientations.OOOO));

        var marshCave = new string[] {
          //  1     2     3     4     5     6     7   
            "003","003","003","003","003","003","003", // 1
            "003","000","494","000","494","000","003", // 2
            "003","494","000","311","000","494","003", // 3
            "003","000","000","000","000","000","003", // 4
            "003","000","000","000","000","000","003", // 5
            "003","1032","000","000","000","000","003", // 6
            "003","003","003","-99","003","003","003", // 7
        };
        dataManager.RoomDefinitions_Interiors.Add(RoomLabels.Interior_MarshCave,
            new RoomDefinition(gameManager, SceneTypes.Interiors, RoomLabels.Interior_MarshCave, marshCave, RoomOrientations.OOOO));

        var tallBuilding = new string[] {
          //  1     2     3     4     5     6     7   
            "629","629","629","629","629","629","629", // 1
            "629","338","388","000","388","383","629", // 2
            "629","000","000","174","000","000","629", // 3
            "629","000","000","000","000","000","629", // 4
            "629","384","000","000","000","000","629", // 5
            "629","385","000","000","000","000","629", // 6
            "629","629","629","-99","629","629","629", // 7
        };
        dataManager.RoomDefinitions_Interiors.Add(RoomLabels.Interior_TallBuilding,
            new RoomDefinition(gameManager, SceneTypes.Interiors, RoomLabels.Interior_TallBuilding, tallBuilding, RoomOrientations.OOOO));

        var shop = new string[] {
          //  1     2     3     4     5     6     7   
            "629","629","629","629","629","629","629", // 1
            "629","000","000","000","000","000","629", // 2
            "629","000","000","000","000","000","629", // 3
            "629","000","000","000","000","000","629", // 4
            "629","000","167","000","000","000","629", // 5
            "629","000","000","000","000","000","629", // 6
            "629","629","629","-99","629","629","629", // 7
        };
        dataManager.RoomDefinitions_Interiors.Add(RoomLabels.Interior_Shop,
            new RoomDefinition(gameManager, SceneTypes.Interiors, RoomLabels.Interior_Shop, shop, RoomOrientations.OOOO));


    }

    #region GetRefForTile
    public TileBase? GetRefForTile(TileIdentifiers tileIdentifier)
    {
        switch (tileIdentifier)
        {
            case TileIdentifiers.T_ExitToOverworld:
                return null;
            case TileIdentifiers.Items_Buyable_HealingPotion:
                return tileRefs.Item_HealingPotion;
            case TileIdentifiers.Items_Buyable_Bombs:
                return tileRefs.Item_Bomb;
            case TileIdentifiers.Items_Buyable_AttackPower:
                return tileRefs.Item_Sword1;
            case TileIdentifiers.TickableItem_Ignited_Bomb:
                return tileRefs.Item_Bomb;
            case TileIdentifiers.T_Dungeon1:
            case TileIdentifiers.T_Dungeon2:
            case TileIdentifiers.T_Dungeon3:
            case TileIdentifiers.T_StairsDown:
                return tileRefs.Dungeon_StairsDown;
            case TileIdentifiers.Empty:
                return null;
            case TileIdentifiers.A_Bridge:
                return tileRefs.A_Bridge;
            case TileIdentifiers.A_Bush:
                return tileRefs.A_Bush;
            case TileIdentifiers.A_Cactus:
                return tileRefs.A_Cactus;
            case TileIdentifiers.A_Cactus2:
                return tileRefs.A_Cactus2;
            case TileIdentifiers.NPC_Campfire:
                return tileRefs.A_Campfire;
            case TileIdentifiers.A_Crop:
                return tileRefs.A_Crop;
            case TileIdentifiers.A_Crop2:
                return tileRefs.A_Crop2;
            case TileIdentifiers.A_Crop3:
                return tileRefs.A_Crop3;
            case TileIdentifiers.A_Crop4:
                return tileRefs.A_Crop4;
            case TileIdentifiers.A_Crop5:
                return tileRefs.A_Crop5;
            case TileIdentifiers.A_CropDirtRows:
                return tileRefs.A_CropDirtRows;
            case TileIdentifiers.A_DeadBranches:
                return tileRefs.A_DeadBranches;
            case TileIdentifiers.T_DoorEntrance:
                return tileRefs.A_DoorEntrance;
            case TileIdentifiers.A_Grass:
                return tileRefs.A_Grass;
            case TileIdentifiers.A_Grass2:
                return tileRefs.A_Grass2;
            case TileIdentifiers.A_Grass3:
                return tileRefs.A_Grass3;
            case TileIdentifiers.A_Grave:
                return tileRefs.A_Grave;
            case TileIdentifiers.A_Grave2:
                return tileRefs.A_Grave2;
            case TileIdentifiers.A_OakTree:
                return tileRefs.A_OakTree;
            case TileIdentifiers.A_PalmTree:
                return tileRefs.A_PalmTree;
            case TileIdentifiers.A_PineTree:
                return tileRefs.A_PineTree;
            case TileIdentifiers.A_PineTree2:
                return tileRefs.A_PineTree2;
            case TileIdentifiers.A_PineTree3:
                return tileRefs.A_PineTree3;
            case TileIdentifiers.A_Stump:
                return tileRefs.A_Stump;
            case TileIdentifiers.A_Stump2:
                return tileRefs.A_Stump2;
            case TileIdentifiers.A_Stump3:
                return tileRefs.A_Stump3;
            case TileIdentifiers.A_Thorns:
                return tileRefs.A_Thorns;
            case TileIdentifiers.A_Vines:
                return tileRefs.A_Vines;
            case TileIdentifiers.A_Water:
                return tileRefs.A_Water;
            case TileIdentifiers.Bone:
                return tileRefs.Bone;
            case TileIdentifiers.BrownTorch:
                return tileRefs.BrownTorch;
            case TileIdentifiers.Cave_Debris:
                return tileRefs.Cave_Debris;
            case TileIdentifiers.Cave_MediumRockWall:
                return tileRefs.Cave_MedRockWall;
            case TileIdentifiers.Cave_BigRockWall:
                return tileRefs.Cave_BigRockWall;
            case TileIdentifiers.Cave_HugeRockWall:
                return tileRefs.Cave_HugeRockWall;
            case TileIdentifiers.Chest_Closed:
                return tileRefs.Chest_Closed;
            case TileIdentifiers.Chest_Open:
                return tileRefs.Chest_Open;
            case TileIdentifiers.Dungeon_Candelabra:
                return tileRefs.Dungeon_Candelabra;
            case TileIdentifiers.Dungeon_Crate:
                return tileRefs.Dungeon_Crate;
            case TileIdentifiers.Dungeon_Floor:
                return tileRefs.Dungeon_Floor;
            case TileIdentifiers.Dungeon_Floor2:
                return tileRefs.Dungeon_Floor2;
            case TileIdentifiers.Dungeon_Lava:
                return tileRefs.Dungeon_Lava;
            case TileIdentifiers.Dungeon_Lava2:
                return tileRefs.Dungeon_Lava2;
            case TileIdentifiers.Dungeon_Lava3:
                return tileRefs.Dungeon_Lava3;
            case TileIdentifiers.Dungeon_SpiderWeb:
                return tileRefs.Dungeon_SpiderWeb;
            case TileIdentifiers.T_StairsUp:
                return tileRefs.Dungeon_StairsUp;
            case TileIdentifiers.Enemy_Alien:
                return tileRefs.Enemy_Alien;
            case TileIdentifiers.Enemy_Bat:
                return tileRefs.Enemy_Bat;
            case TileIdentifiers.Enemy_Crab:
                return tileRefs.Enemy_Crab;
            case TileIdentifiers.Enemy_Creeper:
                return tileRefs.Enemy_Creeper;
            case TileIdentifiers.Enemy_Cyclops:
                return tileRefs.Enemy_Cyclops;
            case TileIdentifiers.Enemy_Demon:
                return tileRefs.Enemy_Demon;
            case TileIdentifiers.Enemy_Ghost:
                return tileRefs.Enemy_Ghost;
            case TileIdentifiers.Enemy_Ghost2:
                return tileRefs.Enemy_Ghost2;
            case TileIdentifiers.Enemy_Ghost3:
                return tileRefs.Enemy_Ghost3;
            case TileIdentifiers.Enemy_Goblin:
                return tileRefs.Enemy_Goblin;
            case TileIdentifiers.Enemy_Ogre:
                return tileRefs.Enemy_Ogre;
            case TileIdentifiers.Enemy_Orc:
                return tileRefs.Enemy_Orc;
            case TileIdentifiers.Enemy_Scorpion:
                return tileRefs.Enemy_Scorpion;
            case TileIdentifiers.Enemy_Spider:
                return tileRefs.Enemy_Spider;
            case TileIdentifiers.Enemy_Skeleton:
                return tileRefs.Enemy_Skeleton;
            case TileIdentifiers.Enemy_Spider2:
                return tileRefs.Enemy_Spider2;
            case TileIdentifiers.Enemy_Spider3:
                return tileRefs.Enemy_Spider3;
            case TileIdentifiers.T_Entrance_Heavy:
            case TileIdentifiers.T_StartCave:
            case TileIdentifiers.T_MarshCave:
            case TileIdentifiers.T_TallBuilding:
            case TileIdentifiers.T_Shop:
                return tileRefs.Entrance_Heavy;
            case TileIdentifiers.T_Entrance_Light:
                return tileRefs.Entrance_Light;
            case TileIdentifiers.NPC_Fire:
                return tileRefs.Fire;
            case TileIdentifiers.GoldTorch:
                return tileRefs.GoldTorch;    
            case TileIdentifiers.Item_Apple:
                return tileRefs.Item_Apple;
            case TileIdentifiers.Item_BlueDiamond:
                return tileRefs.Item_BlueDiamond;
            case TileIdentifiers.Item_Bomb:
                return tileRefs.Item_Bomb;
            case TileIdentifiers.Item_Coin:
                return tileRefs.Item_Coin;
            case TileIdentifiers.Item_Coins:
                return tileRefs.Item_Coins;
            case TileIdentifiers.Item_GoldDiamond:
                return tileRefs.Item_GoldDiamond;
            case TileIdentifiers.Item_Heart:
                return tileRefs.Item_Heart;
            case TileIdentifiers.Item_HeartContainer:
                return tileRefs.Item_HeartContainer;
            case TileIdentifiers.Item_IceMagic:
                return tileRefs.Item_IceMagic;
            case TileIdentifiers.Item_Pouch:
                return tileRefs.Item_Pouch;
            case TileIdentifiers.Item_Ruby:
                return tileRefs.Item_Ruby;
            case TileIdentifiers.Item_Sword1:
                return tileRefs.Item_Sword1;
            case TileIdentifiers.Lettering_A:
                return tileRefs.Lettering_A;
            case TileIdentifiers.Lettering_D:
                return tileRefs.Lettering_D;
            case TileIdentifiers.Lettering_MoveSymbol:
                return tileRefs.Lettering_MoveSymbol;
            case TileIdentifiers.Lettering_S:
                return tileRefs.Lettering_S;
            case TileIdentifiers.Lettering_W:
                return tileRefs.Lettering_W;
            case TileIdentifiers.Minecart_Horizontal:
                return tileRefs.Minecart_Horizontal;
            case TileIdentifiers.Minecart_Vertical:
                return tileRefs.Minecart_Vertical;
            case TileIdentifiers.NPC_Cat:
                return tileRefs.NPC_Cat;
            case TileIdentifiers.NPC_Cat2:
                return tileRefs.NPC_Cat2;
            case TileIdentifiers.NPC_Chicken:
                return tileRefs.NPC_Chicken;
            case TileIdentifiers.NPC_Cow:
                return tileRefs.NPC_Cow;
            case TileIdentifiers.NPC_Cowboy:
                return tileRefs.NPC_Cowboy;
            case TileIdentifiers.NPC_Dog:
                return tileRefs.NPC_Dog;
            case TileIdentifiers.NPC_Duck:
                return tileRefs.NPC_Duck;
            case TileIdentifiers.NPC_Dwarf:
                return tileRefs.NPC_Dwarf;
            case TileIdentifiers.NPC_Elder:
                return tileRefs.NPC_Elder;
            case TileIdentifiers.NPC_Elder2:
                return tileRefs.NPC_Elder2;
            case TileIdentifiers.NPC_Faerie:
                return tileRefs.NPC_Faerie;
            case TileIdentifiers.NPC_FemaleYouth:
                return tileRefs.NPC_FemaleYouth;
            case TileIdentifiers.NPC_Guard:
                return tileRefs.NPC_Guard;
            case TileIdentifiers.NPC_HoodedMan:
                return tileRefs.NPC_HoodedMan;
            case TileIdentifiers.NPC_Horse:
                return tileRefs.NPC_Horse;
            case TileIdentifiers.NPC_King:
                return tileRefs.NPC_King;
            case TileIdentifiers.NPC_Labcoat:
                return tileRefs.NPC_Labcoat;
            case TileIdentifiers.NPC_Labcoat2:
                return tileRefs.NPC_Labcoat2;
            case TileIdentifiers.NPC_MaleYouth:
                return tileRefs.NPC_MaleYouth;
            case TileIdentifiers.NPC_Monk:
                return tileRefs.NPC_Monk;
            case TileIdentifiers.NPC_Queen:
                return tileRefs.NPC_Queen;
            case TileIdentifiers.NPC_Robot:
                return tileRefs.NPC_Robot;
            case TileIdentifiers.NPC_Signpost:
                return tileRefs.NPC_Signpost;
            case TileIdentifiers.NPC_Spearman:
                return tileRefs.NPC_Spearman;
            case TileIdentifiers.NPC_Spearman2:
                return tileRefs.NPC_Spearman2;
            case TileIdentifiers.NPC_Swordsman:
                return tileRefs.NPC_Swordsman;
            case TileIdentifiers.NPC_Villain:
                return tileRefs.NPC_Villain;
            case TileIdentifiers.NPC_Wizard:
                return tileRefs.NPC_Wizard;
            case TileIdentifiers.NPC_Woman:
                return tileRefs.NPC_Woman;
            case TileIdentifiers.NPC_Worker:
                return tileRefs.NPC_Worker;
            case TileIdentifiers.Rocks:
                return tileRefs.Rocks;
            case TileIdentifiers.RotateRight:
                return tileRefs.RotateRight;
            case TileIdentifiers.Skull_Animal:
                return tileRefs.Skull_Animal;
            case TileIdentifiers.Skull_Human:
                return tileRefs.Skull_Human;
            case TileIdentifiers.Town_Banners:
                return tileRefs.Town_Banners;
            case TileIdentifiers.Town_Bed:
                return tileRefs.Town_Bed;
            case TileIdentifiers.Town_Bell:
                return tileRefs.Town_Bell;
            case TileIdentifiers.Town_Bookcase:
                return tileRefs.Town_Bookcase;
            case TileIdentifiers.Town_Brick:
                return tileRefs.Town_Brick;
            case TileIdentifiers.Town_Brick2:
                return tileRefs.Town_Brick2;
            case TileIdentifiers.Town_Chimney:
                return tileRefs.Town_Chimney;
            case TileIdentifiers.Town_Chimney2:
                return tileRefs.Town_Chimney2;
            case TileIdentifiers.Town_Crate:
                return tileRefs.Town_Crate;
            case TileIdentifiers.Town_Fence:
                return tileRefs.Town_Fence;
            case TileIdentifiers.Town_Flag:
                return tileRefs.Town_Flag;
            case TileIdentifiers.Town_Gong:
                return tileRefs.Town_Gong;
            case TileIdentifiers.Town_Keg:
                return tileRefs.Town_Keg;
            case TileIdentifiers.Town_Ladder:
                return tileRefs.Town_Ladder;
            case TileIdentifiers.Town_Mirror:
                return tileRefs.Town_Mirror;
            case TileIdentifiers.Town_OilDrum:
                return tileRefs.Town_OilDrum;
            case TileIdentifiers.Town_Roofing:
                return tileRefs.Town_Roofing;
            case TileIdentifiers.Town_Roofing2:
                return tileRefs.Town_Roofing2;
            case TileIdentifiers.Town_Roofing3:
                return tileRefs.Town_Roofing3;
            case TileIdentifiers.Town_RoofingLeft:
                return tileRefs.Town_RoofingLeft;
            case TileIdentifiers.Town_RoofingRight:
                return tileRefs.Town_RoofingRight;
            case TileIdentifiers.Town_Stool:
                return tileRefs.Town_Stool;
            case TileIdentifiers.Town_Stove:
                return tileRefs.Town_Stove;
            case TileIdentifiers.Town_Table:
                return tileRefs.Town_Table;
            case TileIdentifiers.Town_WateringCan:
                return tileRefs.Town_WateringCan;
            case TileIdentifiers.Town_Well:
                return tileRefs.Town_Well;
            case TileIdentifiers.Town_Window:
                return tileRefs.Town_Window;
            case TileIdentifiers.Urn:
                return tileRefs.Urn;
            case TileIdentifiers.Urn2:
                return tileRefs.Urn2;
            case TileIdentifiers.T_WoodEntrance:
                return tileRefs.WoodEntrance;
            default:
                throw new Exception($"{tileIdentifier} {(int)tileIdentifier} not recognized");
        }
        //if (tileIdentifier != TileIdentifiers.Empty && tileIdentifier != TileIdentifiers.SceneStartPoint)
        //{
        //    Debug.LogWarning(tileIdentifier + " " + (int)tileIdentifier + " not recognized");
        //}
        //return null;
    }
    #endregion    

    public void AssembleAllRoomsForAllMaps()
    {
        foreach (var scene in dataManager.RoomGrid.Keys)
        {
            foreach (var room in dataManager.RoomGrid[scene].Values)
            {
                AssembleRoomForScene(scene, room);
            }
        }
    }

    private void AssembleRoomForScene(Scenes scene, Room room)
    {
        foreach (var mapPosition in room.Tiles.Keys)
        {
            //  Deep copy the object here
            var tileInfoFromDefinition = room.Tiles[mapPosition];
            Interactable newInteractable = null;
            if (tileInfoFromDefinition.Interactable != null)
            {
                newInteractable = tileInfoFromDefinition.Interactable.DeepCopy();
            }
            var newInfo = tileInfoFromDefinition.DeepCopy();
            dataManager.AddOneTileInfoToMap(scene, mapPosition, newInfo);
        }
    }


    private void AddRoomToGrid(Scenes scene, Room toAdd)
    {
        if (!dataManager.RoomGrid.ContainsKey(scene))
        {
            dataManager.RoomGrid.Add(scene, new Dictionary<Vector3Int, Room>());
        }
        dataManager.RoomGrid[scene].Add(toAdd.GridPosition, toAdd);
    }

    private void GenerateDungeon(Scenes scene, SceneTypes sceneType, int maxWidth, int maxHeight)
    {
        if (maxWidth < 2 || maxHeight < 2)
        {
            throw new Exception("Dungeon not big enough");
        }

        var startCorner = (MapCorners)DataManager.Random.Next(0, Enum.GetValues(typeof(MapCorners)).Length);
        var endCorner = GetOppositeCorner(startCorner);
        var startPosition = GetRandomPositionInCorner(startCorner, maxWidth, maxHeight);
        var endPosition = GetRandomPositionInCorner(endCorner, maxWidth, maxHeight);
        var dungeonRoomOrientations = GenerateDungeonOrientations(maxWidth, maxHeight, startPosition, endPosition);
        foreach (var kvp in dungeonRoomOrientations)
        {
            var position = kvp.Key;
            var type = RoomTypes.Other;
            if (position == startPosition)
            {
                type = RoomTypes.Start;
            }
            else if (position == endPosition)
            {
                type = RoomTypes.End;
            }
            AddRoomToGrid(scene, new Room(gameManager, scene, sceneType, GetRoomLabelWithOrientationAndType(sceneType, kvp.Value, type), position));
        }
    }

    private RoomLabels GetRoomLabelWithOrientationAndType(SceneTypes sceneType, RoomOrientations orientation, RoomTypes type)
    {
        var list = dataManager.GetRoomDefinitionsDictionaryByOrientationForSceneType(sceneType)[orientation];
        var matchedTypeList = new List<RoomDefinition>();
        foreach (var room in list)
        {
            if (room.RoomType == type)
            {
                // Get all of those definitions that are of the correct type (ie start, end, other)
                matchedTypeList.Add(room);
            }
        }

        if(matchedTypeList.Count < 0)
        {
            throw new Exception($"Could not find room definition for orientation {orientation} and type {type}, add more definitions");
        }

        // Return random room definition that is guaranteed to match the type and orientation
        return matchedTypeList[DataManager.Random.Next(0, matchedTypeList.Count)].RoomLabel;
    }

    private Dictionary<Vector3Int, RoomOrientations> GenerateDungeonOrientations(int maxWidth, int maxHeight, Vector3Int startPos, Vector3Int endPosition)
    {
        SetupCollections(maxWidth, maxHeight);
        criticalPath = GetCriticalPath(maxWidth, maxHeight, startPos, endPosition);
        SetStringOrientationsForPath(criticalPath);
        foreach (var position in criticalPath)
        {
            //Debug.Log(position);
            criticalPathHashSet.Add(position);
        }
        SetAllExtraneousPaths(maxWidth, maxHeight);
        return GetAllRoomOrientations(maxWidth, maxHeight);
    }

    private Dictionary<Vector3Int, RoomOrientations> GetAllRoomOrientations(int maxWidth, int maxHeight)
    {
        var result = new Dictionary<Vector3Int, RoomOrientations>();
        for (int w = 0; w < maxWidth; w++)
        {
            for (int h = 0; h < maxHeight; h++)
            {
                var position = new Vector3Int(w, h, 0);
                result.Add(position, ConvertStringToRoomOrientation(stringOrientations[position]));
            }
        }

        if (result.Values.Count != maxWidth * maxHeight)
        {
            throw new Exception($"{result.Values.Count} did not match map dimensions width {maxWidth} height {maxHeight}");
        }
        return result;
    }

    private void SetAllExtraneousPaths(int maxWidth, int maxHeight)
    {
        var connectedToCriticalPath = new HashSet<Vector3Int>();
        for (int w = 0; w < maxWidth; w++)
        {
            for (int h = 0; h < maxHeight; h++)
            {
                var position = new Vector3Int(w, h, 0);
                if (!criticalPathHashSet.Contains(position) && !connectedToCriticalPath.Contains(position))
                {
                    var extraneousPath = GetExtraneousPath(maxWidth, maxHeight, position, ref connectedToCriticalPath);
                    SetStringOrientationsForPath(extraneousPath);
                }
            }
        }
    }

    private List<Vector3Int> GetExtraneousPath(int maxWidth, int maxHeight, Vector3Int startPos, ref HashSet<Vector3Int> connectedToCriticalPath)
    {
        var pathStack = new Stack<Vector3Int>();
        Vector3Int next = startPos;
        Vector3Int noPath = new Vector3Int(-1, -1, 0);
        var visited = new HashSet<Vector3Int>();
        while (!criticalPathHashSet.Contains(next) && !connectedToCriticalPath.Contains(next))
        {
            next = MoveToNextNode(maxWidth, maxHeight, next, ref pathStack, ref visited);
            if (next == noPath)
            {
                if(pathStack.Count == 0)
                {
                   // Debug.LogWarning($"Extraneous path was empty. Start position was {startPos}, next {next}, visited count {visited.Count}. Resetting...");
                    next = new Vector3Int(startPos.x, startPos.y, startPos.z);
                    visited.Clear();
                    continue;
                }
                // Go back to the previous node in the critical path
                next = pathStack.Pop();
            }
        }

        var result = new List<Vector3Int>();
        result.Add(next);
        while (pathStack.Count > 0)
        {
            var position = pathStack.Pop();
            result.Add(position);
            connectedToCriticalPath.Add(position);
        }
        result.Reverse();
        return result;
    }

    private void SetupCollections(int maxWidth, int maxHeight)
    {
        criticalPath.Clear();
        criticalPathHashSet.Clear();
        stringOrientations.Clear();
        for (int w = 0; w < maxWidth; w++)
        {
            for (int h = 0; h < maxHeight; h++)
            {
                stringOrientations.Add(new Vector3Int(w, h, 0), "XXXX");
            }
        }
    }

    private void SetStringOrientationsForPath(List<Vector3Int> path)
    {
        for (int i = 0; i < path.Count - 1; i++)
        {
            SetStringOrientation(path[i], path[i + 1]);
        }
    }

    private void SetStringOrientation(Vector3Int current, Vector3Int next)
    {
        var sbCurrent = new StringBuilder(stringOrientations[current]);
        var sbNext = new StringBuilder(stringOrientations[next]);
        var difference = current - next;
        if (difference == Vector3Int.up)
        {
            sbCurrent[(int)Directions.South] = 'O';
            sbNext[(int)Directions.North] = 'O';
        }
        else if (difference == Vector3Int.right)
        {
            sbCurrent[(int)Directions.West] = 'O';
            sbNext[(int)Directions.East] = 'O';
        }
        else if (difference == Vector3Int.down)
        {
            sbCurrent[(int)Directions.North] = 'O';
            sbNext[(int)Directions.South] = 'O';
        }
        else if (difference == Vector3Int.left)
        {
            sbCurrent[(int)Directions.East] = 'O';
            sbNext[(int)Directions.West] = 'O';
        }
        else
        {
            throw new Exception($"Current tile {current} and next {next} were not adjacent");
        }
        stringOrientations[current] = sbCurrent.ToString();
        stringOrientations[next] = sbNext.ToString();
    }

    private List<Vector3Int> GetCriticalPath(int maxWidth, int maxHeight, Vector3Int startPos, Vector3Int endPosition)
    {
        var visited = new HashSet<Vector3Int>();
        var criticalPath = new Stack<Vector3Int>();
        Vector3Int next = startPos;
        Vector3Int noPath = new Vector3Int(-1, -1, 0);
        while (next != endPosition)
        {
            next = MoveToNextNode(maxWidth, maxHeight, next, ref criticalPath, ref visited);
            if (next == noPath)
            {
                if (criticalPath.Count == 0)
                {
                    //Debug.LogWarning($"Critical path was empty. Start position was {startPos}, next {next}, visited count {visited.Count}. Resetting...");
                    next = new Vector3Int(startPos.x, startPos.y, startPos.z);
					visited.Clear();
                    continue;
                }

                // Go back to the previous node in the critical path
                next = criticalPath.Pop();
            }
        }

        var result = new List<Vector3Int>();
        result.Add(endPosition);
        while (criticalPath.Count > 0)
        {
            result.Add(criticalPath.Pop());
        }
        result.Reverse();
        return result;
    }

    private Vector3Int MoveToNextNode(int maxWidth, int maxHeight, Vector3Int current, ref Stack<Vector3Int> pathStack, ref HashSet<Vector3Int> visited)
    {
        if (!visited.Contains(current))
        {
            visited.Add(current);
        }

        var maxNumDirections = Enum.GetValues(typeof(Directions)).Length - 1;
        var randomDir = DataManager.Random.Next(0, maxNumDirections);
        Directions direction;
        var nextPos = Vector3Int.zero;
        for (int i = 0; i < maxNumDirections; i++)
        {
            if (i + randomDir > maxNumDirections)
            {
                direction = (Directions)(randomDir + i - maxNumDirections);
            }
            else
            {
                direction = (Directions)(randomDir + i);
            }
            nextPos = dataManager.GetTilePositionInDirection(direction, current);
            if (!visited.Contains(nextPos) && nextPos.x >= 0 && nextPos.y >= 0
                && nextPos.x < maxWidth && nextPos.y < maxHeight)
            {
                pathStack.Push(current);
                return nextPos;
            }
        }
        // No path
        return new Vector3Int(-1, -1, 0);
    }

    private Vector3Int GetRandomPositionInCorner(MapCorners corner, int maxWidth, int maxHeight)
    {
        int x = 0;
        int y = 0;
        switch (corner)
        {
            case MapCorners.BottomLeft:
                x = DataManager.Random.Next(0, maxWidth / 2);
                y = DataManager.Random.Next(0, maxHeight / 2);
                break;
            case MapCorners.BottomRight:
                x = DataManager.Random.Next(maxWidth / 2, maxWidth);
                y = DataManager.Random.Next(0, maxHeight / 2);
                break;
            case MapCorners.TopLeft:
                x = DataManager.Random.Next(0, maxWidth / 2);
                y = DataManager.Random.Next(maxHeight / 2, maxHeight);
                break;
            case MapCorners.TopRight:
                x = DataManager.Random.Next(maxWidth / 2, maxWidth);
                y = DataManager.Random.Next(maxHeight / 2, maxHeight);
                break;
            default:
                throw new Exception($"Unrecognized corner {corner}");
        }
        //Debug.Log($"{corner}: {x}, {y}");
        return new Vector3Int(x, y, 0);
    }

    private MapCorners GetOppositeCorner(MapCorners corner)
    {
        switch (corner)
        {
            case MapCorners.BottomLeft:
                return MapCorners.TopRight;
            case MapCorners.BottomRight:
                return MapCorners.TopLeft;
            case MapCorners.TopLeft:
                return MapCorners.BottomRight;
            case MapCorners.TopRight:
                return MapCorners.BottomLeft;
            default:
                throw new Exception($"Unrecognized corner {corner}");
        }
    }

    private RoomOrientations ConvertStringToRoomOrientation(string stringOrientation)
    {
        switch (stringOrientation)
        {
            case "OOOO":
                return RoomOrientations.OOOO;
            case "OXXX":
                return RoomOrientations.OXXX;
            case "XOXX":
                return RoomOrientations.XOXX;
            case "XXOX":
                return RoomOrientations.XXOX;
            case "XXXO":
                return RoomOrientations.XXXO;
            case "XOOO":
                return RoomOrientations.XOOO;
            case "OXOO":
                return RoomOrientations.OXOO;
            case "OOXO":
                return RoomOrientations.OOXO;
            case "OOOX":
                return RoomOrientations.OOOX;
            case "OOXX":
                return RoomOrientations.OOXX;
            case "XOOX":
                return RoomOrientations.XOOX;
            case "OXXO":
                return RoomOrientations.OXXO;
            case "OXOX":
                return RoomOrientations.OXOX;
            case "XOXO":
                return RoomOrientations.XOXO;
            case "XXOO":
                return RoomOrientations.XXOO;
            case "XXXX":
                return RoomOrientations.XXXX;
            default:
                throw new Exception($"Unrecognized string orientation {stringOrientation}");
        }
    }
}
