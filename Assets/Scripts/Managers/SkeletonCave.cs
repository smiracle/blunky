﻿public static class SkeletonCave
{
    public static void Setup(IGameManager gameManager)
    {
        var dataManager = gameManager.DataManager;
        var dungeonStart1 = new string[] {
          //  1     2     3     4     5     6     7   
            "003","003","003","000","003","003","003", // 1
            "003","000","000","000","000","000","003", // 2
            "003","000","000","000","000","000","003", // 3
            "000","000","000","289","000","000","000", // 4
            "003","000","979","000","000","000","003", // 5
            "003","000","000","000","000","000","003", // 6
            "003","003","003","000","003","003","003", // 7
        };
        dataManager.RoomDefinitions_SkeletonCave.Add(RoomLabels.DungeonStart1,
            new RoomDefinition(gameManager, SceneTypes.SkeletonCave, RoomLabels.DungeonStart1, dungeonStart1, RoomOrientations.OOOO, RoomTypes.Start));

        var dungeonStart2 = new string[] {
          //  1     2     3     4     5     6     7   
            "003","003","003","000","003","003","003", // 1
            "003","000","000","000","000","000","003", // 2
            "003","000","000","000","000","000","003", // 3
            "000","000","000","289","000","979","000", // 4
            "003","000","000","000","000","000","003", // 5
            "003","000","000","000","000","000","003", // 6
            "003","003","003","000","003","003","003", // 7
        };
        dataManager.RoomDefinitions_SkeletonCave.Add(RoomLabels.DungeonStart2,
            new RoomDefinition(gameManager, SceneTypes.SkeletonCave, RoomLabels.DungeonStart2, dungeonStart2, RoomOrientations.XXOX, RoomTypes.Start));

        var dungeonStart3 = new string[] {
          //  1     2     3     4     5     6     7   
            "003","000","000","000","000","000","003", // 1
            "003","000","000","000","000","000","003", // 2
            "003","000","000","000","000","000","003", // 3
            "000","000","979","000","000","000","003", // 4
            "003","000","000","000","000","000","003", // 5
            "003","000","000","289","000","000","003", // 6
            "003","003","003","003","003","003","003", // 7
        };
        dataManager.RoomDefinitions_SkeletonCave.Add(RoomLabels.DungeonStart3,
            new RoomDefinition(gameManager, SceneTypes.SkeletonCave, RoomLabels.DungeonStart3, dungeonStart3, RoomOrientations.OXXX, RoomTypes.Start));

        var dungeonStart4 = new string[] {
          //  1     2     3     4     5     6     7   
            "003","003","003","003","003","003","003", // 1
            "000","000","000","000","000","000","000", // 2
            "000","000","000","289","000","000","000", // 3
            "000","000","000","000","000","979","000", // 4
            "000","000","000","000","000","000","000", // 5
            "000","000","000","000","000","000","000", // 6
            "000","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_SkeletonCave.Add(RoomLabels.DungeonStart4,
            new RoomDefinition(gameManager, SceneTypes.SkeletonCave, RoomLabels.DungeonStart4, dungeonStart4, RoomOrientations.XOOO, RoomTypes.Start));

        var dungeonStart5 = new string[] {
          //  1     2     3     4     5     6     7   
            "003","003","003","003","003","003","003", // 1
            "000","000","000","000","000","000","003", // 2
            "000","000","979","000","000","289","003", // 3
            "000","000","000","000","000","000","003", // 4
            "000","000","000","000","000","000","003", // 5
            "000","000","000","000","000","000","003", // 6
            "003","003","003","003","003","003","003", // 7
        };
        dataManager.RoomDefinitions_SkeletonCave.Add(RoomLabels.DungeonStart5,
            new RoomDefinition(gameManager, SceneTypes.SkeletonCave, RoomLabels.DungeonStart5, dungeonStart5, RoomOrientations.XXXO, RoomTypes.Start));

        var dungeonStart6 = new string[] {
          //  1     2     3     4     5     6     7   
            "003","003","003","003","003","003","003", // 1
            "000","000","000","000","000","289","003", // 2
            "000","000","000","000","000","000","003", // 3
            "000","000","000","000","000","979","003", // 4
            "000","000","607","000","000","000","003", // 5
            "000","000","000","000","000","000","003", // 6
            "000","000","000","000","000","000","003", // 7
        };
        dataManager.RoomDefinitions_SkeletonCave.Add(RoomLabels.DungeonStart6,
            new RoomDefinition(gameManager, SceneTypes.SkeletonCave, RoomLabels.DungeonStart6, dungeonStart6, RoomOrientations.XXOO, RoomTypes.Start));

        var dungeonStart7 = new string[] {
          //  1     2     3     4     5     6     7   
            "003","000","000","000","000","000","000", // 1
            "003","000","000","000","000","000","000", // 2
            "003","000","289","000","000","000","000", // 3
            "003","979","000","000","000","000","000", // 4
            "003","000","000","000","000","000","000", // 5
            "003","000","000","000","000","000","000", // 6
            "003","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_SkeletonCave.Add(RoomLabels.DungeonStart7,
            new RoomDefinition(gameManager, SceneTypes.SkeletonCave, RoomLabels.DungeonStart7, dungeonStart7, RoomOrientations.OOOX, RoomTypes.Start));

        var dungeonStart8 = new string[] {
          //  1     2     3     4     5     6     7   
            "003","003","003","003","003","003","003", // 1
            "003","289","000","000","000","000","000", // 2
            "003","000","000","000","000","000","000", // 3
            "003","000","000","000","000","000","000", // 4
            "003","000","000","000","000","000","979", // 5
            "003","000","000","000","000","000","000", // 6
            "003","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_SkeletonCave.Add(RoomLabels.DungeonStart8,
            new RoomDefinition(gameManager, SceneTypes.SkeletonCave, RoomLabels.DungeonStart8, dungeonStart8, RoomOrientations.XOOX, RoomTypes.Start));

        var dungeonStart9 = new string[] {
          //  1     2     3     4     5     6     7   
            "003","003","003","003","003","003","003", // 1
            "000","000","000","000","000","000","000", // 2
            "000","000","000","000","000","000","000", // 3
            "000","000","000","289","000","000","000", // 4
            "979","000","000","000","000","000","000", // 5
            "000","000","000","000","000","000","000", // 6
            "003","003","003","003","003","003","003", // 7
        };
        dataManager.RoomDefinitions_SkeletonCave.Add(RoomLabels.DungeonStart9,
            new RoomDefinition(gameManager, SceneTypes.SkeletonCave, RoomLabels.DungeonStart9, dungeonStart9, RoomOrientations.XOXO, RoomTypes.Start));

        var dungeonStart10 = new string[] {
          //  1     2     3     4     5     6     7   
            "003","003","003","003","003","003","003", // 1
            "003","000","000","000","000","000","979", // 2
            "003","000","000","000","000","000","000", // 3
            "003","000","000","289","000","607","000", // 4
            "003","000","000","000","000","000","000", // 5
            "003","000","000","000","000","000","000", // 6
            "003","003","003","003","003","003","003", // 7
        };
        dataManager.RoomDefinitions_SkeletonCave.Add(RoomLabels.DungeonStart10,
            new RoomDefinition(gameManager, SceneTypes.SkeletonCave, RoomLabels.DungeonStart10, dungeonStart10, RoomOrientations.XOXX, RoomTypes.Start));

        var dungeonStart11 = new string[] {
          //  1     2     3     4     5     6     7   
            "003","000","000","000","000","000","003", // 1
            "003","000","000","000","000","000","003", // 2
            "003","000","000","000","000","000","003", // 3
            "003","000","000","289","000","000","003", // 4
            "003","000","000","000","000","000","003", // 5
            "003","000","000","000","000","000","003", // 6
            "003","000","979","000","000","000","003", // 7
        };
        dataManager.RoomDefinitions_SkeletonCave.Add(RoomLabels.DungeonStart11,
            new RoomDefinition(gameManager, SceneTypes.SkeletonCave, RoomLabels.DungeonStart11, dungeonStart11, RoomOrientations.OXOX, RoomTypes.Start));

        var dungeonStart12 = new string[] {
          //  1     2     3     4     5     6     7   
            "003","000","000","000","000","000","000", // 1
            "003","000","000","000","000","000","000", // 2
            "003","000","000","000","000","000","000", // 3
            "003","000","000","289","000","000","000", // 4
            "003","000","000","000","000","000","000", // 5
            "003","000","000","000","000","979","000", // 6
            "003","003","003","003","003","003","003", // 7
        };
        dataManager.RoomDefinitions_SkeletonCave.Add(RoomLabels.DungeonStart12,
            new RoomDefinition(gameManager, SceneTypes.SkeletonCave, RoomLabels.DungeonStart12, dungeonStart12, RoomOrientations.OOXX, RoomTypes.Start));

        var dungeonStart13 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","003", // 1
            "000","979","000","000","000","000","003", // 2
            "000","000","607","000","000","000","003", // 3
            "000","000","000","289","000","000","003", // 4
            "000","000","000","000","000","000","003", // 5
            "000","000","000","000","000","000","003", // 6
            "000","000","000","000","000","000","003", // 7
        };
        dataManager.RoomDefinitions_SkeletonCave.Add(RoomLabels.DungeonStart13,
            new RoomDefinition(gameManager, SceneTypes.SkeletonCave, RoomLabels.DungeonStart13, dungeonStart13, RoomOrientations.OXOO, RoomTypes.Start));

        var dungeonStart14 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","000", // 1
            "000","000","000","000","000","000","000", // 2
            "000","000","000","000","000","000","000", // 3
            "000","213","000","289","000","000","000", // 4
            "000","000","000","000","000","000","000", // 5
            "000","000","000","000","000","000","979", // 6
            "003","003","003","003","003","003","003", // 7
        };
        dataManager.RoomDefinitions_SkeletonCave.Add(RoomLabels.DungeonStart14,
            new RoomDefinition(gameManager, SceneTypes.SkeletonCave, RoomLabels.DungeonStart14, dungeonStart14, RoomOrientations.OOXO, RoomTypes.Start));

        var dungeonStart15 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","003", // 1
            "000","979","000","000","000","000","003", // 2
            "000","000","000","000","000","000","003", // 3
            "000","000","000","289","000","000","003", // 4
            "000","000","000","000","000","000","003", // 5
            "000","000","000","000","000","000","003", // 6
            "003","003","003","003","003","003","003", // 7
        };
        dataManager.RoomDefinitions_SkeletonCave.Add(RoomLabels.DungeonStart15,
            new RoomDefinition(gameManager, SceneTypes.SkeletonCave, RoomLabels.DungeonStart15, dungeonStart15, RoomOrientations.OXXO, RoomTypes.Start));

        var dungeon1_skeles = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","000", // 1
            "000","000","000","979","000","000","000", // 2
            "000","000","000","000","000","000","000", // 3
            "000","000","000","316","000","000","000", // 4
            "000","000","000","000","000","000","000", // 5
            "000","607","000","000","000","000","000", // 6
            "000","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_SkeletonCave.Add(RoomLabels.DungeonSkeles1,
            new RoomDefinition(gameManager, SceneTypes.SkeletonCave, RoomLabels.DungeonSkeles1, dungeon1_skeles, RoomOrientations.OOOO));

        var dungeon1_empty = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","000", // 1
            "000","000","000","979","000","000","000", // 2
            "000","000","000","000","000","213","000", // 3
            "000","000","723","723","723","000","000", // 4
            "000","000","000","000","000","000","000", // 5
            "000","000","000","213","000","000","000", // 6
            "000","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_SkeletonCave.Add(RoomLabels.DungeonEmpty1,
            new RoomDefinition(gameManager, SceneTypes.SkeletonCave, RoomLabels.DungeonEmpty1, dungeon1_empty, RoomOrientations.OOOO));

        var dungeon1_bats = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","000", // 1
            "000","000","000","213","000","000","000", // 2
            "000","000","000","000","000","000","000", // 3
            "000","000","000","409","000","979","000", // 4
            "000","000","000","000","000","000","000", // 5
            "000","000","000","213","000","000","000", // 6
            "000","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_SkeletonCave.Add(RoomLabels.DungeonBats1,
            new RoomDefinition(gameManager, SceneTypes.SkeletonCave, RoomLabels.DungeonBats1, dungeon1_bats, RoomOrientations.OOOO));

        var dungeon1_spiders = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","000", // 1
            "000","000","000","564","000","000","000", // 2
            "000","000","000","000","564","000","000", // 3
            "000","000","000","267","000","000","000", // 4
            "000","979","000","000","564","000","000", // 5
            "000","000","000","564","000","000","000", // 6
            "000","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_SkeletonCave.Add(RoomLabels.DungeonSpiders1,
            new RoomDefinition(gameManager, SceneTypes.SkeletonCave, RoomLabels.DungeonSpiders1, dungeon1_spiders, RoomOrientations.OOOO));

        var dungeon2_skeles = new string[] {
          //  1     2     3     4     5     6     7   
            "003","000","000","000","000","000","003", // 1
            "003","000","000","000","000","000","003", // 2
            "003","000","000","000","000","000","003", // 3
            "003","316","000","979","000","316","003", // 4
            "003","000","213","000","000","000","003", // 5
            "003","000","000","000","000","000","003", // 6
            "003","000","000","000","000","000","003", // 7
        };
        dataManager.RoomDefinitions_SkeletonCave.Add(RoomLabels.DungeonSkeles2,
            new RoomDefinition(gameManager, SceneTypes.SkeletonCave, RoomLabels.DungeonSkeles2, dungeon2_skeles, RoomOrientations.OXOX));

        var dungeon2_spiders = new string[] {
          //  1     2     3     4     5     6     7   
            "003","000","000","000","000","000","003", // 1
            "003","000","000","000","000","000","003", // 2
            "003","000","000","000","000","000","003", // 3
            "003","564","564","000","564","564","003", // 4
            "003","267","000","000","000","000","003", // 5
            "003","000","000","979","000","000","003", // 6
            "003","000","000","000","000","000","003", // 7
        };
        dataManager.RoomDefinitions_SkeletonCave.Add(RoomLabels.DungeonSpiders2,
            new RoomDefinition(gameManager, SceneTypes.SkeletonCave, RoomLabels.DungeonSpiders2, dungeon2_spiders, RoomOrientations.OXOX));

        var dungeonSkeles3 = new string[] {
          //  1     2     3     4     5     6     7   
            "003","003","003","003","003","003","003", // 1
            "003","000","000","000","000","000","003", // 2
            "000","000","000","000","000","000","000", // 3
            "000","000","000","316","232","000","979", // 4
            "000","000","000","000","000","000","000", // 5
            "003","000","000","000","000","000","003", // 6
            "003","003","003","003","003","003","003", // 7
        };
        dataManager.RoomDefinitions_SkeletonCave.Add(RoomLabels.DungeonSkeles3,
            new RoomDefinition(gameManager, SceneTypes.SkeletonCave, RoomLabels.DungeonSkeles3, dungeonSkeles3, RoomOrientations.XOXO));

        var dungeon3_spiders = new string[] {
          //  1     2     3     4     5     6     7   
            "003","003","003","003","003","003","003", // 1
            "003","000","000","000","000","000","003", // 2
            "000","000","000","000","564","000","000", // 3
            "979","000","000","267","000","000","000", // 4
            "000","000","564","000","000","000","000", // 5
            "003","000","000","000","000","000","003", // 6
            "003","003","003","003","003","003","003", // 7
        };
        dataManager.RoomDefinitions_SkeletonCave.Add(RoomLabels.DungeonSpiders3,
            new RoomDefinition(gameManager, SceneTypes.SkeletonCave, RoomLabels.DungeonSpiders3, dungeon3_spiders, RoomOrientations.XOXO));

        var dungeonSkeles4 = new string[] {
          //  1     2     3     4     5     6     7   
            "003","003","003","003","003","003","003", // 1
            "003","000","000","000","000","000","003", // 2
            "000","000","000","000","000","000","003", // 3
            "000","000","000","000","000","979","003", // 4
            "000","000","607","232","000","000","003", // 5
            "000","000","000","000","000","000","003", // 6
            "000","000","000","000","000","003","003", // 7
        };
        dataManager.RoomDefinitions_SkeletonCave.Add(RoomLabels.DungeonSkeles4,
            new RoomDefinition(gameManager, SceneTypes.SkeletonCave, RoomLabels.DungeonSkeles4, dungeonSkeles4, RoomOrientations.XXOO));

        var dungeonSkeles5 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","003", // 1
            "000","000","000","000","000","000","003", // 2
            "000","000","000","000","000","000","003", // 3
            "000","000","000","316","000","979","003", // 4
            "000","000","000","000","000","000","003", // 5
            "000","000","000","000","000","000","003", // 6
            "003","003","003","003","003","003","003", // 7
        };
        dataManager.RoomDefinitions_SkeletonCave.Add(RoomLabels.DungeonSkeles5,
            new RoomDefinition(gameManager, SceneTypes.SkeletonCave, RoomLabels.DungeonSkeles5, dungeonSkeles5, RoomOrientations.OXXO));

        var dungeon_empty5 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","003", // 1
            "000","000","000","000","000","979","003", // 2
            "000","564","000","000","000","000","003", // 3
            "000","213","000","000","000","213","003", // 4
            "000","000","000","000","000","000","003", // 5
            "000","000","000","000","564","000","003", // 6
            "003","003","003","003","003","003","003", // 7
        };
        dataManager.RoomDefinitions_SkeletonCave.Add(RoomLabels.DungeonEmpty5,
            new RoomDefinition(gameManager, SceneTypes.SkeletonCave, RoomLabels.DungeonEmpty5, dungeon_empty5, RoomOrientations.OXXO));

        var dungeonSkeles6 = new string[] {
          //  1     2     3     4     5     6     7   
            "003","000","000","000","000","000","000", // 1
            "003","000","000","000","000","000","000", // 2
            "003","000","000","000","000","000","000", // 3
            "003","979","000","316","000","000","000", // 4
            "003","000","000","000","000","000","000", // 5
            "003","000","000","000","000","000","000", // 6
            "003","003","003","003","003","003","003", // 7
        };
        dataManager.RoomDefinitions_SkeletonCave.Add(RoomLabels.DungeonSkeles6,
            new RoomDefinition(gameManager, SceneTypes.SkeletonCave, RoomLabels.DungeonSkeles6, dungeonSkeles6, RoomOrientations.OOXX));

        var dungeon6_empty = new string[] {
          //  1     2     3     4     5     6     7   
            "003","000","000","000","000","000","000", // 1
            "003","000","000","000","000","000","000", // 2
            "003","000","000","000","000","000","000", // 3
            "003","000","000","213","000","979","000", // 4
            "003","000","000","000","000","000","000", // 5
            "003","000","000","000","000","000","000", // 6
            "003","003","003","003","003","003","003", // 7
        };
        dataManager.RoomDefinitions_SkeletonCave.Add(RoomLabels.DungeonEmpty6,
            new RoomDefinition(gameManager, SceneTypes.SkeletonCave, RoomLabels.DungeonEmpty6, dungeon6_empty, RoomOrientations.OOXX));

        var dungeonSkeles7 = new string[] {
          //  1     2     3     4     5     6     7   
            "003","003","003","003","003","003","003", // 1
            "003","000","000","000","000","000","003", // 2
            "000","000","000","000","000","000","000", // 3
            "000","000","000","316","000","000","000", // 4
            "000","000","000","000","000","000","000", // 5
            "003","000","607","000","000","000","003", // 6
            "003","000","000","000","000","000","003", // 7
        };
        dataManager.RoomDefinitions_SkeletonCave.Add(RoomLabels.DungeonSkeles7,
            new RoomDefinition(gameManager, SceneTypes.SkeletonCave, RoomLabels.DungeonSkeles7, dungeonSkeles7, RoomOrientations.XOOO));

        var dungeon7_spiders = new string[] {
          //  1     2     3     4     5     6     7   
            "003","003","003","003","003","003","003", // 1
            "003","000","000","000","000","000","003", // 2
            "000","000","000","000","267","000","000", // 3
            "000","979","564","564","564","000","000", // 4
            "000","000","000","000","564","267","000", // 5
            "003","000","000","000","000","000","003", // 6
            "003","000","000","000","000","000","003", // 7
        };
        dataManager.RoomDefinitions_SkeletonCave.Add(RoomLabels.DungeonSpiders7,
            new RoomDefinition(gameManager, SceneTypes.SkeletonCave, RoomLabels.DungeonSpiders7, dungeon7_spiders, RoomOrientations.XOOO));

        var dungeonSkeles8 = new string[] {
          //  1     2     3     4     5     6     7   
            "003","003","003","003","003","003","003", // 1
            "000","000","000","000","000","979","003", // 2
            "000","000","000","000","000","000","003", // 3
            "000","000","000","316","232","000","003", // 4
            "000","000","000","000","000","000","003", // 5
            "000","000","000","000","000","000","003", // 6
            "003","003","003","003","003","003","003", // 7
        };
        dataManager.RoomDefinitions_SkeletonCave.Add(RoomLabels.DungeonSkeles8,
            new RoomDefinition(gameManager, SceneTypes.SkeletonCave, RoomLabels.DungeonSkeles8, dungeonSkeles8, RoomOrientations.XXXO));

        var dungeon8_empty = new string[] {
          //  1     2     3     4     5     6     7   
            "003","003","003","003","003","003","003", // 1
            "000","000","000","000","000","000","003", // 2
            "000","000","000","722","000","000","003", // 3
            "000","000","979","000","232","000","003", // 4
            "000","000","000","722","000","000","003", // 5
            "000","000","000","000","000","000","003", // 6
            "003","003","003","003","003","003","003", // 7
        };
        dataManager.RoomDefinitions_SkeletonCave.Add(RoomLabels.DungeonEmpty8,
            new RoomDefinition(gameManager, SceneTypes.SkeletonCave, RoomLabels.DungeonEmpty8, dungeon8_empty, RoomOrientations.XXXO));

        var dungeonSkeles9 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","003", // 1
            "000","003","000","000","000","000","003", // 2
            "000","000","000","000","000","000","003", // 3
            "000","232","000","316","000","979","003", // 4
            "000","000","000","000","000","000","003", // 5
            "000","003","000","000","000","000","003", // 6
            "000","000","000","000","000","000","003", // 7
        };
        dataManager.RoomDefinitions_SkeletonCave.Add(RoomLabels.DungeonSkeles9,
            new RoomDefinition(gameManager, SceneTypes.SkeletonCave, RoomLabels.DungeonSkeles9, dungeonSkeles9, RoomOrientations.OXOO));

        var dungeon9_empty = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","003", // 1
            "000","564","000","000","000","000","003", // 2
            "000","000","564","000","000","000","003", // 3
            "000","000","237","518","000","979","003", // 4
            "000","000","564","000","000","000","003", // 5
            "000","564","000","000","000","000","003", // 6
            "000","000","000","000","000","000","003", // 7
        };
        dataManager.RoomDefinitions_SkeletonCave.Add(RoomLabels.DungeonEmpty9,
            new RoomDefinition(gameManager, SceneTypes.SkeletonCave, RoomLabels.DungeonEmpty9, dungeon9_empty, RoomOrientations.OXOO));

        var dungeon9_spiders = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","003", // 1
            "000","564","000","000","000","000","003", // 2
            "000","000","564","000","000","000","003", // 3
            "000","000","000","267","000","979","003", // 4
            "000","000","564","000","000","000","003", // 5
            "000","564","000","000","213","000","003", // 6
            "000","000","000","000","000","000","003", // 7
        };
        dataManager.RoomDefinitions_SkeletonCave.Add(RoomLabels.DungeonSpiders9,
            new RoomDefinition(gameManager, SceneTypes.SkeletonCave, RoomLabels.DungeonSpiders9, dungeon9_spiders, RoomOrientations.OXOO));

        var dungeonSkeles10 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","000", // 1
            "000","003","000","000","000","232","000", // 2
            "000","000","000","000","000","000","000", // 3
            "000","000","000","316","000","000","000", // 4
            "000","000","607","000","000","000","000", // 5
            "000","003","000","000","000","000","000", // 6
            "003","003","003","003","003","003","003", // 7
        };
        dataManager.RoomDefinitions_SkeletonCave.Add(RoomLabels.DungeonSkeles10,
            new RoomDefinition(gameManager, SceneTypes.SkeletonCave, RoomLabels.DungeonSkeles10, dungeonSkeles10, RoomOrientations.OOXO));

        var dungeonEmpty10 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","000", // 1
            "000","003","000","000","000","000","000", // 2
            "000","000","000","000","000","000","000", // 3
            "000","979","213","000","000","000","000", // 4
            "000","000","000","000","000","000","000", // 5
            "000","003","000","000","000","000","000", // 6
            "003","003","003","003","003","003","003", // 7
        };
        dataManager.RoomDefinitions_SkeletonCave.Add(RoomLabels.DungeonEmpty10,
            new RoomDefinition(gameManager, SceneTypes.SkeletonCave, RoomLabels.DungeonEmpty10, dungeonEmpty10, RoomOrientations.OOXO));

        var dungeon10_bats = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","000", // 1
            "000","000","000","000","000","000","000", // 2
            "000","000","000","000","000","000","000", // 3
            "000","000","237","409","979","000","000", // 4
            "000","000","000","000","000","000","000", // 5
            "000","000","000","000","232","000","000", // 6
            "003","003","003","003","003","003","003", // 7
        };
        dataManager.RoomDefinitions_SkeletonCave.Add(RoomLabels.DungeonBats10,
            new RoomDefinition(gameManager, SceneTypes.SkeletonCave, RoomLabels.DungeonBats10, dungeon10_bats, RoomOrientations.OOXO));

        var dungeonSkeles11 = new string[] {
          //  1     2     3     4     5     6     7   
            "003","003","003","003","003","003","003", // 1
            "003","003","000","000","000","000","003", // 2
            "003","000","607","000","000","000","003", // 3
            "003","000","000","316","000","000","003", // 4
            "003","000","000","000","000","000","003", // 5
            "003","003","000","000","000","979","003", // 6
            "003","000","000","000","000","000","003", // 7
        };
        dataManager.RoomDefinitions_SkeletonCave.Add(RoomLabels.DungeonSkeles11,
            new RoomDefinition(gameManager, SceneTypes.SkeletonCave, RoomLabels.DungeonSkeles11, dungeonSkeles11, RoomOrientations.XXOX));

        var dungeon11_bats = new string[] {
          //  1     2     3     4     5     6     7   
            "003","003","003","003","003","003","003", // 1
            "003","003","000","000","000","979","003", // 2
            "003","000","607","000","213","000","003", // 3
            "003","000","000","409","000","000","003", // 4
            "003","000","000","000","000","000","003", // 5
            "003","003","000","000","000","003","003", // 6
            "003","000","000","000","000","000","003", // 7
        };
        dataManager.RoomDefinitions_SkeletonCave.Add(RoomLabels.DungeonBats11,
            new RoomDefinition(gameManager, SceneTypes.SkeletonCave, RoomLabels.DungeonBats11, dungeon11_bats, RoomOrientations.XXOX));

        var dungeonSkeles12 = new string[] {
          //  1     2     3     4     5     6     7   
            "003","000","000","000","000","000","003", // 1
            "003","000","000","000","000","000","003", // 2
            "003","000","000","000","000","000","003", // 3
            "003","000","000","316","000","000","003", // 4
            "003","000","000","000","232","000","003", // 5
            "003","000","000","000","000","000","003", // 6
            "003","003","003","003","003","003","003", // 7
        };
        dataManager.RoomDefinitions_SkeletonCave.Add(RoomLabels.DungeonSkeles12,
            new RoomDefinition(gameManager, SceneTypes.SkeletonCave, RoomLabels.DungeonSkeles12, dungeonSkeles12, RoomOrientations.OXXX));

        var dungeon12_empty = new string[] {
          //  1     2     3     4     5     6     7   
            "003","000","000","000","000","000","003", // 1
            "003","000","000","000","000","000","003", // 2
            "003","000","896","000","896","000","003", // 3
            "003","000","000","979","000","000","003", // 4
            "003","000","896","000","896","000","003", // 5
            "003","000","000","000","000","000","003", // 6
            "003","003","003","003","003","003","003", // 7
        };
        dataManager.RoomDefinitions_SkeletonCave.Add(RoomLabels.DungeonEmpty12,
            new RoomDefinition(gameManager, SceneTypes.SkeletonCave, RoomLabels.DungeonEmpty12, dungeon12_empty, RoomOrientations.OXXX));

        var dungeon12_bats = new string[] {
          //  1     2     3     4     5     6     7   
            "003","000","000","000","000","000","003", // 1
            "003","000","000","000","000","000","003", // 2
            "003","000","213","000","000","000","003", // 3
            "003","409","000","979","000","409","003", // 4
            "003","000","000","000","000","000","003", // 5
            "003","000","000","000","000","000","003", // 6
            "003","003","003","003","003","003","003", // 7
        };
        dataManager.RoomDefinitions_SkeletonCave.Add(RoomLabels.DungeonBats12,
            new RoomDefinition(gameManager, SceneTypes.SkeletonCave, RoomLabels.DungeonBats12, dungeon12_bats, RoomOrientations.OXXX));

        var dungeon13_bats = new string[] {
          //  1     2     3     4     5     6     7   
            "003","000","000","000","000","000","000", // 1
            "003","000","000","000","000","000","000", // 2
            "003","000","000","000","000","000","000", // 3
            "003","000","000","409","000","000","000", // 4
            "003","000","000","000","000","000","000", // 5
            "003","000","000","000","000","000","000", // 6
            "003","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_SkeletonCave.Add(RoomLabels.DungeonBats13,
            new RoomDefinition(gameManager, SceneTypes.SkeletonCave, RoomLabels.DungeonBats13, dungeon13_bats, RoomOrientations.OOOX));

        var dungeonSkeles14 = new string[] {
          //  1     2     3     4     5     6     7   
            "003","003","003","003","003","003","003", // 1
            "003","000","000","000","000","000","000", // 2
            "003","000","000","232","000","000","000", // 3
            "003","979","000","409","000","000","000", // 4
            "003","000","000","000","000","000","000", // 5
            "003","000","000","000","000","000","000", // 6
            "003","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_SkeletonCave.Add(RoomLabels.DungeonSkeles14,
            new RoomDefinition(gameManager, SceneTypes.SkeletonCave, RoomLabels.DungeonSkeles14, dungeonSkeles14, RoomOrientations.XOOX));

        var dungeon15_bats = new string[] {
          //  1     2     3     4     5     6     7   
            "003","003","003","003","003","003","003", // 1
            "003","000","000","000","000","000","000", // 2
            "003","000","000","000","000","000","000", // 3
            "003","213","000","000","409","000","000", // 4
            "003","000","000","000","000","000","000", // 5
            "003","000","000","000","000","000","000", // 6
            "003","003","003","003","003","003","003", // 7
        };
        dataManager.RoomDefinitions_SkeletonCave.Add(RoomLabels.DungeonBats15,
            new RoomDefinition(gameManager, SceneTypes.SkeletonCave, RoomLabels.DungeonBats15, dungeon15_bats, RoomOrientations.XOXX));

        var dungeonEnd1 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","000", // 1
            "000","213","607","000","607","000","000", // 2
            "000","000","000","000","000","000","000", // 3
            "000","000","000","172","000","000","000", // 4
            "000","000","000","000","000","000","000", // 5
            "000","000","607","000","607","232","000", // 6
            "000","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_SkeletonCave.Add(RoomLabels.DungeonEnd1,
            new RoomDefinition(gameManager, SceneTypes.SkeletonCave, RoomLabels.DungeonEnd1, dungeonEnd1, RoomOrientations.OOOO, RoomTypes.End));

        var dungeonEnd2 = new string[] {
          //  1     2     3     4     5     6     7   
            "003","003","003","003","003","003","003", // 1
            "003","232","607","000","607","000","003", // 2
            "003","000","000","000","000","000","003", // 3
            "003","000","000","172","000","979","003", // 4
            "003","000","000","000","000","000","003", // 5
            "003","000","000","000","000","232","003", // 6
            "003","000","000","000","000","000","003", // 7
        };
        dataManager.RoomDefinitions_SkeletonCave.Add(RoomLabels.DungeonEnd2,
            new RoomDefinition(gameManager, SceneTypes.SkeletonCave, RoomLabels.DungeonEnd2, dungeonEnd2, RoomOrientations.XXOX, RoomTypes.End));

        var dungeonEnd3 = new string[] {
          //  1     2     3     4     5     6     7   
            "003","003","003","003","003","003","003", // 1
            "003","232","000","000","000","000","000", // 2
            "003","000","000","000","000","000","000", // 3
            "003","000","000","172","000","000","000", // 4
            "003","000","607","000","607","000","000", // 5
            "003","232","000","000","000","000","000", // 6
            "003","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_SkeletonCave.Add(RoomLabels.DungeonEnd3,
            new RoomDefinition(gameManager, SceneTypes.SkeletonCave, RoomLabels.DungeonEnd3, dungeonEnd3, RoomOrientations.XOOX, RoomTypes.End));

        var dungeonEnd4 = new string[] {
          //  1     2     3     4     5     6     7   
            "003","003","003","003","003","003","003", // 1
            "000","232","000","000","000","000","003", // 2
            "000","000","000","000","607","000","003", // 3
            "000","000","000","172","000","000","003", // 4
            "000","000","607","000","000","000","003", // 5
            "000","232","000","000","000","000","003", // 6
            "000","000","000","000","000","000","003", // 7
        };
        dataManager.RoomDefinitions_SkeletonCave.Add(RoomLabels.DungeonEnd4,
            new RoomDefinition(gameManager, SceneTypes.SkeletonCave, RoomLabels.DungeonEnd4, dungeonEnd4, RoomOrientations.XXOO, RoomTypes.End));

        var dungeonEnd5 = new string[] {
          //  1     2     3     4     5     6     7   
            "003","003","003","003","003","003","003", // 1
            "000","232","000","000","000","000","000", // 2
            "000","000","000","000","000","000","000", // 3
            "000","607","000","172","000","607","000", // 4
            "000","000","000","000","000","000","000", // 5
            "000","232","000","000","000","979","000", // 6
            "003","003","003","003","003","003","003", // 7
        };
        dataManager.RoomDefinitions_SkeletonCave.Add(RoomLabels.DungeonEnd5,
            new RoomDefinition(gameManager, SceneTypes.SkeletonCave, RoomLabels.DungeonEnd5, dungeonEnd5, RoomOrientations.XOXO, RoomTypes.End));

        var dungeonEnd6 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","003", // 1
            "000","000","000","000","000","000","003", // 2
            "000","000","000","000","000","000","003", // 3
            "000","607","000","172","000","607","003", // 4
            "000","000","000","000","000","000","003", // 5
            "000","232","000","000","000","232","003", // 6
            "003","003","003","003","003","003","003", // 7
        };
        dataManager.RoomDefinitions_SkeletonCave.Add(RoomLabels.DungeonEnd6,
            new RoomDefinition(gameManager, SceneTypes.SkeletonCave, RoomLabels.DungeonEnd6, dungeonEnd6, RoomOrientations.OXXO, RoomTypes.End));

        var dungeonEnd7 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","003", // 1
            "000","000","000","000","000","000","003", // 2
            "000","000","000","000","000","000","003", // 3
            "000","607","000","172","000","607","003", // 4
            "000","000","000","000","000","000","003", // 5
            "000","232","000","000","000","232","003", // 6
            "000","000","000","000","000","000","003", // 7
        };
        dataManager.RoomDefinitions_SkeletonCave.Add(RoomLabels.DungeonEnd7,
            new RoomDefinition(gameManager, SceneTypes.SkeletonCave, RoomLabels.DungeonEnd7, dungeonEnd7, RoomOrientations.OXOO, RoomTypes.End));

        var dungeonEnd8 = new string[] {
          //  1     2     3     4     5     6     7   
            "003","000","000","000","000","000","000", // 1
            "003","000","000","000","000","000","000", // 2
            "003","000","607","000","000","000","000", // 3
            "003","607","000","172","000","000","000", // 4
            "003","000","607","000","000","000","000", // 5
            "003","232","000","000","000","232","000", // 6
            "003","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_SkeletonCave.Add(RoomLabels.DungeonEnd8,
            new RoomDefinition(gameManager, SceneTypes.SkeletonCave, RoomLabels.DungeonEnd8, dungeonEnd8, RoomOrientations.OOOX, RoomTypes.End));

        var dungeonEnd9 = new string[] {
          //  1     2     3     4     5     6     7   
            "003","000","000","000","000","000","000", // 1
            "003","000","000","000","000","232","000", // 2
            "003","607","000","000","000","000","000", // 3
            "003","000","000","000","000","000","000", // 4
            "003","000","172","000","000","000","000", // 5
            "003","232","000","000","607","000","000", // 6
            "003","003","003","003","003","003","003", // 7
        };
        dataManager.RoomDefinitions_SkeletonCave.Add(RoomLabels.DungeonEnd9,
            new RoomDefinition(gameManager, SceneTypes.SkeletonCave, RoomLabels.DungeonEnd9, dungeonEnd9, RoomOrientations.OOXX, RoomTypes.End));

        var dungeonEnd10 = new string[] {
          //  1     2     3     4     5     6     7   
            "003","003","003","003","003","003","003", // 1
            "000","000","607","000","000","000","003", // 2
            "000","000","000","000","000","000","003", // 3
            "000","000","000","000","172","000","003", // 4
            "000","232","000","000","000","232","003", // 5
            "000","000","607","000","000","000","003", // 6
            "003","003","003","003","003","003","003", // 7
        };
        dataManager.RoomDefinitions_SkeletonCave.Add(RoomLabels.DungeonEnd10,
            new RoomDefinition(gameManager, SceneTypes.SkeletonCave, RoomLabels.DungeonEnd10, dungeonEnd10, RoomOrientations.XXXO, RoomTypes.End));

        var dungeonEnd11 = new string[] {
          //  1     2     3     4     5     6     7   
            "003","000","000","000","000","000","003", // 1
            "003","000","607","000","607","000","003", // 2
            "003","000","000","000","000","000","003", // 3
            "003","000","000","172","000","000","003", // 4
            "003","000","000","000","000","000","003", // 5
            "003","232","607","000","607","232","003", // 6
            "003","000","000","000","000","000","003", // 7
        };
        dataManager.RoomDefinitions_SkeletonCave.Add(RoomLabels.DungeonEnd11,
            new RoomDefinition(gameManager, SceneTypes.SkeletonCave, RoomLabels.DungeonEnd11, dungeonEnd11, RoomOrientations.OXOX, RoomTypes.End));

        var dungeonEnd12 = new string[] {
          //  1     2     3     4     5     6     7   
            "000","000","000","000","000","000","000", // 1
            "000","000","607","000","000","000","000", // 2
            "000","232","000","000","000","232","000", // 3
            "000","000","000","172","000","000","000", // 4
            "000","000","000","000","000","000","000", // 5
            "000","000","607","000","000","000","000", // 6
            "003","003","003","003","003","003","003", // 7
        };
        dataManager.RoomDefinitions_SkeletonCave.Add(RoomLabels.DungeonEnd12,
            new RoomDefinition(gameManager, SceneTypes.SkeletonCave, RoomLabels.DungeonEnd12, dungeonEnd12, RoomOrientations.OOXO, RoomTypes.End));

        var dungeonEnd13 = new string[] {
          //  1     2     3     4     5     6     7   
            "003","000","000","000","000","000","003", // 1
            "003","232","607","000","607","232","003", // 2
            "003","000","000","000","000","000","003", // 3
            "003","000","000","172","000","000","003", // 4
            "003","237","000","000","000","237","003", // 5
            "003","000","607","000","607","000","003", // 6
            "003","003","003","003","003","003","003", // 7
        };
        dataManager.RoomDefinitions_SkeletonCave.Add(RoomLabels.DungeonEnd13,
            new RoomDefinition(gameManager, SceneTypes.SkeletonCave, RoomLabels.DungeonEnd13, dungeonEnd13, RoomOrientations.OXXX, RoomTypes.End));

        var dungeonEnd14 = new string[] {
          //  1     2     3     4     5     6     7   
            "003","003","003","003","003","003","003", // 1
            "003","000","607","000","607","232","000", // 2
            "003","237","000","000","000","000","000", // 3
            "003","000","000","172","000","000","000", // 4
            "003","237","000","000","000","000","000", // 5
            "003","232","607","000","607","000","000", // 6
            "003","003","003","003","003","003","003", // 7
        };
        dataManager.RoomDefinitions_SkeletonCave.Add(RoomLabels.DungeonEnd14,
            new RoomDefinition(gameManager, SceneTypes.SkeletonCave, RoomLabels.DungeonEnd14, dungeonEnd14, RoomOrientations.XOXX, RoomTypes.End));

        var dungeonEnd15 = new string[] {
          //  1     2     3     4     5     6     7   
            "003","003","003","003","003","003","003", // 1
            "000","000","607","000","607","000","000", // 2
            "000","000","000","000","000","000","000", // 3
            "000","000","000","172","000","000","000", // 4
            "000","000","000","000","000","000","000", // 5
            "000","000","607","232","607","000","000", // 6
            "000","000","000","000","000","000","000", // 7
        };
        dataManager.RoomDefinitions_SkeletonCave.Add(RoomLabels.DungeonEnd15,
            new RoomDefinition(gameManager, SceneTypes.SkeletonCave, RoomLabels.DungeonEnd15, dungeonEnd15, RoomOrientations.XOOO, RoomTypes.End));
    }
}