﻿using System;
using System.Collections.Generic;
using UnityEngine;

//This is a helper class used by the AudioCollection class. It stores a list of AudioClips
[System.Serializable]
public class ClipBank
{
    public AudioClipBanks AudioClipBank;
    public List<AudioClip> Clips = new List<AudioClip>();
}

// Scriptable Object class used to represent Audio Collection assets.
[CreateAssetMenu(fileName = "New Audio Collection")]
public class AudioCollection : ScriptableObject
{
    public string audioGroup;
    [SerializeField] [Range(0.0f, 1.0f)] private float _volume = 1.0f;
    [SerializeField] [Range(0.0f, 1.0f)] private float _spatialBlend = 1.0f;
    [SerializeField] [Range(0, 256)] private int _priority = 128;
    [SerializeField] private List<ClipBank> _audioClipBanks = new List<ClipBank>();

    public float volume { get { return _volume; } }
    public float spatialBlend { get { return _spatialBlend; } }
    public int priority { get { return _priority; } }

    public AudioClip GetMusicTrack(MusicTracks music)
    {
            if (_audioClipBanks == null)
            {
                throw new System.Exception("Audio clipbank " + AudioClipBanks.Music + " does not exist.");
            }

            ClipBank clipBank = null;
            foreach (var bank in _audioClipBanks)
            {
                if (bank.AudioClipBank == AudioClipBanks.Music)
                {
                    clipBank = bank;
                }
            }

            if (clipBank == null)
            {
                throw new System.Exception("Audio collection does not contain value for " + AudioClipBanks.Music);
            }

            var clipList = clipBank.Clips;
            if (clipList.Count == 0)
            {
                throw new System.Exception("Audio clipbank " + AudioClipBanks.Music + " has no audio clips assigned.");
            }

            if(Enum.GetNames(typeof(MusicTracks)).Length > clipList.Count)
            {
                throw new Exception("Music track " + music + " with enum value of " + ((int)music).ToString() + " has not been configured in the music audio collection.");
            }

            // Select random clip from the bank
            var clip = clipList[(int)music];
            return clip;
    }

    public AudioClip GetRandomClipFromBank(AudioClipBanks bankType)
    {
        if (_audioClipBanks == null)
        {
            throw new System.Exception("Audio clipbank " + bankType + " does not exist.");
        }

        ClipBank clipBank = null;
        foreach (var bank in _audioClipBanks)
        {
            if (bank.AudioClipBank == bankType)
            {
                clipBank = bank;
            }
        }

        if (clipBank == null)
        {
            throw new System.Exception("Audio collection does not contain value for " + bankType);
        }

        var clipList = clipBank.Clips;
        if (clipList.Count == 0)
        {
            throw new System.Exception("Audio clipbank " + bankType + " has no audio clips assigned.");
        }

        // Select random clip from the bank
        var clip = clipList[UnityEngine.Random.Range(0, clipList.Count)];
        return clip;
    }
}
