﻿using TMPro;
using UnityEngine;

public class Hud : MonoBehaviour
{
    private IGameManager gameManager;
    public TextMeshProUGUI HpText;
    public TextMeshProUGUI AttackText;
    public TextMeshProUGUI GoldText;
    public TextMeshProUGUI BombText;
    public GameObject SelectionImage;
    public GameObject BombImage;
    public GameObject ArrowImage;
    public GameObject PotionImage;

    public void Setup(IGameManager gameManager)
    {
        this.gameManager = gameManager;
    }
    public void UpdateHud()
    {
        HpText.text = $"{gameManager.Player.Hp} / {gameManager.Player.MaxHp}";
        AttackText.text = $"{gameManager.Player.AttackMin} - {gameManager.Player.AttackMax}";
        GoldText.text = $"{gameManager.Player.Gold}";
        BombText.text = $"{gameManager.Player.Bombs}";
    }

    public void UpdateToolSelection()
    {
        var selectedIndex = gameManager.Player.SelectedToolIndex;
        var toolsAvailable = gameManager.Player.ToolsAvailable;
        if (toolsAvailable.Count > 0)
        {
            SelectionImage.SetActive(true);

            switch (toolsAvailable[selectedIndex])
            {
                case TileIdentifiers.Item_Bomb:
                    BombImage.SetActive(true);
                    ArrowImage.SetActive(false);
                    PotionImage.SetActive(false);
                    break;
                case TileIdentifiers.Item_BowAndArrow:
                    BombImage.SetActive(false);
                    ArrowImage.SetActive(true);
                    PotionImage.SetActive(false);
                    break;
                case TileIdentifiers.Item_HealthPotion:
                    BombImage.SetActive(false);
                    ArrowImage.SetActive(false);
                    PotionImage.SetActive(true);
                    break;
                case TileIdentifiers.Empty:
                    SelectionImage.SetActive(false);
                    BombImage.SetActive(false);
                    ArrowImage.SetActive(false);
                    PotionImage.SetActive(false);
                    break;
                default:
                    throw new System.Exception($"Invalid tool type {toolsAvailable[selectedIndex]}");
            }
        }
        else
        {
            SelectionImage.SetActive(false);
            BombImage.SetActive(false);
            ArrowImage.SetActive(false);
            PotionImage.SetActive(false);
        }
    }
}