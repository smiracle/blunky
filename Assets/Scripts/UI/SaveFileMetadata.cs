﻿using System;

[Serializable]
public class SaveFileMetadata
{
    public string Filepath;
    public string Filename;
    public long Timestamp;
    public string DisplayText;
}    
