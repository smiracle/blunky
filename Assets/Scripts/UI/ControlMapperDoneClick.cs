﻿using UnityEngine;

public class ControlMapperDoneClick : MonoBehaviour
{
    public void OnControlMapperDoneClick()
    {
        GameObject.FindObjectOfType<GameManager>().EventManager.TriggerDoneControlMapperEvent(this);
    }
}

