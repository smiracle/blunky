﻿using System;
using UnityEngine;

public class PauseMenu : MenuBase, IPauseMenu
{
    private enum MenuElements { ReturnToGame, SaveGame, ControlsSettings, VideoSettings, AudioSettings, ReturnToTitle, ExitGame };
    public void Setup(IGameManager gameManager)
    {
        this.gameManager = gameManager;
        dataManager = gameManager.DataManager;
        uiManager = gameManager.UIManager;
        inputManager = gameManager.InputManager;
        EventManager.ChangeDeviceMouseEvent += ChangedToMouse;
    }
    private void ChangedToMouse(object sender, EventArgs args)
    {
        Options[selectedIndex].color = DeselectedGreyColor;
    }

    public void OnEnterState()
    {
        //if (DataManager.Instance.LastUsedControllerType == ControllerTypes.Joystick)
        //{
        //    MenuLabels[selectedIndex].color = UIManager.Instance.OrangeSelectionColor;
        //}
        //else
        //{
        //    MenuLabels[selectedIndex].color = Color.white;
        //}
        if (inputManager.LastUsedInputType == InputManager.InputTypes.NonMouse)
        {
            Options[selectedIndex].color = WhiteSelectionColor;
        }
    }

    public void OnControllerSubmit()
    {
        OnSubmit(selectedIndex);
    }

    public void OnSubmit(int index)
    {
        switch (index)
        {
            case (int)MenuElements.ReturnToGame:
                uiManager.ChangeState<GameplayState>();
                break;
            case (int)MenuElements.SaveGame:
                uiManager.ChangeState<SaveGameMenuState>();
                break;
            case (int)MenuElements.ControlsSettings:
                uiManager.ChangeState<ControlMapperState>();
                break;
            case (int)MenuElements.VideoSettings:
                uiManager.ChangeState<VideoSettingsMenuState>();
                break;
            case (int)MenuElements.AudioSettings:
                uiManager.ChangeState<AudioSettingsMenuState>();
                break;
            case (int)MenuElements.ReturnToTitle:
                //gameManager.LoadTitleScene();
                gameManager.Camera.transform.SetParent(null);
                gameManager.Player.gameObject.SetActive(false);
                gameManager.PlayMusic(MusicTracks.Title);
                uiManager.ChangeState<TitleMenuState>();
                break;
            case (int)MenuElements.ExitGame:
                ExitGame();
                break;
        }
    }

    public void ExitGame()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
			Application.Quit();
#endif
    }
}