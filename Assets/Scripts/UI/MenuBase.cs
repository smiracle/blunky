﻿using TMPro;
using UnityEngine;

public class MenuBase : MonoBehaviour
{
    [field: SerializeField] public TextMeshProUGUI[] Options { get; set; }
    public int selectedIndex
    {
        get
        {
            return _selectedIndex;
        }
        set
        {
            previouslySelectedIndex = _selectedIndex;
            Options[previouslySelectedIndex].color = DeselectedGreyColor;
            Options[value].color = WhiteSelectionColor;
            _selectedIndex = value;
        }
    }
    public int previouslySelectedIndex { get; set; }
    protected int _selectedIndex;
    protected readonly Color WhiteSelectionColor = Color.white;
    protected readonly Color DeselectedGreyColor = new Color(0.75f, 0.75f, 0.75f);
    protected IGameManager gameManager { get; set; }
    protected UIManager uiManager { get; set; }
    protected DataManager dataManager { get; set; }
    protected InputManager inputManager { get; set; }
    protected AudioManager audioManager { get; set; }

    public virtual void OnControllerMoveDown()
    {
        if (selectedIndex < Options.Length - 1)
        {
            selectedIndex++;
        }
        else
        {
            selectedIndex = 0;
        }
    }

    public virtual void OnControllerMoveUp()
    {
        if (selectedIndex > 0)
        {
            selectedIndex--;
        }
        else
        {
            selectedIndex = Options.Length - 1;
        }
    }

    public void SetActive(bool toggle)
    {
        gameObject.SetActive(toggle);
    }
}