﻿public class LoadMenu : MenuBase, ILoadMenu
{
    private enum MenuElements { Cancel, LoadSlot1, LoadSlot2, LoadSlot3, LoadSlot4, LoadSlot5 };
    SaveFileManager saveFileManager { get; set; }

    public void Setup(IGameManager gameManager)
    {
        uiManager = gameManager.UIManager;
        dataManager = gameManager.DataManager;
        inputManager = gameManager.InputManager;
        saveFileManager = gameManager.SaveFileManager;
    }

    public void OnEnterState()
    {
        dataManager.SaveFileMetadata = saveFileManager.GetSaveFileMetadata();
        for (int i = 1; i < SaveFileManager.MaxNumSaves + 1; i++)
        {
            if (i < dataManager.SaveFileMetadata.Count + 1)
            {
                Options[i].text = dataManager.SaveFileMetadata[i - 1].DisplayText;
            }
            else
            {
                Options[i].text = "Empty";
            }
        }
    }

    public void OnCancel()
    {
        TransitionToTitleMenuState();
    }

    public void OnControllerSubmit()
    {
        OnSubmit(selectedIndex);
    }
    public void OnSubmit(int index)
    {
        if (index == 0)
        {
            OnCancel();
        }
        else
        {
            saveFileManager.Load(index);
        }
    }

    public void TransitionToTitleMenuState()
    {
        uiManager.ChangeState<TitleMenuState>();
    }
}