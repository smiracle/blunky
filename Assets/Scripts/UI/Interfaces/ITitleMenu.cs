﻿using UnityEngine;

public interface ITitleMenu : IMenu, IMenuOptions
{
    void ExitGame();
}