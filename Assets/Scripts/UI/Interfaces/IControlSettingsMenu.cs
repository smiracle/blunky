﻿public interface IControlSettingsMenu : IMenu, IMenuOptionsOldText
{
    void OnExitState();
    void OnControllerMoveLeft();
    void OnControllerMoveRight();
    void TransitionToControlMapperState();
    void ApplyJsonSettings();
}