﻿public interface IPauseMenu : IMenu, IMenuOptions
{
    void ExitGame();

}