﻿using System;

public interface IAudioSettingsMenu : IMenu, IMenuOptions
{   
    void ApplyJsonSettings();
    void OnCancel();
    void OnControllerMoveLeft();
    void OnControllerMoveRight();
    void OnSaveAndApply();
    void OnExitState();
}