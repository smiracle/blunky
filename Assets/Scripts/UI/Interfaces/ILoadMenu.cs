﻿public interface ILoadMenu : IMenu, IMenuOptions
{
    void OnCancel();
    void TransitionToTitleMenuState();
}