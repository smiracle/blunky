﻿public interface IVideoSettingsMenu : IMenu, IMenuOptions
{
    void ApplyJsonSettings();
    void OnControllerMoveLeft();
    void OnControllerMoveRight();
    void OnFullscreenChanged();
    void OnQualityDecrease();
    void OnQualityIncrease();
    void OnResolutionDecrease();
    void OnResolutionIncrease();
    void OnSaveAndApply();
    void OnExitState();
    void OnCancel();
}