﻿public interface IMenu
{
    public void Setup(IGameManager gameManager);
    public void SetActive(bool toggle);
    int selectedIndex { get; set; }
    int previouslySelectedIndex { get; set; }    
    void OnControllerMoveDown();
    void OnControllerMoveUp();
    void OnControllerSubmit();
    void OnSubmit(int index);
    void OnEnterState();
}