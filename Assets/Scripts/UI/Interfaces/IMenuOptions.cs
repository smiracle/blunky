﻿using TMPro;

public interface IMenuOptions
{    
    TextMeshProUGUI[] Options { get; set; }
}
