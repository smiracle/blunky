using TMPro;
using UnityEngine;

public class DialogueWindow : MonoBehaviour
{
    public TextMeshProUGUI DialogueText;
    private IGameManager gameManager;
    public void Setup(IGameManager gameManager)
    {
        this.gameManager = gameManager;
    }
    public void ShowDialogue(string dialogue)
    {
        gameObject.SetActive(true);
        DialogueText.text = dialogue;
        gameManager.UIManager.ChangeState<DialogueState>();
    }
}
