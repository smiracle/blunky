﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SaveFile
{    
    public Scenes ActiveScene;
    public Vector3Int PlayerPosition;
    public Vector3Int[] OverworldPositions;
    public Vector3Int[] DungeonPositions;
}    
