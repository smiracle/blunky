﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.AddressableAssets;

public class DamageTextPositioner : MonoBehaviour
{
    public TextMeshProUGUI TextMeshProUGUI;

    private float timer = 0f;
    private readonly float moveSpeed = 0.5f; // Higher values for faster vertical movement
    private readonly float growShrinkAmount = 0.3f;
    private readonly float fadeDuration = 2f;
    private readonly int rolloverCharacterSpread = 10;
    private readonly float scaleUpAndDownPercentage = 1.5f;
    private IGameManager gameManager;
    private DataManager dataManager;
    private Vector3 worldPosition;

    public void Run(IGameManager gameManager, Vector3 worldPosition, string text, Color color, float fontSize)
    {
        this.worldPosition = worldPosition;
        this.gameManager = gameManager;
        dataManager = gameManager.DataManager;
        timer = fadeDuration;
        transform.SetParent(gameManager.UIManager.Canvas.transform);
        transform.position = WorldToUISpace(gameManager.UIManager.Canvas, worldPosition, Camera.main);
        TextMeshProUGUI.fontSize = fontSize;
        TextMeshProUGUI.text = text;
        TextMeshProUGUI.color = color;
        StartCoroutine(FadeEffectAnimationCoroutine());
    }

    private Vector3 WorldToUISpace(Canvas parentCanvas, Vector3 worldPos, Camera camera)
    {
        //Convert the world for screen point so that it can be used with ScreenPointToLocalPointInRectangle function
        Vector3 screenPos = camera.WorldToScreenPoint(worldPos); // (previously set to Camera.main)

        //Convert the screenpoint to ui rectangle local point
        RectTransformUtility.ScreenPointToLocalPointInRectangle(parentCanvas.transform as RectTransform, screenPos, parentCanvas.worldCamera, out Vector2 movePos);

        //Convert the local point to world point
        return parentCanvas.transform.TransformPoint(movePos);
    }

    private void OnDisable()
    {
        StopCoroutine(FadeEffectAnimationCoroutine());
        //Addressables.ReleaseInstance(gameObject);
    }

    IEnumerator FadeEffectAnimationCoroutine()
    {
        // Need to force the text object to be generated so we have valid data to work with right from the start.
        TextMeshProUGUI.ForceMeshUpdate();

        TMP_TextInfo textInfo = TextMeshProUGUI.textInfo;
        Color32[] newVertexColors;

        int currentCharacter = 0;
        int startingCharacterRange = currentCharacter;
        bool isRangeMax = false;

        while (!isRangeMax)
        {
            int characterCount = textInfo.characterCount;

            // Spread should not exceed the number of characters.
            byte fadeSteps = (byte)Mathf.Max(1, 255 / rolloverCharacterSpread);

            for (int i = startingCharacterRange; i < currentCharacter + 1; i++)
            {
                // Skip characters that are not visible
                if (!textInfo.characterInfo[i].isVisible) continue;

                // Get the index of the material used by the current character.
                int materialIndex = textInfo.characterInfo[i].materialReferenceIndex;

                // Get the vertex colors of the mesh used by this text element (character or sprite).
                newVertexColors = textInfo.meshInfo[materialIndex].colors32;

                // Get the index of the first vertex used by this text element.
                int vertexIndex = textInfo.characterInfo[i].vertexIndex;

                // Get the current character's alpha value.
                byte alpha = (byte)Mathf.Clamp(newVertexColors[vertexIndex + 0].a - fadeSteps, 0, 255);

                // Set new alpha values.
                newVertexColors[vertexIndex + 0].a = alpha;
                newVertexColors[vertexIndex + 1].a = alpha;
                newVertexColors[vertexIndex + 2].a = alpha;
                newVertexColors[vertexIndex + 3].a = alpha;

                // Tint vertex colors
                // Note: Vertex colors are Color32 so we need to cast to Color to multiply with tint which is Color.
                //newVertexColors[vertexIndex + 0] = newVertexColors[vertexIndex + 0] * color;
                //newVertexColors[vertexIndex + 1] = newVertexColors[vertexIndex + 1] * color;
                //newVertexColors[vertexIndex + 2] = newVertexColors[vertexIndex + 2] * color;
                //newVertexColors[vertexIndex + 3] = newVertexColors[vertexIndex + 3] * color;

                if (alpha == 0)
                {
                    startingCharacterRange += 1;

                    if (startingCharacterRange == characterCount)
                    {
                        // Update mesh vertex data one last time.
                        TextMeshProUGUI.UpdateVertexData(TMP_VertexDataUpdateFlags.Colors32);
                        gameObject.SetActive(false);
                    }
                }
            }

            // Upload the changed vertex colors to the Mesh.
            TextMeshProUGUI.UpdateVertexData(TMP_VertexDataUpdateFlags.Colors32);

            if (currentCharacter + 1 < characterCount)
            {
                currentCharacter += 1;
            }

            yield return new WaitForSeconds(fadeDuration / 10);
        }
    }

    private void Update()
    {
        worldPosition += new Vector3(0, moveSpeed, 0) * Time.deltaTime;
        //Convert the player's position to the UI space then apply the offset
        transform.position = WorldToUISpace(gameManager.UIManager.Canvas, worldPosition, Camera.main);
        if (timer > fadeDuration * 0.5f)
        {
            // First half of lifetime - Grow
            transform.localScale += (Vector3.one * scaleUpAndDownPercentage) * growShrinkAmount * Time.deltaTime;
        }
        else
        {
            // Second half of lifetime - Shrink
            transform.localScale -= (Vector3.one * scaleUpAndDownPercentage) * growShrinkAmount * Time.deltaTime;
        }

        timer -= Time.deltaTime;
    }
}

