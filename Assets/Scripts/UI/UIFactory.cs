using UnityEngine;

public class UIFactory
{
    public IAudioSettingsMenu AddAudioSettingsMenu(GameObject go)
    {
        return go.AddComponent<AudioSettingsMenu>();
    }

    public ILoadMenu AddLoadMenu(GameObject go)
    {
        return go.AddComponent<LoadMenu>();
    }
    public ITitleMenu AddTitleMenu(GameObject go)
    {
        return go.AddComponent<TitleMenu>();
    }

    public IVideoSettingsMenu AddVideoSettingsMenu(GameObject go)
    {
        return go.AddComponent<VideoSettingsMenu>();
    }
}
