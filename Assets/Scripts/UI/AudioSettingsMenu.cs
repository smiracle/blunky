﻿using System;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using static InputManager;

public class AudioSettingsMenu : MenuBase, IAudioSettingsMenu
{
    [SerializeField] private Slider[] Sliders;
    private float sliderMoveIncrement = 0.33f;
    private float[] values;

    private AudioSettings oldSettings;
    private string fullpath;
    private enum MenuElements { Master, Music, Sound, Cancel, SaveAndApply };
    private readonly string[] mixerChannelNames = { "Master", "Music", "Sound" };
    private const string filepathSuffix = "UserSettings/";
    private const string filename = "AudioSettings.json";

    // Always stored in linear format and converted back to logarithmic when audio channel is set.
    private struct AudioSettings
    {
        public float MasterVolume;
        public float MusicVolume;
        public float SoundVolume;
    }

    public void Setup(IGameManager gameManager)
    {
        uiManager = gameManager.UIManager;
        dataManager = gameManager.DataManager;
        audioManager = gameManager.AudioManager;
        inputManager = gameManager.InputManager;
        fullpath = Path.Combine(Application.persistentDataPath, filepathSuffix, filename);
        EventManager.ChangeDeviceMouseEvent += ChangedToMouse;
    }
    private void ChangedToMouse(object sender, EventArgs args)
    {
        Options[selectedIndex].color = DeselectedGreyColor;
    }

    public void OnEnterState()
    {
        _selectedIndex = 0;
        if(inputManager.LastUsedInputType == InputTypes.NonMouse)
        {
            Options[selectedIndex].color = WhiteSelectionColor;
        }        

        // Add listeners
        Sliders[(int)MenuElements.Master].onValueChanged.AddListener(delegate { OnMasterSliderChange(); });
        Sliders[(int)MenuElements.Music].onValueChanged.AddListener(delegate { OnMusicSliderChange(); });
        Sliders[(int)MenuElements.Sound].onValueChanged.AddListener(delegate { OnSoundSliderChange(); });

        values = new float[Sliders.Length];
        oldSettings = GetSettings();

        // Update UI to reflect old settings.
        Sliders[(int)MenuElements.Master].value = oldSettings.MasterVolume;
        Sliders[(int)MenuElements.Music].value = oldSettings.MusicVolume;
        Sliders[(int)MenuElements.Sound].value = oldSettings.SoundVolume;
    }

    public void OnExitState()
    {
        Options[selectedIndex].color = DeselectedGreyColor;
        foreach (var slider in Sliders)
        {
            slider.onValueChanged.RemoveAllListeners();
        }
    }

    public void ApplyJsonSettings()
    {
        if (File.Exists(fullpath))
        {
            string json = File.ReadAllText(fullpath);
            var settings = JsonUtility.FromJson<AudioSettings>(json);
            ApplySettings(settings);
        }
    }

    public void OnSaveAndApply()
    {
        SaveSettings();
        selectedIndex = 0;
        if (uiManager.ReturnToTitleMenu)
        {
            uiManager.ChangeState<TitleMenuState>();
        }
        else
        {
            uiManager.ChangeState<PauseMenuState>();
        }
    }

    public void OnCancel()
    {
        ApplySettings(oldSettings);
        if (uiManager.ReturnToTitleMenu)
        {
            uiManager.ChangeState<TitleMenuState>();
        }
        else
        {
            uiManager.ChangeState<PauseMenuState>();
        }
    }

    public void OnControllerSubmit()
    {
        OnSubmit(selectedIndex);
    }

    public void OnSubmit(int index)
    {
        if (index == (int)MenuElements.Cancel)
        {
            OnCancel();
        }
        else if (index == (int)MenuElements.SaveAndApply)
        {
            OnSaveAndApply();
        }
    }

    public void OnControllerMoveRight()
    {
        if (selectedIndex < (int)MenuElements.Cancel)
        {
            // Move slider value
            Sliders[selectedIndex].value += sliderMoveIncrement;
        }
        else if (selectedIndex == (int)MenuElements.Cancel)
        {
            selectedIndex++;
        }
        else if (selectedIndex == (int)MenuElements.SaveAndApply)
        {
            selectedIndex = (int)MenuElements.Master;
        }
    }

    public void OnControllerMoveLeft()
    {
        if (selectedIndex < (int)MenuElements.Cancel)
        {
            // Move slider value
            Sliders[selectedIndex].value -= sliderMoveIncrement;
        }
        else if (selectedIndex == (int)MenuElements.SaveAndApply)
        {
            selectedIndex--;
        }
    }

    private void SaveSettings()
    {
        var dirPath = Path.Combine(Application.persistentDataPath, filepathSuffix);
        if (!Directory.Exists(dirPath))
        {
            Directory.CreateDirectory(dirPath);
        }

        if (File.Exists(fullpath))
        {
            File.Delete(fullpath);
        }

        var settings = new AudioSettings
        {
            MasterVolume = values[(int)MenuElements.Master],
            MusicVolume = values[(int)MenuElements.Music],
            SoundVolume = values[(int)MenuElements.Sound],
        };

        // Write the contents of the settings to the disk
        File.WriteAllText(fullpath, JsonUtility.ToJson(settings));
    }

    private AudioSettings GetSettings()
    {
        if (File.Exists(fullpath))
        {
            string json = File.ReadAllText(fullpath);
            var settings = JsonUtility.FromJson<AudioSettings>(json);
            return settings;
        }
        else
        {
            // First time loading, get the current settings from the audio mixer and load those.
            audioManager.GetAudioGroupByName("Master").audioMixer.GetFloat("MasterVolume", out float masterVol);
            audioManager.GetAudioGroupByName("Music").audioMixer.GetFloat("MusicVolume", out float musicVol);
            audioManager.GetAudioGroupByName("Sound").audioMixer.GetFloat("MusicVolume", out float soundVol);

            // Return in linear format.
            return new AudioSettings()
            {
                MasterVolume = ConvertToLinear(masterVol),
                MusicVolume = ConvertToLinear(musicVol),
                SoundVolume = ConvertToLinear(soundVol),
            };
        }
    }

    private void ApplySettings(AudioSettings settings)
    {
        // Apply in logarithmic format.
        audioManager.GetAudioGroupByName("Master").audioMixer.SetFloat("MasterVolume", ConvertToLog(settings.MasterVolume));
        audioManager.GetAudioGroupByName("Music").audioMixer.SetFloat("MusicVolume", ConvertToLog(settings.MusicVolume));
        audioManager.GetAudioGroupByName("Sound").audioMixer.SetFloat("SoundVolume", ConvertToLog(settings.SoundVolume));
    }

    private float ConvertToLog(float linearValue)
    {
        return Mathf.Log10(linearValue) * 20;
    }

    private float ConvertToLinear(float logVal)
    {
        return Mathf.Pow(10, logVal / 20);
    }

    private void UpdateAudioMixer(int i, float value)
    {
        // Convert to logarithmic value and update the mixer
        audioManager.GetAudioGroupByName(mixerChannelNames[i]).audioMixer.SetFloat(mixerChannelNames[i] + "Volume", ConvertToLog(value));
    }

    private void OnMasterSliderChange()
    {
        var i = (int)MenuElements.Master;
        values[i] = Sliders[i].value;
        UpdateAudioMixer(i, values[i]);
    }

    private void OnMusicSliderChange()
    {
        var i = (int)MenuElements.Music;
        values[i] = Sliders[i].value;
        UpdateAudioMixer(i, values[i]);
    }

    private void OnSoundSliderChange()
    {
        var i = (int)MenuElements.Sound;
        values[i] = Sliders[i].value;
        UpdateAudioMixer(i, values[i]);
    }    
}
