﻿using System;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class VideoSettingsMenu : MenuBase, IVideoSettingsMenu
{
    public Toggle FullscreenToggle;
    public Button ResolutionLeftButton;
    public Button ResolutionRightButton;
    public Button QualityLeftButton;
    public Button QualityRightButton;
    public TextMeshProUGUI ResolutionValueLabel;
    public TextMeshProUGUI QualityValueLabel;

    private string fullpath;
    private VideoSettings oldSettings;
    private List<Resolution> filteredResolutions;
    private List<string> filteredResolutionNames;
    private int resolutionIndex;
    private List<string> qualityNames;
    private const string filepathSuffix = "UserSettings/";
    private const string filename = "VideoSettings.json";

    private enum MenuElements { Fullscreen, Resolution, Quality, Cancel, SaveAndApply };

    private struct VideoSettings
    {
        public bool IsFullscreen;
        public int Height;
        public int Width;
        public int Quality;
    }

    public void Setup(IGameManager gameManager)
    {
        uiManager = gameManager.UIManager;
        dataManager = gameManager.DataManager;
        inputManager = gameManager.InputManager;
        oldSettings = GetOldSettings();
        fullpath = Path.Combine(Application.persistentDataPath, filepathSuffix, filename);
        EventManager.ChangeDeviceMouseEvent += ChangedToMouse;
    }
    private void ChangedToMouse(object sender, EventArgs args)
    {
        Options[selectedIndex].color = DeselectedGreyColor;
    }

    public void OnEnterState()
    {
        if (inputManager.LastUsedInputType == InputManager.InputTypes.NonMouse)
        {
            Options[selectedIndex].color = WhiteSelectionColor;
        }

        filteredResolutions = new List<Resolution>();
        var allResolutions = new List<Resolution>(Screen.resolutions);
        filteredResolutionNames = new List<string>();
        int resolutionWidth = -1;
        int resolutionHeight = -1;
        int index = 0;
        string format;
        foreach (var resolution in allResolutions)
        {
            if (resolutionWidth != resolution.width || resolutionHeight != resolution.height)
            {
                // Create a neatly formatted string to add to the dropdown
                format = string.Format($"{resolution.width} x {resolution.height}");
                filteredResolutionNames.Add(format);

                resolutionWidth = resolution.width;
                resolutionHeight = resolution.height;

                // Determine if this is the user's current resolution
                if (resolutionWidth == oldSettings.Width && resolutionHeight == oldSettings.Height)
                {
                    resolutionIndex = index;
                }

                // Add the filtered resolution to the list
                filteredResolutions.Add(resolution);

                index++;
            }
        }

        // Update UI to reflect old settings.
        FullscreenToggle.isOn = oldSettings.IsFullscreen;
        qualityNames = new List<string>(QualitySettings.names);

        QualityValueLabel.text = qualityNames[QualitySettings.GetQualityLevel()];
        ResolutionValueLabel.text = filteredResolutionNames[resolutionIndex];

        FullscreenToggle.onValueChanged.AddListener(delegate { OnFullscreenChanged(); });
    }

    public void OnExitState()
    {
        FullscreenToggle.onValueChanged.RemoveAllListeners();
    }

    public void OnQualityDecrease()
    {
        QualitySettings.DecreaseLevel();
        QualityValueLabel.text = qualityNames[QualitySettings.GetQualityLevel()];
    }

    public void OnQualityIncrease()
    {
        QualitySettings.IncreaseLevel();
        QualityValueLabel.text = qualityNames[QualitySettings.GetQualityLevel()];
    }

    public void OnResolutionDecrease()
    {
        if (resolutionIndex > 0)
        {
            resolutionIndex--;
            Screen.SetResolution(filteredResolutions[resolutionIndex].width, filteredResolutions[resolutionIndex].height, FullscreenToggle.isOn);
            var h = Screen.height;
            var w = Screen.width;
            ResolutionValueLabel.text = filteredResolutionNames[resolutionIndex];
        }
    }

    public void OnResolutionIncrease()
    {
        if (resolutionIndex < filteredResolutions.Count - 1)
        {
            resolutionIndex++;
            Screen.SetResolution(filteredResolutions[resolutionIndex].width, filteredResolutions[resolutionIndex].height, FullscreenToggle.isOn);
            ResolutionValueLabel.text = filteredResolutionNames[resolutionIndex];
        }
    }

    private VideoSettings GetNewSettings()
    {
        return new VideoSettings()
        {
            IsFullscreen = Screen.fullScreen,
            Height = Screen.height,
            Width = Screen.width,
            Quality = QualitySettings.GetQualityLevel(),
        };
    }

    private void SaveSettings()
    {
        var dirPath = Path.Combine(Application.persistentDataPath, filepathSuffix);
        if (!Directory.Exists(dirPath))
        {
            Directory.CreateDirectory(dirPath);
        }

        if (File.Exists(fullpath))
        {
            File.Delete(fullpath);
        }

        // Write the contents of the settings to the disk
        File.WriteAllText(fullpath, JsonUtility.ToJson(GetNewSettings()));
    }

    private VideoSettings GetOldSettings()
    {
        if (File.Exists(fullpath))
        {
            // Read in the contents of the settings file
            string json = File.ReadAllText(fullpath);
            var settings = JsonUtility.FromJson<VideoSettings>(json);
            return settings;
        }
        else
        {
            // Menu has never been loaded beofre, get the defaults.
            return GetNewSettings();
        }
    }

    public void OnSubmit(int index)
    {
        if (selectedIndex == (int)MenuElements.Fullscreen)
        {
            FullscreenToggle.isOn = !FullscreenToggle.isOn;
        }
        else if (selectedIndex == (int)MenuElements.Cancel)
        {
            OnCancel();
        }
        else if (selectedIndex == (int)MenuElements.SaveAndApply)
        {
            OnSaveAndApply();
        }
    }

    public void OnControllerSubmit()
    {
        OnSubmit(selectedIndex);
    }

    public void OnControllerMoveRight()
    {
        switch (selectedIndex)
        {
            case (int)MenuElements.Resolution:
                OnResolutionIncrease();
                break;
            case (int)MenuElements.Quality:
                OnQualityIncrease();
                break;
            case (int)MenuElements.Cancel:
                selectedIndex++;
                break;
            case (int)MenuElements.SaveAndApply:
                selectedIndex = (int)MenuElements.Fullscreen;
                break;
        }
    }

    public void OnControllerMoveLeft()
    {
        switch (selectedIndex)
        {
            case (int)MenuElements.Resolution:
                OnResolutionDecrease();
                break;
            case (int)MenuElements.Quality:
                OnQualityDecrease();
                break;
            case (int)MenuElements.SaveAndApply:
                selectedIndex--;
                break;
        }
    }

    public void ApplyJsonSettings()
    {
        if (File.Exists(fullpath))
        {
            // Read in the contents of the settings file
            string json = File.ReadAllText(fullpath);
            var settings = JsonUtility.FromJson<VideoSettings>(json);
            ApplySettings(settings);
        }
    }

    public void OnFullscreenChanged()
    {
        Screen.SetResolution(Screen.width, Screen.height, FullscreenToggle.isOn);
    }

    public void OnSaveAndApply()
    {
        SaveSettings();
        if (uiManager.ReturnToTitleMenu)
        {
            uiManager.ChangeState<TitleMenuState>();
        }
        else
        {
            uiManager.ChangeState<PauseMenuState>();
        }
    }

    public void OnCancel()
    {
        ApplySettings(oldSettings);
        if (uiManager.ReturnToTitleMenu)
        {
            uiManager.ChangeState<TitleMenuState>();
        }
        else
        {
            uiManager.ChangeState<PauseMenuState>();
        }
    }

    private void ApplySettings(VideoSettings settings)
    {
        QualitySettings.SetQualityLevel(settings.Quality);
        Screen.SetResolution(settings.Width, settings.Height, settings.IsFullscreen);
    }    
}
