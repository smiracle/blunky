using System;
using UnityEngine;

public class TitleMenu : MenuBase, ITitleMenu
{
    private enum MenuElements { NewGame, Controls, VideoSettings, AudioSettings, ExitGame };    

    public void Setup(IGameManager gameManager)
    {
        this.gameManager = gameManager;
        uiManager = gameManager.UIManager;
        dataManager = gameManager.DataManager;
        inputManager = gameManager.InputManager;
        audioManager = gameManager.AudioManager;
    }
    private void ChangedToMouse(object sender, EventArgs args)
    {
        Options[selectedIndex].color = DeselectedGreyColor;
    }

    public void OnEnterState()
    {
        EventManager.ChangeDeviceMouseEvent += ChangedToMouse;
        dataManager.SaveFileMetadata = gameManager.SaveFileManager.GetSaveFileMetadata();
        //if (dataManager.SaveFileMetadata.Count > 0)
        //{
        //    //Options[(int)MenuElements.Continue].transform.parent.gameObject.SetActive(true);
        //    selectedIndex = (int)MenuElements.Continue;
        //}
        //else
        //{
            //Options[(int)MenuElements.Continue].transform.parent.gameObject.SetActive(false);
            selectedIndex = (int)MenuElements.NewGame;
        //}

        previouslySelectedIndex = selectedIndex;
        if (inputManager.LastUsedInputType == InputManager.InputTypes.NonMouse)
        {
            Options[selectedIndex].color = WhiteSelectionColor;
        }
        else
        {
            Options[selectedIndex].color = DeselectedGreyColor;
        }
    }

    public void OnExitState()
    {
        EventManager.ChangeDeviceMouseEvent -= ChangedToMouse;
    }

    public void OnControllerSubmit()
    {
        OnSubmit(selectedIndex);
    }

    public void OnSubmit(int index)
    {
        switch (index)
        {
            case (int)MenuElements.NewGame:                
                gameManager.StartNewGame(gameManager.PlayerStartPos);
                break;
            case (int)MenuElements.Controls:
                uiManager.ChangeState<ControlMapperState>();
                break;
            case (int)MenuElements.VideoSettings:
                uiManager.ChangeState<VideoSettingsMenuState>();
                break;
            case (int)MenuElements.AudioSettings:
                uiManager.ChangeState<AudioSettingsMenuState>();
                break;
            case (int)MenuElements.ExitGame:
                ExitGame();
                break;
        }
    }

    public override void OnControllerMoveDown()
    {
        if (selectedIndex < Options.Length - 1)
        {
            selectedIndex++;
        }        
        selectedIndex = (int)MenuElements.NewGame;        
    }

    public override void OnControllerMoveUp()
    {
        if (selectedIndex > (int)MenuElements.NewGame)
        {
            selectedIndex--;
        }
        else
        {
            selectedIndex = (int)MenuElements.ExitGame;
        }
    }

    public void ExitGame()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
			Application.Quit();
#endif
    }
}
