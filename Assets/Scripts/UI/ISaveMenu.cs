﻿public interface ISaveMenu : IMenu, IMenuOptions
{
    void OnCancel();
    void TransitionToPauseMenuState();
}