﻿public class SaveMenu : MenuBase, ISaveMenu
{
    private enum MenuElements { Cancel, SaveSlot1, SaveSlot2, SaveSlot3, SaveSlot4, SaveSlot5 };
    SaveFileManager saveFileManager { get; set; }

    public void Setup(IGameManager gameManager)
    {
        uiManager = gameManager.UIManager;
        dataManager = gameManager.DataManager;
        inputManager = gameManager.InputManager;
        saveFileManager = gameManager.SaveFileManager;
    }

    public void OnEnterState()
    {
        dataManager.SaveFileMetadata = saveFileManager.GetSaveFileMetadata();
        for (int i = 1; i < SaveFileManager.MaxNumSaves + 1; i++)
        {
            if (i < dataManager.SaveFileMetadata.Count + 1)
            {
                Options[i].text = dataManager.SaveFileMetadata[i - 1].DisplayText;
                Options[i].gameObject.SetActive(true);
            }
            else if(i <= dataManager.SaveFileMetadata.Count + 1)
            {
                Options[i].text = "Empty";
                Options[i].gameObject.transform.parent.gameObject.SetActive(true);
            }
            else
            {
                Options[i].gameObject.transform.parent.gameObject.SetActive(false);
            }
        }
    }

    public void OnCancel()
    {
        TransitionToPauseMenuState();
    }

    public void OnControllerSubmit()
    {
        OnSubmit(selectedIndex);
    }
    public void OnSubmit(int index)
    {
        if (index == 0)
        {
            OnCancel();
        }
        else
        {
            saveFileManager.SaveToJson(index);
            uiManager.ChangeState<PauseMenuState>();
        }
    }

    public void TransitionToPauseMenuState()
    {
        uiManager.ChangeState<PauseMenuState>();
    }
}