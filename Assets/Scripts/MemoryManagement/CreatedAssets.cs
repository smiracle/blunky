using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class CreatedAssets : MonoBehaviour
{
    private LoadedAddressableLocations _loadedLocations;

    [field: SerializeField] private List<GameObject> Assets { get; } = new List<GameObject>();

    private void Start()
    {
        CreateAndWaitUntilCompleted();
    }

    private async Task CreateAndWaitUntilCompleted()
    {
        _loadedLocations = GetComponent<LoadedAddressableLocations>();

        await Task.Delay(TimeSpan.FromSeconds(1));

        await CreateAddressablesLoader.ByLoadedAddress(_loadedLocations.AssetLocations, Assets);

        foreach (var asset in Assets)
        {
            //ASSET IS CREATED AND IN THE LIST
            //PERFORM ADDITIONAL ACTIONS HERE
            Debug.Log(asset.name);
        }
    }
}