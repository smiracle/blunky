﻿public enum Scenes
{
    Empty = 0,
    Overworld,
    Interior_StartCave,
    Interior_MarshCave,
    Interior_TallBuilding,
    Interior_Shop,
    Dungeon1_SkeletonCave,
    Dungeon2_Volcano,
    Dungeon3_Catacombs,
}