﻿public enum UIStates
{
    Gameplay,
    PauseMenu,
    SaveLoadMenu,
    ControlMapper,
    VideoSettingsMenu
}