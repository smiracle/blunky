﻿public enum AudioClipBanks
{ 
    Music = 0,
    Sounds_Hurt,
    Sounds_Powerup,
    Sounds_Explosion,
    Sounds_Arrow,
    Sounds_Rotate,
    Sounds_Coin,
    Sounds_Heart,
    Sounds_HeartContainer,
    Sounds_Death,
    Sounds_UsePotion,
    Sounds_Apple,
    Sounds_Denied,
}