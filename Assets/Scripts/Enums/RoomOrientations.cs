﻿public enum RoomOrientations
{
    // Top, right, bottom, left. O = open, X = closed
    OOOO,
    OXXX,
    XOXX,
    XXOX,
    XXXO,
    XOOO,
    OXOO,
    OOXO,
    OOOX,
    OOXX,
    XOOX,
    OXXO,
    OXOX,
    XOXO,
    XXOO,
    XXXX,
}