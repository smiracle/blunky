﻿public enum GameStates
{
    FoundQueen,
    ReceivedBombs,
    FoundKing,
    FoundDoctor,
    BowAndArrowUnlocked,
}