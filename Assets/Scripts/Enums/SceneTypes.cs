﻿public enum SceneTypes
{
    Overworld,
    Interiors,
    SkeletonCave,
    Volcano,
    Catacombs
}