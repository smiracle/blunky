﻿// These must be ordered the same as the tracks in the AudioManager's music collection.
public enum MusicTracks
{
    Title,
    Overworld,
    Interior,
    Shop,
    Dungeon,
    Dungeon2
};
