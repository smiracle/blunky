﻿using System;
using UnityEngine;

public class PauseMenuState : UIState
{
    public override void Enter()
    {
        uiManager.Hud.gameObject.SetActive(false);
        uiManager.BlurBackgroundObject.SetActive(true);
        uiManager.PauseMenu.SetActive(true);
        EventManager.MoveEvent += OnMove;
        EventManager.ConfirmEvent += OnSubmit;
        EventManager.CancelEvent += OnCancel;
        uiManager.PauseMenu.OnEnterState();
    }

    public override void Exit()
    {
        Unsubscribe();
        uiManager.PauseMenu.SetActive(false);
    }

    private void OnDisable()
    {
        Unsubscribe();
    }

    private void Unsubscribe()
    {
        EventManager.MoveEvent -= OnMove;
        EventManager.ConfirmEvent -= OnSubmit;
        EventManager.CancelEvent -= OnCancel;
    }

    protected override void OnCancel(object sender, EventArgs args)
    {
        uiManager.ChangeState<GameplayState>();
    }

    protected override void OnMove(object sender, InfoEventArgs<Point> move)
    {
        if (move.info.y == 1)
        {
            uiManager.PauseMenu.OnControllerMoveUp();
        }
        else if (move.info.y == -1)
        {
            uiManager.PauseMenu.OnControllerMoveDown();
        }
    }

    protected override void OnSubmit(object sender, EventArgs args)
    {
        uiManager.PauseMenu.OnControllerSubmit();
    }
}
