﻿using System;
using UnityEngine;

public class VideoSettingsMenuState : UIState
{
    public override void Enter()
    {
        uiManager.VideoSettingsMenu.SetActive(true);
        EventManager.MoveEvent += OnMove;
        EventManager.ConfirmEvent += OnSubmit;
        EventManager.CancelEvent += OnCancel;
        uiManager.VideoSettingsMenu.OnEnterState();
    }

    public override void Exit()
    {
        uiManager.VideoSettingsMenu.SetActive(false);
        EventManager.MoveEvent -= OnMove;
        EventManager.ConfirmEvent -= OnSubmit;
        EventManager.CancelEvent -= OnCancel;
        uiManager.VideoSettingsMenu.OnExitState();
    }

    protected override void OnMove(object sender, InfoEventArgs<Point> move)
    {
        if (move.info.y == 1)
        {
            uiManager.VideoSettingsMenu.OnControllerMoveUp();
        }
        else if (move.info.y == -1)
        {
            uiManager.VideoSettingsMenu.OnControllerMoveDown();
        }
        if (move.info.x == -1)
        {
            uiManager.VideoSettingsMenu.OnControllerMoveLeft();
        }
        else if (move.info.x == 1)
        {
            uiManager.VideoSettingsMenu.OnControllerMoveRight();
        }
    }

    protected override void OnSubmit(object sender, EventArgs args)
    {
        uiManager.VideoSettingsMenu.OnControllerSubmit();
    }

    protected override void OnCancel(object sender, EventArgs args)
    {
        uiManager.VideoSettingsMenu.OnCancel();
    }
}
