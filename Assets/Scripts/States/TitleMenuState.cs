﻿using System;
using UnityEngine;

public class TitleMenuState : UIState
{
    public override void Enter()
    {
        uiManager.ReturnToTitleMenu = true;
        uiManager.TitleMenu.SetActive(true);
        EventManager.MoveEvent += OnMove;
        EventManager.ConfirmEvent += OnSubmit;
        EventManager.CancelEvent += OnCancel;
        uiManager.TitleMenu.OnEnterState();
    }

    public override void Exit()
    {
        uiManager.TitleMenu.SetActive(false);
        Unsubscribe();
    }

    private void Unsubscribe()
    {
        EventManager.MoveEvent -= OnMove;
        EventManager.ConfirmEvent -= OnSubmit;
        EventManager.CancelEvent -= OnCancel;
    }

    private void OnDisable()
    {
        Unsubscribe();
    }

    protected override void OnMove(object sender, InfoEventArgs<Point> move)
    {
        if (move.info.y == 1)
        {
            uiManager.TitleMenu.OnControllerMoveUp();
        }
        else if (move.info.y == -1)
        {
            uiManager.TitleMenu.OnControllerMoveDown();
        }
    }

    protected override void OnSubmit(object sender, EventArgs args)
    {
        uiManager.BlurBackgroundObject.SetActive(true);
        uiManager.TitleMenu.OnControllerSubmit();
    }

    protected override void OnCancel(object sender, EventArgs args)
    {
    }
}
