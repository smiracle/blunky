﻿using System;
using UnityEngine;

public class GameplayState : UIState
{
    public override void Enter()
    {
        uiManager.ReturnToTitleMenu = false;
        uiManager.BlurBackgroundObject.SetActive(false);
        uiManager.Hud.gameObject.SetActive(true);
        EventManager.MoveEvent += gameManager.Player.Move;
        EventManager.ConfirmEvent += OnSubmit;
        EventManager.CancelEvent += OnCancel;
        EventManager.SelectLeftEvent += SelectItemLeft;
        EventManager.SelectRightEvent += SelectItemRight;
    }

    public override void Exit()
    {
        Unsubscribe();
        uiManager.PauseMenu.selectedIndex = 0;
    }

    public void SelectItemLeft(object sender, EventArgs args)
    {
        gameManager.Player.SelectedToolIndex -= 1;
        //Debug.Log($"left. num avail: {gameManager.Player.ToolsAvailable.Count} selected index :{ gameManager.Player.SelectedToolIndex}");
        gameManager.UIManager.Hud.UpdateToolSelection();
    }

    public void SelectItemRight(object sender, EventArgs args)
    {
        gameManager.Player.SelectedToolIndex += 1;
        //Debug.Log($"right. num avail: {gameManager.Player.ToolsAvailable.Count} selected index :{ gameManager.Player.SelectedToolIndex}");
        gameManager.UIManager.Hud.UpdateToolSelection();
    }

    private void OnDisable()
    {
        Unsubscribe();
    }

    private void Unsubscribe()
    {
        EventManager.MoveEvent -= gameManager.Player.Move;
        EventManager.ConfirmEvent -= OnSubmit;
        EventManager.CancelEvent -= OnCancel;
        EventManager.SelectLeftEvent -= SelectItemLeft;
        EventManager.SelectRightEvent -= SelectItemRight;
    }

    protected override void OnCancel(object sender, EventArgs args)
    {
        if (!gameManager.Player.IsMoving)
        {
            uiManager.BlurBackgroundObject.SetActive(true);
            uiManager.ChangeState<PauseMenuState>();
        }
    }

    protected override void OnSubmit(object sender, EventArgs args)
    {
        if (!gameManager.Player.IsMoving)
        {
            var pos = gameManager.Player.Position;
            var scene = gameManager.DataManager.ActiveScene;
            
            // If bombs are selected, and player has enough, and there is nothing on the current tile
            if(gameManager.Player.SelectedTool == TileIdentifiers.Item_Bomb && gameManager.Player.Bombs > 0 && gameManager.DataManager.Tiles[scene][pos].Interactable == null)
            {
                // Spawn a bomb!
                //Debug.Log($"Use {gameManager.Player.SelectedTool} at {scene} {pos}");
                gameManager.Player.Bombs -= 1;
                gameManager.DataManager.CreateTickableItemAtRuntime(TileIdentifiers.TickableItem_Ignited_Bomb, scene, pos);
                gameManager.DataManager.DrawOneTile(gameManager.DataManager.Tiles[scene][pos]);
            }
            else if (gameManager.Player.SelectedTool == TileIdentifiers.Item_HealthPotion && gameManager.Player.HealthPotions > 0)
            {
                if (gameManager.Player.Hp == gameManager.Player.MaxHp)
                {
                    // Don't let them use it at full health
                    gameManager.PlaySound(AudioClipBanks.Sounds_Denied);
                }
                else
                {
                    gameManager.Player.HealthPotions -= 1;
                    gameManager.Player.Hp = gameManager.Player.MaxHp;
                    gameManager.PlaySound(AudioClipBanks.Sounds_UsePotion);
                }
            }
            else if (gameManager.Player.SelectedTool == TileIdentifiers.Item_BowAndArrow && gameManager.Player.Gold > 0)
            {
                gameManager.Player.Gold -= 1;
                gameManager.PlaySound(AudioClipBanks.Sounds_Arrow);
                // Todo: Spawn arrow
            }
        }
    }
}
