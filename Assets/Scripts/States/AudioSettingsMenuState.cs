﻿using System;
using UnityEngine;

public class AudioSettingsMenuState : UIState
{
    public override void Enter()
    {
        uiManager.AudioSettingsMenu.SetActive(true);
        EventManager.MoveEvent += OnMove;
        EventManager.ConfirmEvent += OnSubmit;
        EventManager.CancelEvent += OnCancel;
        uiManager.AudioSettingsMenu.OnEnterState();
    }

    public override void Exit()
    {
        uiManager.AudioSettingsMenu.SetActive(false);
        EventManager.MoveEvent -= OnMove;
        EventManager.ConfirmEvent -= OnSubmit;
        EventManager.CancelEvent -= OnCancel;
        uiManager.AudioSettingsMenu.OnExitState();
    }

    protected override void OnMove(object sender, InfoEventArgs<Point> move)
    {
        if (move.info.y == 1)
        {
            uiManager.AudioSettingsMenu.OnControllerMoveUp();
        }
        else if (move.info.y == -1)
        {
            uiManager.AudioSettingsMenu.OnControllerMoveDown();
        }

        if (move.info.x == -1)
        {
            uiManager.AudioSettingsMenu.OnControllerMoveLeft();
        }
        else if (move.info.x == 1)
        {
            uiManager.AudioSettingsMenu.OnControllerMoveRight();
        }
    }

    protected override void OnSubmit(object sender, EventArgs args)
    {
        uiManager.AudioSettingsMenu.OnControllerSubmit();
    }
}
