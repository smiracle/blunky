﻿public interface IStateMachine
{
    State CurrentState { get; set; }
    void ChangeState<T>() where T : State;
    void DelayedChangeState<T>(float sec) where T : State;
    T GetState<T>() where T : State;
    public IGameManager GameManager { get; set; }
}