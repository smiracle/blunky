﻿using System;
using System.Collections;
using UnityEngine;

[Serializable]
public class StateMachine : MonoBehaviour
{
    public IGameManager GameManager { get; set; }
    public virtual State CurrentState
    {
        get
        {
            return _currentState;
        }
        set
        {
            if (_currentState == value)
            {
                return;
            }

            if (_currentState != null)
            {
                _currentState.Exit();
            }

            _currentState = value;

            if (_currentState != null)
            {
                _currentState.Enter();
            }
        }
    }
    protected State _currentState;

    public virtual T GetState<T>() where T : State
    {
        T target = GetComponent<T>();
        if (target == null)
        {
            target = gameObject.AddComponent<T>();
            target.Initialize(GameManager);
        }
        return target;
    }

    public virtual void DelayedChangeState<T>(float sec) where T : State
    {
        StartCoroutine(WaitingForStateChange<T>(sec));
    }

    private IEnumerator WaitingForStateChange<T>(float sec) where T : State
    {
        yield return new WaitForSeconds(sec);
        ChangeState<T>();
    }

    public virtual void ChangeState<T>() where T : State
    {
        // Uncomment to see state changes, very helpful when understanding unit behavior
        //Debug.Log(GetState<T>().ToString());
        CurrentState = GetState<T>();
    }
}
