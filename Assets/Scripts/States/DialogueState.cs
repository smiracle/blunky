﻿using System;
using UnityEngine;

public class DialogueState : UIState
{    
    public override void Enter()
    {
        uiManager.ReturnToTitleMenu = false;
        EventManager.ConfirmEvent += OnSubmit;
        EventManager.CancelEvent += OnCancel;
        EventManager.MoveEvent += OnMove;
    }

    public override void Exit()
    {
        EventManager.ConfirmEvent -= OnSubmit;
        EventManager.CancelEvent -= OnCancel;
        EventManager.MoveEvent -= OnMove;
    }

    protected override void OnSubmit(object sender, EventArgs args)
    {
        EndDialogue();
    }

    protected override void OnCancel(object sender, EventArgs args)
    {
        EndDialogue();
    }

    protected override void OnMove(object sender, InfoEventArgs<Point> move)
    {
        var previousMove = uiManager.GameManager.Player.PreviousMove;
        if (previousMove.x != move.info.x || previousMove.y != move.info.y)
        {
            // Only end dialogue if move is in a different direction than it was before
            EndDialogue();
        }
    }

    private void EndDialogue()
    {
        uiManager.DialogueWindow.gameObject.SetActive(false);
        eventManager.TriggerEndDialogue(this);
        uiManager.ChangeState<GameplayState>();
    }

}
