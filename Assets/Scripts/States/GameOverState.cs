﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.AddressableAssets;

public class GameOverState : UIState
{
    private GameObject GameOverScreen;
    private bool isWaitPeriodDone;
    private float waitPeriod = 1f;

    public override void Enter()
    {
        isWaitPeriodDone = false;
        uiManager.BlurBackgroundObject.SetActive(true);
        EventManager.ConfirmEvent += OnSubmit;
        EventManager.CancelEvent += OnCancel;
        EventManager.MoveEvent += OnMove;        

        GameOverScreen = GameObjectPooler.Instance.GetObject(gameManager.GameOverScreenPrefab);
        GameOverScreen.SetActive(true);
        GameOverScreen.transform.SetParent(uiManager.Canvas.transform);
        var rect = GameOverScreen.GetComponent<RectTransform>();
        rect.offsetMin = Vector2.zero;
        rect.offsetMax = Vector2.zero;
        gameManager.PlaySound(AudioClipBanks.Sounds_Death);
        StartCoroutine(Wait());
    }
    
    private IEnumerator Wait()
    {
        yield return new WaitForSeconds(waitPeriod);
        isWaitPeriodDone = true;
    }

    private void UnsubscribeAll()
    {
        EventManager.ConfirmEvent -= OnSubmit;
        EventManager.CancelEvent -= OnCancel;
        EventManager.MoveEvent -= OnMove;
    }

    public override void Exit()
    {
        UnsubscribeAll();
    }

    private void OnDisable()
    {
        UnsubscribeAll();
    }

    protected override void OnSubmit(object sender, EventArgs args)
    {
        ReturnToTitle();
    }

    protected override void OnCancel(object sender, EventArgs args)
    {
        ReturnToTitle();
    }

    protected override void OnMove(object sender, InfoEventArgs<Point> move)
    {
        ReturnToTitle();
    }

    private void ReturnToTitle()
    {
        if (GameOverScreen && isWaitPeriodDone)
        {
            gameManager.DataManager.ActiveScene = Scenes.Empty;
            uiManager.Hud.gameObject.SetActive(false);
            uiManager.GameManager.CollisionMap.ClearAllTiles();
            //Addressables.ReleaseInstance(GameOverScreen);
            GameOverScreen.SetActive(false);
            uiManager.ChangeState<TitleMenuState>();
            gameManager.PlayMusic(MusicTracks.Title);
        }
    }

}
