﻿using Rewired.UI.ControlMapper;
using System;
using UnityEngine;
using UnityEngine.AddressableAssets;

public class ControlMapperState : UIState
{
    private GameObject controlMapperGo;
    private ControlMapper controlMapperComp;
    public override void Enter()
    {
        //owner.ControlMapper.Toggle();
        EventManager.CancelEvent += TransitionToPreviousMenu;
        EventManager.DoneControlMapperEvent += TransitionToPreviousMenu;

        if (controlMapperGo == null)
        {
            controlMapperGo = GameObjectPooler.Instance.GetObject(gameManager.UIManager.ControlMapperPrefab);
        }
        controlMapperGo.SetActive(true);
        controlMapperComp = controlMapperGo.gameObject.GetComponent<ControlMapper>();
        controlMapperComp.Toggle();
    }

    public override void Exit()
    {
        //controlMapperGo.SetActive(false);
        controlMapperComp.Toggle();
        Unsubscribe();
    }

    private void Unsubscribe()
    {
        EventManager.CancelEvent -= TransitionToPreviousMenu;
        EventManager.DoneControlMapperEvent -= TransitionToPreviousMenu;
    }

    private void OnDisable()
    {
        Unsubscribe();
    }

    private void TransitionToPreviousMenu(object sender, EventArgs e)
    {
        if (controlMapperComp != null)
        {
            //controlMapperComp.Toggle();
            
            if (uiManager.ReturnToTitleMenu)
            {
                uiManager.ChangeState<TitleMenuState>();
            }
            else
            {
                uiManager.ChangeState<PauseMenuState>();
            }
        }
    }
}
