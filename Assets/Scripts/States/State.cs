﻿using UnityEngine;

public abstract class State : MonoBehaviour
{
    public virtual void Initialize(IGameManager gameManager)
    {
    }

    public virtual void Enter()
    {
    }

    public virtual void Exit()
    {
    }
}