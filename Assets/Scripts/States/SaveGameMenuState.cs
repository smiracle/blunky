﻿using System;
using UnityEngine;

public class SaveGameMenuState : UIState
{
    public override void Enter()
    {
        uiManager.SaveMenu.SetActive(true);
        EventManager.MoveEvent += OnMove;
        EventManager.ConfirmEvent += OnSubmit;
        EventManager.CancelEvent += OnCancel;
        uiManager.SaveMenu.OnEnterState();
    }

    public override void Exit()
    {
        uiManager.SaveMenu.SetActive(false);
        EventManager.MoveEvent -= OnMove;
        EventManager.ConfirmEvent -= OnSubmit;
        EventManager.CancelEvent -= OnCancel;
    }

    protected override void OnMove(object sender, InfoEventArgs<Point> move)
    {
        if (move.info.y == 1)
        {
            uiManager.SaveMenu.OnControllerMoveUp();
        }
        else if (move.info.y == -1)
        {
            uiManager.SaveMenu.OnControllerMoveDown();
        }
    }

    protected override void OnSubmit(object sender, EventArgs args)
    {
        uiManager.SaveMenu.OnControllerSubmit();
    }

    protected override void OnCancel(object sender, EventArgs args)
    {
        uiManager.SaveMenu.OnControllerSubmit();
    }
}
