﻿using System;
using UnityEngine;

public class LoadMenuState : UIState
{
    public override void Enter()
    {
        EventManager.MoveEvent += OnMove;
        EventManager.ConfirmEvent += OnSubmit;
        EventManager.CancelEvent += OnCancel;
        uiManager.LoadMenu.OnEnterState();
        uiManager.LoadMenu.SetActive(true);
    }

    public override void Exit()
    {
        EventManager.MoveEvent -= OnMove;
        EventManager.ConfirmEvent -= OnSubmit;
        EventManager.CancelEvent -= OnCancel;
        uiManager.LoadMenu.SetActive(false);
    }

    protected override void OnMove(object sender, InfoEventArgs<Point> move)
    {
        if (move.info.y == 1)
        {
            uiManager.LoadMenu.OnControllerMoveUp();
        }
        else if (move.info.y == -1)
        {
            uiManager.LoadMenu.OnControllerMoveDown();
        }
    }

    protected override void OnSubmit(object sender, EventArgs args)
    {
        uiManager.LoadMenu.OnControllerSubmit();
    }

    protected override void OnCancel(object sender, EventArgs args)
    {
        uiManager.LoadMenu.OnCancel();
    }
}
