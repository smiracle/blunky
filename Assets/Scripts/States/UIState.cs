﻿using System;
using UnityEngine;

public abstract class UIState : State
{
    protected IGameManager gameManager;
    protected UIManager uiManager;
    protected EventManager eventManager;

    public override void Initialize(IGameManager gameManager)
    {
        this.gameManager = gameManager;
        uiManager = gameManager.UIManager;
        eventManager = gameManager.EventManager;
    }
    public override void Enter()
    {
    }

    public override void Exit()
    {
    }

    protected virtual void OnSubmit(object sender, EventArgs args)
    {
    }
    
    protected virtual void OnCancel(object sender, EventArgs args)
    {
    }

    protected virtual void OnMove(object sender, InfoEventArgs<Point> move)
    {

    }
}