﻿using System.Collections.Generic;
using UnityEngine;

public class Room
{
    public Vector3Int GridPosition;
    public Vector3Int Origin;
    public Dictionary<Vector3Int, TileInfo> Tiles = new Dictionary<Vector3Int, TileInfo>();
    public Scenes Scene;
    public RoomTypes RoomType;
    private RoomOrientations roomOrientation;
    private IGameManager gameManager;
    private DataManager dataManager;

    public Room(IGameManager gameManager, Scenes scene, SceneTypes sceneType, RoomLabels room, Vector3Int gridPosition)
    {
        this.gameManager = gameManager;
        dataManager = gameManager.DataManager;        
        Scene = scene;
        GridPosition = gridPosition;
        Origin = new Vector3Int(gridPosition.x * RoomFactory.RoomDimension, gridPosition.y * RoomFactory.RoomDimension, 0);
        if(!dataManager.GetRoomDefinitionsDictionaryForSceneType(sceneType).ContainsKey(room))
        {
            throw new System.Exception($"Couldnt find label in defs: {room} for scene type {sceneType}");
        }
        Tiles = ConvertTileInfoArrayToDictionary(dataManager.GetRoomDefinitionsDictionaryForSceneType(sceneType)[room].Tiles);
        roomOrientation = dataManager.GetRoomDefinitionsDictionaryForSceneType(sceneType)[room].RoomOrientation;
        RoomType = dataManager.GetRoomDefinitionsDictionaryForSceneType(sceneType)[room].RoomType;
    }

    private Dictionary<Vector3Int, TileInfo> ConvertTileInfoArrayToDictionary(TileInfo[] arr)
    {
        var dict = new Dictionary<Vector3Int, TileInfo>();
        int x = Origin.x;
        int y = Origin.y + RoomFactory.RoomDimension - 1;
        foreach (var tileinfo in arr)
        {
            if (x >= Origin.x + RoomFactory.RoomDimension)
            {
                x = Origin.x;
                y -= 1;
            }
            tileinfo.Position = new Vector3Int(x, y, 0);
            if(tileinfo.Interactable != null)
            {
                tileinfo.Interactable.Position = new Vector3Int(x, y, 0);
            }
            dict.Add(new Vector3Int(x, y, 0), tileinfo);
            x += 1;
        }
        return dict;
    }

    public void Rotate()
    {
        var rotatedTiles = dataManager.RotateTilesClockwiseAndValidate(Tiles, Origin);
        Tiles = rotatedTiles;
    }
}
