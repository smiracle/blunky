﻿using System;
using UnityEngine;
using UnityEngine.Tilemaps;

public class RoomDefinition
{
    public TileInfo[] Tiles;
    public RoomOrientations RoomOrientation;
    public RoomLabels RoomLabel;
    public RoomTypes RoomType;
    private IGameManager gameManager;
    private DataManager dataManager;

    public RoomDefinition(IGameManager gameManager, SceneTypes sceneType, RoomLabels roomLabel, string[] identifiers, RoomOrientations roomOrientation, RoomTypes roomType = RoomTypes.Other)
    {
        this.gameManager = gameManager;
        this.dataManager = gameManager.DataManager;
        RoomLabel = roomLabel;
        Tiles = ConvertTileStringsToTileInfos(identifiers);
        RoomOrientation = roomOrientation;
        dataManager.GetRoomDefinitionsDictionaryByOrientationForSceneType(sceneType)[RoomOrientation].Add(this);
        RoomType = roomType;
    }

    private TileInfo[] ConvertTileStringsToTileInfos(string[] identifiers)
    {
        var arr = new TileInfo[RoomFactory.RoomDimension * RoomFactory.RoomDimension];
        int i = 0;
        foreach (var strId in identifiers)
        {
            int numericalId = int.Parse(strId);
            TileIdentifiers id = (TileIdentifiers)numericalId;
            Interactable interactable = null;
            TileInfo info = new TileInfo(gameManager);
            if (dataManager.IsInteractable(id))
            {
                if (dataManager.IsTransition(id))
                {
                    interactable = CreateTransition(id);
                }
                else if (dataManager.IsNPC(id))
                {
                    interactable = CreateNPC(id);
                }
                else if (dataManager.IsEnemy(id))
                {
                    interactable = CreateEnemy(id);
                }
                else if(dataManager.IsItem(id))
                {
                    interactable = CreateItem(id);
                }
                else if (dataManager.IsTrigger(id))
                {
                    interactable = CreateTrigger(id);
                }
            }
            else
            {
                // Set the basic non-interacable tile identifier
                info.TileIdentifier = id;
                if (dataManager.IsDestructable(id))
                {
                    info.IsDestructable = true;
                }
            }
            info.Interactable = interactable;
            arr[i] = info;
            i++;
        }
        return arr;
    }

    private Transition CreateTransition(TileIdentifiers id)
    {
        TileBase tileBase;
        switch (id)
        {
            case TileIdentifiers.T_ExitToOverworld:
                tileBase = dataManager.RoomFactory.GetRefForTile(TileIdentifiers.T_ExitToOverworld);
                break;
            case TileIdentifiers.T_StairsUp:
                tileBase = dataManager.RoomFactory.GetRefForTile(TileIdentifiers.T_StairsUp);
                break;
            case TileIdentifiers.T_Dungeon1:
            case TileIdentifiers.T_Dungeon2:
            case TileIdentifiers.T_Dungeon3:
                tileBase = dataManager.RoomFactory.GetRefForTile(TileIdentifiers.T_StairsDown);
                break;
            case TileIdentifiers.T_StartCave:
            case TileIdentifiers.T_MarshCave:
            case TileIdentifiers.T_TallBuilding:
            case TileIdentifiers.T_Shop:
                tileBase = dataManager.RoomFactory.GetRefForTile(TileIdentifiers.T_MarshCave);
                break;
            default:
                throw new Exception($"Unknown transition type {id}");
        }

        Scenes sceneToTransitionTo;
        switch (id)
        {
            case TileIdentifiers.T_ExitToOverworld:
                sceneToTransitionTo = Scenes.Overworld;
                break;
            case TileIdentifiers.T_StairsUp:
                sceneToTransitionTo = Scenes.Overworld;
                break;
            case TileIdentifiers.T_Dungeon1:
                sceneToTransitionTo = Scenes.Dungeon1_SkeletonCave;
                break;
            case TileIdentifiers.T_Dungeon2:
                sceneToTransitionTo = Scenes.Dungeon2_Volcano;
                break;
            case TileIdentifiers.T_Dungeon3:
                sceneToTransitionTo = Scenes.Dungeon3_Catacombs;
                break;
            case TileIdentifiers.T_StartCave:
                sceneToTransitionTo = Scenes.Interior_StartCave;
                break;
            case TileIdentifiers.T_MarshCave:
                sceneToTransitionTo = Scenes.Interior_MarshCave;
                break;
            case TileIdentifiers.T_TallBuilding:
                sceneToTransitionTo = Scenes.Interior_TallBuilding;
                break;
            case TileIdentifiers.T_Shop:
                sceneToTransitionTo = Scenes.Interior_Shop;
                break;
            default:
                throw new Exception($"Unknown transition type {id}");
        }

        return new Transition(gameManager)
        {
            TileIdentifier = id,
            GameManager = gameManager,
            Tile = tileBase,
            SceneToTransitionTo = sceneToTransitionTo
        };
    }

    private NPC CreateNPC(TileIdentifiers id)
    {
        var npc = new NPC(gameManager, id)
        {
        
        };
        return npc;
    }

    private Enemy CreateEnemy(TileIdentifiers id)
    {
        int hp = 1;
        int minAttack = 1;
        int maxAttack = 2;
        int speed = 5;
        int moveWaitMaximum = 2;
        int maxSightDistance = 7;
        switch (id)
        {
            case TileIdentifiers.Enemy_Bat:
                hp = 1;
                minAttack = 1;
                maxAttack = 1;
                speed = 6;
                moveWaitMaximum = 1;
                maxSightDistance = 2;
                break;
            case TileIdentifiers.Enemy_Scorpion:
                hp = 3;
                minAttack = 1;
                maxAttack = 1;
                speed = 6;
                moveWaitMaximum = 1;
                maxSightDistance = 4;
                break;
            case TileIdentifiers.Enemy_Skeleton:
                hp = 5;
                minAttack = 1;
                maxAttack = 2;
                break;
            case TileIdentifiers.Enemy_Spider:
                hp = 5;
                minAttack = 1;
                maxAttack = 2;
                speed = 6;
                moveWaitMaximum = 1;
                maxSightDistance = 9;
                break;
            case TileIdentifiers.Enemy_Spider2:
                hp = 7;
                minAttack = 1;
                maxAttack = 3;
                speed = 6;
                moveWaitMaximum = 1;
                maxSightDistance = 9;
                break;
            case TileIdentifiers.Enemy_Spider3:
                hp = 9;
                minAttack = 1;
                maxAttack = 4;
                speed = 6;
                moveWaitMaximum = 1;
                maxSightDistance = 9;
                break;
            case TileIdentifiers.Enemy_Demon:
                hp = 11;
                minAttack = 1;
                maxAttack = 4;
                maxSightDistance = 12;
                break;
            case TileIdentifiers.Enemy_Orc:
                hp = 11;
                minAttack = 1;
                maxAttack = 4;
                break;
            case TileIdentifiers.Enemy_Goblin:
                hp = 8;
                minAttack = 1;
                maxAttack = 4;
                break;
            case TileIdentifiers.Enemy_Cyclops:
                hp = 12;
                minAttack = 1;
                maxAttack = 6;
                break;
            case TileIdentifiers.Enemy_Ogre:
                hp = 13;
                minAttack = 1;
                maxAttack = 5;
                moveWaitMaximum = 3;
                maxSightDistance = 3;
                break;
            case TileIdentifiers.Enemy_Ghost:
                hp = 10;
                minAttack = 1;
                maxAttack = 6;
                speed = 9;
                maxSightDistance = 12;
                break;
            case TileIdentifiers.Enemy_Ghost2:
                hp = 11;
                minAttack = 1;
                maxAttack = 7;
                speed = 9;
                maxSightDistance = 12;
                break;
            case TileIdentifiers.Enemy_Ghost3:
                hp = 13;
                minAttack = 1;
                maxAttack = 8;
                speed = 10;
                maxSightDistance = 12;
                break;
        }
       // Debug.Log($"Creating enemy {id} with {hp} hp");
        var enemy = new Enemy(gameManager)
        {
            TileIdentifier = id,
            Tile = dataManager.RoomFactory.GetRefForTile(id),
            Hp = hp,
            AttackMinimum = minAttack,
            AttackMaximum = maxAttack,
            Speed = speed,
            MoveWaitMaximum = moveWaitMaximum,
            MaxSightDistance = maxSightDistance,
        };
        return enemy;
    }

    private Item CreateItem(TileIdentifiers id)
    {
        switch (id)
        {
            case TileIdentifiers.Item_Sword1:
                break;
        }

        var item = new Item(gameManager)
        {
            TileIdentifier = id,
            Tile = dataManager.RoomFactory.GetRefForTile(id),
        };
        return item;
    }

    private Trigger CreateTrigger(TileIdentifiers id)
    {
        switch (id)
        {
            case TileIdentifiers.RotateRight:
                break;
        }

        var trigger = new Trigger(gameManager)
        {
            TileIdentifier = id,
            Tile = dataManager.RoomFactory.GetRefForTile(id),
        };
        return trigger;
    }
}
