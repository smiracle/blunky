using System.Collections.Generic;
using UnityEngine;

public class TileInfo
{
    public TileIdentifiers TileIdentifier;
    public Vector3Int Position;
    public Vector2 CenterPosition {get { return new Vector2(Position.x + 0.5f, Position.y + 0.5f); } }
    public Interactable Interactable;
    public TileInfo prev;
    public int distance;
    public bool IsDestructable = false;

    public List<TileInfo> adjacencyList = new List<TileInfo>();

    // Needed for BFS
    public bool visited = false;
    public TileInfo parent = null;

    // Needed for A*
    public float f = 0;
    public float g = 0;
    public float h = 0;

    public IGameManager GameManager;
    public DataManager DataManager;

    public TileInfo(IGameManager gameManager)
    {
        GameManager = gameManager;
        DataManager = gameManager.DataManager;
    }

    public TileInfo DeepCopy()
    {
        var copy = new TileInfo(GameManager)
        {
            TileIdentifier = this.TileIdentifier,
            Position = new Vector3Int(Position.x, Position.y, 0),
            Interactable = Interactable == null ? null : Interactable.DeepCopy(),
            prev = this.prev,
            distance = this.distance,

            adjacencyList = new List<TileInfo>(adjacencyList),

            // Needed for BFS
            visited = this.visited,
            parent = this.parent,

            // Needed for A*
            f = this.f,
            g = this.g,
            h = this.h,
            IsDestructable = IsDestructable
        };
        return copy;
    }

    public void Reset()
    {
        adjacencyList.Clear();

        // Needed for BFS
        visited = false;
        parent = null;
        distance = 0;

        f = g = h = 0;
    }

    public bool IsOccupied()
    {
        var drawnCollisionTile = GameManager.CollisionMap.GetTile(Position);
        return drawnCollisionTile != null || Interactable != null || GameManager.Player.Position == Position;
    }
}
