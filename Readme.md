### Blunky
Created 8/2021 for the Kenney Jam 2021, a Unity GameJam. It's far from perfect, but I created this game in just one week.

I'm most proud of the procedural dungeon generation that I was able to implement. It works by creating a "critical path" between a start and end cell on a 2D grid. The rooms are then placed so that they each have a connection to the critical path. Rooms are also rotatable by stepping on arrow tiles. 

You can explore the first dungeon by moving to the entrance in the bottom right corner of the map.

After finishing the first dungeon the player unlocks bombs, which can be used to destroy tiles and create new paths through rooms at will. There are other items that can be bought using gold as well.